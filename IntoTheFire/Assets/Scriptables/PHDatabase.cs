﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BaseActionName
{
    Pull,
    Push,
    Destroy,
    PickUp,
    PickupVictim,
    DropVictim,
    None
}

[CreateAssetMenu(fileName = ("DB"), menuName = "Database/New Database")]
public class PHDatabase : ScriptableObject
{
    [Header("Equipment")]
    public List<EquipData> Equipment = new List<EquipData>();

    [Header("Firefighters")]
    public List<FFData> Firefighters = new List<FFData>();
    public Sprite HealthySprite;
    public Sprite TiredSprite;
    public Sprite ExhaustSprite;

    [Header("Skilltrees")]
    public SkillTreeTemplate AtlasTree;
    public SkillTreeTemplate ForgeTree;
    public SkillTreeTemplate JetTree;
    public SkillTreeTemplate OracleTree;
    public SkillTreeTemplate GraceTree;
    public SkillTreeTemplate ShusuiTree;


    [Header("Action")]
    public EquipData PullAction;
    public EquipData PushAction;
    public EquipData DestroyAction;
    public EquipData PickUpAction;

    [Header("Quirks")]
    public List<QuirkData> Quirks = new List<QuirkData>();


    public Dictionary<EquipName, EquipData> EquipDB = new Dictionary<EquipName, EquipData>();
    public Dictionary<BaseActionName, EquipData> ActionDB = new Dictionary<BaseActionName, EquipData>();
    public Dictionary<FFName, FFData> FirefightersDB = new Dictionary<FFName, FFData>();
    public Dictionary<PassiveSkill, QuirkData> QuirkDB = new Dictionary<PassiveSkill, QuirkData>();
    public Dictionary<FFName, SkillTreeTemplate> SkillTreeDB = new Dictionary<FFName, SkillTreeTemplate>();


    private void Awake()
    {
        //Debug.Log("DB Awake");

        foreach (EquipData item in Equipment)
        {
            EquipDB.Add(item.EName, item);
        }

        foreach (FFData item in Firefighters)
        {
            FirefightersDB.Add(item.FName, item);
        }

        foreach (QuirkData item in Quirks)
        {
            if (QuirkDB.ContainsKey(item.QuirkName))
            {
                Debug.LogError("Quirk DB already contain " + item.QuirkName);
                return;
            }

            QuirkDB.Add(item.QuirkName, item);
        }

        /// Actions -------------------------------------
        ActionDB.Add(BaseActionName.Pull, PullAction);
        ActionDB.Add(BaseActionName.Push, PushAction);
        ActionDB.Add(BaseActionName.Destroy, DestroyAction);
        ActionDB.Add(BaseActionName.PickUp, PickUpAction);

        /// SkillTrees -----------------------------------
        SkillTreeDB.Add(FFName.Atlas, AtlasTree);
        SkillTreeDB.Add(FFName.Jet, JetTree);
        SkillTreeDB.Add(FFName.Forge, ForgeTree);
        SkillTreeDB.Add(FFName.Oracle, OracleTree);
        SkillTreeDB.Add(FFName.Grace, GraceTree);
        SkillTreeDB.Add(FFName.Shusui, ShusuiTree);
    }

    public EquipData GetEquip(EquipName _name) =>  EquipDB[_name];

    public EquipData GetAction(BaseActionName _name) => ActionDB[_name];

    public FFData GetFirefighter(FFName _name) => FirefightersDB[_name];

    public QuirkData GetQuirkDAta(PassiveSkill _name) => QuirkDB[_name];


    public bool HasItem(EquipName _name) => (EquipDB.ContainsKey(_name)) ? true : false;
    public bool HasItem(BaseActionName _name) => (ActionDB.ContainsKey(_name)) ? true : false;

}
