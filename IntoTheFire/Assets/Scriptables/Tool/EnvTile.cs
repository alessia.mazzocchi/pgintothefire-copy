﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

public enum EnvType
{
    None,
    Flame,
    Smoke,
    Window,
    P1SpawnPoint,
    P2SpawnPoint,
    P3SpawnPoint,
    Exit,
    Victim,
    Obstacle,
    Water,
    Fuel,
    TeddyBear,
    Debries
}

public class EnvTile : Tile
{
    [Header("My tile values")]

    public EnvType EnvType;

    [Space]
    [Tooltip("Use depends on the tile")]
    public int Value;
    
    [Space]

    public GameObject Prefab;
}
