﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

public enum RoomsType
{
    None,
    Wall,
    Destructible_Wall,
    Floor,
    Door,
    Window,
    DoorHeavy
}

public class RoomsTile : Tile
{
    [Header("My tile values")]

    public RoomsType RoomsTileMap;

    [Header("Needed for floor")]
    public byte RoomID;
    [Space]

    public GameObject Prefab;
}
