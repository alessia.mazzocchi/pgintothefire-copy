﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

public enum GroundType
{
    None,
    Wood,
    Concrete,
    Debris,
    BrokenGlass,
    Steel,
    Grass,
    Sand,
    Tiles,
    Smoke
}

[CreateAssetMenu(fileName = "GroundTile", menuName = "GroundTile")]
public class GroundTile : Tile
{
    [Header("My tile values")]

    public GroundType GroundType;

    [Space]

    public GameObject Prefab;
}
