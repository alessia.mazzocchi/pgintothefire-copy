﻿using System.Net.Http.Headers;
using UnityEngine;

public enum TargetValue { Charges, Value, Range, Direction }

[CreateAssetMenu(fileName = "NewSkillPoint", menuName = "Skill Tree/New Equip Upgrade")]
public class SPEquipUpgrade : SkillPoint
{
    [Header("Equip Target")]
    public bool IsTypeUpgrade;
    public EquipType TypeTarget;
    public EquipName EquipTarget;
    [Header("Value")]
    public TargetValue TargetValue;
    [Header("This values are ADDED to the current equip value")]
    public int EnhancedValue;
    public Vector2Int EnhancedRange;
    public CheckRadius NewArea;
    [Space]
    public string m_Description;

    public override void Execute(FFData _FF)
    {
        _FF.AddNewHiddenQuirk(this);
    }

    public override string GetDesctiption()
    {
        return m_Description;
    }

    public void EnhanceEquip(Equip[] _eq) 
    {
        for (int i = 0; i < _eq.Length; i++)
        {
            if (IsTypeUpgrade)
            {
                if (_eq[i] != null && _eq[i].EType == TypeTarget)
                {
                    switch (TargetValue)
                    {
                        case TargetValue.Charges:
                            _eq[i].Charges += EnhancedValue;
                            break;

                        case TargetValue.Value:
                            _eq[i].Value += EnhancedValue;
                            break;

                        case TargetValue.Range:
                            _eq[i].EffectRange = new Vector2Int(_eq[i].EffectRange.x + EnhancedRange.x, _eq[i].EffectRange.y + EnhancedRange.y);
                            break;

                        case TargetValue.Direction:
                            _eq[i].Directions = NewArea;
                            break;
                    }
                }
            }
            else
            {
                if (_eq[i] != null && _eq[i].EName == EquipTarget)
                {
                    switch (TargetValue)
                    {
                        case TargetValue.Charges:
                            _eq[i].Charges += EnhancedValue;
                            break;

                        case TargetValue.Value:
                            _eq[i].Value += EnhancedValue;
                            break;

                        case TargetValue.Range:
                            _eq[i].EffectRange = new Vector2Int(_eq[i].EffectRange.x + EnhancedRange.x, _eq[i].EffectRange.y + EnhancedRange.y);
                            break;

                        case TargetValue.Direction:
                            _eq[i].Directions = NewArea;
                            break;
                    }
                }
            }
        }  
    }
}
