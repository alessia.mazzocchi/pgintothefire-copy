﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillPoint : ScriptableObject
{
    public string SkillPointName;

    public Sprite Sprite;

    public string Description { get => GetDesctiption(); }

    public virtual void Execute(FFData _FF) { }   

    public virtual string GetDesctiption() => "NO DESCTIPTION";

    public virtual EquipName UnlockEquip() => EquipName.Cement_Saw;

}
