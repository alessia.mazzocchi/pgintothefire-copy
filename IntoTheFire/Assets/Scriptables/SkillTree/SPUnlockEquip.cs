﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewSkillPoint", menuName = "Skill Tree/New Unlock Skill Point")]
public class SPUnlockEquip : SkillPoint
{
    public EquipName EquipToUnlock;

    public override void Execute(FFData _FF) => _FF.SkillTree.UnlockEquip(UnlockEquip());

    public override EquipName UnlockEquip() => EquipToUnlock;

    public override string GetDesctiption()
    {
        string desc = "";

        switch (EquipToUnlock)
        {
            case EquipName.Fire_Axe:
                desc = "Fire Axe";
                break;
            case EquipName.Sledgehammer:
                desc = "Sledgehammer";
                break;
            case EquipName.Cement_Saw:
                desc = "Cement Saw";
                break;
            case EquipName.Fire_Extinguisher:
                desc = "Fire Extinguisher";
                break;
            case EquipName.Foam_Grenades:
                desc = "Foam Grenades";
                break;
            case EquipName.Water_Nozzle:
                desc = "Water Nozzle";
                break;
            case EquipName.Smoke_Fan:
                desc = "Smoke Fan";
                break;
            case EquipName.Medikit:
                desc = "Medikit";
                break;
            case EquipName.Water_Shield:
                desc = "Water Shield";
                break;
            case EquipName.None:
                desc = "None";
                break;
            default:
                break;
        }

        return "Unlock " + desc;
    }
}
