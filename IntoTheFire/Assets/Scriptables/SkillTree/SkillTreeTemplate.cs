﻿using Malee;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillBranch
{
    ProgressionOne,
    ProgressionTwo,
    Melee,
    Extinguish,
    Utility
}

[System.Serializable]
public class SkillLeaf
{
    public SkillPoint SkillPoint;
    public int Prize;
}


[CreateAssetMenu(fileName = "Skill tree", menuName = "Skill Tree/New Skill Tree")]
public class SkillTreeTemplate : ScriptableObject
{
    [Reorderable(elementNameProperty = "SkillPoint")]
    public SkillTreeBranch ProgressionOne;
    [Space]
    [Reorderable]
    public SkillTreeBranch ProgressionTwo;
    [Space]
    [Reorderable]
    public SkillTreeBranch Melee;
    [Space]
    [Reorderable]
    public SkillTreeBranch Extinguish;
    [Space]
    [Reorderable]
    public SkillTreeBranch Utility;
}

[System.Serializable]
public class SkillTreeBranch : ReorderableArray<SkillLeaf> { }

