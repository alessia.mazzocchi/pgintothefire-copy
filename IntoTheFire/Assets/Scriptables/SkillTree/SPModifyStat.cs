﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;

public enum FFStatistic
{
    Health,
    Speed,
    MovementHeal
}

[CreateAssetMenu(fileName = "NewSkillPoint", menuName = "Skill Tree/New Stat Skill Point")]
public class SPModifyStat : SkillPoint
{
    public FFStatistic Stat;

    public int Value;

    public override void Execute(FFData _ff)
    {
        switch (Stat)
        {
            case FFStatistic.Health:
                _ff.baseHealth += Value;
                break;
            case FFStatistic.Speed:
                _ff.baseMovement += Value;
                break;
            case FFStatistic.MovementHeal:
                _ff.medikitMovementBoost += Value;
                break;
            default:
                break;
        }
    }

    public override string GetDesctiption()
    {
        return "Increase " + Stat.ToString() + " by " + Value;
    }
}
