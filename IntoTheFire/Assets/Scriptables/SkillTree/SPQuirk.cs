﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewSkillPoint", menuName = "Skill Tree/New Quirk Skill Point")]
public class SPQuirk : SkillPoint
{
    public PassiveSkill Skill;

    public string description;

    public override string GetDesctiption()
    {
        return description;
    }
}
