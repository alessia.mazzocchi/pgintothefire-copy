﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding
{
    public int TotalCost;

    public List<Node> GetPossiblePathArea(Vector2Int startPos, int range, FireFighterBehaviour ff = null)
    {
        FireFighterBehaviour _ff = (ff == null) ? LevelManager.instance.GetSelectedFF : ff;

        // Clear all grid cost
        LevelManager.instance.ClearGridCost();

        // Queue for nodes to check
        Queue<Node> toCheck = new Queue<Node>();

        // HashSet for nodes already checked
        HashSet<Node> alreadyChecked = new HashSet<Node>();

        // Get starting node from starting position
        Node startNode = LevelManager.instance.GetNode(startPos.x, startPos.y);
        // Set its cost as 0
        startNode.SetCost(0);
        // Add to already checked
        alreadyChecked.Add(startNode);
        // Add to to check (needed to start the process)
        toCheck.Enqueue(startNode);

        // List for possible path area
        List<Node> area = new List<Node>();

        //Debug.Log("NEW AREA CALCULATION");

        // While there's still a node to check
        while (toCheck.Count > 0)
        {
            // Get current node to check from queue
            Node currentNode = toCheck.Dequeue();

            // Get node neighbours
            List<Node> neighbours = GetPossibleNeighbours(currentNode, alreadyChecked);

            // Foreach node check if it's walkable and if it's in range
            foreach (Node n in neighbours)
            {
                if (n.IsWalkable(_ff) && n.GetCost() <= range)
                {
                    // If possible area already contains this node (due to neighbours rechecking) ignore it
                    if (!area.Contains(n))
                    {
                        // Add node to to check queue and add it to possible area
                        toCheck.Enqueue(n);
                        area.Add(n);
                    }
                }
            }
        }

        // Support list
        List<Node> areaToReturn = new List<Node>();

        // If FF can ignore obstacle, correct path
        if (LevelManager.instance.GetSelectedFF.CanIgnoreObstacle() && LevelManager.instance.GetSelectedFF.IsUsable())
        {
            // Support list
            List<Node> correctedArea = new List<Node>();

            foreach (Node n in area)
            {
                // If node has an obstacle, don't add it to the area (FF can vault over it but cannot stand in it)
                if (!n.HasObstacle && !n.HasVictim)
                    correctedArea.Add(n);
            }

            foreach (Node n in correctedArea)
            {
                if (!n.HasFF)
                    areaToReturn.Add(n);
            }
        }
        else
        {
            foreach (Node n in area)
            {
                if (!n.HasFF && !n.HasVictim)
                    areaToReturn.Add(n);
            }
        }

        return areaToReturn;
    }

    public List<Node> GetSmokeFOV(Vector2Int startPos, int range, FireFighterBehaviour ff = null)
    {
        FireFighterBehaviour _ff = (ff == null) ? LevelManager.instance.GetSelectedFF : ff;

        // Clear all grid cost
        LevelManager.instance.ClearGridCost();

        // Queue for nodes to check
        Queue<Node> toCheck = new Queue<Node>();

        // HashSet for nodes already checked
        HashSet<Node> alreadyChecked = new HashSet<Node>();

        // Get starting node from starting position
        Node startNode = LevelManager.instance.GetNode(startPos.x, startPos.y);
        // Set its cost as 0
        startNode.SetCost(0);
        // Add to already checked
        alreadyChecked.Add(startNode);
        // Add to to check (needed to start the process)
        toCheck.Enqueue(startNode);

        // List for possible path area
        List<Node> area = new List<Node>();

        //Debug.Log("NEW AREA CALCULATION");

        // While there's still a node to check
        while (toCheck.Count > 0)
        {
            // Get current node to check from queue
            Node currentNode = toCheck.Dequeue();

            // Get node neighbours
            List<Node> neighbours = GetPossibleNeighbours(currentNode, alreadyChecked);

            // Foreach node check if it's walkable and if it's in range
            foreach (Node n in neighbours)
            {
                if (n.IsVisibleToOracle() && n.GetCost() <= range)
                {
                    // If possible area already contains this node (due to neighbours rechecking) ignore it
                    if (!area.Contains(n))
                    {
                        // Add node to to check queue and add it to possible area
                        toCheck.Enqueue(n);
                        area.Add(n);
                    }
                }
            }
        }

        //// Support list
        //List<Node> areaToReturn = new List<Node>();

        //// If FF can ignore obstacle, correct path
        //if (LevelManager.instance.GetSelectedFF.CanIgnoreObstacle() && LevelManager.instance.GetSelectedFF.IsUsable())
        //{
        //    // Support list
        //    List<Node> correctedArea = new List<Node>();

        //    foreach (Node n in area)
        //    {
        //        // If node has an obstacle, don't add it to the area (FF can vault over it but cannot stand in it)
        //        if (!n.HasObstacle && !n.HasVictim)
        //            correctedArea.Add(n);
        //    }

        //    foreach (Node n in correctedArea)
        //    {
        //        if (!n.HasFF)
        //            areaToReturn.Add(n);
        //    }
        //}
        //else
        //{
        //    foreach (Node n in area)
        //    {
        //        if (!n.HasFF && !n.HasVictim)
        //            areaToReturn.Add(n);
        //    }
        //}

        return area;
    }

    public List<Node> GetPossiblePathArea_CivAndSmoke(Vector2Int startPos, int range)
    {
        // Clear all grid cost
        LevelManager.instance.ClearGridCost();

        // Queue for nodes to check
        Queue<Node> toCheck = new Queue<Node>();

        // HashSet for nodes already checked
        HashSet<Node> alreadyChecked = new HashSet<Node>();

        // Get starting node from starting position
        Node startNode = LevelManager.instance.GetNode(startPos.x, startPos.y);
        // Set its cost as 0
        startNode.SetCost(0);
        // Add to already checked
        alreadyChecked.Add(startNode);
        // Add to to check (needed to start the process)
        toCheck.Enqueue(startNode);

        // List for possible path area
        List<Node> area = new List<Node>();

        //Debug.Log("NEW AREA CALCULATION");

        // While there's still a node to check
        while (toCheck.Count > 0)
        {
            // Get current node to check from queue
            Node currentNode = toCheck.Dequeue();

            // Get node neighbours
            List<Node> neighbours = GetPossibleNeighbours(currentNode, alreadyChecked);

            // Foreach node check if it's walkable and if it's in range
            foreach (Node n in neighbours)
            {
                if (n.IsAvailableToCivAndSmoke() && n.GetCost() <= range)
                {
                    // If possible area already contains this node (due to neighbours rechecking) ignore it
                    if (!area.Contains(n))
                    {
                        // Add node to to check queue and add it to possible area
                        toCheck.Enqueue(n);
                        area.Add(n);
                    }
                }
            }
        }

        // Support list
        List<Node> areaToReturn = new List<Node>();

        // If FF can ignore obstacle, correct path
        if (LevelManager.instance.GetSelectedFF.CanIgnoreObstacle() && LevelManager.instance.GetSelectedFF.IsUsable())
        {
            // Support list
            List<Node> correctedArea = new List<Node>();

            foreach (Node n in area)
            {
                // If node has an obstacle, don't add it to the area (FF can vault over it but cannot stand in it)
                if (!n.HasObstacle || !n.HasVictim)
                    correctedArea.Add(n);
            }

            foreach (Node n in correctedArea)
            {
                if (!n.HasFF)
                    areaToReturn.Add(n);
            }
        }
        else
        {
            foreach (Node n in area)
            {
                if (!n.HasFF)
                    areaToReturn.Add(n);
            }
        }

        return areaToReturn;
    }

    // Get possible neighbours given a node and a list of already checked nodes
    public List<Node> GetPossibleNeighbours(Node node, HashSet<Node> alreadyChecked)
    {
        // LevelManager instance
        LevelManager lm = LevelManager.instance;
        // List for neighbours nodes
        List<Node> neighbours = new List<Node>();

        // Check surrounding nodes
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                // 0,0 is the center, so the parent node, which must not be counted as neighbour
                if (x == 0 && y == 0)
                    continue;

                // If node has smoke, movement must be limtied to 4 instead of 8 tiles. Smoker doesn't care about it.
                if (node.HasSmoke && !node.Smoke.OracleIsAround())
                {
                    if (lm.GetSelectedFF.Quirk.QuirkName != PassiveSkill.SeeThroughSmoke && ((x == -1 && (y == 1 || y == -1)) || (x == 1 && (y == -1 || y == 1))))
                        continue;
                }
                
                // Translate into grid coordinates
                int checkX = node.GridPosition.x + x;
                int checkY = node.GridPosition.y + y;

                // If coordinates are not outside of bounds, they count as neighbours
                if (lm.IsInsideOfBounds(checkX, checkY))
                {
                    // Get node from coordinates
                    Node n = lm.GetWorldGrid[checkX, checkY];

                    //If it was already checked, don't count it
                    if (!alreadyChecked.Contains(n))
                    {
                        // If ndoe cost is 0, it means that it wasn't checked before
                        if (n.GetCost() == 0)
                        {
                            // Set its parent
                            n.SetParent(node);
                            // Set its cost
                            SetNodeCost(n, x, y);
                        }
                        else
                        {
                            // Get a temp cost using this node as parent
                            int tempCost;
                            tempCost = GetPossibleNodeCost(n, x, y, node);

                            // If temp cost is minor than previous cost, set it as new cost and parent
                            if (tempCost < n.GetCost())
                            {
                                // Set its parent
                                n.SetParent(node);
                                // Set its cost
                                SetNodeCost(n, tempCost);
                            }
                            // Check if there's a possible parent without smoke
                            else if (tempCost == n.GetCost() && node.GetParent() != n && !node.HasSmoke && n.GetParent().HasSmoke)
                            {
                                // Set its parent
                                n.SetParent(node);
                                // Set its cost
                                SetNodeCost(n, tempCost);
                            }
                        }

                        // If node has been checked less than 8 times, let it be available to check again (by adding it to the neighbours list)
                        if (n.GetTimesChecked < 8)
                        {
                            // Add node to neighbour list
                            neighbours.Add(n);
                            n.SetTimesChecked(n.GetTimesChecked + 1);
                        }
                        else // Else add it to already checked list os it won't get checked again
                        {
                            alreadyChecked.Add(n);
                        }
                    }
                }
            }
        }
        return neighbours;
    }

    // Get a node possible cost without actually setting it given a node, a position related to the center node and a possible parent
    private int GetPossibleNodeCost(Node n, int x, int y, Node parent)
    {
        int cost = parent.GetCost();

        // If node is a diagonal neighbour, set cost as parent cost + 2
        if ((x == -1 && (y == 1 || y == -1)) || (x == 1 && (y == -1 || y == 1)))
        {
            cost += 2;
            return cost;

        }
        // Else (horizontal/vertical neighbour) set cost as parent cost + 1
        else if ((x == 0 && (y == 1 || y == -1)) || (y == 0 && (x == -1 || x == 1)))
        {
            cost += 1;
            return cost;
        }
        else
        {
            Debug.Log("INVALID NODE POSITION!");
            return 0;
        }
    }

    // Set node cost given a node and a previously calculated cost
    private void SetNodeCost(Node n, int cost)
    {
        n.SetCost(cost);
    }

    // Given a node and a position related to the center node, calculate and set node cost
    private void SetNodeCost(Node n, int x, int y)
    {
        // If node is a diagonal neighbour, set its cost as parent cost + 2
        if ((x == -1 && (y == 1 || y == -1)) || (x == 1 && (y == -1 || y == 1)))
        {
            n.SetCost(n.GetParent().GetCost() + 2);

        }
        // Else (horizontal/vertical neighbour) set its cost as parent cost + 1
        else if ((x == 0 && (y == 1 || y == -1)) || (y == 0 && (x == -1 || x == 1)))
        {
            n.SetCost(n.GetParent().GetCost() + 1);
        }
        else
            Debug.Log("INVALID NODE POSITION!");
    }

    // Retrace path given a starting (where FF stands) and target position (where FF needs to go)
    public List<Node> RetracePath(Vector2Int startPos, Vector2Int targetPos)
    {
        // Translate grid positions into nodes
        Node startNode = LevelManager.instance.GetNode(startPos.x, startPos.y);
        Node targetNode = LevelManager.instance.GetNode(targetPos.x, targetPos.y);

        // Create path list
        List<Node> path = new List<Node>();
        // Set target node as current node
        Node currentNode = targetNode;

        // If current node doesn't correspond to the starting position
        while (currentNode.GridPosition != startNode.GridPosition)
        {
            // Add node to path
            path.Add(currentNode);
            // Set parent of the current node as current node
            currentNode = currentNode.GetParent();
        }
        // Reverse path to get proper direction
        path.Reverse();

        if (path.Count != 0)
            TotalCost = path[path.Count - 1].GetCost();

        return path;
    }

    // For civilians
    public Node GetFirstAvailableNode_Civ(Vector2Int startPos, int range)
    {
        List<Node> nodeList = GetPossiblePathArea_CivAndSmoke(startPos, range);

        foreach(Node n in nodeList)
        {
            if (!n.HasObstacle && n.RoomsType != RoomsType.Wall && n.RoomsType != RoomsType.DoorHeavy && n.RoomsType != RoomsType.Door && n.RoomsType != RoomsType.Window)
                return n;
        }

        // Made to avoid stack overflow
        if (range > LevelManager.instance.GetGridSize.x && range > LevelManager.instance.GetGridSize.y)
            return EmergencyCivDrop(startPos, 1);

        return GetFirstAvailableNode_Civ(startPos, range + 1);
    }

    // Break the first obstacle available and return that node as available for civilian drop
    public Node EmergencyCivDrop(Vector2Int startPos, int range)
    {
        List<Node> nodeList = GetPossiblePathArea_CivAndSmoke(startPos, range);

        foreach (Node n in nodeList)
        {
            if (n.RoomsType != RoomsType.Wall && n.RoomsType != RoomsType.Door && n.RoomsType != RoomsType.DoorHeavy && n.RoomsType != RoomsType.Window && n.HasObstacle && !n.HasVictim)
            {
                n.Obstacle.Break();
                return n;
            }
        }

        return GetFirstAvailableNode_Civ(startPos, range + 1);
    }

    // For blazes generating smoke
    public Node GetFirstAvailableNode_Smoke(Vector2Int startPos, int range)
    {
        List<Node> nodeList = GetPossiblePathArea_CivAndSmoke(startPos, range);

        foreach (Node n in nodeList)
        {
            if (!n.IsOnFire && !n.HasSmoke && !n.HasNearbyBrokenWindow && n.RoomsType != RoomsType.Wall && n.RoomsType != RoomsType.Door && n.RoomsType != RoomsType.DoorHeavy && n.RoomsType != RoomsType.Window)
                return n;
        }

        // Made to avoid stack overflow
        if (range > LevelManager.instance.GetGridSize.x && range > LevelManager.instance.GetGridSize.y)
            return LevelManager.instance.GetNode(startPos);

        return GetFirstAvailableNode_Smoke(startPos, range + 1);
    }
}
