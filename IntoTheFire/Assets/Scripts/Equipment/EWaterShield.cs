﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EWaterShield : Equip
{
    private void Start()
    {
        ELocatorMode = LocatorMode.Interactive;
    }

    public override void Execute(Entity _entity)
    {
        if (_entity == null) return;

        if (_entity as FireFighterBehaviour)
        {
            (_entity as FireFighterBehaviour).GetActiveActions(this);
        }

        if (!GameManager.Instance.UnlimitedUse)
        {
            CurrentChargets--;
        }

        EventManager.TriggerEvent(EventsID.UpdateUIValues);

    }

    public override PossibleActions GetEquipAction()
    {
        return PossibleActions.Heal;
    }
}
