﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EWaterNozzle : EEextinguisher
{
    public override void Execute(List<Vector2Int> _pos)
    {
        Debug.Log("Water activate");
        LevelManager.instance.KillFire(_pos, (int)Value);
        
        LevelManager.instance.SpawnWaterFromNozzle(_pos);

        if (!GameManager.Instance.UnlimitedUse)
        {
            CurrentChargets--;
        }

        if (!GameManager.Instance.UnlimitedActions)
        {
            LevelManager.instance.FFEndTurnAction();
        }
    }
}
