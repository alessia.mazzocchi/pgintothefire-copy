﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFireAxe : Equip
{
    void Start()
    {
        ELocatorMode = LocatorMode.Interactive;
        //EType = EquipType.Melee;
        //Value = Damage;
    }

    //public override bool CanExecute(Vector2Int _pos)
    //{
    //    if (!CheckDirection(_pos)) return false;

    //    return true;
    //}

    public override PossibleActions GetEquipAction()
    {
        return PossibleActions.TakeDamage;
    }
}
