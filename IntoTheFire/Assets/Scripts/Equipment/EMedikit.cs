﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMedikit : Equip
{

    private void Start()
    {
        ELocatorMode = LocatorMode.Interactive;
    }

    public override bool CanExecute(Vector2Int _pos)
    {
        if (CheckDirection(_pos)) return false;

        if (Charges <= 0) return false;

        return true;
    }

    //public override void OnClick()
    //{
    //    InputManager.Instance.selectedEquip = this;
    //}

    public override PossibleActions GetEquipAction()
    {
        return PossibleActions.Heal;
    }
}
