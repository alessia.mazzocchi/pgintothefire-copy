﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESmokeFan : EEextinguisher
{
    public override void Execute(List<Vector2Int> _pos)
    {
        if (!LevelManager.instance.ThereIsSmoke(_pos)) return;

        LevelManager.instance.KillSmoke(_pos);

        if (!GameManager.Instance.UnlimitedUse)
        {
            CurrentChargets--;
        }

        if (!GameManager.Instance.UnlimitedActions)
        {
            LevelManager.instance.FFEndTurnAction();
        }
    }
}
