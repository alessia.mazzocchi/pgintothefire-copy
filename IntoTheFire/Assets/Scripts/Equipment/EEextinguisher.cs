﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EEextinguisher : Equip
{
    protected List<Vector2Int> currentNodes;

    void Start()
    {
        EType = EquipType.Extinguish;
        ELocatorMode = LocatorMode.Interactive;

        //CurrentChargets = Charges;

        //Value = 1;
    }

    public override bool CanExecute()
    {
        //Debug.Log("Extinguisher check");
        if (CurrentChargets == 0) return false;
        
        return true;
    }

    //public override bool CanExecute(Vector2Int _pos)
    //{
    //    if (CurrentChargets == 0) return false;

    //    return true;
    //}

    public override void OnClick()
    { 
        base.OnClick();

        Vector2Int pos = Owner.GridPos;

        currentNodes = GetDirectionForExtinguisher;
    }

    public override void Execute(List<Vector2Int> _pos)
    {
        if (!LevelManager.instance.ThereIsFire(_pos)) return;

        LevelManager.instance.KillFire(_pos, (int)Value);

        base.Execute(_pos);
    }

    public override void OnRelease()
    {
        base.OnRelease();

        currentNodes.Clear();
    }
}
