﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EquipType
{
    Melee,
    Extinguish,
    Support
}

public enum EquipName
{
    Fire_Axe,
    Sledgehammer,
    Cement_Saw,
    Fire_Extinguisher,
    Foam_Grenades,
    Water_Nozzle,
    Smoke_Fan,
    Medikit,
    Water_Shield,
    None
}

public enum CheckRadius { FourDirection, EightDirection }

public enum PropagationEffect { Linear, Area }

public class Equip : MonoBehaviour
{
    /// <summary>
    /// Fire fighter owner, used to get the gridpos
    /// </summary>
    [HideInInspector]
    public FireFighterBehaviour Owner;
    public void SetOwner(FireFighterBehaviour _owner)
    {
        Owner = _owner;
    }

    public Vector2Int GridPos { get => Owner.GridPos; }

    /// <summary>
    /// Probably not needed
    /// </summary>
    [HideInInspector]
    public EquipType EType;
    [HideInInspector]
    public string UIName;
    public EquipName EName;

    public float Value;
    public int Charges;
    private int m_currentCharges;
    public int CurrentChargets { get => m_currentCharges; set => m_currentCharges = value; }

    [Space]
    [Tooltip("The range of effect for the extinguisher, like 3x1")]
    public Vector2Int EffectRange;
    [Space]
    public CheckRadius Directions = CheckRadius.FourDirection;
    public bool EnlightOwner;
    public PropagationEffect PropagationEffect = PropagationEffect.Linear;
    [Tooltip("The range of activation int the given direction")]
    [Min(1)]
    public int ActivationRangeMin = 1;
    public int ActivationRangeMax = 1;

    public Vector2Int RangeMinMax = new Vector2Int(1, 1);

    [HideInInspector]
    public LocatorMode ELocatorMode;

    //public virtual LocatorMode MyLocatorMode() { return ELocatorMode; }
    [Space]
    public Sprite Sprite;

    public Sprite AlternativeSprite;

    public string Description;

    public void SetEquipmentStat(EquipData _data)
    {
        try
        {
            if (_data == null)
            {
                Debug.LogError("No quip data found for " + this.name);
                return;
            }
        }
        catch (System.NullReferenceException)
        {
            Debug.LogError("No quip data found for " + this.name);
            throw;
        }

        EType = _data.EType;

        EName = _data.EName;

        UIName = _data.UIName;

        Value = _data.Value;

        Charges = _data.Charges;
        CurrentChargets = Charges;

        PropagationEffect = _data.PropagationEffect;

        EnlightOwner = _data.EnlightOwner;

        EffectRange = _data.EffectRange;

        Directions = _data.ActivateDirections;

        RangeMinMax = _data.RangeMinMax;

        Sprite = _data.Sprite;

        AlternativeSprite = _data.AlterantiveSprite;

        Description = _data.Description;
    }


    public virtual bool CanExecute() { return true; }
    public virtual bool CanExecute(Vector2Int _pos) { return Owner.CheckDirection(_pos, Directions, RangeMinMax); }
    public virtual bool CanExecute(Vector2Int[] _pos) { return Owner.CheckDirection(_pos, Directions, RangeMinMax); }

    public virtual void Execute() 
    {
        // Do something...
        if (!GameManager.Instance.UnlimitedUse)
        {
            CurrentChargets--;
        }

        if (!GameManager.Instance.UnlimitedActions)
        {
            LevelManager.instance.FFEndTurnAction();
        }
    }

    public virtual void Execute(Vector2Int position) 
    {
        // Do something...
        if (!GameManager.Instance.UnlimitedUse)
        {
            CurrentChargets--;
        }

        if (!GameManager.Instance.UnlimitedActions)
        {
            LevelManager.instance.FFEndTurnAction();
        }
    }

    public virtual void Execute (List<Vector2Int> _positions)
    {
        // Do something...

        if (!GameManager.Instance.UnlimitedUse)
        {
            CurrentChargets--;
        }

        if (!GameManager.Instance.UnlimitedActions)
        {
            LevelManager.instance.FFEndTurnAction();
        }
    }

    public virtual void Execute(Entity _entity) 
    {
        if (_entity == null) return;

        if (_entity as FireFighterBehaviour)
        {
            if ((_entity as FireFighterBehaviour).HasCompatibility(this))
            {
                (_entity as FireFighterBehaviour).GetActiveActions(this);

                if (!GameManager.Instance.UnlimitedActions)
                {
                    LevelManager.instance.FFEndTurnAction();
                }
            }
        }
        else if (_entity as Obstacle)
        {
            if ((_entity as Obstacle).LocatorEnlightCondition(this))
            {
                (_entity as Obstacle).GetActiveActions(this);

                if (!GameManager.Instance.UnlimitedActions)
                {
                    LevelManager.instance.FFEndTurnAction();
                }
            }
        }

        if (!GameManager.Instance.UnlimitedUse)
        {
            CurrentChargets--;
            //EventManager.TriggerEvent(EventsID.UpdateUIValues);
        }
    }

    /// <summary>
    /// Return the node rechable for the item
    /// </summary>
    public List<Vector2Int> GetDirectionNodes
    {
        get => Owner.GetDirectionNodes(RangeMinMax, Directions, EnlightOwner);
    }

    public List<Vector2Int> GetDirectionForExtinguisher
    {
        get => Owner.GetDirectionNodesExtinguisher(RangeMinMax, Directions, EnlightOwner);
    }

    ///// <summary>
    ///// Check Obstacle position with the player range in 4 directions
    ///// DEPRECATO
    ///// </summary>
    ///// <param name="_pos"></param>
    ///// <returns></returns>
    //private bool CheckFourDirections(Vector2Int _pos)
    //{
    //    // Position equal to the current Player;
    //    Vector2Int m_pos = Owner.GridPos;
    //    for (int i = ActivationRangeMin; i <= ActivationRangeMax; i++)
    //    {
    //        // Up
    //        if (new Vector2Int(m_pos.x, m_pos.y + i) == _pos) return true;
    //        // Down
    //        if (new Vector2Int(m_pos.x, m_pos.y - i) == _pos) return true;
    //        // Right
    //        if (new Vector2Int(m_pos.x + i, m_pos.y) == _pos) return true;
    //        // Left 
    //        if (new Vector2Int(m_pos.x - i, m_pos.y) == _pos) return true;
    //    }

    //    return false;
    //}

    public bool CheckDirection(Vector2Int _pos)
    {
        //if (Directions == CheckRadius.FourDirection)
        //    return CheckEightDirections(_pos);

        //else
        //    return CheckEightDirections(_pos);

        return Owner.CheckDirection(_pos, Directions, RangeMinMax);
    }

   

    public virtual void OnClick()
    {
        AudioData audio = new AudioData();
        audio.SetUISound(UISounds.SelectEquip, EName);
        EventManager.TriggerEvent(EventsID.PlayAudio, audio);

        InputManager.Instance.EquipSelected(this);

        //Debug.Log("Equip item");
    }

    public virtual void OnRelease()
    {
        // TODO CREATE CASE EXTINGUISH
        LevelManager.instance.DeactiveNodes(ELocatorMode);
    }

    public virtual PossibleActions GetEquipAction() { return PossibleActions.Heal; }

}
