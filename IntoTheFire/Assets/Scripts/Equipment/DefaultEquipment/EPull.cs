﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EPull : Equip
{
    private void Start()
    {
        ELocatorMode = LocatorMode.Enlight;
        EType = EquipType.Support;
    }

    public override PossibleActions GetEquipAction()
    {
        return PossibleActions.Pull;
    }
}