﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EPush : Equip
{
    private void Start()
    {
        ELocatorMode = LocatorMode.Interactive;
        EType = EquipType.Melee;
    }

    public override PossibleActions GetEquipAction()
    {
        return PossibleActions.Push;
    }
}
