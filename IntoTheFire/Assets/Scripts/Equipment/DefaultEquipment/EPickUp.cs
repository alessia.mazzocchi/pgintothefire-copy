﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EPickUp : Equip
{
    public Entity pickedVictim;

    public bool hasVictim { get => (pickedVictim != null) ? true : false; }

    private void Start()
    {
        ELocatorMode = LocatorMode.Interactive;
        EType = EquipType.Support;
    }


    public override PossibleActions GetEquipAction()
    {
        return PossibleActions.PickUp;
    }

    public override void Execute(Vector2Int position)
    {
        if (pickedVictim != null)
        {
            if (LevelManager.instance.CheckIfOnExitNode(position))
            {
                if (pickedVictim is Victim)
                    (pickedVictim as Victim).DropOnExitPoint();
                else if (pickedVictim is FireFighterBehaviour)
                    (pickedVictim as FireFighterBehaviour).DropOnExitPoint();

                return;
            }
            else if (LevelManager.instance.GetNode(position).HasFF && LevelManager.instance.GetNode(position).FireFighter.CanPickupVictim()) // Drop on another FF
            {
                Debug.Log("Pass on FF");
                if(pickedVictim is Victim)
                    (pickedVictim as Victim).PickUp(LevelManager.instance.GetNode(position).FireFighter);

                else if(pickedVictim is FireFighterBehaviour)
                    (pickedVictim as FireFighterBehaviour).PickUp(LevelManager.instance.GetNode(position).FireFighter);

                EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);
                EventManager.TriggerEvent(EventsID.UpdateUIValues);
                return;

            }
            else // Drop on floor
            {
                if (LevelManager.instance.GetNode(position).HasFF || LevelManager.instance.GetNode(position).HasObstacle)
                {
                    Debug.Log("Not space");
                    return;
                }

                LevelManager.instance.GetSelectedFF.RemoveVictimCarried(position, pickedVictim);
                EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);
                EventManager.TriggerEvent(EventsID.UpdateUIValues);
                return;
            }
        }

        base.Execute(position);
    }
}
