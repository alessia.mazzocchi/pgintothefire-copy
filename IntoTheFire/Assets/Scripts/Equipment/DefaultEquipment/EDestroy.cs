﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EDestroy : Equip
{
    private void Start()
    {
        ELocatorMode = LocatorMode.Interactive;
        EType = EquipType.Melee;
        
        Value = 1;
        Charges = -1;
    }


    public override PossibleActions GetEquipAction()
    {
        return PossibleActions.TakeDamage;
    }
}
