﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationReciver : MonoBehaviour
{
    private FireFighterBehaviour m_FF;

    private void Awake()
    {
        m_FF = GetComponentInParent<FireFighterBehaviour>();
    }

    public void TriggerEvent()
    {
        m_FF.ExecuteEquipAnimation();
    }

    public void TurnOffEquipMesh()
    {
        m_FF.TurnOffEquipMesh();
    }

    public void FootstepSound()
    {
        m_FF.PlayFootstep();
    }

    //public void JetJumpSound()
    //{
    //    m_FF.PlayJetJump();
    //}
}
