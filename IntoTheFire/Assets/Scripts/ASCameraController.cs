﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ASCameraController : MonoBehaviour
{
    public GameObject Cam;

    public Transform[] CamPoint;

    private int m_camIndex;
    public int CamIndex
    {
        get
        {
            return m_camIndex;
        }
        private set
        {
            m_camIndex = value;
            m_camIndex = (int)Mathf.Repeat(m_camIndex, 4);
        }
    }
    private bool isFixing;
    private int currentIndex;
    private float lerpTime;

    void Start()
    {
        CamIndex = 0;
        SetCameraPos();
    }

    void Update()
    {
        Movement();

        if (isFixing)
        {
            lerpTime += Time.deltaTime;

            float t = lerpTime / 1;
            Cam.transform.position = Vector3.Lerp(Cam.transform.position, CamPoint[CamIndex].position, t);

            Cam.transform.rotation = Quaternion.Lerp(Cam.transform.rotation, CamPoint[CamIndex].rotation, t);


            if(/*Vector2.Distance(Cam.transform.position, CamPoint[CamIndex].position) < 0.2f*/
                Cam.transform.position == CamPoint[CamIndex].position
                )
            {
                Cam.transform.position = CamPoint[CamIndex].position;
                lerpTime = 0;
                isFixing = false;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                CamIndex++;
                SetCameraPos();
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                CamIndex--;
                SetCameraPos();
            }
        }
    }

    public void SetCameraPos()
    {
        isFixing = true;
    }

    private void Movement()
    {
        float horizontal = Input.GetAxis("Horizontal");

        float vertical = Input.GetAxis("Vertical");

        Vector3 dir = new Vector3(horizontal, 0, vertical);
        
        transform.Translate(transform.TransformDirection(dir) * Time.deltaTime * 3f);
    }
}
