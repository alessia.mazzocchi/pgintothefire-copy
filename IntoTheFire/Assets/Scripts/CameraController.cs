﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance;

    private Camera m_cam;
    public Camera Cam 
    {
        get
        {
            if (m_cam == null)
                m_cam = Camera.main;

            return m_cam;
        }
    }

    private bool CameraLock = false;

    [Header("Camera Setting")]
    public int StartingFov = 50;

    public float FovMin = 30;
    public float FovMax = 50;
    public float zoomSensitivity = 10;
    public bool MouseBorderPan = true;
    [Space]
    public float KeyboardpanSpeed = 20f;
    public float MousePanSpeed = 20f;
    public float SpeedIncreaseTime;
    private float currentSpeedIncreaseTime;
    private float currentPanSpeed;
    public float panBorderThickness = 10f;
    public Vector2 X_panLimits;
    public Vector2 Y_panLimits;

    private bool fixRot;
    private bool fixPos;
    private bool followPath;
    private Vector3 dir;
    private bool borderMove;

    private Tween shakeTween;
    [Header("Rotation")]
    public float RotationTime = 0.30f;
    private float currentRotationTime;
    public AnimationCurve RotationCurve;

    private Quaternion startRot;
    private Quaternion EndRot;
    public float RotationSpeed = 1;
    public bool CameraRotationLock = false;

    [Header("Follow")]
    public float FollowMoveTime = 0.4f;
    public float LEventMoveTime = 0.7f;
    private float moveTime;
    private float currentMoveToPositionTime;
    public AnimationCurve FollowCurve;

    private Vector3 startPos;
    private Vector3 endPos;

    private Vector3 m_velocity = new Vector3(1, 1, 1);
    private bool followTarget;

    private System.Action currentAction;
    private Coroutine smoothSpeedCoroutine;

    [Header("Overview")]
    public float OverviewMovementTime;
    private float currentOverviewMovevmentTime;
    public AnimationCurve OverviewCurve;
    public float WaitTime;

    private List<Vector3> cameraPath;
    private int pathIndex = 0;

    private Transform m_Target;
    private Transform Target 
    { 
        get 
        {   
            if (m_Target == null)
                m_Target = LevelManager.instance.GetSelectedFF.gameObject.transform;

            return m_Target;
        }
        set
        {
            m_Target = value;
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Cam.fieldOfView = StartingFov;
        currentPanSpeed = MousePanSpeed;
    }

    private void OnEnable()
    {
        EventManager.StartListening<FireFighterBehaviour>(EventsID.FFChange, MoveToPosition);
        EventManager.StartListening<bool>(EventsID.FFStartedMoving, SetFollow);
        EventManager.StartListening<bool>(EventsID.FFStoppedMoving, SetFollow);
        EventManager.StartListening(EventsID.GameStart, SetCameraLimits);
    }

    private void OnDisable()
    {
        EventManager.StopListening<FireFighterBehaviour>(EventsID.FFChange, MoveToPosition);
        EventManager.StopListening<bool>(EventsID.FFStartedMoving, SetFollow);
        EventManager.StopListening<bool>(EventsID.FFStoppedMoving, SetFollow);
        EventManager.StopListening(EventsID.GameStart, SetCameraLimits);
    }

    void Update()
    {
        if (CameraLock) return;

        if (LevelManager.instance.GameStatus == GameStatus.Play)
        {
            /// Movement
            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            {
                dir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

                transform.Translate(dir * KeyboardpanSpeed * Time.deltaTime, Space.Self);
            }

            if (MouseBorderPan)
            {
                // Pan
                if (Input.mousePosition.y >= Screen.height - panBorderThickness)
                {
                    transform.Translate(transform.forward * currentPanSpeed * Time.deltaTime, Space.World);
                }

                if (Input.mousePosition.y <= panBorderThickness)
                {
                    transform.Translate(-transform.forward * currentPanSpeed * Time.deltaTime, Space.World);
                }

                if (Input.mousePosition.x >= Screen.width - panBorderThickness)
                {
                    transform.Translate(transform.right * currentPanSpeed * Time.deltaTime, Space.World);
                }

                if (Input.mousePosition.x <= panBorderThickness)
                {
                    transform.Translate(-transform.right * currentPanSpeed * Time.deltaTime, Space.World);
                }
            }

            /// Zoom
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                float fov = Cam.fieldOfView;
                fov += Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity * -1;
                fov = Mathf.Clamp(fov, FovMin, FovMax);

                Cam.fieldOfView = fov;
            }

            if (CameraRotationLock)
            {
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    if (!fixRot)
                    {
                        startRot = transform.rotation;

                        EndRot = transform.rotation * Quaternion.AngleAxis(90, Vector3.up);

                        fixRot = true;
                    }
                }

                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (!fixRot)
                    {
                        startRot = transform.rotation;

                        EndRot = transform.rotation * Quaternion.AngleAxis(-90, Vector3.up);

                        fixRot = true;
                    }
                }
            }
            else
            {

                if (Input.GetAxis("CameraRotation") != 0)
                {
                    Quaternion rot = Quaternion.AngleAxis(Input.GetAxis("CameraRotation") * RotationSpeed, Vector3.up);
                    transform.rotation *= rot;
                }
            }

            /// ---------------------------------------

            /// Clamp position
            Vector3 pos = transform.position;

            pos.x = Mathf.Clamp(pos.x, -X_panLimits.x, X_panLimits.y);
            pos.z = Mathf.Clamp(pos.z, -Y_panLimits.x, Y_panLimits.y);

            transform.localPosition = pos;
            m_velocity = Vector3.zero;
        }
      
    }

    private void FixedUpdate()
    {
        if (fixRot)
        {
            currentRotationTime += Time.fixedDeltaTime;

            float t = currentRotationTime / RotationTime;

            transform.rotation = Quaternion.Lerp(startRot, EndRot, RotationCurve.Evaluate(t));


            if (t >= 1)
            {
                fixRot = false;
                currentRotationTime = 0;

                transform.rotation = EndRot;
            }
        }

        if (fixPos)
        {
            currentMoveToPositionTime += Time.fixedDeltaTime;

            float t = currentMoveToPositionTime / moveTime;

            transform.position = Vector3.Lerp(startPos, endPos, FollowCurve.Evaluate(t));

            if (t >= 1)
            {
                fixPos = false;
                CameraLock = false;

                currentMoveToPositionTime = 0;

                transform.position = endPos;

                if (currentAction != null)
                    currentAction.Invoke();

                currentAction = null;
            }
        }
    }

    private void LateUpdate()
    {
        if (followTarget)
            transform.position = Vector3.SmoothDamp(transform.position, Target.transform.position, ref m_velocity, 0.4f);
    }

    public void SetFollow(bool _active)
    {
        followTarget = _active;
        CameraLock = _active;
    }

    public void MoveToPosition(FireFighterBehaviour _ff)
    {
        currentMoveToPositionTime = 0;

        CameraLock = true;

        fixPos = true;

        Target = _ff.gameObject.transform;

        startPos = transform.position;

        endPos = Target.position;

        moveTime = FollowMoveTime;
    }

    public void MoveToPosition(Vector3 _destiantion, System.Action _action = null)
    {
        currentMoveToPositionTime = 0;

        CameraLock = true;

        fixPos = true;

        startPos = transform.position;

        endPos = _destiantion;

        moveTime = LEventMoveTime;

        currentAction = _action;
    }

    public void SetCameraPath(List<Vector3> _positions)
    {
        if(_positions.Count == 0)
        {
            LevelManager.instance.CameraEndLevelOverview();
        }
        else
        {
            cameraPath = _positions;
            pathIndex = 0;

            CameraLock = true;

            startPos = transform.position;

            endPos = cameraPath[0];

            StartCoroutine(CameraOverview());
        }
    }

    private IEnumerator CameraOverview()
    {
        yield return new WaitForSecondsRealtime(0.6f);

        while (pathIndex < cameraPath.Count)
        {
            currentOverviewMovevmentTime += Time.fixedDeltaTime;

            float t = currentOverviewMovevmentTime / OverviewMovementTime;

            transform.position = Vector3.Lerp(startPos, endPos, OverviewCurve.Evaluate(t));

            yield return new WaitForFixedUpdate();

            if (t >= 1)
            {
                currentOverviewMovevmentTime = 0;
                transform.position = endPos;

                pathIndex++;

                startPos = transform.position;

                if(pathIndex < cameraPath.Count)
                    endPos = cameraPath[pathIndex];

                yield return new WaitForSecondsRealtime(WaitTime);
            }
        }

        CameraLock = false;

        LevelManager.instance.CameraEndLevelOverview();
    }

    private IEnumerator PanSpeedCoroutine()
    {
        Debug.Log("Increase");
        while(currentSpeedIncreaseTime != SpeedIncreaseTime)
        {
            currentSpeedIncreaseTime += Time.fixedDeltaTime;
            currentPanSpeed = (currentSpeedIncreaseTime * KeyboardpanSpeed) / SpeedIncreaseTime;

            currentPanSpeed = Mathf.Clamp(currentPanSpeed, 0, KeyboardpanSpeed);
            yield return null;
        }

        currentSpeedIncreaseTime = 0;
    }

    public void RotateCamera(bool _ToRight)
    {
        if (Input.GetAxis("CameraRotation") != 0) return;

        float speed = _ToRight ? 1 : -1;
        Quaternion rot = Quaternion.AngleAxis(speed * RotationSpeed, Vector3.up);
        transform.rotation *= rot;

    }

    private void SetCameraLimits()
    {
        X_panLimits.y = LevelManager.instance.GetGridSize.x - 1;
        Y_panLimits.y = LevelManager.instance.GetGridSize.y - 1;
    }

    public void DebriesShake()
    {
        if (shakeTween != null && shakeTween.IsPlaying()) return;

        if (shakeTween == null)
            transform.DOShakePosition(0.4f, new Vector3(0.2f, 0.2f, 0f)).SetAutoKill(false);
        else
            shakeTween.Restart();
    }
}
