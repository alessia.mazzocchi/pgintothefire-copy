﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ASMouseOverHandler <T> where T : IASMouseOver
{
    private T lastHit;
    private LayerMask layer;
    private Camera m_Camera;
    public Camera Camera
    {
        get
        {
            if (m_Camera == null || !m_Camera.gameObject.activeInHierarchy)
                m_Camera = Camera.main;

            return m_Camera;
        }
    }
    #region Constructors

    public ASMouseOverHandler()
    {
        m_Camera = Camera.main;
    }

    public ASMouseOverHandler(Camera _cam)
    {
        m_Camera = _cam;
    }
    #endregion


    public void OnUpdateOver()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity))
        {
            if (hit.collider != null)
            {
                T tmpHit; 
                hit.collider.TryGetComponent(out tmpHit);
                if (tmpHit == null) return;

                if (lastHit != null && lastHit.ASGameObject() == tmpHit.ASGameObject()) return;

                lastHit?.MouseExit();

                lastHit = tmpHit;
                
                lastHit.MouseEnter();
            }
        }
        else
        {  
            lastHit?.MouseExit();

            lastHit = default;   
        }
    }

    public void OnClick() 
    {
        lastHit?.MouseClick();
    }

    public void OnOver() 
    {
        throw new NotImplementedException();
    }

    public void ClearLastHit()
    {
        lastHit?.MouseExit();

        lastHit = default;
    }
}


//public sealed class ASHubInteract : ASMouseOverHandler <IHubMouseOver> { }

//public sealed class ASGameInteract : ASMouseOverHandler <IGameMouseOver> { }