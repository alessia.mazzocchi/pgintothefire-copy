﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LiquidType { Water, Fuel}

public class LiquidBehaviour : Entity, ITooltip
{
    public LiquidType Type;

    public int HealthMax;

    private int m_CurrentHealth;

    public int HealthCurrent
    {
        get => m_CurrentHealth;

        set
        {
            m_CurrentHealth = value;

            m_CurrentHealth = Mathf.Clamp(m_CurrentHealth, 0, HealthMax);
        }
    }

    public GameObject go => this.gameObject;

    private void OnEnable()
    {
        m_CurrentHealth = HealthMax;
    }

    public void TakeDamage(int _damage)
    {
        HealthCurrent -= _damage;
    }

    // DEV: Check was not called into TakeDamage because it is called from the node itself for easier access (Liquid would need to pass through LevelManager to get node)
    public bool DeathCheck()
    {
        if (HealthCurrent <= 0)
            return true;

        return false;
    }

    public void DeleteLiquid()
    {
        gameObject.SetActive(false);
    }

    public void ShowTip(bool _active)
    {
        if (!LevelManager.instance.GetNode(GridPos).HasSmoke || LevelManager.instance.GetNode(GridPos).HasSmoke && LevelManager.instance.GetNode(GridPos).Smoke.SeeThrough)
            UIManager.Instance.SetTooltip(_active, this);
    }
}
