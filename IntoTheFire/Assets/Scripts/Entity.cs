﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Entity : MonoBehaviour
{
    TooltipTypes TooltipType = TooltipTypes.Entity;

    //[HideInInspector]
    public Vector2Int GridPos;

    protected Node MyNode
    {
        get
        {
            return LevelManager.instance.GetNode(GridPos.x, GridPos.y);

            //return temp;
        }
    }

    protected virtual void Awake() { }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.OnSceneLoad, Load);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.OnSceneLoad, Load);
    }

    public virtual void Load()
    {
        GridPos = GetGridPosition();

        LinkToCell();
    }

    public Vector2Int GetGridPosition()
    {
        float size = LevelManager.instance.GetGridCellSize();

        int m_X = Mathf.RoundToInt((int)transform.position.x / (size + LevelManager.instance.GetGridCellGap()));
        int m_Y = Mathf.RoundToInt((int)transform.position.z / (size + LevelManager.instance.GetGridCellGap()));

        Vector2Int pos = new Vector2Int(m_X, m_Y);

        return pos;
    }

    public Vector2Int GetGridPosition(Vector3 _worldPosition)
    {
        float size = LevelManager.instance.GetGridCellSize();

        int m_X = Mathf.RoundToInt((int)_worldPosition.x / (size + LevelManager.instance.GetGridCellGap()));
        int m_Y = Mathf.RoundToInt((int)_worldPosition.z / (size + LevelManager.instance.GetGridCellGap()));

        Vector2Int pos = new Vector2Int(m_X, m_Y);

        return pos;
    }

    public virtual void LinkToCell() { }

    public void OnGridCellEnter() { }

    public List<Vector2Int> GetDirectionNodes(Vector2Int _range, CheckRadius _dir, bool _EnlightOwner = false)
    {
        List<Vector2Int> nodesList;

        nodesList = (_dir == CheckRadius.FourDirection) ? Get4DirectionNode(_range) : Get8DirectionNode(_range);

        if (_EnlightOwner) nodesList.Add(GridPos);

        return nodesList;
    }

    public List<Vector2Int> GetDirectionNodesExtinguisher(Vector2Int _range, CheckRadius _dir, bool _EnlightOwner = false)
    {
        List<Vector2Int> nodesList;

        nodesList = (_dir == CheckRadius.FourDirection) ? Get4DirectionNodeExtinguisher(_range) : Get8DirectionNodeExtinguisher(_range);

        //nodesList = (_dir == CheckRadius.FourDirection) ? Get4DirectionNode(_range) : Get8DirectionNode(_range);

        if (_EnlightOwner) nodesList.Add(GridPos);

        return nodesList;
    }

    /// <summary>
    /// Check Obstacle position with the player range in 4 directions
    /// </summary>
    /// <param name="_ObstaclePos"></param>
    /// <returns></returns>
    private bool CheckFourDirections(Vector2Int _ObstaclePos, Vector2Int _range)
    {
        for (int i = _range.x; i <= _range.y; i++)
        {
            // Up
            if (new Vector2Int(GridPos.x, GridPos.y + i) == _ObstaclePos) return true;
            // Down
            if (new Vector2Int(GridPos.x, GridPos.y - i) == _ObstaclePos) return true;
            // Right
            if (new Vector2Int(GridPos.x + i, GridPos.y) == _ObstaclePos) return true;
            // Left 
            if (new Vector2Int(GridPos.x - i, GridPos.y) == _ObstaclePos) return true;
        }

        return false;
    }

    private bool CheckFourDirections(Vector2Int[] _ObstaclePos, Vector2Int _range)
    {
        for (int j = 0; j < _ObstaclePos.Length; j++)
        {
            for (int i = _range.x; i <= _range.y; i++)
            {
                // Up
                if (new Vector2Int(GridPos.x, GridPos.y + i) == _ObstaclePos[j]) return true;
                // Down
                if (new Vector2Int(GridPos.x, GridPos.y - i) == _ObstaclePos[j]) return true;
                // Right
                if (new Vector2Int(GridPos.x + i, GridPos.y) == _ObstaclePos[j]) return true;
                // Left 
                if (new Vector2Int(GridPos.x - i, GridPos.y) == _ObstaclePos[j]) return true;
            }
        }
       

        return false;
    }

    /// <summary>
    /// Check Obstacle position with the player range in 8 directions
    /// </summary>
    /// <param name="_pos"></param>
    /// <returns></returns>
    private bool CheckEightDirections(Vector2Int _pos, Vector2Int _range)
    {
        for (int i = _range.x; i <= _range.y; i++)
        {
            // Up
            if (new Vector2Int(GridPos.x, GridPos.y + i) == _pos) return true;
            // Down
            if (new Vector2Int(GridPos.x, GridPos.y - i) == _pos) return true;
            // Right
            if (new Vector2Int(GridPos.x + i, GridPos.y) == _pos) return true;
            // Left 
            if (new Vector2Int(GridPos.x - i, GridPos.y) == _pos) return true;

            // Up - Right 
            if (new Vector2Int(GridPos.x + i, GridPos.y + i) == _pos) return true;
            // Up - Left
            if (new Vector2Int(GridPos.x - i, GridPos.y + i) == _pos) return true;
            // Down - Right
            if (new Vector2Int(GridPos.x + i, GridPos.y - i) == _pos) return true;
            // Down - Left
            if (new Vector2Int(GridPos.x - i, GridPos.y - i) == _pos) return true;
        }

        return false;
    }

    private bool CheckEightDirections(Vector2Int[] _pos, Vector2Int _range)
    {
        for (int j = 0; j < _pos.Length; j++)
        {
            for (int i = _range.x; i <= _range.y; i++)
            {
                // Up
                if (new Vector2Int(GridPos.x, GridPos.y + i) == _pos[j]) return true;
                // Down
                if (new Vector2Int(GridPos.x, GridPos.y - i) == _pos[j]) return true;
                // Right
                if (new Vector2Int(GridPos.x + i, GridPos.y) == _pos[j]) return true;
                // Left 
                if (new Vector2Int(GridPos.x - i, GridPos.y) == _pos[j]) return true;

                // Up - Right 
                if (new Vector2Int(GridPos.x + i, GridPos.y + i) == _pos[j]) return true;
                // Up - Left
                if (new Vector2Int(GridPos.x - i, GridPos.y + i) == _pos[j]) return true;
                // Down - Right
                if (new Vector2Int(GridPos.x + i, GridPos.y - i) == _pos[j]) return true;
                // Down - Left
                if (new Vector2Int(GridPos.x - i, GridPos.y - i) == _pos[j]) return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Check if the given position is in range
    /// </summary>
    /// <param name="_OtherPosition"></param>
    /// <param name="_dir"></param>
    /// <param name="_range"></param>
    /// <returns></returns>
    public bool CheckDirection(Vector2Int _OtherPosition, CheckRadius _dir, Vector2Int _range)
    {
        if (_dir == CheckRadius.FourDirection)
            return CheckFourDirections(_OtherPosition, _range);

        else
            return CheckEightDirections(_OtherPosition, _range);
    }

    public bool CheckDirection(Vector2Int[] _OtherPosition, CheckRadius _dir, Vector2Int _range)
    {
        if (_dir == CheckRadius.FourDirection)
            return CheckFourDirections(_OtherPosition, _range);

        else
            return CheckEightDirections(_OtherPosition, _range);
    }

    protected List<Vector2Int> Get4DirectionNode(Vector2Int _range)
    {
        LevelManager lm = LevelManager.instance;

        List<Vector2Int> temp = new List<Vector2Int>();
        List<Vector2Int> nodes = new List<Vector2Int>();

        for (int i = _range.x; i <= _range.y; i++)
        {
            // Up
            temp.Add(new Vector2Int(GridPos.x, GridPos.y + i));
            // Down
            temp.Add(new Vector2Int(GridPos.x, GridPos.y - i));
            // Left
            temp.Add(new Vector2Int(GridPos.x - i, GridPos.y));
            // Right
            temp.Add(new Vector2Int(GridPos.x + i, GridPos.y));
        }

        foreach (Vector2Int node in temp)
        {
            int checkX = node.x;
            int checkY = node.y;

            // If coordinates are not outside of bounds, they count
            if (lm.IsInsideOfBounds(checkX, checkY))
            {
                nodes.Add(node);
            }
        }

        return nodes;
    }

    protected List<Vector2Int> Get4DirectionNodeExtinguisher(Vector2Int _range)
    {
        LevelManager lm = LevelManager.instance;

        List<Vector2Int> temp = new List<Vector2Int>();
        List<Vector2Int> nodes = new List<Vector2Int>();

        Vector2Int node;
        // UP
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x, GridPos.y + i);
            if (lm.IsInsideOfBounds(node) && lm.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }
       
        //Down
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x, GridPos.y - i);
            if (lm.IsInsideOfBounds(node) && lm.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }

        //Left
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x - i, GridPos.y);
            if (lm.IsInsideOfBounds(node) && lm.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }

        //Right
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x + i, GridPos.y);
            if (lm.IsInsideOfBounds(node) && lm.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }


        foreach (Vector2Int n in temp)
        {
            int checkX = n.x;
            int checkY = n.y;

            // If coordinates are not outside of bounds, they count
            //if (lm.IsInsideOfBounds(checkX, checkY))
            //{
                nodes.Add(n);
            //}
        }

        return nodes;
    }

    // DEV: these methods should be moved to pathfinding

    private List<Vector2Int> Get8DirectionNodeExtinguisher(Vector2Int _range)
    {
        LevelManager lm = LevelManager.instance;

        List<Vector2Int> temp = new List<Vector2Int>();
        List<Vector2Int> nodes = new List<Vector2Int>();

        //for (int i = _range.x; i <= _range.y; i++)
        //{
        //    // Up
        //    temp.Add(new Vector2Int(GridPos.x, GridPos.y + i));
        //    // Down
        //    temp.Add(new Vector2Int(GridPos.x, GridPos.y - i));
        //    // Left
        //    temp.Add(new Vector2Int(GridPos.x - i, GridPos.y));
        //    // Right
        //    temp.Add(new Vector2Int(GridPos.x + i, GridPos.y));

        //    // Up - right
        //    temp.Add(new Vector2Int(GridPos.x + i, GridPos.y + i));
        //    // Up - left  
        //    temp.Add(new Vector2Int(GridPos.x - i, GridPos.y + i));
        //    // Down - right
        //    temp.Add(new Vector2Int(GridPos.x + i, GridPos.y - i));
        //    // Down - left  
        //    temp.Add(new Vector2Int(GridPos.x - i, GridPos.y - i));
        //}

        Vector2Int node;
        // UP
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x, GridPos.y + i);
            if (lm.IsInsideOfBounds(node) && LevelManager.instance.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }

        //Down
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x, GridPos.y - i);
            if (lm.IsInsideOfBounds(node) && LevelManager.instance.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }

        //Left
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x - i, GridPos.y);
            if (lm.IsInsideOfBounds(node) && LevelManager.instance.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }

        //Right
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x + i, GridPos.y);
            if (lm.IsInsideOfBounds(node) && LevelManager.instance.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }

        // Up - right
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x + i, GridPos.y + i);
            if (lm.IsInsideOfBounds(node) && LevelManager.instance.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }


        // Up - Left
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x - i, GridPos.y + i);
            if (lm.IsInsideOfBounds(node) && LevelManager.instance.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }

        // Down - Right
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x + i, GridPos.y - i);
            if (lm.IsInsideOfBounds(node) && LevelManager.instance.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }

        // Down - Left
        for (int i = _range.x; i <= _range.y; i++)
        {
            node = new Vector2Int(GridPos.x - i, GridPos.y - i);
            if (lm.IsInsideOfBounds(node) && LevelManager.instance.GetNode(node).RoomsType == RoomsType.Floor)
                temp.Add(node);
            else
                break;
        }

        foreach (Vector2Int n in temp)
        {
            int checkX = n.x;
            int checkY = n.y;

            // If coordinates are not outside of bounds, they count
            //if (checkX >= 0 && checkX < lm.GetGridSize.x && checkY >= 0 && checkY < lm.GetGridSize.y)
            //{
                nodes.Add(n);
            //}
        }


        return nodes;
    }

    private List<Vector2Int> Get8DirectionNode(Vector2Int _range)
    {
        LevelManager lm = LevelManager.instance;

        List<Vector2Int> temp = new List<Vector2Int>();
        List<Vector2Int> nodes = new List<Vector2Int>();

        for (int i = _range.x; i <= _range.y; i++)
        {
            // Up
            temp.Add(new Vector2Int(GridPos.x, GridPos.y + i));
            // Down
            temp.Add(new Vector2Int(GridPos.x, GridPos.y - i));
            // Left
            temp.Add(new Vector2Int(GridPos.x - i, GridPos.y));
            // Right
            temp.Add(new Vector2Int(GridPos.x + i, GridPos.y));

            // Up - right
            temp.Add(new Vector2Int(GridPos.x + i, GridPos.y + i));
            // Up - left  
            temp.Add(new Vector2Int(GridPos.x - i, GridPos.y + i));
            // Down - right
            temp.Add(new Vector2Int(GridPos.x + i, GridPos.y - i));
            // Down - left  
            temp.Add(new Vector2Int(GridPos.x - i, GridPos.y - i));
        }

        foreach (Vector2Int node in temp)
        {
            int checkX = node.x;
            int checkY = node.y;

            // If coordinates are not outside of bounds, they count
            if (checkX >= 0 && checkX < lm.GetGridSize.x && checkY >= 0 && checkY < lm.GetGridSize.y)
            {
                nodes.Add(node);
            }
        }


        return nodes;
    }

    //public void OnPointerEnter(PointerEventData eventData)
    //{
    //    UIManager.Instance.SetTooltip(true, this);
    //}

    //public void OnPointerExit(PointerEventData eventData)
    //{
    //    UIManager.Instance.SetTooltip(false, this);
    //}

    //private void OnMouseEnter()
    //{
    //    UIManager.Instance.SetTooltip(true, this);
    //}

    //private void OnMouseExit()
    //{
    //    UIManager.Instance.SetTooltip(false, this);
    //}
}
