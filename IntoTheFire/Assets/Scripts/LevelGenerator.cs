﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;
using UnityEditor;

public class LevelGenerator : MonoBehaviour
{
#if UNITY_EDITOR

    public string LevelName;

    [Header("Grid values")]

    #region Grid values
    public float GridWorldXSize = 1;
    public float GridWorldYSize = 1;
    public float GridCellsGap;

    #endregion
    private int Rows;
    private int Columns;

    private string m_GridName = "Grid ";
    private string m_modelContainerName = "ModelsContainer";

    private Node[,] WorldGrid;

    [Space]
    public GameObject LevelToLoad;
    
    #region Private var

    private GameObject m_prefabContainer;
    public GameObject PrefabContainer
    {
        get
        {
            if (m_prefabContainer != null)
                return m_prefabContainer;
            else
                Debug.LogWarning("Load current Level prefab");

            return null;
        }
        set
        {
            m_prefabContainer = value;
        }
    }

    private GameObject m_modelsContainer;
    public GameObject ModelsContainer
    {
        get
        {
            if (m_modelsContainer == null)
            {
                m_modelsContainer = GameObject.Find(m_modelContainerName);
            }

            return m_modelsContainer;
        }
        set
        {
            m_modelsContainer = value;
        }
    }

    private LevelManager m_levelManagerRef;
    public LevelManager LevelManagerRef
    {
        get
        {
            if(m_levelManagerRef == null)
            {

                m_levelManagerRef = LevelToLoad.GetComponent<LevelManager>();

                if(m_levelManagerRef == null)
                {
                    Debug.LogError("No Level to Load found");
                    return null;
                }
            }

            return m_levelManagerRef;
        }
    }

    private Grid m_gridRef;
    public Grid GridRef
    {
        get
        {
            return (m_gridRef != null)? m_gridRef : null;
        }
        set
        {
            m_gridRef = value;
        }
    }

    private Tilemap m_RoomsTilemap;
    public Tilemap RoomsTilemap
    {
        get
        {
            if (m_RoomsTilemap == null)
            {
                m_RoomsTilemap = GameObject.Find("RoomsTilemap").GetComponent<Tilemap>();
            }

            return m_RoomsTilemap;
        }
        set
        {
            m_RoomsTilemap = value;
        }
    }

    private Tilemap m_GroundTilemap;
    public Tilemap GroundTilemap
    {
        get
        {
            if (m_GroundTilemap == null)
            {
                m_GroundTilemap = GameObject.Find("GroundTilemap").GetComponent<Tilemap>();
            }

            return m_GroundTilemap;
        }
        set
        {
            m_GroundTilemap = value;
        }
    }

    private Tilemap m_EnvsTilemap;
    public Tilemap EnvsTilemap
    {
        get
        {
            if (m_EnvsTilemap == null)
            {
                m_EnvsTilemap = GameObject.Find("EnvsTilemap").GetComponent<Tilemap>();
            }

            return m_EnvsTilemap;
        }
        set
        {
            m_EnvsTilemap = value;
        }
    }
    #endregion]

    #region Tilemaps

    public void GenerateTilemap()
    {
        GameObject grid = new GameObject("Grid");
        grid.transform.SetParent(PrefabContainer.transform);

        GridRef = grid.AddComponent<Grid>();
        GridRef.cellLayout = GridLayout.CellLayout.Rectangle;
        GridRef.cellSwizzle = GridLayout.CellSwizzle.XZY;

        //Ground
        GameObject tile = new GameObject("GroundTilemap");

        tile.transform.SetParent(grid.transform);
        tile.AddComponent<Tilemap>();
        tile.AddComponent<TilemapRenderer>();

        GroundTilemap = tile.GetComponent<Tilemap>(); ;
        GroundTilemap.orientation = Tilemap.Orientation.XZ;

        m_levelManagerRef.GroundTilemap = GroundTilemap;


        // Rooms tilemap
        tile = new GameObject("RoomsTilemap");

        tile.transform.SetParent(grid.transform);
        tile.AddComponent<Tilemap>();
        tile.AddComponent<TilemapRenderer>();

        RoomsTilemap = tile.GetComponent<Tilemap>(); ;
        RoomsTilemap.orientation = Tilemap.Orientation.XZ;

        m_levelManagerRef.RoomsTilemap = RoomsTilemap;


        //Envs
        tile = new GameObject("EnvsTilemap");

        tile.transform.SetParent(grid.transform);
        tile.AddComponent<Tilemap>();
        tile.AddComponent<TilemapRenderer>();

        EnvsTilemap = tile.GetComponent<Tilemap>(); ;
        EnvsTilemap.orientation = Tilemap.Orientation.XZ;

        m_levelManagerRef.EnvsTilemap = EnvsTilemap;

    }

    private void GetTilemapSize()
    {
        // Check when one side has no more tile
        bool xDone = false;
        bool yDone = false;

        // Coordinates
        int x = 0;
        int y = 0;

        int tempX = 0;
        int tempY = 0;

        Vector3Int tilePos = new Vector3Int(x, y, 0);

        while (RoomsTilemap.HasTile(tilePos))
        {
            // Check if the next RIGHT tile has a tile
            if (RoomsTilemap.HasTile(new Vector3Int(x + 1, y, 0)))
                // If so we can move on on X
                tempX = x + 1;

            // Check if the UPPER tile has a tile
            if (RoomsTilemap.HasTile(new Vector3Int(x, y + 1, 0)))
                // If so we can move on on Y
                tempY = y + 1;


            // if X is equal to the previus X it means that there are no more tile on the right, so check TRUE the exit condition
            if (x < tempX)
                x = tempX;
            else
                xDone = true;


            // if Y is equal to the previus Y it means that there are no more tile above, so check TRUE the exit condition
            if (y < tempY)
                y = tempY;
            else
                yDone = true;

            // Update coordinates for next cycles
            tilePos = new Vector3Int(x, y, 0);

            // EXIT CONDITION: 
            if (xDone && yDone)
                break;
        }

        Rows = x;
        Columns = y;

        Debug.Log("Tilemap size: " + x + " - " + y);
    }

    #endregion

    #region Level Manager

    public void GenerateLevelManager()
    {
        PrefabContainer = new GameObject(LevelName + "_Prefab");
        LevelToLoad = PrefabContainer;

        GameObject temp_lm = new GameObject("LevelManager");

        temp_lm.transform.SetParent(PrefabContainer.transform);
        m_levelManagerRef = temp_lm.AddComponent<LevelManager>();
        //temp_lm.AddComponent<MissionManager>();
    }

    public void InitializeLevelManager()
    {
        m_levelManagerRef.GridComponent = GridRef;

        m_levelManagerRef.RoomsTilemap = RoomsTilemap;

        m_levelManagerRef.GroundTilemap = GroundTilemap;

        m_levelManagerRef.EnvsTilemap = EnvsTilemap;

        m_levelManagerRef.InitializeGrid();
    }

    public void LMMemoryTest()
    {
        m_levelManagerRef.GetNode(0, 0);
    }

    #endregion


    public void GenerateLevel()
    {
        GenerateLevelManager();
        GenerateTilemap();

        InitializeLevelManager();
    }

    public void GenerateModels()
    {
        if (ModelsContainer != null)
            DestroyImmediate(ModelsContainer);

        ModelsContainer = new GameObject(m_modelContainerName);
        ModelsContainer.transform.SetParent(PrefabContainer.transform);

        GetTilemapSize();

        for (int i = 0; i <= Rows; i++)
        {
            for (int j = 0; j <= Columns; j++)
            {
                Vector3Int pos = new Vector3Int(i, j, 0);

                #region Ground
                //GroundTile tempG = GroundTilemap.GetTile<GroundTile>(pos);

                //if(tempG != null)
                //{

                //}
                #endregion

                #region Rooms

                RoomsTile tempR = RoomsTilemap.GetTile<RoomsTile>(pos);

                if (tempR != null)
                {
                    if (tempR.RoomsTileMap == RoomsType.Destructible_Wall || tempR.RoomsTileMap == RoomsType.Window || tempR.RoomsTileMap == RoomsType.Door || tempR.RoomsTileMap == RoomsType.DoorHeavy)
                    {
                        // Changed method to instantiate an object and mantain prefab link
                        GameObject go = PrefabUtility.InstantiatePrefab(tempR.Prefab as GameObject) as GameObject;
                        go.transform.position = RoomsTilemap.GetCellCenterWorld(pos);
                        //GameObject go = Instantiate(tempR.Prefab, RoomsTilemap.GetCellCenterWorld(pos), Quaternion.identity);

                        go.transform.SetParent(m_modelsContainer.transform);
                        //go.transform.localScale = new Vector3(GridWorldXSize, go.transform.localScale.y, go.transform.localScale.z);

                        RoomsTile TD = RoomsTilemap.GetTile<RoomsTile>(new Vector3Int(i, j - 1, 0));
                        RoomsTile TU = RoomsTilemap.GetTile<RoomsTile>(new Vector3Int(i , j + 1, 0));

                        RoomsTile TL = RoomsTilemap.GetTile<RoomsTile>(new Vector3Int(i - 1, j, 0));
                        RoomsTile TR = RoomsTilemap.GetTile<RoomsTile>(new Vector3Int(i + 1, j, 0));

                        // Rotate the wall based on thers
                        if(TR == null || TL == null)
                        {
                            Debug.Log("Rotate");
                            go.transform.Rotate(Vector3.up, 90);
                        }

                        else if(TR != null && TR.RoomsTileMap == RoomsType.Floor && TL != null && TL.RoomsTileMap == RoomsType.Floor)
                        {
                            // Debug.Log("Rotate");
                            go.transform.Rotate(Vector3.up, 90);
                        }
                
                        // TODO: set wall variables like destructive
                    }
                }

                #region Envs
                //else
                //{
                EnvTile tempE = EnvsTilemap.GetTile<EnvTile>(pos);

                if( tempE != null)
                {
                    if(tempE.EnvType == EnvType.Obstacle || tempE.EnvType == EnvType.Debries)
                    {
                        // Changed method to instantiate an object and mantain prefab link
                        GameObject go = PrefabUtility.InstantiatePrefab(tempE.Prefab as GameObject) as GameObject;
                        go.transform.position = EnvsTilemap.GetCellCenterWorld(pos);
                        //GameObject go = Instantiate(tempE.Prefab, EnvsTilemap.GetCellCenterWorld(pos), Quaternion.identity);

                        go.transform.SetParent(m_modelsContainer.transform);

                    }
                }
                //}
                #endregion

                // TODO
                #endregion
            }
        }
    }

    public void DeleteModels()
    {
        if (ModelsContainer != null)
            DestroyImmediate(ModelsContainer.gameObject);
    
        else
        {
            GameObject go = GameObject.Find(m_modelContainerName);

            if (go != null)
                DestroyImmediate(go);
            else
                Debug.LogWarning("Models container not found");
        }
    }

    public void LoadLevelPrefab()
    {
        if (LevelToLoad == null)
        {
            Debug.LogWarning("Level To load empty");
            return;
        }

        PrefabContainer = LevelToLoad;

        m_levelManagerRef = LevelToLoad.GetComponentInChildren<LevelManager>();

        RoomsTilemap = m_levelManagerRef.RoomsTilemap;

        GridRef = PrefabContainer.GetComponentInChildren<Grid>();
    }

    public void CreateNewFFList()
    {
        FFTeamBase temp = new FFTeamBase();

        temp.NewFFList();
    }

#endif
}
