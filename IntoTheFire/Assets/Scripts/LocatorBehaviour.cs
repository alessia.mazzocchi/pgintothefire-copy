﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;


public enum LocatorStatus
{
    On,
    Off,
    Stop
}

public enum LocatorMode
{
    Move,
    Interactive,
    Enlight,
    Deactive,
    None,
}

public class LocatorBehaviour : MonoBehaviour
{
    public Vector2Int GridIndex;
    private bool m_isActive;
    public bool IsActive
    {
        get
        {
            return m_isActive;
        }
        set
        {
            m_isActive = value;
            m_BoxCollider.enabled = value;
            LocatorModel.SetActive(value);
            if(value == false)
            {
                TurnOffSides();
            }
        }
    }

    private LocatorStatus m_status;

    private LocatorMode prevStatus = LocatorMode.None;
    private Equip prevEquip;
    private Color prevColor;

    public LocatorMode Mode;
    public bool isPlayerOn;
    private MeshRenderer m_Renderer;
    private BoxCollider m_BoxCollider;
    public Color MoveColor;
    public Color ExtinguishColor;
    public Color OnFFColor;
    public Color DeactiveColor;
    public Texture PlayerEnlightSprite;
    public Texture InteractableTexture;
    public Texture DisableInteractTexture;

    public Texture LineTexture;
    public Texture CurveTerxture;

    private float currentLerpTime;
    [Space]
    public GameObject LocatorModel;
    public GameObject SelectorModel;
    [Header("Sides-v2")]
    public MeshRenderer TopTopLeft;
    public MeshRenderer TopTopRight;
    public MeshRenderer DownDownRight;
    public MeshRenderer DownDownLeft;
    public MeshRenderer RightTopTop;
    public MeshRenderer RightDownDown;
    public MeshRenderer LeftDownDown;
    public MeshRenderer LeftTopTop;
    [Space]
    public GameObject TopLeftCornerv2;
    public GameObject TopRightCornerv2;
    public GameObject DownLeftCornerv2;
    public GameObject DownRightCornerv2;

    private Tween scaleTween;
    [Space]
    public GameObject AlertQuad;

    #region Extinguish
    private List<Vector2Int> m_extinguishNeighboursList;
    public List<Vector2Int> ExtinguishNeighboursList
    {
        get
        {
            if(m_extinguishNeighboursList.Count == 0 && Mode != LocatorMode.Deactive)
            {
                if (InputManager.Instance.selectedEquip == null) return new List<Vector2Int>();

                Vector2 ERange = InputManager.Instance.selectedEquip.EffectRange;
                PropagationEffect Prop = InputManager.Instance.selectedEquip.PropagationEffect;

                if (Prop == PropagationEffect.Linear)
                {
                    string dir = GetExtinguishDirection(InputManager.Instance.selectedEquip.Owner.GridPos);
                    
                    FindNeighboursLinear(dir, ERange);
                }
                else if (Prop == PropagationEffect.Area)
                {
                    //Debug.Log("AREA");
                    FindhNeighboursArea(ERange);
                }
            } 

            return m_extinguishNeighboursList;
        }
    }
    #endregion

    private void Awake()
    {
        m_Renderer = LocatorModel.GetComponent<MeshRenderer>();
        m_BoxCollider = GetComponent<BoxCollider>();

        m_extinguishNeighboursList = new List<Vector2Int>();
    }

    private void Start()
    {
        m_status = LocatorStatus.Stop;

        SelectorModel.SetActive(false);
        AlertQuad.SetActive(false);
    }

    public void SetLocator(Vector2Int _gridPos)
    {
        GridIndex = _gridPos;
    }

    public void Active(LocatorMode _mode)
    {
        //Save previus mode
        if (IsActive && prevStatus == LocatorMode.None && !isPlayerOn)
        {
            prevStatus = Mode;
            prevEquip = InputManager.Instance.selectedEquip;
            prevColor = m_Renderer.material.GetColor("_BaseColor");
        }

        Mode = _mode;

        if(!isPlayerOn)
            TurnOffSides();

        m_Renderer.GetComponent<Animator>().enabled = true;
        switch (_mode)
        {
            case LocatorMode.Move:
                m_Renderer.material.SetColor("_BaseColor", MoveColor);

                break;
            case LocatorMode.Interactive:
                m_Renderer.material.SetColor("_BaseColor", ExtinguishColor);
                m_Renderer.material.SetTexture("_BaseMap", InteractableTexture);

                break;
            case LocatorMode.Enlight:
                m_Renderer.material.SetColor("_BaseColor", ExtinguishColor);
                m_Renderer.material.SetTexture("_BaseMap", InteractableTexture);

                break;
            case LocatorMode.Deactive:
                m_Renderer.material.SetColor("_BaseColor", DeactiveColor);
                m_Renderer.material.SetTexture("_BaseMap", InteractableTexture);
                m_Renderer.GetComponent<Animator>().enabled = false;

                break;
            default:
                break;
        }


        IsActive = true;
    }

    public void SetWallLocator()
    {
        IsActive = true;
        m_BoxCollider.enabled = false;
        LocatorModel.SetActive(false);

        TurnOffSides();
    }

    public void Disactive()
    {
        if (prevStatus != LocatorMode.None && prevEquip == InputManager.Instance.selectedEquip && !isPlayerOn)
        {
            if (!(LevelManager.instance.GetNode(GridIndex).Obstacle is WindowObs))
                m_extinguishNeighboursList?.Clear();
            
            m_Renderer.material.SetColor("_BaseColor", prevColor);

            prevEquip = null;
            LocatorMode oldMode = prevStatus;

            Active(oldMode);
            prevStatus = LocatorMode.None;
        }
        else if (isPlayerOn)
        {
            Debug.Log("Here asr");
            SetPlayerColor(true);
        }
        else
        {
            if (!(LevelManager.instance.GetNode(GridIndex).Obstacle is WindowObs))
                m_extinguishNeighboursList?.Clear();

            IsActive = false;
            TurnOffSides();
        }
    }


    public void LocatorSideActivation()
    {
        /// Side true = side active
        bool up = false;
        bool down = false;
        bool left = false;
        bool right = false;

        // Left
        Vector2Int pos = new Vector2Int(GridIndex.x - 1, GridIndex.y);
        if (LevelManager.instance.IsInsideOfBounds(pos))
        {
            if (LevelManager.instance.GetNode(pos).Locator.IsActive)
            {
                LeftDownDown.gameObject.SetActive(false);
                LeftTopTop.gameObject.SetActive(false);
            }
            else
            {
                left = true;

                ChangeSideTexture(LeftDownDown, LineTexture);
                ChangeSideTexture(LeftTopTop, LineTexture);

                LeftDownDown.gameObject.SetActive(true);
                LeftTopTop.gameObject.SetActive(true);
            }
        }
        else
        {
            left = true;

            ChangeSideTexture(LeftDownDown, LineTexture);
            ChangeSideTexture(LeftTopTop, LineTexture);

            LeftDownDown.gameObject.SetActive(true);
            LeftTopTop.gameObject.SetActive(true);
        }

        //Right
        pos = new Vector2Int(GridIndex.x + 1, GridIndex.y);
        if (LevelManager.instance.IsInsideOfBounds(pos))
        {
            if (LevelManager.instance.GetNode(pos).Locator.IsActive)
            {
                RightTopTop.gameObject.SetActive(false);
                RightDownDown.gameObject.SetActive(false);
            }
            else
            {
                right = true;

                ChangeSideTexture(RightTopTop, LineTexture);
                ChangeSideTexture(RightDownDown, LineTexture);

                RightTopTop.gameObject.SetActive(true);
                RightDownDown.gameObject.SetActive(true);
            }
        }
        else
        {
            right = true;

            ChangeSideTexture(RightTopTop, LineTexture);
            ChangeSideTexture(RightDownDown, LineTexture);

            RightTopTop.gameObject.SetActive(true);
            RightDownDown.gameObject.SetActive(true);
        }

        //Up
        pos = new Vector2Int(GridIndex.x, GridIndex.y + 1);
        if (LevelManager.instance.IsInsideOfBounds(pos))
        {
            if (LevelManager.instance.GetNode(pos).Locator.IsActive)
            {
                TopTopLeft.gameObject.SetActive(false);
                TopTopRight.gameObject.SetActive(false);
            }
            else
            {
                up = true;
                ChangeSideTexture(TopTopLeft, LineTexture);
                ChangeSideTexture(TopTopRight, LineTexture);

                TopTopLeft.gameObject.SetActive(true);
                TopTopRight.gameObject.SetActive(true);
            }
        }
        else
        {
            up = true;
            ChangeSideTexture(TopTopLeft, LineTexture);
            ChangeSideTexture(TopTopRight, LineTexture);

            TopTopLeft.gameObject.SetActive(true);
            TopTopRight.gameObject.SetActive(true);
        }

        //Down
        pos = new Vector2Int(GridIndex.x, GridIndex.y - 1);
        if (LevelManager.instance.IsInsideOfBounds(pos))
        {
            if (LevelManager.instance.GetNode(pos).Locator.IsActive)
            {
                DownDownLeft.gameObject.SetActive(false);
                DownDownRight.gameObject.SetActive(false);
            }
            else
            {
                down = true;
                ChangeSideTexture(DownDownLeft, LineTexture);
                ChangeSideTexture(DownDownRight, LineTexture);

                DownDownLeft.gameObject.SetActive(true);
                DownDownRight.gameObject.SetActive(true);
            }
        }
        else
        {
            down = true;
            ChangeSideTexture(DownDownLeft, LineTexture);
            ChangeSideTexture(DownDownRight, LineTexture);

            DownDownLeft.gameObject.SetActive(true);
            DownDownRight.gameObject.SetActive(true);
        }

        // Top Right Corner
        if (up && right)
        {
            pos = new Vector2Int(GridIndex.x + 1, GridIndex.y + 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                //if (!LevelManager.instance.GetNode(pos).Locator.IsActive)
                //{
                ChangeSideTexture(RightTopTop, CurveTerxture);
                RightTopTop.gameObject.SetActive(true);
                TopTopRight.gameObject.SetActive(false);
                //}
            }
        }

        // Top Right Corner - INVERSE
        if (!up && !right)
        {
            pos = new Vector2Int(GridIndex.x + 1, GridIndex.y + 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (!LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    TopTopRight.gameObject.SetActive(false);
                    RightTopTop.gameObject.SetActive(false);
                    TopRightCornerv2.SetActive(true);
                }
            }
        }

        // Top Right Corner - CLEAR LINE
        if (up && !right)
        {
            pos = new Vector2Int(GridIndex.x + 1, GridIndex.y + 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    TopTopRight.gameObject.SetActive(false);
                }
            }
        }

        // Top Right Corner - CLEAR LINE
        if (!up && right)
        {
            pos = new Vector2Int(GridIndex.x + 1, GridIndex.y + 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    RightTopTop.gameObject.SetActive(false);
                }
            }
        }


        // Top Left Corner
        if (up && left)
        {
            pos = new Vector2Int(GridIndex.x - 1, GridIndex.y + 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                //if (!LevelManager.instance.GetNode(pos).Locator.IsActive)
                //{
                ChangeSideTexture(TopTopLeft, CurveTerxture);
                TopTopLeft.gameObject.SetActive(true);
                LeftTopTop.gameObject.SetActive(false);
                //}
            }
        }

        // Top Left Corner - INVERSE
        if (!up && !left)
        {
            pos = new Vector2Int(GridIndex.x - 1, GridIndex.y + 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (!LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    LeftTopTop.gameObject.SetActive(false);
                    TopTopLeft.gameObject.SetActive(false);
                    TopLeftCornerv2.SetActive(true);
                }
            }
        }

        // Top Left Corner - CLEAR LINE
        if (up && !left)
        {
            pos = new Vector2Int(GridIndex.x - 1, GridIndex.y + 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    TopTopLeft.gameObject.SetActive(false);
                }
            }
        }

        // Top Left Corner - CLEAR LINE
        if (!up && left)
        {
            pos = new Vector2Int(GridIndex.x - 1, GridIndex.y + 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    LeftTopTop.gameObject.SetActive(false);
                }
            }
        }


        // Down Right Corner
        if (down && right)
        {
            pos = new Vector2Int(GridIndex.x + 1, GridIndex.y - 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                //if (!LevelManager.instance.GetNode(pos).Locator.IsActive)
                //{
                ChangeSideTexture(DownDownRight, CurveTerxture);
                DownDownRight.gameObject.SetActive(true);
                RightDownDown.gameObject.SetActive(false);
                //}
            }
        }

        // Down Right Corner - INVERSE
        if (!down && !right)
        {
            pos = new Vector2Int(GridIndex.x + 1, GridIndex.y - 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (!LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    RightDownDown.gameObject.SetActive(false);
                    DownDownRight.gameObject.SetActive(false);
                    DownRightCornerv2.SetActive(true);
                }
            }
        }

        // Down Right Corner - CLEAR LINE
        if (down && !right)
        {
            pos = new Vector2Int(GridIndex.x + 1, GridIndex.y - 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    DownDownRight.gameObject.SetActive(false);
                }
            }
        }

        // Down Right Corner - CLEAR LINE
        if (!down && right)
        {
            pos = new Vector2Int(GridIndex.x + 1, GridIndex.y - 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    RightDownDown.gameObject.SetActive(false);
                }
            }
        }

        // Down Left Corner
        if (down && left)
        {
            pos = new Vector2Int(GridIndex.x - 1, GridIndex.y - 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                //if (!LevelManager.instance.GetNode(pos).Locator.IsActive)
                //{
                ChangeSideTexture(LeftDownDown, CurveTerxture);
                LeftDownDown.gameObject.SetActive(true);
                DownDownLeft.gameObject.SetActive(false);
                //}
            }
        }

        // Down Left Corner - INVERSE
        if (!down && !left)
        {
            pos = new Vector2Int(GridIndex.x - 1, GridIndex.y - 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (!LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    DownDownLeft.gameObject.SetActive(false);
                    LeftDownDown.gameObject.SetActive(false);
                    DownLeftCornerv2.SetActive(true);
                }
            }
        }

        // Down Left Corner - CLEAR LINE
        if (down && !left)
        {
            pos = new Vector2Int(GridIndex.x - 1, GridIndex.y - 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    DownDownLeft.gameObject.SetActive(false);
                }
            }
        }

        // Down Left Corner - CLEAR LINE
        if (!down && left)
        {
            pos = new Vector2Int(GridIndex.x - 1, GridIndex.y - 1);
            if (LevelManager.instance.IsInsideOfBounds(pos))
            {
                if (LevelManager.instance.GetNode(pos).Locator.IsActive)
                {
                    LeftDownDown.gameObject.SetActive(false);
                }
            }
        }
    }

    public void SetPlayerColor(bool _active)
    {
        if (_active)
        {
            Mode = LocatorMode.Enlight;
            m_Renderer.material.SetColor("_BaseColor", OnFFColor);
            m_Renderer.material.SetTexture("_BaseMap", PlayerEnlightSprite);
        }
        else
        {
            m_Renderer.material.SetTexture("_BaseMap", null);
        }

        isPlayerOn = _active;
        IsActive = _active;
    }

    private void ChangeSideTexture(MeshRenderer _mesh, Texture _texture)
    {
        _mesh.material.SetTexture("_BaseMap", _texture);
    }

    public void TurnOffSides()
    {
        TopTopLeft.gameObject.SetActive(false);
        TopTopRight.gameObject.SetActive(false);

        DownDownLeft.gameObject.SetActive(false);
        DownDownRight.gameObject.SetActive(false);

        LeftDownDown.gameObject.SetActive(false);
        LeftTopTop.gameObject.SetActive(false);

        RightDownDown.gameObject.SetActive(false);
        RightTopTop.gameObject.SetActive(false);

        TopRightCornerv2.SetActive(false);
        TopLeftCornerv2.SetActive(false);
        DownLeftCornerv2.SetActive(false);
        DownRightCornerv2.SetActive(false);

        SelectorModel.SetActive(false);
    }

    public void ActiveAlert(bool _isActive)
    {
        AlertQuad.SetActive(_isActive);
    }

    public void Move()
    {
        LevelManager.instance.MovePlayer(GridIndex);
    }

    public void EnlightExtinguishNeighbors(bool _active)
    {
        LevelManager lm = LevelManager.instance;
        List<Vector2Int> temp = new List<Vector2Int>(ExtinguishNeighboursList);
        temp.Remove(GridIndex);

        if (_active)
        {
            lm.ActiveNodes(temp, LocatorMode.Enlight, false);
        }
        else
        {
            lm.DeactiveNodes(temp, LocatorMode.Enlight);
        }
    }

    private string GetExtinguishDirection(Vector2Int _EquipPos)
    {
        //Debug.Log("Player: " + _EquipPos + " | " + "Grid: " + GridIndex);
        if (_EquipPos.x > GridIndex.x) return "Left";
        if (_EquipPos.x < GridIndex.x) return "Right";

        if (_EquipPos.y > GridIndex.y) return "Down";
        if (_EquipPos.y < GridIndex.y) return "Up";

        return "ERROR";
    }

    /// <summary>
    /// DEV: DON'T TOUCH EVER
    /// </summary>
    /// <param name="_dir"></param>
    /// <param name="_range"></param>
    private void FindNeighboursLinear(string _dir, Vector2 _range)
    {
        int X_start = 0;
        int X_end = 0;

        int Y_start = 0;
        int Y_end = 0;

        switch (_dir)
        {
            case "Up":
                X_start = GridIndex.x - Mathf.FloorToInt(_range.x / 2);
                X_end = GridIndex.x + Mathf.FloorToInt(_range.x / 2);

                Y_start = GridIndex.y;
                Y_end = GridIndex.y + Mathf.RoundToInt(_range.y);

                for (int i = X_start; i <= X_end; i++)
                {
                    for (int j = Y_start; j < Y_end; j++)
                    {
                        //Debug.Log(LevelManager.instance.GetNode(i, j).RoomsType + "Node: " + i + "-" + j);
                        if (LevelManager.instance.IsInsideOfBounds(i, j) && LevelManager.instance.GetNode(i, j).RoomsType == RoomsType.Floor)
                        {
                            NodeExtistingCheck(i, j);
                        }
                        else
                        {
                            //Debug.Log("Stop link UP");
                            break;
                        }
                    }

                    //if (stop) break;
                }

                break;

            case "Down":
                X_start = GridIndex.x - Mathf.FloorToInt(_range.x / 2);
                X_end = GridIndex.x + Mathf.FloorToInt(_range.x / 2);

                Y_start = GridIndex.y;
                Y_end = GridIndex.y - Mathf.RoundToInt(_range.y);

                for (int i = X_start; i <= X_end; i++)
                {
                    for (int j = Y_start; j > Y_end; j--)
                    {
                        if (LevelManager.instance.IsInsideOfBounds(i, j) && LevelManager.instance.GetNode(i, j).RoomsType == RoomsType.Floor)
                            NodeExtistingCheck(i, j);
                        else
                        {
                            //Debug.Log("Stop link");

                            break;
                        }
                    }

                    //if (stop) break;
                }
                break;

            case "Left":
                X_start = GridIndex.y + Mathf.FloorToInt(_range.x / 2);
                X_end = GridIndex.y - Mathf.FloorToInt(_range.x / 2);

                Y_start = GridIndex.x;
                Y_end = GridIndex.x - Mathf.RoundToInt(_range.y);

                //Debug.Log("X Area: " + X_start + "-" + X_end + " = " + Mathf.Abs(X_start - X_end));
                //Debug.Log("Y Area: " + Y_start + "-" + Y_end + " = " + Mathf.Abs(Y_start - Y_end));


                for (int i = X_start; i >= X_end; i--)
                {
                    for (int j = Y_start; j > Y_end; j--)
                    {
                        if (LevelManager.instance.IsInsideOfBounds(j, i) && LevelManager.instance.GetNode(j, i).RoomsType == RoomsType.Floor)
                            NodeExtistingCheck(j, i);
                        else
                            break;
                    }
                    
                }

                //for (int i = X_start; i > X_end; i--)
                //{
                //    for (int j = Y_start; j >= Y_end; j--)
                //    {
                //        if (LevelManager.instance.IsInsideOfBounds(i, j) && LevelManager.instance.GetNode(i, j).RoomsType == RoomsType.Floor)
                //            NodeExtistingCheck(i, j);
                //    }
                //}
                break;

            case "Right":
                X_start = GridIndex.y + Mathf.FloorToInt(_range.x / 2);
                X_end = GridIndex.y - Mathf.FloorToInt(_range.x / 2);

                Y_start = GridIndex.x;
                Y_end = GridIndex.x + Mathf.RoundToInt(_range.y);

                //Debug.Log("X Area: " + X_start + "-" + X_end + " = " + Mathf.Abs(X_start - X_end));
                //Debug.Log("Y Area: " + Y_start + "-" + Y_end + " = " + Mathf.Abs(Y_start - Y_end));

                for (int i = X_start; i >= X_end; i--)
                {
                    for (int j = Y_start; j < Y_end; j++)
                    {
                        if (LevelManager.instance.IsInsideOfBounds(j, i) && LevelManager.instance.GetNode(j, i).RoomsType == RoomsType.Floor)
                            NodeExtistingCheck(j, i);
                        else
                            break;
                    }
                }
                break;
        }
    }

    public void FindhNeighboursArea(Vector2 _range)
    {
        int X_start = GridIndex.x -  Mathf.FloorToInt(_range.x / 2);
        int X_end = GridIndex.x + Mathf.FloorToInt(_range.x / 2);

        int Y_start = GridIndex.y - Mathf.FloorToInt(_range.y / 2);
        int Y_end = GridIndex.y + Mathf.FloorToInt(_range.y / 2); ;

        for (int i = X_start; i <= X_end; i++)
        {
            for (int j = Y_start; j <= Y_end; j++)
            {
                NodeExtistingCheck(i, j);
            }
        }
    }

    private void NodeExtistingCheck(int _x, int _y)
    {
        if (LevelManager.instance.IsInsideOfBounds(_x, _y))
        {
            if (LevelManager.instance.GetNode(_x, _y).CanBeEnlighted())
            {
                m_extinguishNeighboursList.Add(new Vector2Int(_x, _y));
            }
        }
    }

    public void ExtinguishFire()
    {
        Debug.Log("equip " + InputManager.Instance.selectedEquip + " value " + (int)InputManager.Instance.selectedEquip.Value);

        // Calculate the direction and remove flame in that zone;
        LevelManager.instance.KillFire(m_extinguishNeighboursList, (int)InputManager.Instance.selectedEquip.Value);
    }

    #region Mouse interaction

    public void OnClick()
    {
        if (Mode == LocatorMode.Move)
        {
            Move();
        }
        else if (Mode == LocatorMode.Interactive)
        {
            Debug.Log("Node count: " + ExtinguishNeighboursList.Count);
            InputManager.Instance.selectedEquip.Execute(ExtinguishNeighboursList);
        }
    }

    public void OnClick(Equip _equip, out bool _success)
    {
        _success = true;
        if (_equip == null || Mode == LocatorMode.Deactive || Mode == LocatorMode.Enlight)
        {
            _success = false;
            return;
        }

        if (_equip as EEextinguisher)
        {
            LevelManager.instance.GetSelectedFF.SetEquipAnimation(_equip, transform,() => InputManager.Instance.selectedEquip.Execute(ExtinguishNeighboursList));
            //InputManager.Instance.selectedEquip.Execute(ExtinguishNeighboursList);
        }
        else if (_equip as EPickUp) // Exeption with Pickup or it will confuse with the damage items
        {
            // Drop
            if (InputManager.Instance.SelectedPickup().hasVictim)
            {
                LevelManager.instance.GetSelectedFF.SetEquipAnimation(_equip, transform, () =>
                {
                    (InputManager.Instance.selectedEquip as EPickUp).Execute(GridIndex);
                    LevelManager.instance.ShowFFRange(true);
                });
            }

            // Pickup
            else
            {
                if (LevelManager.instance.GetNode(GridIndex).HasVictim)
                {
                    // Use the action point
                    LevelManager.instance.GetSelectedFF.SetEquipAnimation(_equip, transform, () =>
                    {
                        InputManager.Instance.selectedEquip.Execute();
                        LevelManager.instance.GetNode(GridIndex).Victim.GetActiveActions(_equip);
                    });
                   
                }
                else if (LevelManager.instance.GetNode(GridIndex).HasExhaustedFF)
                {
                    LevelManager.instance.GetSelectedFF.SetEquipAnimation(_equip, transform, () =>
                    {
                        InputManager.Instance.selectedEquip.Execute();
                        LevelManager.instance.GetNode(GridIndex).FireFighter.GetActiveActions(_equip);
                    });

                }
                else if (LevelManager.instance.GetNode(GridIndex).HasTeddyBear)
                {
                    LevelManager.instance.GetSelectedFF.SetEquipAnimation(_equip, transform, () =>
                    {
                        InputManager.Instance.selectedEquip.Execute();
                        (LevelManager.instance.GetNode(GridIndex).Obstacle as TeddyBear).GetActiveActions(InputManager.Instance.selectedEquip);
                    });
                }
            }
        }
        else if (_equip as EMedikit || _equip as EWaterShield)
        {
            LevelManager.instance.GetSelectedFF.SetEquipAnimation(_equip, transform, () => InputManager.Instance.selectedEquip.Execute(LevelManager.instance.GetNode(GridIndex).FireFighter));
        }
        else
        {
            // Do not damage victims
            LevelManager.instance.GetSelectedFF.SetEquipAnimation(_equip, LevelManager.instance.GetNode(GridIndex).Obstacle.transform,() => InputManager.Instance.selectedEquip.Execute(LevelManager.instance.GetNode(GridIndex).Obstacle));
        }
    }

    public void SetEnlightForWindows(List<Vector2Int> nodes)
    {
        m_extinguishNeighboursList = new List<Vector2Int>(nodes);
    }

    public void MouseEnter()
    {
        switch (Mode)
        {
            case LocatorMode.Move:
                m_status = LocatorStatus.On;
                currentLerpTime = 0;
                SelectorModel.SetActive(true);

                if (LevelManager.instance.GameStatus == GameStatus.Play && !LevelManager.instance.FfIsMoving && LevelManager.instance.CurrentPhase == TurnPhase.Player)
                    InputManager.Instance.SetPathOnMovement(GridIndex);

                CustomCursor.SetCursor(CursorStyle.Hand);
                break;

            case LocatorMode.Interactive:
                EnlightExtinguishNeighbors(true);
                SelectorModel.SetActive(true);
                CustomCursor.SetCursor(CursorStyle.Hand);

                break;

            case LocatorMode.Enlight:
                break;

            case LocatorMode.Deactive:
                CustomCursor.SetCursor(CursorStyle.Block);
                break;

            default:
                break;
        }

    }


    public void MouseExit()
    {
        switch (Mode)
        {
            case LocatorMode.Move:
                m_status = LocatorStatus.Off;
                currentLerpTime = 0;

                if (LevelManager.instance.GameStatus == GameStatus.Play && !LevelManager.instance.FfIsMoving && LevelManager.instance.CurrentPhase == TurnPhase.Player)
                    LevelManager.instance.GetSelectedFF.ResetPreview();
                break;

            case LocatorMode.Interactive:
                EnlightExtinguishNeighbors(false);
                break;

            case LocatorMode.Enlight:

                break;

            default:
                break;
        }
        
        SelectorModel.SetActive(false);
        CustomCursor.SetCursorDefault();
    }

    #endregion
}
