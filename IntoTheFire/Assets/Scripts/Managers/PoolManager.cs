﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PoolType
{
    Flame,
    Locator,
    FF,
    Trail,
    Victim,
    Foam,
    DamageFeedback,
    Smoke,
    Water,
    Fuel,
    ExitFeeback,
    TeddyBear,
    BreakParticle
}

[System.Serializable]
public struct ObjectToPool
{
    public PoolType PoolType;
    public GameObject ObjectPrefab;
    public int amountToPool;
    public bool CanSpawnNew;
}

public class Poolable
{
    public PoolType poolType { get; set; }
    public GameObject GameObject { get; set; }

    public Poolable(PoolType type, GameObject obj)
    {
        poolType = type;
        GameObject = obj;
    }
}

public class PoolManager : MonoBehaviour
{
    public static PoolManager instance;

    public ObjectToPool[] PoolSetting;
    public List<Poolable> Pool;

    public Dictionary<PoolType, ObjectToPool> PoolInfo;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    void Start()
    {
        Pool = new List<Poolable>();
        PoolInfo = new Dictionary<PoolType, ObjectToPool>();
        GameObject PoolContainer = new GameObject("PoolContainer");

        foreach (ObjectToPool item in PoolSetting)
        {
            PoolInfo.Add(item.PoolType, item);

            for (int i = 0; i < item.amountToPool; i++)
            {
                CreateNewItem(item.PoolType, PoolContainer.transform);
            }
        }
    }

    public GameObject GetItem(PoolType type)
    {
        // Return if there is no item in the pool
        if (!PoolInfo.ContainsKey(type))
        {
            Debug.LogError("Item type not found in the pool: " + type);
            return null;
        }

        // Get the firt free object
        for (int i = 0; i < Pool.Count; i++)
        {
            if (Pool[i].poolType == type)
                if (!Pool[i].GameObject.activeInHierarchy)
                    return Pool[i].GameObject;
        }

        // Instantiate new object if possible
        if (PoolInfo[type].CanSpawnNew)
        {
            Debug.LogWarning("Spawned a new Item " + type);
            return CreateNewItem(type);
        }
        else
        {
            Debug.LogError("Denied the spawn of a new Item " + type);
            return null;
        }
    }

    public GameObject CreateNewItem(PoolType _type, Transform _parent = null)
    {
        GameObject obj = Instantiate(PoolInfo[_type].ObjectPrefab, Vector3.zero, Quaternion.identity, _parent);
        obj.SetActive(false);
        Poolable temp = new Poolable(PoolInfo[_type].PoolType, obj);
        Pool.Add(temp);

        return obj;
    }

    //public void InitializePool(PoolType _type)
    //{
    //    GameObject obj = Instantiate(PoolInfo[_type].ObjectPrefab, Vector3.zero, Quaternion.identity);
    //    obj.SetActive(false);
    //    Poolable temp = new Poolable(PoolInfo[_type].PoolType, obj);
    //    Pool.Add(temp);
    //}
}
