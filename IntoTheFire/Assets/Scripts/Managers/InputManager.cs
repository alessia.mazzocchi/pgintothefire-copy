﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using System.Runtime.InteropServices;

public class InputManager : Singleton<InputManager>
{
    private LocatorBehaviour m_Locator;
    public LocatorBehaviour Locator
    {
        get
        {
            if (m_Locator == null)
            {
                GameObject go = PoolManager.instance.CreateNewItem(PoolType.Locator);
                m_Locator = go.GetComponent<LocatorBehaviour>();
            }

            return m_Locator;
        }
    }

    private Camera m_Cam;

    private Camera Cam
    {
        get
        {
            if (m_Cam == null)
                m_Cam = Camera.main;
            return m_Cam;
        }
    }

    public LayerMask Defaultlayer;
    public LayerMask LocatorLayer;

    public Equip selectedEquip;
    private LocatorBehaviour lastLocator;
    private LineRenderer line;

    private ITooltip lastTooltip;

    void Start()
    {
        //Cam = Camera.main;
        Defaultlayer = LayerMask.GetMask("Default");
        LocatorLayer = LayerMask.GetMask("Locator");
        line = PoolManager.instance.GetItem(PoolType.Trail).GetComponent<LineRenderer>();
        line.positionCount = 0;
        line.gameObject.SetActive(true);
        //DontDestroyOnLoad(this);
    }

    void Update()
    {
        if(LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            if (LevelManager.instance.ActiveTooltip)
            {
                TooltipRay();
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (EventSystem.current.IsPointerOverGameObject()) return;

                if (selectedEquip == null)
                {
                    // Look for player
                    RaycastHit hit;

                    // Look for locator
                    if (Physics.Raycast(Cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, LocatorLayer))
                    {
                        LocatorBehaviour obj = hit.collider.gameObject.GetComponent<LocatorBehaviour>();

                        if (obj != null)
                        {
                            obj.OnClick();
                            return;
                        }
                    }

                }
                else
                {
                    UseEquip();
                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                if (selectedEquip != null)
                {
                    EquipDeselected();
                }
            }

            if (LevelManager.instance.GetSelectedFF != null)
                LocatorRay();

            EquipmentShortcuts();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                LevelManager.instance.endButton.OnClick();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                if (LevelManager.instance.GameStatus == GameStatus.Pause) return;

                if (LevelManager.instance.CurrentPhase == TurnPhase.Player)
                {
                    LevelManager.instance.UndoMove();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            LevelManager.instance.PauseMenu();
        }
    }

    private void LateUpdate()
    {
        if (LevelManager.instance.ActiveTooltip && LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            TooltipRay();
        }
    }

    //TODO REWORK
    public void Interact()
    {
        RaycastHit hit;
        if(Physics.Raycast(Cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, Defaultlayer))
        {
            IInteractable obj = hit.collider.gameObject.GetComponent<IInteractable>();

            if (obj != null && obj.CanAct)
            {
                // Show UI information or pickup the victim!!!
            }
        }
    }

    public void UseEquip()
    {
        RaycastHit hit;

        if (Physics.Raycast(Cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, LocatorLayer))
        {
            LocatorBehaviour obj = hit.collider.gameObject.GetComponent<LocatorBehaviour>();

            if (obj != null)
            {
                obj.OnClick(selectedEquip, out bool success);

                UIManager.Instance.UpdateSelectedEquipButton();

                if (success)
                    EquipActivated();
                else
                    EquipDeselected();
            }
        }
    }

    public void EquipSelected(Equip _equip)
    {
        if (selectedEquip == _equip)
        {
            EquipDeselected();
            UIManager.Instance.UpdateSelectedEquipButton();
            return;
        }

        if (selectedEquip != null)
            EquipDeselected(true);

        selectedEquip = _equip;

        if(_equip.EType == EquipType.Extinguish)
            LevelManager.instance.ActiveNodes(_equip.GetDirectionForExtinguisher, _equip.ELocatorMode);
        else
        {
            LevelManager.instance.ActiveNodes(_equip.GetDirectionNodes, _equip.ELocatorMode);
        }

    }

    public void EquipDeselected(bool _OtherEquipSelected = false)
    {
        if(selectedEquip != null)
            LevelManager.instance.DeactiveNodes(selectedEquip.ELocatorMode);

        UIManager.Instance.EquipDeselected();
        lastLocator = null;
        selectedEquip = null;

        if(!_OtherEquipSelected)
            LevelManager.instance.ShowFFRange(true);
    }

    public void EquipActivated()
    {
        if (selectedEquip != null)
            LevelManager.instance.DeactiveNodes(selectedEquip.ELocatorMode);

        UIManager.Instance.EquipDeselected();
        lastLocator = null;
        LevelManager.instance.ShowFFRange(false);
    }

    public void EquipmentShortcuts()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
           UIManager.Instance.EquipHandler.ActiveFFEquipAtKeycode(1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            UIManager.Instance.EquipHandler.ActiveFFEquipAtKeycode(2);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            UIManager.Instance.EquipHandler.ActiveFFEquipAtKeycode(3);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            UIManager.Instance.EquipHandler.ActiveFFEquipAtKeycode(4);
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            UIManager.Instance.EquipHandler.ActiveFFEquipAtKeycode(5);
        }
    }

    public bool IsSelectedEquipPickup()
    {
        Debug.Log("Pickup selected: " + (selectedEquip as EPickUp));
        return (selectedEquip as EPickUp);
    }

    public EPickUp SelectedPickup()
    {
        return selectedEquip as EPickUp;
    }

    public void TooltipRay()
    {
        //Debug.DrawLine(Camera.main.ScreenPointToRay(Input.mousePosition).origin, Camera.main.ScreenPointToRay(Input.mousePosition).direction, Color.red, 9999f);

        RaycastHit hit;
        if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, Defaultlayer))
        {
            //Debug.DrawLine(Camera.main.ScreenPointToRay(Input.mousePosition).origin, hit.point, Color.red, 9999f);

            if (hit.collider != null)
            {
                //Debug.Log("Collider hit: " + hit.collider.gameObject.name);

                if (lastTooltip != null && hit.collider.gameObject == lastTooltip.go) // Stop execution if overing on the same object
                {
                    //Debug.Log("Return");
                    return;
                }
                else
                {
                    //Debug.Log("Same object");
                    lastTooltip?.ShowTip(false); 
                }

                ITooltip obs = hit.collider.gameObject.GetComponent<ITooltip>();
            
                if(obs != null)
                {
                    obs.ShowTip(true);
                    //Debug.Log("Change");
                    lastTooltip = obs;
                }
            }
            else
            {
                //Debug.Log("No collider found");
                lastTooltip?.ShowTip(false);
                lastTooltip = null;
            }
        }
        else
        {
            //Debug.Log("Raycast found nothing");
            lastTooltip?.ShowTip(false);
            lastTooltip = null;
        }
    }

    private void LocatorRay()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, LocatorLayer))
        { 
            if(hit.collider != null)
            {
                LocatorBehaviour tmpLoc = hit.collider.GetComponent<LocatorBehaviour>();

                if (lastLocator != null && lastLocator == tmpLoc) return;

                lastLocator?.MouseExit();
                line.positionCount = 0;

                lastLocator = tmpLoc;

                lastLocator.MouseEnter();
            }
        }
        else
        {
            if(lastLocator != null)
            {
                lastLocator?.MouseExit();
                line.positionCount = 0;
                lastLocator = null;
            } 
        }
    }

    public void SetPathOnMovement(Vector2Int _gridPos)
    {
        if (LevelManager.instance.GetSelectedFF != null && LevelManager.instance.GetSelectedFF.GridPos != Vector2.zero && InputManager.Instance.selectedEquip == null && LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            Pathfinding p = new Pathfinding();
            List<Node> path = p.RetracePath(LevelManager.instance.GetSelectedFF.GridPos, _gridPos);

            line.positionCount = path.Count + 1;
            line.SetPosition(0, LevelManager.instance.GetNode(LevelManager.instance.GetSelectedFF.GridPos).WorldPosition);
            Vector3 pos = new Vector3();
            for (int i = 0; i < path.Count; i++)
            {
                pos = new Vector3(path[i].WorldPosition.x, path[i].WorldPosition.y + 0.1f, path[i].WorldPosition.z);
                line.SetPosition(i + 1, pos);
            }

            //line.material.SetTextureScale("_BaseMap", new Vector2(line.positionCount, 1.0f));

            if(path.Count != 0)
            {
                LevelManager.instance.GetSelectedFF.PathPreview(path);

                //AudioData audio = new AudioData();
                //audio.SetUISound(UISounds.CellScrolling);
                //EventManager.TriggerEvent(EventsID.PlayAudio, audio);

            }
        }
    }

    public void ReleaseTooltip()
    {
        lastTooltip?.ShowTip(false);
        lastTooltip = null;
    }

    public void ClearLastRayHit()
    {
        if (lastLocator != null)
        {
            lastLocator?.MouseExit();
            line.positionCount = 0;

            lastLocator = null;
        }
    }
}
