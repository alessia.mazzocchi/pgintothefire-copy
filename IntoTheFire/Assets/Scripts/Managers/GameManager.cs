﻿using System;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    [HideInInspector]
    public bool NotFirstLoad;
    public bool ShowTutorial = true;

    private PHDatabase m_DB;
    public  PHDatabase DB
    {
        get
        {
            if(m_DB == null)
            {
                if (DBAsset == null)
                    DBAsset = Resources.Load<PHDatabase>("DB");

                m_DB = Instantiate(DBAsset);
            }
            
            return m_DB;
        }

        private set
        {
            m_DB = value;
        }
    }

    public PHDatabase DBAsset;

    public SkillTreeTemplate GetSkillTreeTemplate(FFName _name) => DB.SkillTreeDB[_name];   

    private List<FFData> FFTeamData;
    /// <summary>
    /// Get the list of the loaded firefighters
    /// </summary>
    public List<FFData> GetFFTeamData { get => FFTeamData; }

    [Header("Scenes")]
    [Scene]
    public string MainMenuScene;

    [Header("Multiplier")]
    public float HealthyMultiplier = 0.5f;
    public float TiredMultiplier = 0.25f; 

    [Header("Flames settings")]
    public FlameStats PilotStat;
    [Space]
    public FlameStats FlameStat;
    [Space]
    public FlameStats BlazeStat;
    public Dictionary<FlameLevel, FlameStats> AllFlameStats = new Dictionary<FlameLevel, FlameStats>();

    [HideInInspector]
    public int Medals;
    [HideInInspector]
    public int Stars;

    #region Save Values
    [HideInInspector]
    private FFName[] m_FFSelectedForMission;
    public FFName[] FFSelectedForMission
    {
        get => m_FFSelectedForMission;
    }

    private FFEquipSet[] m_SelectedEquipSets;
    public FFEquipSet[] SelectedEquipSets
    {
        get
        {
            if(m_SelectedEquipSets == null)
            {
                m_SelectedEquipSets = new FFEquipSet[FFSelectedForMission.Length];

                for (int i = 0; i < m_SelectedEquipSets.Length; i++)
                {
                    m_SelectedEquipSets[i].EquipOne = EquipName.None;
                    m_SelectedEquipSets[i].EquipTwo= EquipName.None;
                }
            }

            return m_SelectedEquipSets;
        }
    }
    #region Progression

    private Vector2Int m_Progression;
    public Vector2Int Progression
    {
        get => m_Progression;

        private set => m_Progression = value;
    }

    [HideInInspector]
    public List<Vector2Int> ProgressionLimit;
    [HideInInspector]
    public readonly Vector2Int NoLevelToUnlock = new Vector2Int(-1, -1);
    
    private Vector2Int m_LevelToUnlock;
    public Vector2Int LevelToUnlock
    {
        get => m_LevelToUnlock;
    }

    #endregion

    private Dictionary<Vector2Int, LevelSaveData> m_LevelSaveData;
    public Dictionary<Vector2Int, LevelSaveData> LevelSaveData
    {
        get
        {
            if (m_LevelSaveData == null) 
                m_LevelSaveData = new Dictionary<Vector2Int, LevelSaveData>();
            
            return m_LevelSaveData; 
        }

        private set => m_LevelSaveData = value; 
    }

    [HideInInspector]
    public LevelData LevelLoaded;

    [HideInInspector]
    public Dictionary<HubState, bool> TutorialExecution;
    #endregion

    [Header("Dev mode")]
    public bool UnlimitedActions;
    public bool UnlimitedUse;
    public bool NoMovementCost;
    public bool NoSmokeOnFlame;
    public bool ActionStopMovement = false;
    public bool SkipCameraOverview;
    public bool ResetSaveOnPlay;

    private LevelManager m_currentLevel;
    public LevelManager CurrentLevel
    {
        get
        {
            if (m_currentLevel == null)
                m_currentLevel = LevelManager.instance;

            return m_currentLevel;
        }
    }
    protected override void Awake()
    {
        base.Awake();

        if(m_DB == null)
        {
            if (DBAsset == null)
                DBAsset = Resources.Load<PHDatabase>("DB");

            if (DBAsset == null)
            {
                Debug.LogError("Database not found");
                return;
            }

            DB = Instantiate(DBAsset);
        }

        AllFlameStats.Add(FlameLevel.Pilot, PilotStat);
        AllFlameStats.Add(FlameLevel.Flame, FlameStat);
        AllFlameStats.Add(FlameLevel.Blaze, BlazeStat);

        #region IN-DEV LOAD
        m_FFSelectedForMission = new FFName[3];
        m_FFSelectedForMission[0] = FFName.Forge;
        m_FFSelectedForMission[1] = FFName.Atlas;
        m_FFSelectedForMission[2] = FFName.Jet;

        #endregion

        LoadFFValues();

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
#if !UNITY_EDITOR
        UnlimitedActions = false;
        NoMovementCost = false;
        ActionStopMovement = false;
        SkipCameraOverview = false;
        NoSmokeOnFlame = false;
        ResetSaveOnPlay = false;
#endif

        CustomCursor.LoadCursorsSprites();
        CustomCursor.SetCursorDefault();

        //CustomCursor.SetCursorDefault();
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.N))
        //{
        //    CustomCursor.SetCursor(EquipName.Fire_Axe);
        //    Debug.Log("Change");
        //}

        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    CustomCursor.SetCursorDefault();
        //}

        if (Input.GetKeyDown(KeyCode.R))
        {
            // DEV: temp
            Debug.Log(DB.SkillTreeDB.ContainsKey(FFName.Atlas));
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            // DEV: temp
            SaveSystem.Instance.SaveGame();
        }
#endif
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            // DEV: temp
            Medals = 10000;
            Debug.LogWarning("Medals:" + Medals);
        }

    }

    

    public void Quit()
    {
        Application.Quit();
    }

    #region Start-up

    public void LoadFFValues()
    {
        /// -------------- LOAD SAVES --------------- \\\
        if (ResetSaveOnPlay)
            SaveSystem.Instance.DeleteSave();

        Save save = new Save();

        bool loadSave = false;

        if (SaveSystem.Instance.HasASaveFile())
        {
            save = SaveSystem.Instance.GetSave();

            Progression = new Vector2Int(save.ProgressionX, save.ProgressionY);

            ///// DEBUG ///
            //Progression = new Vector2Int(2, 0);
            //Debug.Log("Load saves");
            /////

            if (Progression != new Vector2Int(0, 0))
                m_LevelToUnlock = NoLevelToUnlock;
            
            LevelSaveData = SaveSystem.Instance.DeserializeLevelSaveData(save);
            ShowTutorial = false;
            
            loadSave = true;
        }
        /// ------------------------------------------ \\\
        
        
        /// Create Firefighters stencil ------------------
        FFTeamData = new List<FFData>();

        foreach (KeyValuePair<FFName, FFData> item in DB.FirefightersDB)
        {
            FFData newData = Instantiate(item.Value);

            if (loadSave)
            {
                newData.HealthStatus = save.AllFFData[item.Key].HealthStatus;
                newData.SkillTree = save.AllFFData[item.Key].SkillTree;
            }

            FFTeamData.Add(newData);
        }
        
        for (int i = 0; i < FFTeamData.Count; i++)
        {
            SkillTreeData saveData = new SkillTreeData(FFTeamData[i].SkillTree);

            FFTeamData[i].SkillTree = saveData;

            /// Progression bar ONE
            for (int j = 0; j < FFTeamData[i].SkillTree.ProgressiveOne; j++)
                ExecuteSkill(GetSkillTreeTemplate(FFTeamData[i].FName).ProgressionOne[j].SkillPoint, FFTeamData[i]);
            
            
            /// Progression bar TWO
            for (int j = 0; j < FFTeamData[i].SkillTree.ProgressiveTwo; j++)
                ExecuteSkill(GetSkillTreeTemplate(FFTeamData[i].FName).ProgressionTwo[j].SkillPoint, FFTeamData[i]);


            /// Unlock Melee
            for (int j = 0; j < FFTeamData[i].SkillTree.Melee; j++)
                ExecuteSkill(GetSkillTreeTemplate(FFTeamData[i].FName).Melee[j].SkillPoint, FFTeamData[i]);


            /// Unlock Extinguisher
            for (int j = 0; j < FFTeamData[i].SkillTree.Extinguish; j++)
                ExecuteSkill(GetSkillTreeTemplate(FFTeamData[i].FName).Extinguish[j].SkillPoint, FFTeamData[i]);


            ///Unlock Support
            for (int j = 0; j < FFTeamData[i].SkillTree.Utility; j++)
                ExecuteSkill(GetSkillTreeTemplate(FFTeamData[i].FName).Utility[j].SkillPoint, FFTeamData[i]);

        }

        void ExecuteSkill(SkillPoint _sp, FFData _Data)
        {
            if (_sp is SPModifyStat)
                (_sp as SPModifyStat).Execute(_Data);

            else if (_sp is SPEquipUpgrade)
                (_sp as SPEquipUpgrade).Execute(_Data);

            else if (_sp is SPUnlockEquip)
                (_sp as SPUnlockEquip).Execute(_Data);
        }
    }
    #endregion

    #region Utility

    private bool IsSelectedForMission(FFData _data)
    {
        foreach (FFName name in m_FFSelectedForMission)
        {
            if (name == _data.FName)
                return true;
        }

        return false;
    }

    public EquipData GetEquipNewInstance(EquipName _Name)
    {
        if (DB.HasItem(_Name))
        {
            return Instantiate(DB.GetEquip(_Name));
        }

        return null;
    }

    public EquipData GetEquipData(EquipName _Name)
    {
        if (DB.HasItem(_Name))
        {
            return DB.GetEquip(_Name);
        }

        return null;
    }

    public EquipData GetActionNewInstance(BaseActionName _Name)
    {
        if (DB.HasItem(_Name))
        {
            return Instantiate(DB.GetAction(_Name));
        }

        return null;
    }

    private FFData GetFFStatNewInstance(FFName _Name)
    {
        return Instantiate(DB.GetFirefighter(_Name));
    }

    /// <summary>
    /// Get the basic info of the firefighter
    /// </summary>
    /// <param name="_Name"></param>
    /// <returns></returns>
    public FFData GetFFInfo(FFName _Name)
    {
        return DB.GetFirefighter(_Name);
    }

    /// <summary>
    /// Return the loaded info of the Firefighter
    /// </summary>
    /// <param name="_name"></param>
    /// <returns></returns>
    public FFData GetFFDataLoaded(FFName _name)
    {
        foreach (FFData item in GetFFTeamData)
        {
            if (item.FName == _name) return item;
        }

        return null;
    }


    public int GetFireDamage(FlameLevel _level)
    {
        return AllFlameStats[_level].PassingDamageToFFs;
    }

    public Sprite GetHealthStatusSprite(FFHealhStatus _status)
    {
        switch (_status)
        {
            case FFHealhStatus.Healthy: return DB.HealthySprite;

            case FFHealhStatus.Tired: return DB.TiredSprite;

            case FFHealhStatus.Exhausted: return DB.ExhaustSprite;

            default: return DB.HealthySprite;
        }
    }

    #endregion

    public void AddReward(RewardTypes type, int amount)
    {
        if (type == RewardTypes.Medals)
            Medals += amount;
        else
            Stars += amount;
    }

    public void LevelCompleteSave(int _medals, Reward[] _stars)
    {
        if (LevelLoaded == null) { Debug.LogWarning("No LevelToLoad data"); return; } 

        Vector2Int index = LevelLoaded.LevelIndex;
        bool[] ownedStars = new bool[_stars.Length];

        for (int i = 0; i < _stars.Length; i++)
        {
            ownedStars[i] = (_stars[i] != null) ? true : false;
        }

        if (LevelSaveData.ContainsKey(LevelLoaded.LevelIndex))
        {
            if (LevelSaveData[index].MedalsEarned < _medals)
            {
                int amountToSum = _medals - LevelSaveData[index].MedalsEarned;
                AddReward(RewardTypes.Medals, amountToSum);
                LevelSaveData[index].MedalsEarned += amountToSum;

                for (int i = 0; i < LevelSaveData[index].SideObjectiveAchived.Length; i++)
                {
                    if (!LevelSaveData[index].SideObjectiveAchived[i] && ownedStars[i])
                    {
                        AddReward(RewardTypes.Stars, _stars[i].Amount);
                        LevelSaveData[index].SideObjectiveAchived[i] = true;
                    }
                }
            }
        }
        // New Level
        else
        {
            LevelSaveData.Add(index, new LevelSaveData(_medals, ownedStars));
            UpdateProgression(index);

            AddReward(RewardTypes.Medals, _medals);

            for (int i = 0; i < _stars.Length; i++)
            {
                if (_stars[i] != null)
                    AddReward(RewardTypes.Stars, _stars[i].Amount);
            }
        }
    }

    public void UpdateProgression(Vector2Int _index)
    {
        if ((Progression.x < _index.x) || (Progression.x == _index.x && Progression.y <= _index.y))
        {
            Progression = new Vector2Int(_index.x, _index.y + 1);
            foreach (Vector2Int item in ProgressionLimit)
            {
                if (_index == item)
                {
                    Vector2Int prog = new Vector2Int(_index.x + 1, 0);
                    Progression = prog;
                    return;
                }
            }

            m_LevelToUnlock = Progression;
        }
    }

    public void ResetLevelToLoad() => m_LevelToUnlock = NoLevelToUnlock;

    public bool LevelCompleted(Vector2Int _index) => LevelSaveData.ContainsKey(_index);

    /// <summary>
    /// Return current level MedalsRecord
    /// </summary>
    /// <returns></returns>
    public int GetLevelMedalsRecord()
    {
        if (LevelLoaded == null) return 0;

        if (LevelSaveData.ContainsKey(LevelLoaded.LevelIndex))
        {
            return LevelSaveData[LevelLoaded.LevelIndex].MedalsEarned;
        }

        Debug.LogWarning($"Key {LevelLoaded.LevelIndex} not found");
        return 0;
    }

    public int GetLevelMedalsRecord(Vector2Int _index)
    {
        if (LevelSaveData.ContainsKey(_index))
        {
            return LevelSaveData[_index].MedalsEarned;
        }

        Debug.LogWarning($"Key {_index} not found");
        return 0;
    }

    /// <summary>
    /// Return current level stars record
    /// </summary>
    /// <returns></returns>
    public int GetLevelStarsRecord()
    {
        if (LevelLoaded == null) return 0;

        if (LevelSaveData.ContainsKey(LevelLoaded.LevelIndex))
        {
            bool [] side = LevelSaveData[LevelLoaded.LevelIndex].SideObjectiveAchived;
            int achive = 0;

            foreach (bool n in side)
                if(n) achive++;

            return achive;
        }

        Debug.LogWarning($"Key {LevelLoaded.LevelIndex} not found");
        return 0;
    }

    public int GetLevelStarsRecord(Vector2Int _index)
    {
        if (LevelSaveData.ContainsKey(_index))
        {
            bool[] side = LevelSaveData[_index].SideObjectiveAchived;
            int achive = 0;

            foreach (bool n in side)
                if (n) achive++;

            return achive;
        }

        Debug.LogWarning($"Key {_index} not found");
        return 0;
    }

    public bool GetLevelSideObjectiveCompleted(Vector2Int _Leves, int _index)
    {
        if (LevelSaveData.ContainsKey(_Leves))
        {
            bool[] side = LevelSaveData[_Leves].SideObjectiveAchived;

            if(side.Length > _index)
            {
                return side[_index];
            }
        }

        //Debug.LogWarning("No level data or index out of range");
        return false;
    }

    #region Scene management

    public void MoveFromHubToLevel()
    {
        m_FFSelectedForMission = HubBriefingHandler.Instance.GetFFTeam();
        Debug.Log(m_FFSelectedForMission.Length);
        m_SelectedEquipSets = new FFEquipSet[m_FFSelectedForMission.Length];

        for (int i = 0; i < m_FFSelectedForMission.Length; i++)
        {
            m_SelectedEquipSets[i] = HubBriefingHandler.Instance.GetFFEquipSet(m_FFSelectedForMission[i]);
        }

        LevelLoaded = AMLevelSelection.instance.GetLevelToLoad();
        Cursor.visible = false;

        LoadScene(LevelLoaded.LevelScene);
    }

    public int GetSceneIndex() => SceneManager.GetActiveScene().buildIndex;

    public void LoadScene(string _path)
    {
        SceneManager.LoadScene(_path);
    }

    public void LoadScene(int _Index)
    {
        SceneManager.LoadScene(_Index);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene(MainMenuScene);
        Cursor.visible = false;
        Time.timeScale = 1;
    }

    public void ReloadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
    #endregion

    public void TriggerLevelLoad(Scene _scene, LoadSceneMode _mode)
    {
        CurrentLevel?.Load();
    }
}