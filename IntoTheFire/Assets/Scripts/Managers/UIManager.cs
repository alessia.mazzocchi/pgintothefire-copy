﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    private Canvas m_Canvas;
    public Canvas Canvas { get => (m_Canvas == null) ? m_Canvas = FindObjectOfType<UIGameCanvas>().Canvas : m_Canvas; }

    private UIEquipButton PullEquip;
    public void SetPullEquip(UIPlayerPull _button) => PullEquip = _button;

    private UIEquipButton PushEquip;
    public void SetPushEquip(UIPlayerPush _button) => PushEquip = _button;

    private UIEquipButton PickupEquip;
    public void SetPickupEquip(UIPlayerPickup _button) => PickupEquip = _button;


    private UIEquipButton m_selectedEquip;
    public UIEquipButton SelectedButton
    {
        get
        {
            return m_selectedEquip;
        }
        set
        {
            m_selectedEquip = value;
        }
    }

    private UIPlayerEquipment m_equipHandler;
    //private UIPlayerHealth m_healthHandler;
    //private UIPlayerMovement m_movememtHandler;
    private UIMouseOverTooltips m_mouseOverTooltips;
    private TempUIRecapTooltip m_UIRecapTooltip;
    private UIFFScrollHandler m_ScrollHandler;
    private UIEventLogHandler m_EventLogHandler;
    private UILevelObjective m_LevelObjective;

    public UIPlayerEquipment EquipHandler {  get => (m_equipHandler == null) ? m_equipHandler = FindObjectOfType<UIPlayerEquipment>() : m_equipHandler; }
    //public UIPlayerHealth HealthHandler { get => (m_healthHandler == null) ? m_healthHandler = FindObjectOfType<UIPlayerHealth>() : m_healthHandler; }
    //public UIPlayerMovement MovementHandler { get => (m_movememtHandler == null) ? m_movememtHandler = FindObjectOfType<UIPlayerMovement>() : m_movememtHandler; }
    public UIMouseOverTooltips MouseOverTooltips { get => (m_mouseOverTooltips == null) ? m_mouseOverTooltips = FindObjectOfType<UIMouseOverTooltips>() : m_mouseOverTooltips; }
    public TempUIRecapTooltip UIRecapTooltip { get => (m_UIRecapTooltip == null) ? m_UIRecapTooltip = FindObjectOfType<TempUIRecapTooltip>() : m_UIRecapTooltip; }
    public UIFFScrollHandler ScrollHandler { get => (m_ScrollHandler == null) ? m_ScrollHandler = FindObjectOfType<UIFFScrollHandler>() : m_ScrollHandler; }
    public UIEventLogHandler EventLogHandler { get => (m_EventLogHandler == null) ? m_EventLogHandler = FindObjectOfType<UIEventLogHandler>() : m_EventLogHandler; }
    public UILevelObjective LevelObjective { get => (m_LevelObjective == null) ? m_LevelObjective = FindObjectOfType<UILevelObjective>() : m_LevelObjective; }


    public GameObject PauseMenu;

    public void EquipSelected(UIEquipButton _e)
    {
        SelectedButton = _e;
    }

    public void EquipDeselected()
    {
        SelectedButton?.ActiveButton(true);
    }

    public void UpdateSelectedEquipButton()
    {
        if(SelectedButton != null)
        {
            SelectedButton.SetInteractive();
        }
    }

    private void Start()
    {
        m_Canvas = FindObjectOfType<Canvas>();
    }

    public void InitializeUI()
    {
        EquipHandler?.LoadEquip();
        ScrollHandler?.Load();
        EventLogHandler?.Load();
        LevelObjective?.Load();
        MouseOverTooltips.DeactiveAll();
    }

    public void DamageFeedback(Vector3 _worldPos, string _dmg, DamageType _type)
    {
        GameObject go = PoolManager.instance.GetItem(PoolType.DamageFeedback);
        RectTransform rect = go.GetComponent<RectTransform>();

        rect.SetParent(m_Canvas.transform);
        
        UIDamageFeedback tmp = go.GetComponent<UIDamageFeedback>();

        tmp.Set( _worldPos, _dmg, _type);

        go.SetActive(true);
    }

    public void UpdateMainObjective()
    {
        LevelObjective.UpdateMainObjective();
    }

    public void UpdateSideObjectives()
    {
        LevelObjective.UpdateSideObjectives();
    }

    #region Tooltips

    public void SetTooltip(bool on, Entity entity = null)
    {
        if (on)
        {
            if (entity as FireFighterBehaviour)
                MouseOverTooltips.SetTooltip(entity as FireFighterBehaviour);
            else if (entity as Obstacle)
                MouseOverTooltips.SetTooltip(entity as Obstacle);
            else if (entity as FlameBehaviour)
                MouseOverTooltips.SetTooltip(entity as FlameBehaviour);
            else if (entity is LiquidBehaviour)
                MouseOverTooltips.SetTooltip(entity as LiquidBehaviour);

        }
        else
        {
            MouseOverTooltips.DeactiveAll();
        }
    }

    public void SetTooltip(bool on, FFData data)
    {
        if (on)
        {
            MouseOverTooltips.SetTooltip(data);
        }
        else
        {
            MouseOverTooltips.DeactiveAll();
        }
    }

    public void SetTooltip(bool on, string description)
    {
        if (on)
        {
            MouseOverTooltips.SetTooltip(description);
        }
        else
        {
            MouseOverTooltips.DeactiveAll();
        }
    }

    public void SetTooltip(bool on, Equip equip)
    {
        if (on)
        {
            MouseOverTooltips.SetTooltip(equip);
        }
        else
        {
            MouseOverTooltips.DeactiveAll();
        }
    }
    

    public void SetUIRecapTooltip(bool _active, ScriptableObject _data)
    {
        if (_active)
        {
            //UIRecapTooltip.SetUIRecapTooltip(_data);
        }
        else
        {
            //UIRecapTooltip.DeactiveAll();
        }
    }
    #endregion

    public void Pause()
    {
        if (Time.timeScale == 0)
            PauseMenu.SetActive(true);
        else
            PauseMenu.SetActive(false);

    }
}
