﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HubTutorialState
{
    public List<string> HubTStep;
    
    [HideInInspector]
    public HubState State;

    [HideInInspector]
    public int T_Index = -1;

    public bool IsDone()
    {
        if (T_Index == HubTStep.Count - 1)
            return true;

        return false;
    }

    public string GetCurrentText()
    {
        return HubTStep[T_Index];
    }
}

public class IFHubTutorial : MonoBehaviour
{
    public IFTutorialCanvas Canvas;
    public static IFHubTutorial instance;
    HubHandler.StateDelegate changeState;

    private Dictionary<HubState, bool> TutorialExecution;
    private HubTutorialState currentTutorial;

    [Header("Hub")]
    public HubTutorialState HubTStep;

    [Header("Level Selection")]
    public HubTutorialState LevelSelectionTStep;

    [Header("Briefing")]
    public HubTutorialState BriefingTStep;

    [Header("FFOverview")]
    public HubTutorialState FFOverviewStep;

    private void Awake()
    {
        instance = this;
        Canvas.TutorialContainer.gameObject.SetActive(false);
    }


    public void ChangeStateStep() 
    {
        switch (HubHandler.Instance.HubState)
        {
            case HubState.Hub:
                // Show Hub Tutorial;
                if (!TutorialExecution[HubState.Hub])
                {
                    currentTutorial = HubTStep;
                    StartTutorial();
                }
                break;

            case HubState.LevelSelection:
                // Show Hub Tutorial;
                if (!TutorialExecution[HubState.LevelSelection])
                {
                    currentTutorial = LevelSelectionTStep;
                    StartTutorial();
                }
                break;

            case HubState.Briefing:
                // Show Hub Tutorial;
                if (!TutorialExecution[HubState.Briefing] && AMLevelSelection.instance.GetLevelToLoad().LevelIndex == new Vector2Int(1,0))
                {
                    currentTutorial = BriefingTStep;
                    StartTutorial();
                }
                break;

            case HubState.Overview:
                if (!TutorialExecution[HubState.Overview])
                {
                    currentTutorial = FFOverviewStep;
                    StartTutorial();
                }
                break;

            case HubState.MainMenu:

                break;

            default:
                break;
        }

        void StartTutorial()
        {
            Canvas.TutorialContainer.gameObject.SetActive(true);
            Canvas.TutorialBackground.gameObject.SetActive(true);
            HubHandler.Instance.inTutorial = true;

            NextTutorial();
        }
    }

    public void ActiveTutorial(bool _active)
    {
        if (_active)
        {
            Debug.Log("Add on change");
            changeState = new HubHandler.StateDelegate(ChangeStateStep);

            HubHandler.Instance.onChangeState += changeState;

            HubTStep.State = HubState.Hub;
            LevelSelectionTStep.State = HubState.LevelSelection;
            BriefingTStep.State = HubState.Briefing;

            if(GameManager.Instance.TutorialExecution == null)
            {
                TutorialExecution = new Dictionary<HubState, bool>();
                TutorialExecution.Add(HubState.Hub, false);
                TutorialExecution.Add(HubState.LevelSelection, false);
                TutorialExecution.Add(HubState.Briefing, false);
                TutorialExecution.Add(HubState.Overview, false);

                GameManager.Instance.TutorialExecution = TutorialExecution;
            }
            else
            {
                TutorialExecution = GameManager.Instance.TutorialExecution;
            }
        }
        else if (!_active && changeState != null)
        {
            HubHandler.Instance.onChangeState -= changeState;
        }

        Canvas.TutorialContainer.gameObject.SetActive(false);

    }

    public void NextTutorial()
    {
        if (!currentTutorial.IsDone())
        {
            currentTutorial.T_Index++;
            Canvas.TutorialText.text = currentTutorial.GetCurrentText();
        }
        else
        {
            TutorialExecution[currentTutorial.State] = true;
            Canvas.TutorialContainer.gameObject.SetActive(false);
            HubHandler.Instance.inTutorial = false;

            if (currentTutorial.State == HubState.Briefing)
                ActiveTutorial(false);
        }
    }


}
