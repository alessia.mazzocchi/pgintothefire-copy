﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct FreeTutoarialEvent
{
    [HideInInspector]
    public bool Completed;
    public Vector2Int[] Position;
    public TutorialEvent Event;

    public bool ContainPosition(Vector2Int _pos)
    {
        for (int i = 0; i < Position.Length; i++)
        {
            if (Position[i] == _pos) return true;
        }

        return false;
    }
}

public class TutorialManager : Singleton<TutorialManager>
{
    public TutorialEvent[] TutorialEvents;
    [Space]
    public FreeTutoarialEvent[] FreeTutorialEvents;

    private Queue<TutorialEvent> m_TutorialEvents = new Queue<TutorialEvent>();
    public TextMeshProUGUI DescriptionText;
    public Image BG;

    [Space]
    public GameObject TargetPrefab;

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.GameStart, GameStart);
        EventManager.StartListening(EventsID.NextTutorialPrompt, NextStep);
        EventManager.StartListening<bool>(EventsID.FFStoppedMoving, FFEndMovementCheck);

        EnqueueAll();

        for (int i = 0; i < FreeTutorialEvents.Length; i++)
        {
            FreeTutorialEvents[i].Completed = false;
        }
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.GameStart, GameStart);
        EventManager.StopListening(EventsID.NextTutorialPrompt, NextStep);
        EventManager.StopListening<bool>(EventsID.FFStoppedMoving, FFEndMovementCheck);

    }

    private void Update()
    {
        // TUTORIAL ONLY
        if (Input.GetKeyDown(KeyCode.O) && m_TutorialOn)
        {
            CallForTutorialNextStep();
        }
    }

    private void EnqueueAll()
    {
        foreach (TutorialEvent te in TutorialEvents)
        {
            if(te != null)
                m_TutorialEvents.Enqueue(Instantiate(te));
        }
    }

    private void GameStart()
    {
        StartFirstAvailablePrompt();
        BG.enabled = true;
    }

    private void StartFirstAvailablePrompt()
    {
        if (m_TutorialEvents.Count > 0)
            m_TutorialEvents.Peek().WaitToStart();
    }

    private void NextStep()
    {
        // DEV: hide text box
        DescriptionText.text = "";

        if (m_TutorialEvents.Count > 0)
        {
            if (m_TutorialEvents.Peek().Step == TutorialSteps.WaitingToStart)
            {
                m_TutorialEvents.Peek().EndStartingPrompt();

                m_TutorialEvents.Peek().StartPrompt();
            }
            else if (m_TutorialEvents.Peek().Step == TutorialSteps.WaitingToEnd)
            {
                m_TutorialEvents.Dequeue().EndPrompt();
                StartFirstAvailablePrompt();
            }
        }
    }

    private void FFEndMovementCheck(bool _bool)
    {
        for (int i = 0; i < FreeTutorialEvents.Length; i++)
        {
            if (!FreeTutorialEvents[i].Completed)
            {
                if (FreeTutorialEvents[i].ContainPosition(LevelManager.instance.GetSelectedFF.GridPos))
                {
                    FreeTutorialEvents[i].Event.StartPrompt();
                    FreeTutorialEvents[i].Completed = true;
                }
            }
        }
    }

    #region Temporary

    private bool m_TutorialOn;

    public void TutorialPromptReady()
    {
        m_TutorialOn = true;
    }

    private void CallForTutorialNextStep()
    {
        EventManager.TriggerEvent(EventsID.NextTutorialPrompt);
    }

    public void TutorialPromptEnded()
    {
        m_TutorialOn = false;
    }

    #endregion

    public void NextTutorial()
    {
        GameManager.Instance.LoadScene(GameManager.Instance.GetSceneIndex() + 1);
    }
}
