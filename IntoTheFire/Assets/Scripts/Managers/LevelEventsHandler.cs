﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEventsHandler : MonoBehaviour
{ 
    private LEventInfo LevelEventsContainer;
    private int currentTurn = 0;

    private Queue<LEvent> eventsQueue = new Queue<LEvent>();

    private bool goToNextEvent = false;
    private bool waitForCameraMovement = false;
    private bool onExecution = false;

    public void Set(LEventInfo _eventList)
    {
        LevelEventsContainer = Instantiate(_eventList);
    }

    public void StartEventTurnChecks(int _turn)
    {
        if (onExecution) return;

        onExecution = true;

        eventsQueue.Clear();
        goToNextEvent = true;

        /// Search the list to find element with this turn
        for (int i = 0; i < LevelEventsContainer.list.Length; i++)
        {
            /// Found the element
            if (GetEvent(i) != null && GetEvent(i).Turn == _turn)
            {
                eventsQueue.Enqueue(GetEvent(i));
            }
        }

        StartCoroutine(EventsExecutions());
    }

    public void NextTurnEventControll(int _turn)
    {
        for (int i = 0; i < LevelEventsContainer.list.Length; i++)
        {
            if(GetEvent(i) != null && GetEvent(i).Turn == _turn - 1)
            {
                UIManager.Instance.EventLogHandler.NewEvent(GetEvent(i));
                SetAlertOnRoom(GetEvent(i));
            }
        }
    }
    private LEvent GetEvent(int _turn)
    {
        return LevelEventsContainer.list[_turn];
    }

    public void SetAlertOnRoom(LEvent _event)
    {
        switch (_event.EventEType)
        {
            case LEventType.Cell:

                for (int i = 0; i < _event.Cells.Length; i++)
                {
                    LevelManager.instance.GetRoom(LevelManager.instance.GetNode(_event.Cells[i]).RoomID).SetAlertOnRoom(true);
                }
                break;
            
            case LEventType.Room:

                for (int i = 0; i < _event.Rooms.Length; i++)
                {
                    LevelManager.instance.GetRoom(_event.Rooms[i]).SetAlertOnRoom(true);
                }
                break;

            case LEventType.Level:
                foreach (KeyValuePair<int, RoomHandler> item in LevelManager.instance.RoomsDictionary)
                {
                    item.Value.SetAlertOnRoom(true);
                }
                break;
        }
    }

    private Vector2Int[] GetEventCells(int _turn)
    {
        return LevelEventsContainer.GetEventCells(_turn);
    }

    private IEnumerator EventsExecutions()
    {
        LEvent ev = null;
        goToNextEvent = true;
        while (eventsQueue.Count > 0)
        {
            /// Load event ad wait for camera movement
            if (goToNextEvent)
            {
                goToNextEvent = false;
                waitForCameraMovement = false;
                ev = eventsQueue.Dequeue();
                UIManager.Instance.EventLogHandler.ShowCurrentEventLog(ev);

                switch (ev.EventEType)
                {
                    case LEventType.Cell:
                        if(ev.Cells.Length == 0 ) { Debug.LogError("No cells int event: " + ev.LData.name + " in turn" + ev.Turn); ev = null; StopCoroutine(EventsExecutions()); break; }

                        CameraController.Instance.MoveToPosition(LevelManager.instance.GetNode(ev.Cells[0]).WorldPosition, CameraOnEventPoint);

                        yield return new WaitUntil(CanExecutionContinue);
                        break;


                    case LEventType.Room:
                        if (ev.Rooms.Length == 0) { Debug.LogError("No rooms int event: " + ev.LData.name + " in turn" + ev.Turn); ev = null;  StopCoroutine(EventsExecutions()); break; }

                        CameraController.Instance.MoveToPosition(LevelManager.instance.GetRoom(ev.Rooms[0]).WorldPosition, CameraOnEventPoint);

                        yield return new WaitUntil(CanExecutionContinue);
                        break;


                    case LEventType.Level: // Level get executed immediatly
                        for (int i = 0; i < ev.Repeats; i++)
                        {
                            ev.LData.ExecuteOnLevel(ev.FullCover);
                        }

                        goToNextEvent = true;
                        break;

                    default:
                        break;
                }
            }
            /// Execute event
            else
            {
                yield return new WaitForSeconds(0.6f);

                if (ev.EventEType == LEventType.Cell)
                {
                    for (int j = 0; j < ev.Cells.Length; j++)
                        ev.LData.ExecuteOnCell(ev.Cells[j]);

                    goToNextEvent = true;
                }
                else if (ev.EventEType == LEventType.Room)
                {
                    for (int i = 0; i < ev.Repeats; i++)
                    {
                        ev.LData.ExecuteInRoom(ev.Rooms, ev.FullCover);
                    }

                    goToNextEvent = true;
                }

                //UIManager.Instance.EventLogHandler.TurnOffCurrentEventLog();
                yield return new WaitForSeconds(1.75f);

            }

            yield return new WaitForFixedUpdate();
        }

        /// Execute last event
        if(ev != null)
        {
            if (ev.EventEType == LEventType.Cell)
            {
                for (int j = 0; j < ev.Cells.Length; j++)
                    ev.LData.ExecuteOnCell(ev.Cells[j]);

            }
            else if (ev.EventEType == LEventType.Room)
            {
                for (int i = 0; i < ev.Repeats; i++)
                {
                    Debug.Log("Try " + i);
                    ev.LData.ExecuteInRoom(ev.Rooms, ev.FullCover);
                }

                goToNextEvent = true;
            }
        }

        yield return new WaitForSeconds(1.5f);

        //UIManager.Instance.EventLogHandler.TurnOffCurrentEventLog();

        onExecution = false;
        LevelManager.instance.EndEventsExecution();
    }

    private void CameraOnEventPoint()
    {
        waitForCameraMovement = true;
    }

    private bool CanExecutionContinue()
    {
        return waitForCameraMovement;
    }
}
