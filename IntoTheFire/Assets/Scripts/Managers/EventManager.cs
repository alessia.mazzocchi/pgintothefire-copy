﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurnPhase
{
    GameEvents,
    VictoryCheck,
    Player,
    AI,
    Env,
	End
}
  
// All event types
public enum EventsID
{
	FFChange,
	FFStartedMoving,
	FFStoppedMoving,
	FireExpansion,
	ChangePhase,
	EndTurn,
	Victory,
	Defeat,
	UpdateUIValues,
	UpdateVictimActionsUI,
	FireEvolution,
	GameStart,
	SmokeCreation,
	SideObjectiveCheck,
	OnSceneLoad,
	NextTutorialPrompt,
	PlayAudio
}

public class EventManager : Singleton<EventManager>
{
	// Generic callbacks
	public delegate void Callback();
	public delegate void Callback<T>(T arg1);
	public delegate void Callback<T, U>(T arg1, U arg2);
	public delegate void Callback<T, U, K>(T arg1, U arg2, K arg3);

	// Dictionary containing all events
	private static Dictionary<EventsID, Delegate> m_EventsDictionary = new Dictionary<EventsID, Delegate>();

	#region Trigger methods

	// Trigger an event based on event type
	public static void TriggerEvent(EventsID InEventID)
	{
		// Check if dictionary contains that type of event
		if (m_EventsDictionary.ContainsKey(InEventID))
		{
			// If it exists, trigger it
			(m_EventsDictionary[InEventID] as Callback)?.Invoke();
		}
	}

	// Trigger an event based on event type and 1 different variable(s) passed by input
	public static void TriggerEvent<T>(EventsID InEventID, T InVariable)
	{
		if (m_EventsDictionary.ContainsKey(InEventID))
		{
			(m_EventsDictionary[InEventID] as Callback<T>)?.Invoke(InVariable);
		}
	}

	// Trigger an event based on event type and 2 different variable(s) passed by input
	public static void TriggerEvent<T, U>(EventsID InEventID, T InVariable1, U InVariable2)
	{
		if (m_EventsDictionary.ContainsKey(InEventID))
		{
			(m_EventsDictionary[InEventID] as Callback<T, U>)?.Invoke(InVariable1, InVariable2);
		}
	}

	// Trigger an event based on event type and 3 different variable(s) passed by input
	public static void TriggerEvent<T, U, K>(EventsID InEventID, T InVariable1, U InVariable2, K InVariable3)
	{
		if (m_EventsDictionary.ContainsKey(InEventID))
		{
			(m_EventsDictionary[InEventID] as Callback<T, U, K>)?.Invoke(InVariable1, InVariable2, InVariable3);
		}
	}

	#endregion

	#region StartListening methods

	// Add a listener method to the events dictionary based on event type (basic)
	public static void StartListening(EventsID InEventID, Callback InListener)
	{
		// If the referred method doesn't exist in the dictionary, create a new one
		if (!m_EventsDictionary.ContainsKey(InEventID))
		{
			m_EventsDictionary.Add(InEventID, InListener);
			return;
		}

		// Else add the listener to the dictionary with its referred event
		m_EventsDictionary[InEventID] = (Callback)m_EventsDictionary[InEventID] + InListener;
	}

	// Add a listener method to the events dictionary based on event type (1 input variable(s))
	public static void StartListening<T>(EventsID InEventID, Callback<T> InListener)
	{
		if (!m_EventsDictionary.ContainsKey(InEventID))
		{
			m_EventsDictionary.Add(InEventID, InListener);
			return;
		}

		m_EventsDictionary[InEventID] = (Callback<T>)m_EventsDictionary[InEventID] + InListener;
	}

	// Add a listener method to the events dictionary based on event type (2 input variable(s))
	public static void StartListening<T, U>(EventsID InEventID, Callback<T, U> InListener)
	{
		if (!m_EventsDictionary.ContainsKey(InEventID))
		{
			m_EventsDictionary.Add(InEventID, InListener);
			return;
		}

		m_EventsDictionary[InEventID] = (Callback<T, U>)m_EventsDictionary[InEventID] + InListener;
	}

	// Add a listener method to the events dictionary based on event type (3 input variable(s))
	public static void StartListening<T, U, K>(EventsID InEventID, Callback<T, U, K> InListener)
	{
		if (!m_EventsDictionary.ContainsKey(InEventID))
		{
			m_EventsDictionary.Add(InEventID, InListener);
			return;
		}

		m_EventsDictionary[InEventID] = (Callback<T, U, K>)m_EventsDictionary[InEventID] + InListener;
	}

	#endregion

	#region StopListening methods

	// Add a listener method to the events dictionary based on event type (basic)
	public static void StopListening(EventsID InEventID, Callback listener)
	{
		// If the referred method exists in the dictionary, remove it
		if (m_EventsDictionary.ContainsKey(InEventID))
		{
			m_EventsDictionary[InEventID] = (Callback)m_EventsDictionary[InEventID] - listener;

			//if(m_EventsDictionary[InEventID].Method == null)
			//{
			//	m_EventsDictionary.Remove(InEventID);
			//}
		}
	}

	// Remove a listener method from the events dictionary based on event type (1 input variable(s))
	public static void StopListening<T>(EventsID InEventID, Callback<T> listener)
	{
		if (m_EventsDictionary.ContainsKey(InEventID))
		{
			m_EventsDictionary[InEventID] = (Callback<T>)m_EventsDictionary[InEventID] - listener;

			//if (m_EventsDictionary[InEventID].Method == null)
			//{
			//	m_EventsDictionary.Remove(InEventID);
			//}
		}
	}

	// Remove a listener method from the events dictionary based on event type (2 input variable(s))
	public static void StopListening<T, U>(EventsID InEventID, Callback<T, U> listener)
	{
		if (m_EventsDictionary.ContainsKey(InEventID))
		{
			m_EventsDictionary[InEventID] = (Callback<T, U>)m_EventsDictionary[InEventID] - listener;

			//if (m_EventsDictionary[InEventID].Method == null)
			//{
			//	m_EventsDictionary.Remove(InEventID);
			//}
		}
	}

	// Remove a listener method from the events dictionary based on event type (3 input variable(s))
	public static void StopListening<T, U, K>(EventsID InEventID, Callback<T, U, K> listener)
	{
		if (m_EventsDictionary.ContainsKey(InEventID))
		{
			m_EventsDictionary[InEventID] = (Callback<T, U, K>)m_EventsDictionary[InEventID] - listener;

			//if (m_EventsDictionary[InEventID].Method == null)
			//{
			//	m_EventsDictionary.Remove(InEventID);
			//}
		}
	}

	#endregion
}
