﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveManager
{
    private static FFStats PlayerOneStat;
    private static FFStats PlayerTwoStat;
    private static FFStats PlayerThreeStat;


    /// <summary>
    /// Save fire fighters status, equip, ecc.
    /// </summary>
    /// <param name="_stat"></param>
    public static void SavePlayerOne(FFStats _stat)
    {
        PlayerOneStat = _stat;
    }

    public static void SavePlayerTwo(FFStats _stat)
    {
        PlayerTwoStat = _stat;
    }

    public static void SavePlayerThree(FFStats _stat)
    {
        PlayerThreeStat = _stat;
    }
}
