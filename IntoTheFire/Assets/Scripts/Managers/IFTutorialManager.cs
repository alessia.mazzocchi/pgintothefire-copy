﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;
using DG.Tweening;
using Malee;

public class IFTutorialManager : MonoBehaviour
{
    private IFTutorialCanvas m_IFCanvas;
    public IFTutorialCanvas IFCanvas
    {
        get
        {
            if(m_IFCanvas == null)
            {
                m_IFCanvas = FindObjectOfType<IFTutorialCanvas>();
                m_IFCanvas.IFTutorial = this;
            }
            return m_IFCanvas;
        }
    }

    //public TextMeshProUGUI descriptioText;
    //public Image TextBG;
    //public Image BlackScreen;

    [Space]
    [Reorderable(elementNameProperty = "Turn")]
    public TutorialStep TutorialStep;
    private int tutorialIndex = 0;
    private int textIndex = 0;

    private TutorialStepInfo currentTutorial;

    bool updateText;
    string textToWrite;
    int textAnimationIndex = 0;
    public float DelayTime = 0.1f;
    private float currentDelayTime;
    private void OnEnable()
    {
        EventManager.StartListening(EventsID.GameStart, UpdateTutorial);
        EventManager.StartListening(EventsID.ChangePhase, UpdateTutorial);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.GameStart, UpdateTutorial);
        EventManager.StopListening(EventsID.ChangePhase, UpdateTutorial);
    }

    private void Start()
    {
        IFCanvas.TutorialContainer.gameObject.SetActive(false);
        //BlackScreen.gameObject.SetActive(false);
        //TextBG.gameObject.SetActive(false);
    }


    //private void FixedUpdate()
    //{
    //    if (updateText)
    //    {
    //        currentDelayTime += Time.fixedDeltaTime;

    //        if(currentDelayTime >= DelayTime)
    //        {
    //            descriptioText.text += textToWrite[textAnimationIndex];
    //            currentDelayTime = 0;
    //            textAnimationIndex++;

    //            if(textAnimationIndex == textToWrite.Length)
    //            {
    //                updateText = false;
    //            }
    //        }
    //    }
    //}

    private void UpdateTutorial()
    {
        if (LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            tutorialIndex = GetTutorialIndex();
            if (tutorialIndex == -1) return;

            currentTutorial = TutorialStep[tutorialIndex];

            IFCanvas.TutorialText.text = currentTutorial.Text[textIndex];
            textIndex = 0;

            IFCanvas.TutorialContainer.gameObject.SetActive(true);

            IFCanvas.TutorialBackground.gameObject.SetActive(true);
            IFCanvas.TutorialBackground.transform.localScale = Vector3.one;
            

            LevelManager.instance.TutorialPause();
        }
    }

    // Used by button
    public void NextTutorialStep()
    {
        textIndex++;

        if (textIndex < currentTutorial.Text.Length)
        {
            IFCanvas.TutorialText.text = currentTutorial.Text[textIndex];
        }
        else
        {
            IFCanvas.TutorialContainer.DOColor(Color.clear, 0.5f);
            IFCanvas.TutorialBackground.gameObject.transform.DOScale(Vector2.zero, 0.5f).SetEase(Ease.InBack).OnComplete(OnEndPrompAnimation);
            textIndex = 0;

            LevelManager.instance.ResumeTutorial();
        }


        void OnEndPrompAnimation()
        {
            IFCanvas.TutorialBackground.gameObject.SetActive(false);
            IFCanvas.TutorialContainer.gameObject.SetActive(false);
        }
    }

    private int GetTutorialIndex()
    {
        for (int i = 0; i < TutorialStep.Length; i++)
        {
            if (TutorialStep[i].Turn == LevelManager.instance.GetCurrentTurns())
                return i;
        }

        // Step not found
        return -1;
    }

    public void GoToPlayButton()
    {
        LevelManager.instance.ResumeTutorial();
        Debug.Log("Play");
    }

    public void NextTutorialLevel()
    {
        GameManager.Instance.LoadScene(GameManager.Instance.GetSceneIndex() + 1);
        Time.timeScale = 1;
    }

}

[System.Serializable]
public class TutorialStep : ReorderableArray<TutorialStepInfo>
{ }

[System.Serializable]
public class TextStep : ReorderableArray<string>
{ }

[System.Serializable]
public struct TutorialStepInfo
{
    public int Turn;
    [Reorderable]
    public TextStep Text;
}
