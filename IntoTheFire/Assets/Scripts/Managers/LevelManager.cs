﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

[System.Serializable]
public struct FFEquipSet
{
    public EquipName EquipOne;

    public EquipName EquipTwo;
}

public class UndoAction
{
    public FFName LastFF;

    public bool CanUndo;

    public Vector2Int LastPos;
    public int LastCost;

    public UndoAction()
    {
        CanUndo = false;
    }
}

public enum LookDirection { Up, Down, Left, Right }
public enum GameStatus { Play, Pause }

[RequireComponent(typeof(MissionManager))]
public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    private GameStatus m_gameStatus = GameStatus.Pause;
    public GameStatus GameStatus
    {
        get => m_gameStatus;
        private set => m_gameStatus = value;
    }
    public LEventInfo MyLevelEventInfo;
    [Space]
    public LookDirection LookDir;
    private LevelEventsHandler levelEventHandler;

    #region WorldGrid
    public Node[,] GetWorldGrid { get { return WorldGrid; } }

    private Node[,] WorldGrid;

    public Node GetNode(int x, int y)
    {
        return WorldGrid[x, y];
    }

    public Node GetNode(Vector2Int _pos)
    {
        return WorldGrid[_pos.x, _pos.y];
    }

    public List<Node> Path;

    public RoomHandler GetRoom(int _index)
    {
        return RoomsDictionary[_index];
    }

    public RoomHandler GetRoomByCell(Vector2Int _cell)
    {
        foreach (KeyValuePair<int, RoomHandler> item in RoomsDictionary)
        {
            if (item.Value.Contain(_cell)) return item.Value;
        }

        return null;
    }

    private int[] GetAllRoomsID()
    {
        int[] tmp = new int[RoomsDictionary.Count];
        int index = 0;

        foreach (KeyValuePair<int,RoomHandler> item in RoomsDictionary)
        {
            tmp[index] = item.Key;
            index++;
        }

        return tmp;
    }

    public int MaxSize
    {
        get { return m_GridSize.x * m_GridSize.y; }
    }

    private Vector2Int m_GridSize;

    public Vector2Int GetGridSize
    {
        get
        {
            if (gridX == -1 && gridY == -1)
            {
                CalculateTilemapSize();

                m_GridSize = new Vector2Int(gridX, gridY);
            }
            return m_GridSize;
        }
    }

    /// <summary>
    /// - 1 indicates that the grid size has not been calculated yet
    /// </summary>
    private int gridX = -1;
    private int gridY = -1;

    #endregion

    [HideInInspector]
    public Grid GridComponent;

    private Tilemap m_GroundTilemap;
    public Tilemap GroundTilemap 
    { 
        get 
        { 
            if(m_GroundTilemap == null)
            {
                m_GroundTilemap = GridComponent.transform.Find("GroundTilemap").GetComponent<Tilemap>();
            }

            return m_GroundTilemap; 
        }
        set 
        {
            m_GroundTilemap = value;
        }
    }

    private Tilemap m_RoomTilemap;
    public Tilemap RoomsTilemap
    {
        get
        {
            if (m_RoomTilemap == null)
            {
                m_RoomTilemap = GridComponent.transform.Find("RoomsTilemap").GetComponent<Tilemap>();
            }

            return m_RoomTilemap;
        }
        set
        {
            m_RoomTilemap = value;
        }
    }

    private Tilemap m_EnvTilemap;
    public Tilemap EnvsTilemap
    {
        get
        {
            if (m_EnvTilemap == null)
            {
                m_EnvTilemap = GridComponent.transform.Find("EnvsTilemap").GetComponent<Tilemap>();
            }

            return m_EnvTilemap;
        }
        set
        {
            m_EnvTilemap = value;
        }
    }

    private RoomHandler[] LevelRooms;
    public Dictionary<int, RoomHandler> RoomsDictionary;
    private int[] m_RoomsIDs;
    public int[] RoomsIDs
    {
        get
        {
            if(m_RoomsIDs == null)
            {
                m_RoomsIDs = GetAllRoomsID();
            }

            return m_RoomsIDs;
        }
    }

    #region Setup values

    private Node SpawnPlayer1;
    private Node SpawnPlayer2;
    private Node SpawnPlayer3;

    private List<Node> ExitPoint;
    private List<Node> VictimPoint;

    private FireFighterBehaviour[] FFTeamList;
    public FireFighterBehaviour[] GetFFTeamList { get { return FFTeamList; } }

    public bool IsInTeamList(FFName name)
    {
        foreach (FireFighterBehaviour item in FFTeamList)
        {
            if (item.Name == name)
                return true;
        }

        return false;
    }

    private FireFighterBehaviour selectedFF;

    [HideInInspector]
    public List<FireFighterBehaviour> FFInLevel = new List<FireFighterBehaviour>();

    public FireFighterBehaviour GetFF (FFName _name)
    {
        foreach (FireFighterBehaviour item in FFInLevel)
        {
            if (item.Name == _name) return item;
        }

        return null;
    }

    public FireFighterBehaviour GetSelectedFF
    {
        get => selectedFF;
    }

    public FFName GetFFNameByIndex(int _index) => FFInLevel[_index].Name;

    #endregion

    #region Game flow

    public int MaxRounds { get { return MissionManager.Instance.MaxRounds; } }
    private int m_CurrentTurns;
    public int GetCurrentTurns() { return m_CurrentTurns; }

    public int m_MinimumCiviliansToSave { get { return MissionManager.Instance.MinCiviliansToSave; } }
    private int m_CurrentlySavedCivilians;
    public int GetSavedCivilians() { return m_CurrentlySavedCivilians; }

    private float m_PhaseDelay = 1f;
    private float m_CurrentPhaseDelay;
    private bool m_WaitForDelay;
    private TurnPhase m_CurrentPhase = TurnPhase.GameEvents;
    public TurnPhase CurrentPhase
    {
        get { return m_CurrentPhase; }
    }

    [HideInInspector]
    public bool ActiveTooltip;
    [HideInInspector]
    public bool wantToEndTurn;
    #endregion

    private Pathfinding Pathfinder = new Pathfinding();
    private List<Node> areaPath = new List<Node>();

    public Node ExitPointNode;

    List<Vector2Int> enlightArea = new List<Vector2Int>();

    List<Vector2Int> InteractiveArea = new List<Vector2Int>();

    private bool OracleInTeam = false;
    private FireFighterBehaviour OracleFF = null;

    [HideInInspector]
    public bool FfIsMoving = false;

    [HideInInspector]
    public List<Victim> VictimsList = new List<Victim>();

    [Header("Locked valies")]
    public bool FirefightersLocked = false;
    public FFName[] FFInMission = { FFName.Forge, FFName.Atlas, FFName.Jet };
    [Space]
    public bool EquipLocked = false;
    public FFEquipSet[] FFEquip = new FFEquipSet[3];

    /// ----------------  UNDO SECTION  ---------------------
    private Dictionary<FFName, UndoAction> UndoActions;

    #region Undo Utility

    public bool CanUndo {  get => UndoActions[selectedFF.Name].CanUndo;  }
    public Vector2Int UndoLastPosition { get => UndoActions[selectedFF.Name].LastPos; }
    public int UndoLastCost { get => UndoActions[selectedFF.Name].LastCost; }
    public void ResetUndoActions()
    {
        foreach (KeyValuePair<FFName, UndoAction> item in UndoActions)
        {
            item.Value.CanUndo = false;
        }
    }

    #endregion
    
    /// -----------------------------------------------------

    [HideInInspector]
    public UITurnEndButton endButton;

    private bool m_ThereIsMinOneFlame;

    public bool ThereIsMinOneFlame
    {
        get { return m_ThereIsMinOneFlame; }
    }

    public void SetFirePresence(bool value)
    {
        m_ThereIsMinOneFlame = value;
    }

    private bool m_GameHasStarted = false;

    public bool GameHasStarted { get { return m_GameHasStarted; } }

    private void OnEnable()
    {
        // DEV: started/stopped might be fused into one event
        EventManager.StartListening<bool>(EventsID.FFStartedMoving, FFMoving);
        EventManager.StartListening<bool>(EventsID.FFStoppedMoving, FFMoving);
        //EventManager.StartListening(EventsID.GameStart, StartGameSession);
    }

    private void OnDisable()
    {
        EventManager.StopListening<bool>(EventsID.FFStartedMoving, FFMoving);
        EventManager.StopListening<bool>(EventsID.FFStoppedMoving, FFMoving);
        //EventManager.StopListening(EventsID.GameStart, StartGameSession);
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogError("Second Level Manager founded");
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        ActiveTooltip = true;
        Cursor.visible = true;

        Load();
    }

    void Update()
    {
        if (m_gameStatus == GameStatus.Pause) return;

        if (Input.GetKeyDown(KeyCode.F))
        {
            ToggleToolTip();
        }

        if (!FfIsMoving && m_CurrentPhase == TurnPhase.Player)
            FFSelection();

        ManagePhases();

        if (Input.GetKeyDown(KeyCode.K))
        {
            EventManager.TriggerEvent(EventsID.Victory);
            m_gameStatus = GameStatus.Pause;
            m_CurrentPhase = TurnPhase.End;
        }

        //if (Input.GetKeyDown(KeyCode.Return) && m_CurrentPhase == TurnPhase.Player)
        //    EndPlayerPhase();
    }

    #region Initializations

    public void InitializeGrid()
    {
        //CalculateGridTilemapSize();
        WorldGrid = new Node[GetGridSize.x, GetGridSize.y];

        Vector3 worldBottomLeft = transform.position - Vector3.right * m_GridSize.x / 2 - Vector3.forward * m_GridSize.y / 2;

        Vector3Int currentIndex = Vector3Int.zero;
        for (int i = 0; i < gridX; i++)
        {
            for (int j = 0; j < gridY; j++)
            {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (i * GetGridCellSize() + GetGridCellSize() / 2) + Vector3.forward * (j * GetGridCellSize() + GetGridCellSize() / 2);

                currentIndex = new Vector3Int(i, j, 0);

                WorldGrid[i, j] = new Node(worldPoint, new Vector2(i, j));

                WorldGrid[i, j].WorldPosition = RoomsTilemap.GetCellCenterWorld(new Vector3Int(i, j, 0));
                WorldGrid[i, j].GridPosition = new Vector2Int(i, j);


                // --- GROUND --- //

                GroundTile tempGround = GroundTilemap.GetTile<GroundTile>(currentIndex);
                if (tempGround != null)
                    WorldGrid[i, j].GroundType = tempGround.GroundType;
                //else
                    //Debug.LogWarning("Error - There should not be any empty tile on Groundtilemap");

                if(tempGround != null && WorldGrid[i, j].GroundType == GroundType.Smoke)
                    WorldGrid[i, j].SetSmoke();

                // --- ROOMS --- //
                RoomsTile tempRooms = RoomsTilemap.GetTile<RoomsTile>(currentIndex);
                if (tempRooms != null)
                {
                    //Obstacle temp = tempRooms.Prefab.GetComponent<Obstacle>();
                    //if ((WallObs)temp)
                    //    Debug.Log("Wall");

                    WorldGrid[i, j].RoomsType = tempRooms.RoomsTileMap;
                    WorldGrid[i, j].RoomID = tempRooms.RoomID;
                }
                else
                    Debug.LogWarning("Error - There should not be any empty tile on Roomstilemap");

                #region Actions on roooms tilemap 
                //DEV
                //if (WorldGrid[i, j].RoomsType != RoomsType.Wall)
                //{
                //    //WorldGrid[i, j].SetLocator(PoolManager.instance.GetItem(PoolType.Locator));
                //    WorldGrid[i, j].TurnOnNode();
                //}
                #endregion

                // --- ENV --- //
                EnvTile tempEnv = EnvsTilemap.GetTile<EnvTile>(currentIndex);
                if (tempEnv != null)
                {
                    WorldGrid[i, j].EnvType = tempEnv.EnvType;
                    WorldGrid[i, j].OriginalEnvType = tempEnv.EnvType;
                }
                else
                    WorldGrid[i, j].EnvType = EnvType.None;

                #region Actions on env tilemap

                switch (WorldGrid[i, j].EnvType)
                {
                    case EnvType.None:
                        break;
                    case EnvType.Flame:
                        // Set the flame at the level set on tilemap
                        WorldGrid[i, j].SetOnFire(true, tempEnv.Value);
                        break;
                    case EnvType.Smoke:
                        WorldGrid[i, j].SetSmoke();
                        break;
                    case EnvType.Window:
                        break;
                    case EnvType.P1SpawnPoint:
                        SpawnPlayer1 = WorldGrid[i, j];
                        break;
                    case EnvType.P2SpawnPoint:
                        SpawnPlayer2 = WorldGrid[i, j];
                        break;
                    case EnvType.P3SpawnPoint:
                        SpawnPlayer3 = WorldGrid[i, j];
                        break;
                    case EnvType.Exit:
                        ExitPoint.Add(WorldGrid[i, j]);
                        break;
                    case EnvType.Victim:
                        VictimPoint.Add(WorldGrid[i, j]);
                        WorldGrid[i, j].SpawnVictim();
                        break;
                    case EnvType.Obstacle:
                        WorldGrid[i, j].SetObstaclePresence(true);
                        break;
                    case EnvType.Water:
                        WorldGrid[i, j].SpawnLiquid(LiquidType.Water);
                        break;
                    case EnvType.Fuel:
                        WorldGrid[i, j].SpawnLiquid(LiquidType.Fuel);
                        break;

                    case EnvType.TeddyBear:
                        WorldGrid[i, j].SpawnTeddyBear();
                        break;
                    default:
                        break;
                }
                #endregion

                WorldGrid[i, j].SetLocator();
            }
        }

#if UNITY_STANDALONE
        EnvsTilemap.enabled = false;
        GroundTilemap.enabled = false;
        RoomsTilemap.enabled = false;
#endif
    }

    public void Load()
    {
        #region New istances
        ExitPoint = new List<Node>();
        VictimPoint = new List<Node>();
        UndoActions = new Dictionary<FFName, UndoAction>();
        //Undo = new UndoAction();

        if (MyLevelEventInfo != null)
        {
            levelEventHandler = gameObject.AddComponent<LevelEventsHandler>();
            levelEventHandler.Set(MyLevelEventInfo);
        }

        /// Can be used to delay the game start
        InputManager.Instance.enabled = true;
        #endregion

        InitializeGrid();

        GenerateRooms();

        ResetDelay();

        m_CurrentTurns = MaxRounds;

        SpawnPlayers();

        CameraController.Instance.transform.position = GetFFTeamList[0].transform.position;

        UIManager.Instance.InitializeUI();

        m_GameHasStarted = true;

        EventManager.TriggerEvent(EventsID.OnSceneLoad);
        // Wait for button pression
        //EventManager.TriggerEvent(EventsID.GameStart);

        AudioData audio = new AudioData();
        audio.SetOST(MusicOSTs.InGame);
        EventManager.TriggerEvent(EventsID.PlayAudio, audio);
    }

    private void CalculateTilemapSize()
    {
        // Check when one side has no more tiles
        bool xDone = false;
        bool yDone = false;

        // Coordinates
        int x = 0;
        int y = 0;

        int tempX = 0;
        int tempY = 0;

        Vector3Int tilePos = new Vector3Int(x, y, 0);

        while (RoomsTilemap.HasTile(tilePos))
        {
            // Check if the next RIGHT tile has a tile
            if (RoomsTilemap.HasTile(new Vector3Int(x + 1, y, 0)))
                // If so we can move on on X
                tempX = x + 1;

            // Check if the UPPER tile has a tile
            if (RoomsTilemap.HasTile(new Vector3Int(x, y + 1, 0)))
                // If so we can move on on Y
                tempY = y + 1;


            // if X is equal to the previus X it means that there are no more tile on the right, so check TRUE the exit condition
            if (x < tempX)
                x = tempX;
            else
                xDone = true;


            // if Y is equal to the previus Y it means that there are no more tile above, so check TRUE the exit condition
            if (y < tempY)
                y = tempY;
            else
                yDone = true;

            // Update coordinates for next cycles
            tilePos = new Vector3Int(x, y, 0);

            // EXIT CONDITION: 
            if (xDone && yDone)
                break;
        }

        gridX = x + 1;
        gridY = y + 1;

        //Debug.Log("Tilemap size: " + x + " - " + y);
    }

    #region Rooms

    public void GenerateRooms()
    {
        RoomsDictionary = new Dictionary<int, RoomHandler>();
        LevelRooms = new RoomHandler[99];

        for (int i = 0; i < GetGridSize.x; i++)
        {
            for (int j = 0; j < GetGridSize.y; j++)
            {
                if (WorldGrid[i, j].RoomsType == RoomsType.Floor)
                    AddCellToRoom(i, j);
            }
        }

        for (int i = 0; i < LevelRooms.Length; i++)
        {
            if (LevelRooms[i] != null)
            {
                RoomsDictionary.Add(i, LevelRooms[i]);
            }
        }
    }

    private void AddCellToRoom(int _x, int _y)
    {
        int ID = WorldGrid[_x, _y].RoomID;

        if (LevelRooms[ID] == null)
            LevelRooms[ID] = new RoomHandler();

        LevelRooms[ID].AreaNode.Add(new Vector2Int(_x, _y));

        //GameObject go = new GameObject(_x + " - " + _y);
        //go.transform.position = WorldGrid[_x, _y].WorldPosition;

    }

    private void TestMethod()
    {
        int i = 3;
        int j = 3;

        WorldGrid?[i + 1, j].TurnOnNode(LocatorMode.Move);
        WorldGrid?[i - 1, j].TurnOnNode(LocatorMode.Move);
        WorldGrid?[i, j + 1].TurnOnNode(LocatorMode.Move);
        WorldGrid?[i, j - 1].TurnOnNode(LocatorMode.Move);
        WorldGrid?[i + 1, j + 1].TurnOnNode(LocatorMode.Move);
        WorldGrid?[i + 1, j - 1].TurnOnNode(LocatorMode.Move);
        WorldGrid?[i - 1, j + 1].TurnOnNode(LocatorMode.Move);
        WorldGrid?[i - 1, j - 1].TurnOnNode(LocatorMode.Move);

        Debug.Log("Turn on");
    }

    #endregion


    public void SetSceneReferences(Obstacle _item)
    {
        WorldGrid[_item.GridPos.x, _item.GridPos.y].Obstacle = _item;
    }

    public void SetSceneReferences(Victim _item)
    {
        WorldGrid[_item.GridPos.x, _item.GridPos.y].Victim = _item;
    }

    public void SetSceneReferences(Vector2Int _gridPos, Obstacle _item)
    {
        WorldGrid[_gridPos.x, _gridPos.y].Obstacle = _item;
    }

    #region Players

    private void SpawnPlayers()
    {
        FFName[] FFToLoad = new FFName[1];
        FFEquipSet[] EquipToLoad = new FFEquipSet[1];

        FFToLoad = (FirefightersLocked)? FFInMission : GameManager.Instance.FFSelectedForMission;
        EquipToLoad = (EquipLocked)? FFEquip : GameManager.Instance.SelectedEquipSets;

        // Ignore the NONE firefighters
        int cont = 0;
        for (int i = 0; i < FFToLoad.Length; i++)
        {
            if (FFToLoad[i] != FFName.None)
                cont++;
        }

        // Create a temp team array
        FFName[] tempTeam = new FFName[cont];
        int m_index = 0;

        for (int i = 0; i < FFToLoad.Length; i++)
        {
            if(FFToLoad[i] != FFName.None)
            {
                tempTeam[m_index] = FFToLoad[i];
                m_index++;
            }
        }

        FFToLoad = tempTeam;


        FFTeamList = new FireFighterBehaviour[FFToLoad.Length];

        // Get player prefab from pool
        for (int i = 0; i < FFTeamList.Length; i++)
        {
            FFTeamList[i] = PoolManager.instance.CreateNewItem(PoolType.FF).GetComponent<FireFighterBehaviour>();
            FFTeamList[i].transform.forward = GetLookDir(LookDir);
        }

        // Move position to the spawn point
        try
        {
            FFTeamList[0].transform.position = SpawnPlayer1.WorldPosition;
        }
        catch (System.IndexOutOfRangeException) { }

        if (FFTeamList.Length > 1)
        {
            try
            {
                //if (FFTeamList[1] != )
                FFTeamList[1].transform.position = SpawnPlayer2.WorldPosition;
            }
            catch (System.IndexOutOfRangeException) { }

            if (FFTeamList.Length > 2)
            {
                try
                {
                    FFTeamList[2].transform.position = SpawnPlayer3.WorldPosition;
                }
                catch (System.IndexOutOfRangeException) { }
            }
        }
        
        for (int i = 0; i < FFTeamList.Length; i++)
        {
            FFTeamList[i].ID = i;
            FFTeamList[i].SetDataStat(GameManager.Instance.GetFFDataLoaded(FFToLoad[i]), EquipToLoad[i]);
            FFTeamList[i].name = FFTeamList[i].Name.ToString();

            UndoActions.Add(FFTeamList[i].Name, new UndoAction());

            if (FFTeamList[i].Name == FFName.Oracle)
            {
                OracleInTeam = true;
                OracleFF = FFTeamList[i];
            }
        }


        // Set values and activate the object

        selectedFF = FFTeamList[0];

        for (int i = 0; i < FFTeamList.Length; i++)
        {
            GetNode(FFTeamList[i].GridPos.x, FFTeamList[i].GridPos.y).OnEntityEnter(FFTeamList[i]);

            FFInLevel.Add(FFTeamList[i]);
        }

        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.YoureUp);
        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.ArmyOfTwo);

        Vector3 GetLookDir(LookDirection _dir)
        {
            switch (_dir)
            {
                case LookDirection.Up: return new Vector3(0, 0, 1);
                   
                case LookDirection.Down: return new Vector3(0, 0, -1);
                   
                case LookDirection.Left: return new Vector3(-1, 0, 0);
                   
                case LookDirection.Right: return new Vector3(1, 0, 0);

                default: return Vector3.zero; 
            }
        }
    }


    #endregion

    #endregion

    #region Pathfinding

    public Node GetNodeFromWorldPoint(Vector3 worldPosition)
    {
        float percentX = (worldPosition.x + m_GridSize.x / 2) / m_GridSize.x;
        float percentY = (worldPosition.z + m_GridSize.y / 2) / m_GridSize.y;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((m_GridSize.x - 1) * percentX);
        int y = Mathf.RoundToInt((m_GridSize.y - 1) * percentY);
        return WorldGrid[x, y];
    }

    public void ClearGridCost()
    {
        for (int i = 0; i < GetGridSize.x; i++)
        {
            for (int j = 0; j < GetGridSize.y; j++)
            {
                WorldGrid[i, j].SetCost(0);
                WorldGrid[i, j].SetTimesChecked(0);
            }
        }
    }

    #endregion

    private void FFSelection()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            int newFF = (int)Mathf.Repeat(GetFFIndex(GetSelectedFF.Name) - 1, (int)GetFFTeamList.Length);

            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.PlayerSwitch);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);

            ChangeSelectedFF(newFF);
        }
    }

    public void ChangeSelectedFF(int _index)
    {
        InputManager.Instance.EquipDeselected(true);
        selectedFF.ResetPreview();

        selectedFF = FFTeamList[_index];
        for (int i = 0; i < GetFFTeamList.Length; i++)
        {
            if (GetFFTeamList[i] != selectedFF)
            {
                GetNode(GetFFTeamList[i].GridPos).Locator.IsActive = false;
            }
            else
            {
                GetNode(GetFFTeamList[i].GridPos).Locator.IsActive = true;
            }
        }

       // Show range only if alive
        ShowFFRange(selectedFF.HealthCurrent > 0);

        UIManager.Instance.ScrollHandler.Roll(selectedFF.Name);

        EventManager.TriggerEvent<FireFighterBehaviour>(EventsID.FFChange, selectedFF);
        EventManager.TriggerEvent(EventsID.UpdateUIValues);
    }

    private int GetUsableFF()
    {
        for (int i = 0; i < FFTeamList.Length; i++)
        {
            if (FFTeamList[i].IsUsable())
                return i;
        }

        return 0;
    }

    private int GetFFIndex(FFName _name)
    {
        for (int i = 0; i < FFTeamList.Length; i++)
        {
            if (FFTeamList[i].Name == _name)
                return i;
        }

        return 0;
    }

    public void FFEndTurnAction()
    {
        selectedFF.CanAct = false;
        selectedFF.UseAction();
        SetUndoCurrentFF(false);

        if(GameManager.Instance.ActionStopMovement)
            selectedFF.currentMovement = 0;

        EventManager.TriggerEvent(EventsID.UpdateUIValues);
        // UI feedback?
    }

    public void SetUndoCurrentFF(bool _canUndo)
    {
        UndoActions[selectedFF.Name].CanUndo = _canUndo;
        //Undo.CanUndo = _canUndo;
    }

    /// <summary>
    /// DEPRECATO
    /// </summary>
    /// <param name="index"></param>
    public void ActivePlayer(int index)
    {
        InputManager.Instance.EquipDeselected(true);
        ShowFFRange(false);

        selectedFF = FFTeamList[index];

        EventManager.TriggerEvent<FireFighterBehaviour>(EventsID.FFChange, selectedFF);
        EventManager.TriggerEvent(EventsID.UpdateUIValues);

        ShowFFRange(true);
    }

    public void ShowFFRange(bool on)
    {
        ClearAreaNodes();

        if (on)
        {
            Node ffNode = WorldGrid[selectedFF.GridPos.x, selectedFF.GridPos.y];
            int x = ffNode.GridPosition.x;
            int y = ffNode.GridPosition.y;
            Vector2Int pos = new Vector2Int(x, y);

            areaPath = Pathfinder.GetPossiblePathArea(pos, selectedFF.GetCurrentMovement);

            int cont = 0;
            foreach (Node n in areaPath)
            {
                WorldGrid[n.GridPosition.x, n.GridPosition.y].TurnOnNode(LocatorMode.Move);
                cont++;
            }

            // Turn off side meshes

            foreach (Node n in areaPath)
            {
                WorldGrid[n.GridPosition.x, n.GridPosition.y].Locator.LocatorSideActivation();
            }

            ffNode.Locator.LocatorSideActivation();
        }
    }

    public void ClearAreaNodes()
    {
        foreach (Node n in areaPath)
        {
            WorldGrid[n.GridPosition.x, n.GridPosition.y].TurnOffNode();
        }
        areaPath.Clear();
    }

    //public void ClearAreaNodes(List<Vector2Int> _list)
    //{
    //    foreach (Vector2Int item in _list)
    //    {
    //        if (area.Contains(WorldGrid[item.y, item.y]))
    //        {
    //            Debug.Log("Removed");
    //            WorldGrid[item.y, item.y].TurnOffNode();

    //            area.Remove(WorldGrid[item.y, item.y]);
    //        }
    //    }
    //}

    public void ActiveNodes(List<Vector2Int> _nodes, LocatorMode _mode, bool _clear = true)
    {
        if(_clear)
            ClearAreaNodes();

        GetNode(GetSelectedFF.GridPos).TurnOffLocatorSide();

        foreach (Vector2Int item in _nodes)
        {
            Node temp;

            try
            {
                temp = WorldGrid?[item.x, item.y];
            }
            catch (System.IndexOutOfRangeException ex)
            {
                continue;
            }

            if(_mode == LocatorMode.Enlight)
            {
                if (temp.CanBeEnlighted())
                {
                    // Extinguish mode have the priority on enlightment, prevent from turning off
                    //if (temp.Locator.gameObject.activeInHierarchy && temp.LocatorMode == LocatorMode.Extinguish) return;
                    enlightArea.Add(item);
                    Debug.Log("Wrnog");
                    temp.TurnOnNode(_mode);
                }
            }
            else if (_mode == LocatorMode.Move)
            {
                if (!areaPath.Contains(temp))
                {
                    Debug.Log("Active");
                    areaPath.Add(temp);
                    temp.TurnOnNode(_mode);
                }
                else
                {
                    Debug.Log("Already in area");
                }
            }
            else if(_mode == LocatorMode.Interactive)
            {
                if (temp.CanBeEnlighted())
                {
                    if (InputManager.Instance.selectedEquip as EPickUp)
                    {
                        // Drop
                        if ((InputManager.Instance.selectedEquip as EPickUp).pickedVictim != null)
                        {
                            if (GetNode(item).HasFF)
                            {
                                if (GetNode(item).FireFighter.CanPickupVictim())
                                {
                                    InteractiveArea.Add(item);

                                    temp.TurnOnNode(_mode);
                                }
                                else
                                {
                                    InteractiveArea.Add(item);

                                    temp.TurnOnNode(LocatorMode.Deactive);
                                }
                            }
                            else
                            {
                                // Already fill
                                if (GetNode(item).HasObstacle || GetNode(item).HasVictim)
                                {
                                    //Debug.Log(GetNode(item).HasObstacle + "-" + GetNode(item).HasVictim);
                                    InteractiveArea.Add(item);

                                    temp.TurnOnNode(LocatorMode.Deactive);
                                }
                                // Empty
                                else
                                {
                                    InteractiveArea.Add(item);

                                    temp.TurnOnNode(_mode);

                                    
                                }
                            }
                        }
                        // Pickup
                        else 
                        {
                            if (!GetNode(item).HasObstacle)
                            {
                                if (GetNode(item).HasVictim || GetNode(item).HasFF && GetNode(item).FireFighter.HealthCurrent == 0)
                                {
                                    InteractiveArea.Add(item);

                                    temp.TurnOnNode(_mode);
                                }
                                else
                                {
                                    InteractiveArea.Add(item);

                                    temp.TurnOnNode(LocatorMode.Deactive);
                                }
                            }
                            else
                            {
                                if (GetNode(item).HasTeddyBear)
                                {
                                    InteractiveArea.Add(item);

                                    temp.TurnOnNode(_mode);
                                }
                                else
                                {
                                    InteractiveArea.Add(item);

                                    temp.TurnOnNode(LocatorMode.Deactive);
                                }
                            }
                        }
                    }
                    else if (InputManager.Instance.selectedEquip.EType == EquipType.Melee)
                    {
                        if (GetNode(item).HasObstacle && GetNode(item).Obstacle.LocatorEnlightCondition(InputManager.Instance.selectedEquip))
                        {
                            InteractiveArea.Add(item);

                            temp.TurnOnNode(_mode);
                        }
                        else
                        {
                            InteractiveArea.Add(item);

                            temp.TurnOnNode(LocatorMode.Deactive);
                        }
                    }
                    else if (InputManager.Instance.selectedEquip.EType == EquipType.Support)
                    {
                        if (GetNode(item).HasFF || GetNode(item).HasObstacle && GetNode(item).HasVictim)
                        {
                            InteractiveArea.Add(item);
                            temp.TurnOnNode(_mode);
                        }
                        else
                        {
                            InteractiveArea.Add(item);

                            temp.TurnOnNode(LocatorMode.Deactive);
                        }
                    }
                    else if (InputManager.Instance.selectedEquip.EType == EquipType.Extinguish && InputManager.Instance.selectedEquip.EName != EquipName.Smoke_Fan)
                    {
                        if (InputManager.Instance.selectedEquip is EWaterNozzle)
                        {
                            if (GetNode(item).RoomsType == RoomsType.Floor)
                            {
                                InteractiveArea.Add(item);
                                temp.TurnOnNode(_mode);
                            }
                        }
                        else
                        {
                            //Debug.Log("Wrong");
                            if (ThereIsFire(GetNode(item).Locator.ExtinguishNeighboursList))
                            {
                                InteractiveArea.Add(item);

                                temp.TurnOnNode(_mode);
                            }
                            else
                            {
                                InteractiveArea.Add(item);

                                temp.TurnOnNode(LocatorMode.Deactive);
                            }
                        }
                    }
                    else if (InputManager.Instance.selectedEquip.EType == EquipType.Extinguish && InputManager.Instance.selectedEquip.EName == EquipName.Smoke_Fan)
                    {
                        if (ThereIsSmoke(GetNode(item).Locator.ExtinguishNeighboursList))
                        {
                            InteractiveArea.Add(item);

                            temp.TurnOnNode(_mode);
                        }
                        else
                        {
                            InteractiveArea.Add(item);

                            temp.TurnOnNode(LocatorMode.Deactive);
                        }
                    }
                    else
                    {
                        InteractiveArea.Add(item);

                        temp.TurnOnNode(_mode);
                    }
                }
            }
        }
    }


    public void DeactiveNodes(List<Vector2Int> _nodes, LocatorMode _mode)
    {
        if( _mode == LocatorMode.Move)
        {
            foreach (Vector2Int item in _nodes)
            {
                if (IsInsideOfBounds(item.x, item.y))
                {
                    if (areaPath.Contains(WorldGrid[item.y, item.y]))
                    {
                        areaPath.Remove(WorldGrid[item.y, item.y]);
                        WorldGrid[item.x, item.y].TurnOffNode();
                    }
                }
            }
        }
        else if( _mode == LocatorMode.Enlight)
        {
            foreach (Vector2Int item in _nodes)
            {
                if (IsInsideOfBounds(item.x, item.y))
                {
                    enlightArea.Remove(item);

                    // Ignore the extinguish locator
                    if (WorldGrid[item.x, item.y].LocatorMode != LocatorMode.Interactive) 
                    {
                        WorldGrid[item.x, item.y].TurnOffNode();
                    }
                }
            }
        }
        else if (_mode == LocatorMode.Interactive)
        {
            foreach (Vector2Int item in _nodes)
            {
                if (IsInsideOfBounds(item.x, item.y))
                {
                    InteractiveArea.Remove(item);
                    WorldGrid[item.x, item.y].TurnOffNode();
                }
            }

        }
    }

    public void DeactiveNodes(LocatorMode _mode)
    {
        if (_mode == LocatorMode.Move)
        {
            foreach (Node item in areaPath)
            {
                item.TurnOffNode();
            }

            areaPath.Clear();
        }
        else if (_mode == LocatorMode.Enlight)
        {
            foreach (Vector2Int item in enlightArea)
            {
                WorldGrid[item.x, item.y].TurnOffNode();
            }

            enlightArea.Clear();
        }
        else if (_mode == LocatorMode.Interactive)
        {
            // Clear enlightment first
            foreach (Vector2Int item in enlightArea)
            {
                WorldGrid[item.x, item.y].TurnOffNode();
            }

            enlightArea.Clear();

            // Clear extinguish
            foreach (Vector2Int item in InteractiveArea)
            {
                WorldGrid[item.x, item.y].TurnOffNode();
            }
            InteractiveArea.Clear();
        }
    }

    public bool IsInsideOfBounds(int x, int y)
    {
        if (x >= 0 && x < GetGridSize.x && y >= 0 && y < GetGridSize.y)
            return true;
        return false;
    }

    public bool IsInsideOfBounds(Vector2Int _pos)
    {
        if (_pos.x >= 0 && _pos.x < GetGridSize.x && _pos.y >= 0 && _pos.y < GetGridSize.y)
            return true;
        return false;
    }

    public void MovePlayer(Vector2Int _pos)
    {
        SetUndoCurrentFF(true);

        //Undo.LastPos = selectedFF.GridPos;
        //Undo.LastFF = selectedFF.Name;
        UndoActions[selectedFF.Name].LastPos = selectedFF.GridPos;
        UndoActions[selectedFF.Name].LastFF = selectedFF.Name;
        ShowFFRange(false);

        selectedFF.StartMoving(_pos);
        Node n = GetNode(_pos.x, _pos.y);

        UndoActions[selectedFF.Name].LastCost = n.GetCost();

        EventManager.TriggerEvent(EventsID.UpdateUIValues);
    }

    public void UndoMove()
    {
        if (FfIsMoving) return;

        if (!CanUndo) return;

        if (InputManager.Instance.selectedEquip != null)
            InputManager.Instance.EquipDeselected();

        ShowFFRange(false);

        //if(selectedFF.Name != Undo.LastFF)
        //    ChangeSelectedFF(GetFFIndex(Undo.LastFF));

        selectedFF.ForceMovePosition(UndoLastPosition);

        selectedFF.UndoMove(UndoLastCost);
        //selectedFF.currentMovement += Undo.LastCost;

        selectedFF.CheckSmokeNearby();

        SetUndoCurrentFF(false);

        CameraController.Instance.MoveToPosition(selectedFF);

        EventManager.TriggerEvent(EventsID.UpdateUIValues);

        ShowFFRange(true);
    }

    private void FFMoving(bool isMoving)
    {
        FfIsMoving = isMoving;

        ShowFFRange(!isMoving);
    }

    public void ToggleToolTip()
    {
        ActiveTooltip = !ActiveTooltip;
        UIManager.Instance.MouseOverTooltips.DeactiveAll();
    }

    public bool CheckIfOnExitNode(Vector2Int pos)
    {
        if (WorldGrid[pos.x, pos.y].OriginalEnvType == EnvType.Exit ||
            WorldGrid[pos.x, pos.y].EnvType == EnvType.Exit)
            return true;
        return false;

    }

    public void ChangeSavedCivilians(int amount)
    {
        m_CurrentlySavedCivilians += amount;
        UIManager.Instance.UpdateMainObjective();
    }

    public void UpdateOracleFOV()
    {
        if (OracleInTeam)
            OracleFF.CheckSmokeNearby();
    }


    /// <summary>
    /// Return the x value of the cell size, they should to be squared
    /// </summary>
    /// <returns></returns>
    public float GetGridCellSize()
    {
        return GridComponent.cellSize.x;
    }

    public float GetGridCellGap()
    {
        return GridComponent.cellGap.x;
    }

    public bool CheckVictory()
    {
        if (MissionManager.Instance.VictoryCheck(MissionManager.Instance.MissionType))
        {
            AudioManager.Instance.Snapshot(true);

            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.Victory);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);

            return true;
        }

        return false;
    }

    public bool CheckDefeat()
    {
        if (MissionManager.Instance.DefeatCheck())
        {
            AudioManager.Instance.Snapshot(true);

            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.Defeat);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);

            EventManager.TriggerEvent(EventsID.Defeat);
            // DEV: temp
            //Time.timeScale = 0;

            return true;
        }

        return false;
    }

    public void AddExitNode(Vector2Int _exit)
    {
        ExitPoint.Add(GetNode(_exit));

        GameObject go = PoolManager.instance.GetItem(PoolType.ExitFeeback);

        GetNode(_exit).SetExit(go);
    }

    public bool NoNodeOnFire()
    {
        foreach (Node n in WorldGrid)
        {
            if(n.IsOnFire)
            {
                return false;
            }
        }
        return true;
    }

    public void KillFire(List<Vector2Int> _nodes, int _damage)
    {
        foreach (Vector2Int item in _nodes)
        {
            if (IsInsideOfBounds(item.x, item.y))
            {
                GetNode(item).TryToExtinguish(_damage);
            }
        }
    }

    public void SpawnWaterFromNozzle(List<Vector2Int> _nodes)
    {
        foreach (Vector2Int item in _nodes)
        {
            if (IsInsideOfBounds(item.x, item.y) && !GetNode(item).HasFuel && !GetNode(item).HasWater && !GetNode(item).IsOnFire)
                GetNode(item).SpawnLiquid(LiquidType.Water);

            Debug.Log("Depose water");
        }
    }

    public bool ThereIsFire(List <Vector2Int> _nodes)
    {
        foreach (Vector2Int item in _nodes)
        {
            if (GetNode(item).IsOnFire) return true;
        }

        return false;
    }

    public void KillSmoke(List<Vector2Int> _nodes)
    {
        foreach (Vector2Int item in _nodes)
        {
            if (IsInsideOfBounds(item.x, item.y))
                GetNode(item).ClearSmoke(true);
        }
    }

    public bool ThereIsSmoke(List<Vector2Int> _nodes)
    {
        Debug.Log(_nodes.Count);

        foreach (Vector2Int item in _nodes)
        {
            if (GetNode(item).HasSmoke) return true;
        }

        return false;
    }

    #region Turn management
    private void StartGameSession()
    {
        if (GameManager.Instance.SkipCameraOverview)
        {
            CameraEndLevelOverview();
        }
        else
        {
            GameStatus = GameStatus.Pause;

            List<Vector3> victimPos = new List<Vector3>();

            for (int i = 0; i < VictimPoint.Count; i++)
            {
                victimPos.Add(VictimPoint[i].WorldPosition);
            }

            CameraController.Instance.SetCameraPath(victimPos);

            EventManager.TriggerEvent(EventsID.ChangePhase);
            EventManager.TriggerEvent(EventsID.UpdateUIValues);
        }
    }

    public void TriggerGameStart()
    {
        EventManager.TriggerEvent(EventsID.GameStart);

        StartGameSession();

        Time.timeScale = 1;
    }

    public void PauseMenu()
    {
        GameStatus = (GameStatus == GameStatus.Pause) ? GameStatus.Play : GameStatus.Pause;

        Time.timeScale = (GameStatus == GameStatus.Pause) ? 0 : 1;

        bool on = (GameStatus == GameStatus.Pause) ? true : false;
        AudioManager.Instance.Snapshot(on);

        UIManager.Instance.Pause();
    }

    /// <summary>
    /// Move from Pause or Play status w/out the pause menu, use to stop feedbacks like pathfinding 
    /// </summary>
    /// <param name="_flowOn"></param>
    public void ChangeFlow(bool _flowOn)
    {
        GameStatus = (_flowOn) ? GameStatus.Play : GameStatus.Pause;
    }

    public void TutorialPause()
    {
        GameStatus = GameStatus.Pause;
    }

    public void ResumeTutorial()
    {
        GameStatus = GameStatus.Play;

        Time.timeScale = 1;

    }

    private void ChangePhase(TurnPhase nextPhase)
    {
        m_CurrentPhase = nextPhase;

        if (nextPhase == TurnPhase.Player)
        {
            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.ChangeTurnPlayer);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);

            m_WaitForDelay = false;

            foreach (FireFighterBehaviour ff in FFInLevel)
            {
                if (ff.Name == FFName.Oracle)
                    ff.CheckSmokeNearby();

                ff.TakeStandingDamage();
            }

            foreach (FireFighterBehaviour ff in FFInLevel)
            {
                if (ff.Name != FFName.Oracle)
                ff.CheckSmokeNearby();
            }

            levelEventHandler?.NextTurnEventControll(m_CurrentTurns);

            ChangeSelectedFF(GetUsableFF());
        }
        else
            m_WaitForDelay = true;

        if (nextPhase == TurnPhase.AI)
        {
            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.ChangeTurnEnv);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);

            foreach (KeyValuePair<int, RoomHandler> item in RoomsDictionary)
            {
                if (item.Value.hasAlert)
                    item.Value.SetAlertOnRoom(false);
            }
        }

        InputManager.Instance.ClearLastRayHit();
        
        if (nextPhase == TurnPhase.GameEvents)
            EventManager.TriggerEvent(EventsID.EndTurn);

        EventManager.TriggerEvent(EventsID.ChangePhase);

        //Debug.Log("New phase: " + m_CurrentPhase);
    }

    private void ManagePhases()
    {
        if (!m_WaitForDelay) { m_CurrentPhaseDelay = m_PhaseDelay; }

        if (m_CurrentPhaseDelay >= m_PhaseDelay)
        {
            ResetDelay();

            switch (m_CurrentPhase)
            {
                case TurnPhase.GameEvents:
                    if(levelEventHandler != null)
                        levelEventHandler?.StartEventTurnChecks(m_CurrentTurns);
                    else
                        ChangePhase(TurnPhase.VictoryCheck);

                    break;

                case TurnPhase.VictoryCheck:
                    if (CheckVictory())
                    {
                        EventManager.TriggerEvent(EventsID.Victory);
                        m_gameStatus = GameStatus.Pause;
                        m_CurrentPhase = TurnPhase.End;
                        return;
                    }
                    else
                    {
                        CheckDefeat();
                        ChangePhase(TurnPhase.Player);
                    }
                    break;

                case TurnPhase.Player:

                    break;

                case TurnPhase.AI:
                    ExecuteCivAI();
                    ChangePhase(TurnPhase.Env);
                    break;

                case TurnPhase.Env:
                    ExecuteEnv();
                    StartCoroutine(FireSpread());
                    EndTurn();
                    ChangePhase(TurnPhase.GameEvents);
                    break;

                default:
                    break;
            }
        }
        else
            m_CurrentPhaseDelay += Time.deltaTime;
    }

    public void CheckIfGameOver()
    {
        foreach (FireFighterBehaviour ff in FFTeamList)
        {
            if (ff.HealthCurrent > 0)
                return;
        }

        ShowFFRange(false);
        ChangePhase(TurnPhase.VictoryCheck);
    }

    public bool IsATutorial()
    {
        if (GetComponent<IFTutorialManager>() != null)
            return true;
        return false;
    }

    public void CameraEndLevelOverview()
    {
        //if(GetComponent<IFTutorialManager>() == null)
        GameStatus = GameStatus.Play;
    }

    public void EndEventsExecution()
    {
        ChangePhase(TurnPhase.VictoryCheck);
    }

    public bool NoMoreActions()
    {
        for (int i = 0; i < FFTeamList.Length; i++)
        {
            if (FFTeamList[i].CanAct || FFTeamList[i].currentMovement > 0)
                return false;
        }

        return true;
    }

    IEnumerator FireSpread()
    {
        SetFirePresence(false);

        EventManager.TriggerEvent<FlameLevel>(EventsID.FireExpansion, FlameLevel.Pilot);

        yield return new WaitForSeconds(0.2f);

        EventManager.TriggerEvent<FlameLevel>(EventsID.FireExpansion, FlameLevel.Flame);

        yield return new WaitForSeconds(0.2f);

        EventManager.TriggerEvent<FlameLevel>(EventsID.FireExpansion, FlameLevel.Blaze);

        yield return new WaitForSeconds(0.2f);

        EventManager.TriggerEvent(EventsID.FireEvolution);

        yield return new WaitForSeconds(0.2f);

        EventManager.TriggerEvent(EventsID.SmokeCreation);

        if (m_ThereIsMinOneFlame)
        {
            AudioData audio = new AudioData();
            audio.SetEnvSound(EnvSounds.FireBurning);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);
        }

        StopCoroutine(FireSpread());
    }

    public bool EndTurnCheck()
    {
        return true;
    }

    public void EndPlayerPhase()
    {
        ResetUndoActions();
        UIManager.Instance.MouseOverTooltips.DeactiveAll();
        wantToEndTurn = false;
        ShowFFRange(false);
        ChangePhase(TurnPhase.AI);
    }

    private void EndTurn()
    {
        m_CurrentTurns--;
        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.PerfectTiming);
    }

    private void ResetDelay()
    {
        m_CurrentPhaseDelay = 0;
    }

    private void ExecuteEvents()
    {

    }

    private void ExecuteCivAI()
    {

    }

    private void ExecuteEnv()
    {
       
    }

    #endregion

    //public void CheckIfGameOver()
    //{
    //    foreach (FireFighterBehaviour ff in FFTeamList)
    //    {
    //        if (ff.HealthCurrent > 0)
    //            return;
    //    }

    //    ShowFFRange(false);
    //    ChangePhase(TurnPhase.VictoryCheck);
    //}

}