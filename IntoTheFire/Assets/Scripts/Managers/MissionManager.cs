﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MissionTypes { Escape, Rescue, Extinguish}

public enum DifficultyTypes { Easy, Medium, Hard}

public enum RewardTypes { Medals, Stars}

public enum ObjectiveState
{
    None, Completed, Failed
}

public enum SideObjectiveTypes
{
    PerfectTiming,
    YoureUp,
    StrongWilled,
    Hero,
    FireFree,
    OpenUp,
    FreshAir,
    OpenSpace,
    CleaningUp,
    MasterOfIKEA,
    ArmyOfTwo,
    CleanView,
    FireWhat,
    DoNotKnock,
    BlazeOff,
    FlameOff,
    PilotOff,
    TeddyBear
}

public class MissionResults
{
    public string Name;
    public DifficultyTypes Difficulty;
    public int CiviliansSaved;
    public int RoundsLeft;
    public int FFHurt;

    public int TotMedals;
    public int BaseMedals;
    public int ExtraMedals;
    public int TotStars;

    private float Multiplier = 1;

    public List<SideObjective> SideObjectives { get { return MissionManager.Instance.SideObjectives; } }

    public MissionResults GetFinalResults()
    {
        CalculateResults();

        //SaveSystem.Instance.SaveGame();

        return this;
    }

    public int GetMaxPossibleMedals()
    {
        float multiplier = Multiplier + (GameManager.Instance.HealthyMultiplier * 3);
        int value = (int)(BaseMedals * multiplier);

        return value;
    }

    private void CalculateResults()
    {
        MissionManager mm = MissionManager.Instance;
        LevelManager lm = LevelManager.instance;
        GameManager gm = GameManager.Instance;

        Name = GameManager.Instance.LevelLoaded.LevelName;

        Difficulty = mm.Difficulty;

        CiviliansSaved = lm.GetSavedCivilians();

        RoundsLeft = lm.GetCurrentTurns() + 1;

        BaseMedals = mm.MainReward.Amount;

        Multiplier = 1;

        FFHurt = 0;

        foreach (FireFighterBehaviour ff in lm.GetFFTeamList)
        {
            if (ff.HealthStatus == FFHealhStatus.Healthy)
            {
                Multiplier += gm.HealthyMultiplier;
            }
            else if (ff.HealthStatus == FFHealhStatus.Tired)
            {
                Multiplier += gm.TiredMultiplier;
            }
        }

        foreach (FFData ffTotal in gm.GetFFTeamData)
        {
            if (lm.IsInTeamList(ffTotal.FName))
            {
                if (!lm.IsATutorial())
                {
                    if (ffTotal.HealthStatus == FFHealhStatus.Healthy)
                    {
                        ffTotal.HealthStatus = FFHealhStatus.Tired;
                    }
                    else if (ffTotal.HealthStatus == FFHealhStatus.Tired)
                    {
                        ffTotal.HealthStatus = FFHealhStatus.Exhausted;
                    }
                }

                if (!lm.GetFF(ffTotal.FName).IsUsable() && !lm.GetFF(ffTotal.FName).WasEvacuated())
                {
                    FFHurt++;

                    if (!lm.IsATutorial())
                        ffTotal.HealthStatus = FFHealhStatus.Exhausted;
                }
            }
            else
            {
                if (!lm.IsATutorial())
                {
                    if (ffTotal.HealthStatus == FFHealhStatus.Exhausted)
                        ffTotal.HealthStatus = FFHealhStatus.Tired;
                    else if (ffTotal.HealthStatus == FFHealhStatus.Tired)
                        ffTotal.HealthStatus = FFHealhStatus.Healthy;
                }
            }
        }
        
        TotMedals = (int)(BaseMedals * Multiplier);

        ExtraMedals = TotMedals - BaseMedals;

        TotStars = 0;

        Reward[] sideObjs = new Reward[SideObjectives.Count];

        for (int i = 0; i < SideObjectives.Count; i++)
        {
            if (SideObjectives[i].IsCompleted())
            {
                sideObjs[i] = SideObjectives[i].Reward;
            }
        }

        foreach (SideObjective so in SideObjectives)
        {
            if (so.IsCompleted())
            {
                TotStars += so.Reward.Amount;
            }
        }

        // Override current SideObjective with previus saves
        if (GameManager.Instance.LevelSaveData.ContainsKey(GameManager.Instance.LevelLoaded.LevelIndex))
        {
            for (int i = 0; i < SideObjectives.Count; i++)
            {
                if (GameManager.Instance.GetLevelSideObjectiveCompleted(GameManager.Instance.LevelLoaded.LevelIndex, i))
                {
                    MissionManager.Instance.SideObjectives[i].state = ObjectiveState.Completed;
                }
            }
        }

        GameManager.Instance.LevelCompleteSave(TotMedals, sideObjs);

        SaveSystem.Instance.SaveGame();
    }
}

public class MissionManager : Singleton<MissionManager>
{
    public string Name;
    [TextArea]
    public string Description;
    public DifficultyTypes Difficulty;
    public MissionTypes MissionType = MissionTypes.Rescue;
    public int MaxRounds = 10;
    public int MinCiviliansToSave;
    public Sprite Icon;
    [Space]
    public Reward MainReward;
    [Space]
    public List<SideObjective> SideObjectives = new List<SideObjective>();

    private void OnEnable()
    {
        EventManager.StartListening<SideObjectiveTypes>(EventsID.SideObjectiveCheck, CheckSideObjective);
        //EventManager.StartListening(EventsID.Victory, EndLevelCheck);
    }

    private void OnDisable()
    {
        EventManager.StopListening<SideObjectiveTypes>(EventsID.SideObjectiveCheck, CheckSideObjective);
        //EventManager.StopListening(EventsID.Victory, EndLevelCheck);
    }

    public bool DefeatCheck()
    {
        LevelManager lm = LevelManager.instance;
        if (lm.GetCurrentTurns() <= 0)
            return true;

        int UnavailableFFs = 0;

        foreach (FireFighterBehaviour ff in lm.FFInLevel)
        {
            if (ff != null && !ff.IsUsable())
            {
                UnavailableFFs++;
            }
        }

        if (UnavailableFFs == lm.FFInLevel.Count)
        {
            return true;
        }

        return false;
    }

    public bool VictoryCheck(MissionTypes _MissionType)
    {
        LevelManager lm = LevelManager.instance;

        switch (_MissionType)
        {
            case MissionTypes.Escape:
                int UnavailableFFs = 0;
                int FFsOnExitPoint = 0;
                foreach (FireFighterBehaviour ff in lm.FFInLevel)
                {
                    if (ff != null)
                    {
                        if (!ff.IsUsable())
                            UnavailableFFs++;
                        else if (lm.CheckIfOnExitNode(ff.GridPos))
                            FFsOnExitPoint++;
                    }
                }

                if (lm.GetCurrentTurns() == 0 && FFsOnExitPoint >= 1)
                {
                    return true;
                }

                if (UnavailableFFs == lm.FFInLevel.Count)
                {
                    return false;
                }
                else
                {
                    if (FFsOnExitPoint == lm.FFInLevel.Count)
                    {
                        return true;
                    }
                    else
                    {
                        // DEV: at least one escaped check
                        if (UnavailableFFs + FFsOnExitPoint == lm.FFInLevel.Count)
                        {
                            return true;
                        }
                    }
                }
                return false;

            case MissionTypes.Rescue:
                if (LevelManager.instance.GetSavedCivilians() >= MinCiviliansToSave)
                {
                    MissionType = MissionTypes.Escape;
                    return VictoryCheck(MissionType);
                }
                return false;

            case MissionTypes.Extinguish:
                if (LevelManager.instance.NoNodeOnFire())
                    return VictoryCheck(MissionTypes.Escape);
                return false;
            default:
                Debug.Log("Invalid MissionType");
                return false;
        }
    }

    public int GetMaxPossibleMedals()
    {
        float multiplier = 1 + (0.5f * 3);
        int value = (int)(MainReward.Amount * multiplier);

        return value;
    }

    public int GetMaxPossibleMedals(FFName[] _name)
    {
        int count = 0;
        for (int i = 0; i < _name.Length; i++)
            if (_name[i] != FFName.None) count++;
        
        float multiplier = 1 + (0.5f * count);
        int value = (int)(MainReward.Amount * multiplier);

        return value;
    }

    public int GetBaseMedals()
    {
        return MainReward.Amount;
    }

    public void InitializeSideObjectives()
    {
        SideObjectives = new List<SideObjective>();
    }

    private void CheckSideObjective(SideObjectiveTypes type)
    {
        foreach (SideObjective so in SideObjectives)
        {
            if (so.ObjectiveType == type)
                so.CheckAndSetProgressMidLevel();
        }
    }

    public void EndLevelCheck()
    {
        foreach (SideObjective so in SideObjectives)
        {
            if (so.state == ObjectiveState.None)
                so.EndLevelUncompletedCheck();
        }
    }
}

[System.Serializable]
public class SideObjective
{
    public Reward Reward;
    [Space]
    public SideObjectiveTypes ObjectiveType;
    [HideInInspector]
    public ObjectiveState state = ObjectiveState.None;

    public int Value;
    public FFName FirefighterName;

    private int currentValue = 0;
    public string GetDescription
    {
        get
        {
            return ComputeObjectiveDescription();
        }
    }

    public bool IsCompleted()
    {
        if (state == ObjectiveState.Completed)
            return true;
        return false;
    }

    public void SetObjectiveState(ObjectiveState newState)
    {
        state = newState;
    }

    public void CheckAndSetProgressMidLevel()
    {
        switch (ObjectiveType)
        {
            case SideObjectiveTypes.PerfectTiming:
                if (state != ObjectiveState.Failed && LevelManager.instance.GetCurrentTurns() < Value)
                {
                    SetObjectiveState(ObjectiveState.Failed);
                    Debug.Log("FAILED PERFECT TIMING");
                }
                break;

            case SideObjectiveTypes.YoureUp:
                bool ffIsHere = false;
                foreach (FireFighterBehaviour ff in LevelManager.instance.GetFFTeamList)
                {
                    if (ff.Name == FirefighterName)
                        ffIsHere = true;
                }

                if (!ffIsHere)
                {
                    SetObjectiveState(ObjectiveState.Failed);
                    Debug.Log("FAILED YOU'RE UP");
                }
                break;

            case SideObjectiveTypes.StrongWilled:
                if (state != ObjectiveState.Failed)
                {
                    SetObjectiveState(ObjectiveState.Failed);
                    Debug.Log("FAILED STRONG WILLED");
                }
                break;

            case SideObjectiveTypes.Hero:
                if (state != ObjectiveState.Failed)
                {
                    SetObjectiveState(ObjectiveState.Failed);
                    Debug.Log("FAILED HERO");
                }
                break;

            case SideObjectiveTypes.FireFree:
                if (state != ObjectiveState.Completed)
                {
                    currentValue++;

                    if (currentValue >= Value)
                    {
                        SetObjectiveState(ObjectiveState.Completed);
                        Debug.Log("COMPLETED FIRE FREE");
                    }
                }
                break;

            case SideObjectiveTypes.OpenUp:
                if (state != ObjectiveState.Completed)
                {
                    currentValue++;

                    if (currentValue >= Value)
                    {
                        SetObjectiveState(ObjectiveState.Completed);
                        Debug.Log("COMPLETED OPEN UP");
                    }
                }
                break;

            case SideObjectiveTypes.FreshAir:
                if (state != ObjectiveState.Completed)
                {
                    currentValue++;

                    if (currentValue >= Value)
                    {
                        SetObjectiveState(ObjectiveState.Completed);
                        Debug.Log("COMPLETED FRESH AIR");
                    }
                }
                break;

            case SideObjectiveTypes.OpenSpace:
                if (state != ObjectiveState.Completed)
                {
                    currentValue++;

                    if (currentValue >= Value)
                    {
                        SetObjectiveState(ObjectiveState.Completed);
                        Debug.Log("COMPLETED OPEN SPACE");
                    }
                }
                break;

            case SideObjectiveTypes.CleaningUp:
                if (state != ObjectiveState.Completed)
                {
                    currentValue++;

                    if (currentValue >= Value)
                    {
                        SetObjectiveState(ObjectiveState.Completed);
                        Debug.Log("COMPLETED CLEANING UP");
                    }
                }
                break;

            case SideObjectiveTypes.MasterOfIKEA:
                if (state != ObjectiveState.Failed)
                {
                    SetObjectiveState(ObjectiveState.Failed);
                    Debug.Log("FAILED MASTER OF IKEA");
                }
                break;

            case SideObjectiveTypes.ArmyOfTwo:
                if (LevelManager.instance.GetFFTeamList.Length > 2)
                {
                    SetObjectiveState(ObjectiveState.Failed);
                    Debug.Log("FAILED ARMY OF TWO");
                }
                break;

            case SideObjectiveTypes.CleanView:
                if (state != ObjectiveState.Completed)
                {
                    currentValue++;

                    if (currentValue >= Value)
                    {
                        SetObjectiveState(ObjectiveState.Completed);
                        Debug.Log("COMPLETED CLEAN VIEW");
                    }
                }
                break;

            case SideObjectiveTypes.FireWhat:
                if (state != ObjectiveState.Failed)
                {
                    SetObjectiveState(ObjectiveState.Failed);
                    Debug.Log("FAILED FIRE WHAT");
                }
                break;

            case SideObjectiveTypes.DoNotKnock:
                if (state != ObjectiveState.Failed)
                {
                    SetObjectiveState(ObjectiveState.Failed);
                    Debug.Log("FAILED DO NOT KNOCK");
                }
                break;

            case SideObjectiveTypes.BlazeOff:
                if (state != ObjectiveState.Completed)
                {
                    currentValue++;

                    if (currentValue >= Value)
                    {
                        SetObjectiveState(ObjectiveState.Completed);
                        Debug.Log("COMPLETED BLAZE OFF");
                    }
                }
                break;

            case SideObjectiveTypes.FlameOff:
                if (state != ObjectiveState.Completed)
                {
                    currentValue++;

                    if (currentValue >= Value)
                    {
                        SetObjectiveState(ObjectiveState.Completed);
                        Debug.Log("COMPLETED FLAME OFF");
                    }
                }
                break;

            case SideObjectiveTypes.PilotOff:
                if (state != ObjectiveState.Completed)
                {
                    currentValue++;

                    if (currentValue >= Value)
                    {
                        SetObjectiveState(ObjectiveState.Completed);
                        Debug.Log("COMPLETED PILOT OFF");
                    }
                }
                break;

            case SideObjectiveTypes.TeddyBear:
                if (state != ObjectiveState.Completed)
                {
                    SetObjectiveState(ObjectiveState.Completed);
                    Debug.Log("COMPLETED TEDDY BEAR");
                }
                break;

            default:
                break;
        }

        UIManager.Instance.UpdateSideObjectives();
    }

    public void EndLevelUncompletedCheck()
    {
        switch (ObjectiveType)
        {
            case SideObjectiveTypes.PerfectTiming:
                SetObjectiveState(ObjectiveState.Completed);
                break;

            case SideObjectiveTypes.YoureUp:
                SetObjectiveState(ObjectiveState.Completed);
                break;

            case SideObjectiveTypes.StrongWilled:
                SetObjectiveState(ObjectiveState.Completed);
                break;

            case SideObjectiveTypes.Hero:
                SetObjectiveState(ObjectiveState.Completed);
                break;

            case SideObjectiveTypes.FireFree:
                SetObjectiveState(ObjectiveState.Failed);
                break;

            case SideObjectiveTypes.OpenUp:
                SetObjectiveState(ObjectiveState.Failed);
                break;

            case SideObjectiveTypes.FreshAir:
                SetObjectiveState(ObjectiveState.Failed);
                break;

            case SideObjectiveTypes.OpenSpace:
                SetObjectiveState(ObjectiveState.Failed);
                break;

            case SideObjectiveTypes.CleaningUp:
                SetObjectiveState(ObjectiveState.Failed);
                break;

            case SideObjectiveTypes.MasterOfIKEA:
                SetObjectiveState(ObjectiveState.Completed);
                break;

            case SideObjectiveTypes.ArmyOfTwo:
                SetObjectiveState(ObjectiveState.Completed);
                break;

            case SideObjectiveTypes.CleanView:
                SetObjectiveState(ObjectiveState.Failed);
                break;

            case SideObjectiveTypes.FireWhat:
                SetObjectiveState(ObjectiveState.Completed);
                break;

            case SideObjectiveTypes.DoNotKnock:
                SetObjectiveState(ObjectiveState.Completed);
                break;

            case SideObjectiveTypes.BlazeOff:
                SetObjectiveState(ObjectiveState.Failed);
                break;

            case SideObjectiveTypes.FlameOff:
                SetObjectiveState(ObjectiveState.Failed);
                break;

            case SideObjectiveTypes.PilotOff:
                SetObjectiveState(ObjectiveState.Failed);
                break;

            case SideObjectiveTypes.TeddyBear:
                SetObjectiveState(ObjectiveState.Failed);
                break;

            default:
                break;
        }
    }

    public string ComputeObjectiveDescription()
    {
        switch (ObjectiveType)
        {
            case SideObjectiveTypes.PerfectTiming:
                return "Complete the mission before the end of turn " + Value;

            case SideObjectiveTypes.YoureUp:
                return "Complete the mission with " + FirefighterName + " in your team";

            case SideObjectiveTypes.StrongWilled:
                return "Complete the mission without any casualties";

            case SideObjectiveTypes.Hero:
                return "Complete the level without having any civilian going unconcious";

            case SideObjectiveTypes.FireFree:
                return "Completely extinguish " + Value + " fires";

            case SideObjectiveTypes.OpenUp:
                return "Break down " + Value + " doors";

            case SideObjectiveTypes.FreshAir:
                return "Break " + Value + " windows";

            case SideObjectiveTypes.OpenSpace:
                return "Demolish " + Value + " weakened walls";

            case SideObjectiveTypes.CleaningUp:
                return "Clear out " + Value + " squares of debris";

            case SideObjectiveTypes.MasterOfIKEA:
                return "Complete the level without moving any furniture";

            case SideObjectiveTypes.ArmyOfTwo:
                return "Complete the mission with a team of 2 Firefighters";

            case SideObjectiveTypes.CleanView:
                return "Clear out " + Value + " squares of smoke";

            case SideObjectiveTypes.FireWhat:
                return "Complete the mission without completely extinguishing any fires";

            case SideObjectiveTypes.DoNotKnock:
                return "Don't break any doors";

            case SideObjectiveTypes.BlazeOff:
                return "Extinguish " + Value + " Blazes in the level";

            case SideObjectiveTypes.FlameOff:
                return "Extinguish " + Value + " Flames in the level";

            case SideObjectiveTypes.PilotOff:
                return "Extinguish " + Value + " Pilots in the level";

            case SideObjectiveTypes.TeddyBear:
                return "Rescue the Plushie";

            default:
                return "TO IMPLEMENT";
                
        }
    }
}

[System.Serializable]
public class Reward
{
    public RewardTypes RewardType;
    public int Amount;

    public void Execute()
    {
        switch (RewardType)
        {
            case RewardTypes.Medals:
                GameManager.Instance.AddReward(RewardTypes.Medals, Amount);
                break;
            case RewardTypes.Stars:
                GameManager.Instance.AddReward(RewardTypes.Stars, Amount);
                break;
            default:
                break;
        }
    }
}
