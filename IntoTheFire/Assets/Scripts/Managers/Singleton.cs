﻿using UnityEngine;

public class Singleton <T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;

    public static T Instance
    {
        get
        {
            if (instance == null)
                instance = MonoBehaviour.FindObjectOfType<T>();

            if(instance == null)
            {
                GameObject go = new GameObject("Instanced Manager");
                instance = go.AddComponent<T>();
            }

             return instance;
        }
    }

    protected virtual void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
    }
}
