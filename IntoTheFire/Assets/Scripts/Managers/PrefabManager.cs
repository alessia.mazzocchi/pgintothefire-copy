﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InfoWallType
{
    NONE,
    CONCRETE_WALL,
    WALLPAPER_WALL,
    BRIKS_WALL,
    WOOD_WALL,
}

public class PrefabManager : MonoBehaviour
{
    public static PrefabManager instance;

    [SerializeField]
    public List<InfoWallPrefab> WallPrefabsList;

    public GameObject Wall;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }
}

[System.Serializable]
public struct InfoWallPrefab
{
    public GameObject Prefab;

    [SerializeField]
    private InfoWallType m_WallType;
    public InfoWallType WallType
    {
        get
        {
            return m_WallType;
        }
    }
}

