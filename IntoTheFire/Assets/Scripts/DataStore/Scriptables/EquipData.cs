﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EquipStat", menuName = "Database/New Equip Stat")]
public class EquipData : ScriptableObject
{
    public EquipName EName;
    public string UIName;
    public EquipType EType;
    public int Value;
    public int Charges;
    [Header("Extinguisher area")]
    public PropagationEffect PropagationEffect;
    [Tooltip("Define if thw owner is a valid target")]
    public bool EnlightOwner;
    [Tooltip("The range of effect for the extinguisher, like 3x1")]
    public Vector2Int EffectRange;
    [Space]
    public CheckRadius ActivateDirections = CheckRadius.FourDirection;
    [Tooltip("The range of activation int the given direction, 1 min distance")]

    public Vector2Int RangeMinMax = new Vector2Int(1, 1);

    [Space]
    public Sprite Sprite;

    public Sprite AlterantiveSprite;

    public string Description;
}
