﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PromptTypes
{
    Explanation,
    Movement,
    Extinguish,
    PickupCivilian,
    EvacuateCivilian,
    Exit,
    Damage,
    Push
}

public enum TutorialSteps
{
    WaitingToStart,
    WaitingToEnd
}

[CreateAssetMenu(fileName = ("Tutorial event"))]
public class TutorialEvent : ScriptableObject
{
    [Header("Text prompt")]
    [TextArea]
    public string TextPrompt;

    [Header("Trigger nodes")]
    [Tooltip("Tutorial step will be prompted when stepping on these nodes")]
    public Vector2Int[] StartNodes;
    [Tooltip("Tutorial step will be marked completed when proper action is performed in one of these nodes")]
    public Vector2Int[] EndNodes;

    [Header("Prompt type")]
    public PromptTypes PromptType;

    [Header("Set true if prompt needs to start without node trigger")]
    public bool ImmediatelyStart;

    [HideInInspector]
    public TutorialSteps Step;

    private List<GameObject> targetList = new List<GameObject>();

    // First step
    public void WaitToStart()
    {
        // If it needs to start immediately, go to second step
        if (ImmediatelyStart)
        {
            StartPrompt();
        }
        else
        {
            // Set proper nodes ready to check for movement action
            foreach (Vector2Int node in StartNodes)
            {
                LevelManager.instance.GetNode(node).TutorialPromptReady(TutorialSteps.WaitingToStart);
            }
        }
    }

    // Second step
    public void StartPrompt()
    {
        // Set current step as waiting for progress
        Step = TutorialSteps.WaitingToEnd;

        // DEV: Trigger UI text
        TutorialManager.Instance.DescriptionText.text = TextPrompt;
        Debug.Log(TextPrompt);

        if (PromptType == PromptTypes.Explanation)
        {
            // DEV: Simply needs a confirm, maybe a button?
            TutorialManager.Instance.TutorialPromptReady();
        }
        else
        {
            if (name == "T2_Step3(Clone)")
            {
                foreach (Vector2Int node in EndNodes)
                {
                    if (LevelManager.instance.GetSelectedFF.GridPos == node)
                    {
                        EventManager.TriggerEvent(EventsID.NextTutorialPrompt);
                        return;
                    }
                }
            }

            foreach (Vector2Int node in EndNodes)
            {
                LevelManager.instance.GetNode(node).TutorialPromptReady(TutorialSteps.WaitingToEnd, PromptType);
                GameObject target = Instantiate(TutorialManager.Instance.TargetPrefab, LevelManager.instance.GetNode(node).WorldPosition, Quaternion.identity);
                targetList.Add(target);
                Debug.Log("NODE: " + node);
            }

            if (PromptType == PromptTypes.Extinguish)
            {
                // Highlight extinguisher UI button
            }
            else if (PromptType == PromptTypes.Damage)
            {
                // Highlight punch/axe UI button
            }
            else if (PromptType == PromptTypes.PickupCivilian)
            {
                // Highlight pickup UI button
            }
            else
            {
                // Highlight node
            }
        }
    }

    public void EndPrompt()
    {
        foreach (Vector2Int node in EndNodes)
        {
            LevelManager.instance.GetNode(node).TutorialPromptEnded();
        }

        foreach (Vector2Int node in StartNodes)
        {
            LevelManager.instance.GetNode(node).TutorialPromptEnded();
        }


        foreach (GameObject go in targetList)
        {
            Destroy(go);
        }

        TutorialManager.Instance.TutorialPromptEnded();

        Debug.Log("<b>End event</b>");
    }

    public void EndStartingPrompt()
    {
        foreach (Vector2Int node in StartNodes)
        {
            LevelManager.instance.GetNode(node).TutorialPromptEnded();
        }
    }
}
