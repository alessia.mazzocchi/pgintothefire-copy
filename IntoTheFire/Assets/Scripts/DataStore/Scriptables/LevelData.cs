﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "Database/New Level Data")]
public class LevelData : ScriptableObject
{
    public string LevelName;
    public GameObject LevelPrefab;

    [Scene]
    public string LevelScene;

    private LevelManager m_levelManager;
    private MissionManager m_MissionManager;
    public MissionManager MissionManager
    {
        get
        {
            if(m_MissionManager == null)
            {
                m_MissionManager = LevelPrefab.GetComponentInChildren<MissionManager>();
            }
            return m_MissionManager;
        }
    }

    private bool m_Unlocked = false;
    public bool IsUnlocked { get { return m_Unlocked; } }

    [HideInInspector]
    public Vector2Int LevelIndex;

    private void Awake()
    {
        if(LevelPrefab != null)
            m_levelManager = LevelPrefab.GetComponentInChildren<LevelManager>();

        if (LevelPrefab != null && m_MissionManager == null)
            m_MissionManager = LevelPrefab.GetComponentInChildren<MissionManager>();

    }


    public int GetTurns() => MissionManager.MaxRounds;
    
    public bool IsFFTeamLocked() => m_levelManager.FirefightersLocked;
    public FFName[] GetFFLocked() => m_levelManager.FFInMission;

    public bool IsEquipLocked() => m_levelManager.EquipLocked;
    public FFEquipSet[] GetEquipLocked() => m_levelManager.FFEquip;


    public MissionTypes GetMissionType() => MissionManager.MissionType;

    public List<SideObjective> GetSideObjective()
    {
        return MissionManager.SideObjectives;
    }

    public string GetSideObjectiveExtesiveDescription()
    {
        string temp = "";

        for (int i = 0; i < MissionManager.SideObjectives.Count; i++)
        {
            temp += "[ ] " + MissionManager.SideObjectives[i].GetDescription + " \n";
        }

        return temp;
    }

    public int GetSiseObjectiveSize() => MissionManager.SideObjectives.Count;

    public string GetMissionDescription() => MissionManager.Description;

    public int GetMissionCivilian() => MissionManager.MinCiviliansToSave;
    public DifficultyTypes GetDifficultyType() => MissionManager.Difficulty;

    public int GetMaxMedals() 
    {
        if(m_levelManager.FirefightersLocked)
            return MissionManager.GetMaxPossibleMedals(m_levelManager.FFInMission);
        else
            return MissionManager.GetMaxPossibleMedals();

    }

    public int GetBaseMedals() => MissionManager.GetBaseMedals();
    public void UnlockLevel() => m_Unlocked = true;
}

