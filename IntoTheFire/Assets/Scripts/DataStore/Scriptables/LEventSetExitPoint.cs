﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SetExitPoint", menuName = ("LevelEvents/SetExitPoint"))]
public class LEventSetExitPoint : LEventData
{
    public override void ExecuteOnCell(Vector2Int _pos)
    {
        List<Vector2Int> area = GetExecutionRange(_pos);

        foreach (Vector2Int item in area)
        {
            LevelManager.instance.AddExitNode(item);
        }
    }

    protected override bool FoundCellCheck(Node _nd)
    {
        return true;
    }

    protected override bool FoundRoomCheck(int _roomIndex)
    {
        return true;
    }

    protected override string GetAssetName()
    {
        return "SetExitPoint" + Range.x + "x" + Range.y + "_P";
    }
}
