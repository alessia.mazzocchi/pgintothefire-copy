﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ClearSmoke", menuName = ("LevelEvents/ClearSmoke"))]
public class LEventClearSmoke : LEventData
{

    public override void ExecuteOnCell(Vector2Int _pos)
    {
        List<Vector2Int> area = GetExecutionRange(_pos);

        foreach (Vector2Int item in area)
        {
            LevelManager.instance.GetNode(item).ClearSmoke();
        }
    }

    protected override bool FoundCellCheck(Node _nd)
    {
        return true;
    }

    protected override bool FoundRoomCheck(int _roomIndex)
    {
        return true;
    }


    protected override string GetAssetName()
    {
        return "ClearSmoke" + Range.x + "x" + Range.y;
    }
}
