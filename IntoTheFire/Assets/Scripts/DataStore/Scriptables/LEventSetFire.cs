﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "SetFire", menuName = ("LevelEvents/SetFire"))]
public class LEventSetFire : LEventData
{
    [Header("Level rates")]
    [Range(0,1)]
    public float PiloteRate = 1f;
    [Range(0, 1)]
    public float FlameRate = 0;
    [Range(0, 1)]
    public float BlazeRate = 0;

    public override void ExecuteOnCell(Vector2Int _pos)
    {
        List<Vector2Int> area = GetExecutionRange(_pos);

        foreach (Vector2Int item in area)
        {
            LevelManager.instance.GetNode(item).SetOnFire(false, GetFireOdds());
        }

        Debug.Log("Set on fire");

    }

    //public override void ExecuteInRoom(int[] _roooms)
    //{
    //    bool roomFound = false;
    //    int roomCycle = 0;
    //    int roomLimit = _roooms.Length * 2 + _roooms.Length / 2;

    //    bool cellFound = false;

    //    RoomHandler room = null;

    //    /// Check for free cell
    //    while (!roomFound && roomCycle <= roomLimit)
    //    {
    //        int randRoom = Random.Range(0, _roooms.Length);

    //        if (LevelManager.instance.RoomsDictionary[randRoom].IsThereCellToSetFire())
    //        {
    //            room = LevelManager.instance.RoomsDictionary[Random.Range(0, _roooms.Length)];
    //            roomFound = true;
    //        }

    //        roomCycle++;
    //    }

    //    /// if there aren't free cells, stop the execution
    //    if (!roomFound)
    //    {
    //        Debug.Log("No space");
    //        return;
    //    }

    //    Vector2Int pos = Vector2Int.zero;

    //    while (!cellFound)
    //    {
    //        pos = room.AreaNode[(int)Random.Range(0f, (float)room.AreaNode.Count)];

    //        Node nd = LevelManager.instance.GetNode(pos);

    //        if (!nd.HasObstacle && !nd.IsOnFire)
    //            cellFound = true;
    //    }

    //    List<Vector2Int> area = GetExecutionRange(pos);

    //    foreach (Vector2Int item in area)
    //    {
    //        LevelManager.instance.GetNode(item).SetOnFire(false, GetFireOdds());
    //    }
    //}

    protected override bool FoundCellCheck(Node _nd)
    {
        if (/*!_nd.HasObstacle &&*/ _nd.CanBeOnFire()) return true;

        return false;
    }

    protected override bool FoundRoomCheck(int _roomIndex)
    {
        if (LevelManager.instance.RoomsDictionary[_roomIndex].IsThereCellToSetFire()) return true;

        return false;
    }

    private int GetFireOdds()
    {
        float rnd = 0;

        rnd = Random.Range(0f, 1);
        if (rnd < BlazeRate)
            return 2;

        rnd = Random.Range(0f, 1 - BlazeRate);
        if (rnd < FlameRate)
            return 1;

        return 0;
    }

    protected override string GetAssetName()
    {
        return "SetFire_" + Range.x + "x" + Range.y + "_P" + PiloteRate + "_F" + FlameRate + "_B" + BlazeRate;
    }

}
