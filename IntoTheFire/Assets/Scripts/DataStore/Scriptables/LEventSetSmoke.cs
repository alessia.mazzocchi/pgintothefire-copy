﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SetSmoke", menuName = ("LevelEvents/SetSmoke"))]
public class LEventSetSmoke : LEventData
{

    public override void ExecuteOnCell(Vector2Int _pos)
    {
        List<Vector2Int> area = GetExecutionRange(_pos);

        foreach (Vector2Int item in area)
        {
            LevelManager.instance.GetNode(item).SetSmoke();
        }
    }

    protected override bool FoundCellCheck(Node _node)
    {
        if (_node.IsOnFire) return false;
        if (_node.HasSmoke) return false;

        return true;
    }

    protected override bool FoundRoomCheck(int _roomIndex)
    {
        if (LevelManager.instance.RoomsDictionary[_roomIndex].IsThereCellToSetSmoke()) return true;

        return false;
    }

    protected override string GetAssetName()
    {
        return "SetSmoke" + Range.x + "x" + Range.y;
    }
}
