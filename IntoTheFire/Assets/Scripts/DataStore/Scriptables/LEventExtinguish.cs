﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Extinguish", menuName = ("LevelEvents/Extinguish"))]

public class LEventExtinguish : LEventData
{
    [Range(1,3)]
    public int ExtinguishDamage = 1;

    public override void ExecuteOnCell(Vector2Int _pos)
    {
        List<Vector2Int> area = GetExecutionRange(_pos);

        foreach (Vector2Int item in area)
        {
            LevelManager.instance.GetNode(item).TryToExtinguish(ExtinguishDamage);
        }
    }

    protected override bool FoundCellCheck(Node _nd)
    {
        return true;
    }

    protected override bool FoundRoomCheck(int _roomIndex)
    {
        return true;
    }

    protected override string GetAssetName()
    {
        return "Extinguish_Dmg"+ ExtinguishDamage + "_" + Range.x + "x" + Range.y;
    }
}
