﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malee;

public enum LEventType { Cell, Room, Level}

[CreateAssetMenu(fileName = "LEventInfo_", menuName ="LevelEvents/New LEvent Info")]
public class LEventInfo : ScriptableObject
{
    [Reorderable(elementNameProperty = "EventName")]
    public LEventList list = new LEventList();

    public Vector2Int[] GetEventCells(int _turn)
    {
        return list[_turn].Cells;
    }

    public LEvent GetEvent(int _turn)
    {
        return list[_turn];
    }

}

[System.Serializable]
public class LEvent
{
    public string EventName;
    public string EventDestription { get => LData.ActionDescription; }
    public int Turn;

    public LEventType EventEType;
    [Tooltip("Apply the effect to the entire room ore level - Useless for cell")]
    public bool FullCover;
    [Space]
    public int[] Rooms;

    public Vector2Int[] Cells;

    [Space]
    [Min (1)]
    public int Repeats = 1;
    public LEventData LData;
}

[System.Serializable]
public class LEventList : ReorderableArray<LEvent>
{

}
