﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Levent fall", menuName = "LevelEvents/Generate Liquid")]
public class LeventGenerateLiquid : LEventData
{
    public LiquidType LiquidType;

    public override void ExecuteOnCell(Vector2Int _cell)
    {
        List<Vector2Int> area = GetExecutionRange(_cell);

        foreach (Vector2Int item in area)
        {
            LevelManager.instance.GetNode(item).SpawnLiquid(LiquidType);
        }
    }

    protected override bool FoundCellCheck(Node _node)
    {
        if (_node.RoomsType != RoomsType.Wall) return true;
        if (!_node.IsOnFire) return true;

        return false;
    }

    protected override bool FoundRoomCheck(int _roomIndex)
    {
        return true;
    }

    protected override string GetAssetName()
    {
        return "Generate_" + LiquidType + "_" + Range.x + "x" + Range.y;
    }
}
