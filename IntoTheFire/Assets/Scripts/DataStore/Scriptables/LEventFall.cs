﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Levent fall", menuName = "LevelEvents/Fall")]
public class LEventFall : LEventData
{
    //public string FornitureName;
    public GameObject FallObject;

    public bool isCivilian;

    public override void ExecuteOnCell(Vector2Int _cell)
    {
        AudioData audio = new AudioData();

        if (isCivilian)
            audio.SetEnvSound(EnvSounds.CivilianFalling);
        else
            audio.SetEnvSound(EnvSounds.DebrisFall);

        EventManager.TriggerEvent(EventsID.PlayAudio, audio);

        List<Vector2Int> area = GetExecutionRange(_cell);

        foreach (Vector2Int item in area)
        {
             LevelManager.instance.GetNode(item).FallItem(FallObject);
        }
    }

    protected override bool FoundCellCheck(Node _node)
    {
        if (!_node.HasObstacle && _node.RoomsType == RoomsType.Floor) return true;

        return false;
    }

    protected override bool FoundRoomCheck(int _roomIndex)
    {
        if (LevelManager.instance.RoomsDictionary[_roomIndex].IsThereCellForObstalce()) return true;

        return false;
    }

    protected override string GetAssetName()
    {
        return "Fall_" + FallObject.name + "_" + Range.x + "x" + Range.y;
    }
}
