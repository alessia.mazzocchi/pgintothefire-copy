﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LEventData : ScriptableObject
{
    public Vector2Int Range = new Vector2Int(1, 1);
    [Space]
    [Multiline]
    public string ActionDescription;
    public virtual void ExecuteOnLevel(bool _fullcover = false) 
    {
        //int[] rooms = LevelManager.instance.RoomsIDs;

        ExecuteInRoom(LevelManager.instance.RoomsIDs, _fullcover);
    }

    public virtual void ExecuteInRoom(int[] _rooms, bool _fullCover = false) 
    {
        bool roomFound = false;

        bool cellFound = false;

        RoomHandler room = null;

        List<int> roomSet = new List<int>();
        for (int i = 0; i < _rooms.Length; i++)
            roomSet.Add(_rooms[i]);
        
        /// Pick Random room from array
        if (!_fullCover)
        {
            /// Check for free cell
            while (!roomFound && roomSet.Count > 0)
            {
                int rndIndex = Random.Range(0, roomSet.Count);
                int randRoom = roomSet[rndIndex];
                roomSet.RemoveAt(rndIndex);

                if (FoundRoomCheck(randRoom))
                {
                    room = LevelManager.instance.RoomsDictionary[_rooms[(int)Random.Range(0f, _rooms.Length)]];

                    roomFound = true;
                }
            }

            /// if there aren't free cells, stop the execution
            if (!roomFound)
            {
                Debug.Log("No space");
                return;
            }

            Vector2Int pos = Vector2Int.zero;

            int cellLimit = room.Size;
            int limit = 0;

            while (!cellFound && cellLimit > limit)
            {
                pos = room.AreaNode[(int)Random.Range(0f, (float)room.AreaNode.Count - 1)];
                limit++;

                Node nd = LevelManager.instance.GetNode(pos);

                if (FoundCellCheck(nd))
                    cellFound = true;
            }

            if(cellFound)
                ExecuteOnCell(pos);

            cellFound = false;
        }
        /// Execute event to all cells in room;
        else
        {
            for (int i = 0; i < _rooms.Length; i++)
            {
                List<Vector2Int> tmpArea =  LevelManager.instance.GetRoom(_rooms[i]).AreaNode;

                Debug.Log(tmpArea.Count);
                for (int j = 0; j < tmpArea.Count; j++)
                {
                    ExecuteOnCell(tmpArea[j]);
                }
            }
        }
    }

    public virtual void ExecuteOnCell(Vector2Int _pos) { }

    protected List<Vector2Int> GetExecutionRange(Vector2Int _position)
    {
        List<Vector2Int> executionRange = new List<Vector2Int>();

        int X_start = _position.x - Mathf.FloorToInt(Range.x / 2);
        int X_end = _position.x + Mathf.FloorToInt(Range.x / 2);

        int Y_start = _position.y - Mathf.FloorToInt(Range.y / 2);
        int Y_end = _position.y + Mathf.FloorToInt(Range.y / 2); ;

        // Odd
        if(Range.x % 2 != 0 && Range.y % 2 != 0)
        {
            for (int i = X_start; i <= X_end; i++)
            {
                for (int j = Y_start; j <= Y_end; j++)
                {
                    if (LevelManager.instance.IsInsideOfBounds(i, j))
                    {
                        executionRange.Add(new Vector2Int(i, j));
                    }
                }
            }

        }
        // Even
        else if (Range.x % 2 == 0 && Range.y % 2 == 0)
        {
            for (int i = X_start; i < X_end; i++)
            {
                for (int j = Y_start; j < Y_end; j++)
                {
                    if (LevelManager.instance.IsInsideOfBounds(i, j))
                    {
                        executionRange.Add(new Vector2Int(i, j));
                    }
                }
            }
        }
        else if (Range.x % 2 != 0 && Range.y % 2 == 0)
        {
            for (int i = X_start; i <= X_end; i++)
            {
                for (int j = Y_start; j < Y_end; j++)
                {
                    if (LevelManager.instance.IsInsideOfBounds(i, j))
                    {
                        executionRange.Add(new Vector2Int(i, j));
                    }
                }
            }
        }
        else if (Range.x % 2 == 0 && Range.y % 2 != 0)
        {
            for (int i = X_start; i < X_end; i++)
            {
                for (int j = Y_start; j <= Y_end; j++)
                {
                    if (LevelManager.instance.IsInsideOfBounds(i, j))
                    {
                        executionRange.Add(new Vector2Int(i, j));
                    }
                }
            }
        }

        return executionRange;
    }

    protected virtual bool FoundRoomCheck(int _roomIndex)
    {
        return true;
    }

    protected virtual bool FoundCellCheck(Node _node)
    {
        return true;
    }

    public void Rename()
    {
        #if UNITY_EDITOR

        string _newName = GetAssetName();

        string assetPath = AssetDatabase.GetAssetPath(GetInstanceID());
        AssetDatabase.RenameAsset(assetPath, _newName);
        AssetDatabase.SaveAssets();
        #endif
    }

    protected virtual string GetAssetName()
    {
        return "Empty method";
    } 
}
