﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "QuirkData", menuName = "Database/New Quirk Data")]
public class QuirkData : ScriptableObject
{
    public PassiveSkill QuirkName;
    public string UIName;
    public Sprite Sprite;
    public string Description;

    [Header("Medic values")]
    public int HelathBoost;
    public int ChargeBoost;

    public string GetUIName
    {
        get
        {
            return UIName;
        }
    }
}
