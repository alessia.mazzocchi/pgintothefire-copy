﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

[XmlRoot("Team")]
public class FFTeamBase
{
    [XmlArray("FFteam"), XmlArrayItem("FireFighter")]
    public List<FFData> FFteam = new List<FFData>();

    private string newListPath = "Assets/Database/Firefighters/NewCardList.xml";
    private string commonPath = "Assets/Texts/";

    public void NewFFList()
    {
        for (int i = 0; i < 6; i++)
        {
            FFteam.Add(new FFData());
        }

        var serializer = new XmlSerializer(typeof(FFTeamBase));
        var stream = new FileStream(newListPath, FileMode.Create);
        serializer.Serialize(stream, this);
        stream.Close();
    }

    public static List<FFData> LoadTeam(string path)
    {
        var serializer = new XmlSerializer(typeof(FFTeamBase));
        var stream = new FileStream(path, FileMode.Open);

        var container = serializer.Deserialize(stream) as FFTeamBase;
        stream.Close();

        return container.FFteam;
    }

}
