﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PassiveSkill
{
    Strong,
    IgnoreObstacle,
    NoFireDamage,
    MoreHeal,
    SeeThroughSmoke,
    SuperExtinguish,
    None
}

public enum FFName
{
    Forge,
    Atlas,
    Jet,
    Oracle,
    Grace,
    Shusui,
    None
}

[System.Serializable]
public struct SkillTreeData
{
    public int ProgressiveOne;

    public int ProgressiveTwo;

    public int Melee;

    public int Extinguish;

    public int Utility;

    private List<EquipName> equipUnlocked;

    public SkillTreeData(int value)
    {
        ProgressiveOne = 0;
        ProgressiveTwo = 0;
        Melee = 1;
        Extinguish = 1;
        Utility = 1;

        equipUnlocked = new List<EquipName>();
    }

    public SkillTreeData(SkillTreeData _Data)
    {
        ProgressiveOne = _Data.ProgressiveOne;
        ProgressiveTwo = _Data.ProgressiveTwo;
        Melee = _Data.Melee;
        Extinguish = _Data.Extinguish;
        Utility = _Data.Utility;

        equipUnlocked = new List<EquipName>();
    }

    /// <summary>
    /// Set the given Equip as unlocked
    /// </summary>
    /// <param name="_equip"></param>
    public void UnlockEquip(EquipName _equip)
    {
        if (!equipUnlocked.Contains(_equip))
        {
            equipUnlocked.Add(_equip);
        }
    }

    /// <summary>
    /// Return true if the equip has been unlocked
    /// </summary>
    /// <param name="_equip"></param>
    /// <returns></returns>
    public bool IsEquipUnlock(EquipName _equip) => equipUnlocked.Contains(_equip) ? true : false;

}

[CreateAssetMenu(fileName = "FFData_", menuName = "Database/New FF Stat")]
[System.Serializable]
public class FFData : ScriptableObject
{
    public FFName FName;
    public string FFRole;
    public FFHealhStatus HealthStatus;

    public QuirkData quirk;
    [HideInInspector]
    public List<SPEquipUpgrade> hiddenQuirk;
    public SkillTreeData SkillTree;

    public int baseMovement;
    public int baseHealth;

    public int medikitMovementBoost;

    public Sprite BaseSprite;


    public void AddNewHiddenQuirk(SPEquipUpgrade _upgrade)
    {
        SPEquipUpgrade tmp = Instantiate(_upgrade);
        hiddenQuirk.Add(tmp);
    }
}