﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelSaveData 
{
    public int MedalsEarned;

    public bool[] SideObjectiveAchived;

    public LevelSaveData(int _medals, bool[] _sideObj) 
    {
        MedalsEarned = _medals;
        SideObjectiveAchived = _sideObj;
    }

    public int GetSideObjAchived
    {
        get
        {
            int count = 0;
            for (int i = 0; i < SideObjectiveAchived.Length; i++)
            {
                if (SideObjectiveAchived[i])
                    count++;
            }

            return count;
        }
    }
}

[System.Serializable]
public class CharacterSave
{
    public FFHealhStatus HealthStatus;
    public SkillTreeData SkillTree;

    public CharacterSave(FFHealhStatus _healthStatus, SkillTreeData _skillTree)
    {
        HealthStatus = _healthStatus;
        SkillTree = _skillTree;
    }
}

[System.Serializable]
public class Save
{
    public int Medals;

    public int Stars;

    public Dictionary<FFName, CharacterSave> AllFFData = new Dictionary<FFName, CharacterSave>();

    public List<bool> AllLevelsUnlockStatus = new List<bool>();

    /// <summary>
    /// X = District progression, Y = Level progression
    /// </summary>
    public int ProgressionX, ProgressionY;

    public bool ShowTutorial;

    //public Dictionary<Vector2Int, LevelSaveData> LevelSaveData = new Dictionary<Vector2Int, LevelSaveData>();

    public int[] LevelProgressionX, LevelProgressionY;
    public LevelSaveData[] LevelSavesData;

}
