﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : Singleton<SaveSystem>
{
    // Saves game
    public void SaveGame()
    {
        Save save = CreateSaveObject();

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");

        //Debug.Log(Application.persistentDataPath);
        bf.Serialize(file, save);
        file.Close();

        Debug.Log("Game Saved");
    }

    // Checks if a save file exists
    public bool HasASaveFile()
    {
        return File.Exists(Application.persistentDataPath + "/gamesave.save");
    }

    // Fetches, deserializes and returns a save file (given that it exists)
    public Save GetSave()
    {
        Save save = new Save();

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);

        save = (Save)bf.Deserialize(file);
        file.Close();

        return save;
    }

    // Creates a save file and saves data (DEV: script data to save here)
    private Save CreateSaveObject()
    {
        Save save = new Save();

        GameManager gm = GameManager.Instance;

        foreach (FFData ff in gm.GetFFTeamData)
        {
            save.AllFFData.Add(ff.FName, new CharacterSave(ff.HealthStatus, ff.SkillTree));
        }

        save.Medals = gm.Medals;
        save.Stars = gm.Stars;

        save.ProgressionX = gm.Progression.x;
        save.ProgressionY = gm.Progression.y;

        SerializeLevelSaveData(out save.LevelProgressionX, out save.LevelProgressionY, out save.LevelSavesData);

        save.ShowTutorial = gm.ShowTutorial;
        return save;
    }

    // Deletes the save file (if found)
    public void DeleteSave()
    {
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            File.Delete(Application.persistentDataPath + "/gamesave.save");
        }
    }

    public void SerializeLevelSaveData(out int[] ProgressionX, out int[] ProgressionY, out LevelSaveData[] data)
    {
        int SaveLenght = GameManager.Instance.LevelSaveData.Count;
        ProgressionX = new int[SaveLenght];
        ProgressionY = new int[SaveLenght];
        data = new LevelSaveData[SaveLenght];

        int index = 0;
        foreach (KeyValuePair<Vector2Int,LevelSaveData> item in GameManager.Instance.LevelSaveData)
        {
            ProgressionX[index] = item.Key.x;
            ProgressionY[index] = item.Key.y;
            data[index] = item.Value;

            index++;
        }
    }

    public Dictionary<Vector2Int, LevelSaveData> DeserializeLevelSaveData(Save _save)
    {
        Dictionary<Vector2Int, LevelSaveData> s = new Dictionary<Vector2Int, LevelSaveData>();

        for (int i = 0; i < _save.LevelSavesData.Length; i++)
        {
            Vector2Int elementIndex = new Vector2Int(_save.LevelProgressionX[i], _save.LevelProgressionY[i]);

            s.Add(elementIndex, _save.LevelSavesData[i]);
        }

        return s;
    }
}
