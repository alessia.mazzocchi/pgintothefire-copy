﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSettings : MonoBehaviour
{
    [SerializeField]
    private Toggle toggle;
    [SerializeField]
    private AudioSource myAudio;

    public void Awake()
    {
        // Check if the PlayerPrefs has a cached setting for the “music” key. If there is no value there, it creates a key-value pair for the music key with a value of 1.
        // It also sets the toggle to on and enables the AudioSource. This will be run the first time the player runs the game.
        // The value of 1 is used because you cannot store a Boolean (but you can use 0 as false and 1 as true).
        if (!PlayerPrefs.HasKey("music"))
        {
            PlayerPrefs.SetInt("music", 1);
            toggle.isOn = true;
            myAudio.enabled = true;
            PlayerPrefs.Save();
        }
        // This checks the “music” key saved in the PlayerPrefs.
        // If the value is set to 1, the player had music on, so it enables the music and sets the toggle to on.
        // Otherwise, it sets the music to off and disables the toggle.
        else
        {
            if (PlayerPrefs.GetInt("music") == 0)
            {
                myAudio.enabled = false;
                toggle.isOn = false;
            }
            else
            {
                myAudio.enabled = true;
                toggle.isOn = true;
            }
        }
    }

    public void ToggleMusic()
    {
        if (toggle.isOn)
        {
            PlayerPrefs.SetInt("music", 1);
            myAudio.enabled = true;
        }
        else
        {
            PlayerPrefs.SetInt("music", 0);
            myAudio.enabled = false;
        }
        PlayerPrefs.Save();
    }
}
