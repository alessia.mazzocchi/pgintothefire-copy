﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Node : IHeapItem<Node>
{
    private bool m_WaitingForTutorialStart;
    private bool m_WaitingForTutorialProgress;
    private PromptTypes m_TutorialType;

    public GroundType GroundType;
    public RoomsType RoomsType;
    public EnvType EnvType;

    public EnvType OriginalEnvType;

    private LocatorBehaviour m_Locator;
    public LocatorBehaviour Locator
    {
        get
        {
            if(m_Locator == null)
            {
                GameObject go = PoolManager.instance.CreateNewItem(PoolType.Locator);
                m_Locator = go.GetComponent<LocatorBehaviour>();
                m_Locator.gameObject.SetActive(true);
                m_Locator.IsActive = false;

                //if (RoomsType != RoomsType.Floor)
                //    Locator.SetWallLocator();
            }

            return m_Locator;
        }
    }

    public LocatorMode LocatorMode { get => Locator.Mode; }

    public SmokeBehaviour Smoke;
    public FlameBehaviour Flame;
    public LiquidBehaviour Liquid;
    public Victim Victim;

    public Obstacle Obstacle;
    public FireFighterBehaviour FireFighter;
    public GameObject ViewFeedback;
    public bool HasSmoke
    { get { return Smoke != null; } }

    public bool IsOnFire;

    public Vector2Int GridPosition;
    public Vector3 WorldPosition;
    public byte RoomID;

    private bool m_HasNearbyBrokenWindow;
    public bool HasNearbyBrokenWindow { get { return m_HasNearbyBrokenWindow; } }

    public int GetFireDamage
    {
        get
        {
            return GameManager.Instance.GetFireDamage(Flame.Level);
        }
    }

    private bool m_HasFuel;
    public bool HasFuel
    {
        get
        {
            if (Liquid != null && Liquid.Type == LiquidType.Fuel)
                m_HasFuel = true;
            else
                m_HasFuel = false;

            return m_HasFuel;
        }
    }

    private bool m_HasWater;
    public bool HasWater
    {
        get
        {
            if (Liquid != null && Liquid.Type == LiquidType.Water)
                m_HasWater = true;
            else
                m_HasWater = false;

            return m_HasWater;
        }
    }

    private bool m_HasObstacle;

    public bool HasObstacle 
    { 
        get 
        {
            if (Obstacle != null && Obstacle.isActive) return true;
            if (Obstacle != null && RoomsType == RoomsType.Destructible_Wall && Obstacle is WallObs && (Obstacle as WallObs).Destructible) return true;
            if (Obstacle != null && !Obstacle.isActive) return false;
            if (Obstacle == null) return false;

            return false;
        }
    }

    public bool HasVictim
    {
        get
        {
            if (Victim != null) return true;

            return false;
        }
    }

    public bool HasExhaustedFF
    {
        get
        {
            if (FireFighter != null && !FireFighter.IsUsable()) return true;

            return false;
        }
    }

    public bool HasTeddyBear
    {
        get
        {
            if(Obstacle != null)
                if (Obstacle.TryGetComponent<TeddyBear>(out TeddyBear td))
                    return true;

            return false;
        }
    }

    public bool HasFF
    {
        get => (FireFighter != null) ? true : false;
    }

    //public bool HasObstacle { get { return Obstacle != null && Obstacle.isActive; } }
    
    #region Pathfinding

    ////public Vector3 WorldPosition;
    //public int GridX;
    //public int GridY;

    private Node Parent;

    int m_HeapIndex;

    // DEV: cost needed for pathfinding (BSF)
    private int cost;
    private int m_TimesChecked;

    public int GetTimesChecked { get { return m_TimesChecked; } }
    public void SetTimesChecked(int i) { m_TimesChecked = i; }

    public int gCost;
    public int hCost;
    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }

    public Node(Vector3 _worldPos, Vector2 _gridPos)
    {
        WorldPosition = _worldPos;
        GridPosition.x = Mathf.RoundToInt(_gridPos.x);
        GridPosition.y = Mathf.RoundToInt(_gridPos.y);
    }

    #region Heap opt

    public int HeapIndex
    {
        get { return m_HeapIndex; }
        set { m_HeapIndex = value; }
    }

    public int CompareTo(Node nodeToCompare)
    {
        int compare = fCost.CompareTo(nodeToCompare.fCost);
        if (compare == 0)
        {
            compare = hCost.CompareTo(nodeToCompare.hCost);
        }

        return -compare;
    }

    #endregion

    #endregion

    public void SetNode(GroundType _ground, RoomsType _rooms, EnvType _env)
    {
        GroundType = _ground;
        RoomsType = _rooms;
        EnvType = _env;
    }

    public void SetExit(GameObject _go)
    {
        OriginalEnvType = EnvType.Exit;
        ViewFeedback = _go;
        ViewFeedback.transform.position = WorldPosition;
        ViewFeedback.SetActive(true);
    }

    public void SetObstaclePresence(bool thereIsObstacle)
    {
        m_HasObstacle = thereIsObstacle;
    }

    public void SetLocator( )
    {
        Locator.SetLocator(GridPosition);
        Locator.transform.position = WorldPosition;
    }

    public void TurnOnNode(LocatorMode _mode)
    {
        // Extinguish mode has high priority on Enlight so ignore this instruciton
        if(_mode == LocatorMode.Enlight)
        {
            if (Locator.IsActive && LocatorMode == LocatorMode.Interactive)
                return;
        }

        Locator.Active(_mode);
    }

    //public void TurnOnNode(LocatorMode _mode, Color _color)
    //{
    //    // Extinguish mode has high priority on Enlight so ignore this instruciton
    //    if (_mode == LocatorMode.Enlight)
    //    {
    //        if (Locator.gameObject.activeInHierarchy && LocatorMode == LocatorMode.Interactive)
    //            return;
    //    }

    //    Locator.Active(_mode);
    //}

    public void EnlightPlayerNode(bool _active)
    {
        //if (Locator.IsActive && LocatorMode == LocatorMode.Interactive)
        //    return;

        Locator.SetPlayerColor(_active);
    }


    public void TurnOffNode()
    {
        Locator.Disactive();
    }

    public void TurnOffLocatorSide()
    {
        Locator.TurnOffSides();
    }

    public bool IsAvailableToCivAndSmoke()
    {
        if (RoomsType == RoomsType.Wall || RoomsType == RoomsType.Door || RoomsType == RoomsType.DoorHeavy || RoomsType == RoomsType.Window) return false;

        return true;
    }

    public bool IsWalkable()
    {
        /// Quirk that ignore obstacle
        if ((HasObstacle) && LevelManager.instance.GetSelectedFF.Quirk.QuirkName != PassiveSkill.IgnoreObstacle) return false;

        /// Quirk that ignore obstacle can't pass throught closed walls
        if ((HasObstacle || HasVictim) && LevelManager.instance.GetSelectedFF.Quirk.QuirkName == PassiveSkill.IgnoreObstacle && RoomsType == RoomsType.Wall) return false;
        if ((HasObstacle || HasVictim) && LevelManager.instance.GetSelectedFF.Quirk.QuirkName == PassiveSkill.IgnoreObstacle && RoomsType == RoomsType.Destructible_Wall) return false;

        if (RoomsType == RoomsType.Wall || RoomsType == RoomsType.Door || RoomsType == RoomsType.DoorHeavy || RoomsType == RoomsType.Window) return false;

        if (HasSmoke && (!Smoke.SeeThrough && Parent.HasSmoke && !Parent.Smoke.SeeThrough && !Parent.Smoke.OracleIsAround()) && LevelManager.instance.GetSelectedFF.Quirk.QuirkName != PassiveSkill.SeeThroughSmoke) return false;

        if (HasSmoke && LevelManager.instance.GetSelectedFF.Quirk.QuirkName != PassiveSkill.SeeThroughSmoke && (!Smoke.SeeThrough && Parent.HasSmoke)) return false;

        return true;
    }

    public bool IsWalkable(FireFighterBehaviour ff)
    {
        /// Quirk that ignore obstacle
        if ((HasObstacle) && ff.Quirk.QuirkName != PassiveSkill.IgnoreObstacle) return false;

        if (HasObstacle && Obstacle.CantJumpOver) return false;

        /// Quirk that ignore obstacle can't pass throught closed walls
        if ((HasObstacle || HasVictim) && ff.Quirk.QuirkName == PassiveSkill.IgnoreObstacle && RoomsType == RoomsType.Wall) return false;
        if ((HasObstacle || HasVictim) && ff.Quirk.QuirkName == PassiveSkill.IgnoreObstacle && RoomsType == RoomsType.Destructible_Wall) return false;

        if (HasSmoke && HasVictim) return false;

        if (RoomsType == RoomsType.Wall || RoomsType == RoomsType.Door || RoomsType == RoomsType.DoorHeavy || RoomsType == RoomsType.Window) return false;

        if (HasSmoke && ff.Quirk.QuirkName == PassiveSkill.SeeThroughSmoke) return true;

        if (HasSmoke && ff.Quirk.QuirkName != PassiveSkill.SeeThroughSmoke && Smoke.OracleIsAround()) return true;

        if (HasSmoke && ff.Quirk.QuirkName != PassiveSkill.SeeThroughSmoke && (!Smoke.SeeThrough && Parent.HasSmoke && !Parent.HasFF && !Parent.Smoke.SeeThrough && !Parent.Smoke.OracleIsAround())) return false;

        if (Parent.HasSmoke && !Parent.HasFF && !Parent.Smoke.OracleIsAround()) return false;

        return true;
    }

    public bool IsVisibleToOracle()
    {
        if (RoomsType == RoomsType.Wall || RoomsType == RoomsType.Destructible_Wall || 
            RoomsType == RoomsType.Door || RoomsType == RoomsType.DoorHeavy || RoomsType == RoomsType.Window) return false;

        return true;
    }



    public void SpawnVictim()
    {
        Victim = PoolManager.instance.CreateNewItem(PoolType.Victim).GetComponent<Victim>();
        Victim.transform.position = WorldPosition;
        Victim.RandomizeRotation();
        Victim.go.SetActive(true);
        LevelManager.instance.VictimsList.Add(Victim);
    }

    public void SpawnTeddyBear()
    {
        Obstacle = PoolManager.instance.CreateNewItem(PoolType.TeddyBear).GetComponent<TeddyBear>();
        Obstacle.transform.position = WorldPosition;
        Obstacle.gameObject.SetActive(true);
    }

    public void TryToSetOnFire(FlameBehaviour parentFlame)
    {
        if (RoomsType == RoomsType.Wall || RoomsType == RoomsType.Door || RoomsType == RoomsType.Window || RoomsType == RoomsType.DoorHeavy) 
        {
            //Debug.Log("Tile was a wall/door/window");
            return;
        }
        else if (HasWater)
        {
            int damage;

            switch (parentFlame.Level)
            {
                case FlameLevel.Pilot:
                    damage = 0;
                    break;
                case FlameLevel.Flame:
                    damage = 1;
                    break;
                case FlameLevel.Blaze:
                    damage = 2;
                    break;
                default:
                    damage = 0;
                    break;
            }

            ReduceWater(damage);
        }
        else if (IsOnFire)
        {
            //Debug.Log("Reduces turns to evolve");
            Flame.ReduceTurnsToEvolve(parentFlame.currentStats.SpreadCountDiminisher);
        }
        else
        {
            SetOnFire();

            //Debug.Log("Parent flame in " + parentFlame.GridPos + " cast fire on " + GridPosition);
            //if (Obstacle != null && Obstacle.isActive)
                // Obstacle.SetOnFire(); DEV: obs on fire
        }

    }

    public void SetOnFire(bool immediatlyAdd = false, int lv = 0)
    {
        if (!CanBeOnFire())
        {
            Debug.Log("Tile can't be on fire");
            return;
        }

        if (Smoke != null)
            ClearSmoke();

        Flame = PoolManager.instance.CreateNewItem(PoolType.Flame).GetComponent<FlameBehaviour>();
        Flame.GridPos = GridPosition;
        Flame.transform.position = WorldPosition;

        // Set the level flame via enum index
        Flame.Level = (FlameLevel)System.Enum.ToObject(typeof(FlameLevel), lv);

        Flame.gameObject.SetActive(true);
        IsOnFire = true;
        //Debug.Log("new flame");

        
    }

    public bool CanBeEnlighted() => (RoomsType == RoomsType.Floor || 
                                     RoomsType == RoomsType.Destructible_Wall ||
                                     RoomsType == RoomsType.Door ||
                                     RoomsType == RoomsType.DoorHeavy ||
                                     RoomsType == RoomsType.Window) ? true : false;

    public bool CanBeOnFire()
    {
        if (RoomsType == RoomsType.Wall) { Debug.Log("Room Wall"); return false; }
        if (RoomsType == RoomsType.Door) { Debug.Log("Room Door"); return false; }
        if (RoomsType == RoomsType.DoorHeavy) { Debug.Log("Room Door"); return false; }

        if (IsOnFire) { Debug.Log("Already on fire: " + GridPosition); return false; }
        return true;
    }

    // Must be called from the equipement
    public void TryToExtinguish(int damage)
    {
        if (IsOnFire && Flame.TryToExtinguish(damage))
        {
            Extinguish();
        }
    }

    private void Extinguish()
    {
        // TUTORIAL ONLY
        if (m_WaitingForTutorialProgress && m_TutorialType == PromptTypes.Extinguish)
        {
            Debug.Log("Tutorial extinguish");
            CallForTutorialNextStep();
        }

        Flame.Extinguish();
        Flame = null;
        IsOnFire = false;
    }

    public void FallItem(GameObject _obstacle)
    {
        Victim victim = _obstacle.GetComponent<Victim>();

        if (victim)
        {
            Debug.Log("Fall event");
            //victim.Load();
            Victim obs = GameObject.Instantiate(_obstacle).GetComponent<Victim>();

            obs.Fall(WorldPosition);
            ObstacleEnter(obs.GetComponent<Obstacle>());
            return;
        }


        if (!HasObstacle)
        {
            if (RoomsType != RoomsType.Floor) return;

            Obstacle obs = GameObject.Instantiate(_obstacle).GetComponent<Obstacle>();
       
            obs.transform.rotation = new Quaternion(obs.transform.rotation.x, Random.rotation.y, obs.transform.rotation.z, obs.transform.rotation.w);

            obs.Load();
            ObstacleEnter(obs);

            obs.StartFall(WorldPosition);
        }
    }


    public void SetSmoke()
    {
        if (Smoke != null || IsOnFire) return;

        Smoke = PoolManager.instance.CreateNewItem(PoolType.Smoke).GetComponent<SmokeBehaviour>();
        Smoke.GridPos = GridPosition;
        Smoke.transform.position = WorldPosition;

        Smoke.gameObject.SetActive(true);
    }

    public void ClearSmoke(bool ffAction = false)
    {
        if (Smoke != null)
        {
            Smoke.Erase(ffAction);
            Smoke = null;
        }
    }

    public void SignalBrokenWindow()
    {
        m_HasNearbyBrokenWindow = true;

        if (HasSmoke)
            ClearSmoke(true);
    }

    public void SpawnLiquid(LiquidType type)
    {
        if (IsOnFire) return;

        if(Liquid != null)
        {
            Liquid.gameObject.SetActive(false);
            Liquid.transform.DOScale(Vector3.zero, 0.5f);
        }
        float delay = Random.Range(0, 0.35f);
        float time = Random.Range(0.4f, 0.6f);
        if (type == LiquidType.Fuel)
        {
            Liquid = PoolManager.instance.CreateNewItem(PoolType.Fuel).GetComponent<LiquidBehaviour>();
            Liquid.transform.DOScale(Vector3.zero, time).From().SetDelay(delay);

        }
        else if (type == LiquidType.Water)
        {
            Liquid = PoolManager.instance.CreateNewItem(PoolType.Water).GetComponent<LiquidBehaviour>();
            Liquid.transform.DOScale(Vector3.zero, time).From().SetDelay(delay);
        }

        Liquid.GridPos = GridPosition;
        Liquid.transform.position = WorldPosition;

        Liquid.gameObject.SetActive(true);
    }

    public void UseFuel()
    {
        Liquid.DeleteLiquid();
        Liquid = null;
    }

    public void ReduceWater(int damage)
    {
        Liquid.TakeDamage(damage);

        if (Liquid.DeathCheck())
        {
            Liquid.DeleteLiquid();
            Liquid = null;
        }
    }

    public void OnEntityExit(FireFighterBehaviour _item)
    {
        FireFighter = null;

        EnlightPlayerNode(false);
        if (OriginalEnvType == EnvType.Exit)
            EnvType = EnvType.Exit;
        else
            EnvType = EnvType.None;
    }

    public void OnEntityEnter(FireFighterBehaviour _item)
    {
        // TUTORIAL ONLY
        if (m_WaitingForTutorialStart || (m_WaitingForTutorialProgress && m_TutorialType == PromptTypes.Movement || m_TutorialType == PromptTypes.Exit))
            CallForTutorialNextStep();

        FireFighter = _item;
        EnlightPlayerNode(true);
        EnvType = EnvType.Obstacle;

        if (Smoke != null)
            LevelManager.instance.SetUndoCurrentFF(false);
        Debug.Log(EnvType);
        if (OriginalEnvType == EnvType.Exit && _item.HasVictim())
        {
            Debug.Log("FF on node");
            _item.RemoveVictimsCarriedOnExitPoint(GridPosition);
        }

        _item.CheckSmokeNearby();
    }

    public void ObstacleEnter(Obstacle _obstacle)
    {
        //Victim case
        if (_obstacle is Victim)
        {
            // Already has a victim, move to the nearest node
            if (HasVictim || HasFF && !FireFighter.CanPickupVictim())
            {
                Node n = null;
                Pathfinding path = new Pathfinding();
                Vector2Int checkPos = GridPosition;
                bool emptyNode = false;

                while (!emptyNode)
                {
                    n = path.GetFirstAvailableNode_Civ(GridPosition, 1);

                    if (!n.HasFF)
                    {
                        emptyNode = true;
                    }
                    else if(n.HasFF && n.FireFighter.CanPickupVictim())
                    {
                        emptyNode = true;
                    }
                    else if (n.HasFF && !n.FireFighter.CanPickupVictim())
                    {
                        checkPos = n.GridPosition;
                    }
                }
               
                if (n.HasFF)
                {
                    (_obstacle as Victim).PickUpFallVictim(n.FireFighter);
                }
                else
                {
                    n.ObstacleEnter(_obstacle);
                }
            }

            // Has free FF on node
            else if (HasFF)
            {
                (_obstacle as Victim).PickUpFallVictim(FireFighter);
            }
            // Empty
            else
            {
                Victim = _obstacle as Victim;
                Victim.GridPos = GridPosition;
            }

            LevelManager.instance.VictimsList.Add(Victim);
        }
        // Fornuiture case
        else
        {
            Obstacle = _obstacle;
            Obstacle.GridPos = GridPosition;
            Debug.Log("Obstacle enter");
            //if (HasFF)
            //{
            //    FireFighter.TakeDebriesDamage(Obstacle.FallDamage);
            //}
            //if (HasVictim)
            //{
            //    Victim.TakeDamange(Obstacle.FallDamage);
            //}
        }
    }

    public void ObstacleExit(Obstacle _obstacle = null)
    {
        // TUTORIAL ONLY
        if (m_WaitingForTutorialProgress && m_TutorialType == PromptTypes.EvacuateCivilian && HasVictim && LevelManager.instance.CheckIfOnExitNode(GridPosition))
            CallForTutorialNextStep();

        if (_obstacle is Victim)
        {
            Victim = null;
        }
       
        Obstacle = null;
    }

    public void NewExitPoint()
    {

    }

    public int GetCost()
    {
        return cost;
    }

    public void SetCost(int newCost)
    {
        cost = newCost;
    }

    public Node GetParent()
    {
        return Parent;
    }

    public void SetParent(Node newParent)
    {
        Parent = newParent;
    }

    public void FallAnimationDamages() 
    {
        if (HasFF)
        {
            FireFighter.TakeDebriesDamage(Obstacle.FallDamage);
        }
        if (HasVictim)
        {
            Victim.TakeDamange(Obstacle.FallDamage);
        }

        CameraController.Instance.DebriesShake();
    }

    public void TutorialPromptReady(TutorialSteps step, PromptTypes type = PromptTypes.Explanation )
    {
        switch (step)
        {
            case TutorialSteps.WaitingToStart:
                m_WaitingForTutorialStart = true;
                break;
            case TutorialSteps.WaitingToEnd:
                m_TutorialType = type;
                m_WaitingForTutorialProgress = true;
                break;
            default:
                break;
        }

        if (step == TutorialSteps.WaitingToEnd)
        {
            switch (type)
            {
                case PromptTypes.PickupCivilian:
                    Victim.TutorialPromptReady(type);
                    break;
                case PromptTypes.Damage:
                case PromptTypes.Push:
                    Obstacle.TutorialPromptReady(type);
                    break;
                default:
                    break;
            }
        }
    }

    private void CallForTutorialNextStep()
    {
        EventManager.TriggerEvent(EventsID.NextTutorialPrompt);
    }

    public void TutorialPromptEnded()
    {
        if (m_TutorialType == PromptTypes.PickupCivilian) 
        {
            if (Victim == null)
                GameObject.Find("Victim")?.GetComponent<Victim>()?.TutorialPromptEnded();
            else
                Victim.TutorialPromptEnded();
        }
        else if(m_TutorialType == PromptTypes.Damage || m_TutorialType == PromptTypes.Push)
        {
            if (Obstacle == null)
                GameObject.Find("Obstacle")?.GetComponent<Obstacle>()?.TutorialPromptEnded();
            else
                Obstacle.TutorialPromptEnded();
        }

        m_WaitingForTutorialStart = false;
        m_WaitingForTutorialProgress = false;
    }
}