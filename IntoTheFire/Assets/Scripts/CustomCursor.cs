﻿using UnityEngine;

public enum CursorStyle
{
    Default,
    Hand,
    Block
}

public static class CustomCursor
{
    /// <summary>
    /// Path
    /// </summary>
    public const string DEFAULT_CURSOR = "CursorSprites/Cursor_Default";
    public const string AXE_CURSOR = "CursorSprites/Cursor_Axe";
    public const string BLOCK_CURSOR = "CursorSprites/Cursor_Block";
    public const string HAND_CURSOR = "CursorSprites/Cursor_Hand";

    /// <summary>
    /// Textures
    /// </summary>
    private static Texture2D defaultTexture;
    private static Texture2D axeTexture;
    private static Texture2D blockTexture;
    private static Texture2D handTexture;

    private static bool TextureLoaded = false;
    public static bool IsTexturesLoaded { get => TextureLoaded; }

    public static void LoadCursorsSprites()
    {
        defaultTexture = Resources.Load<Texture2D>(DEFAULT_CURSOR);
        axeTexture = Resources.Load<Texture2D>(AXE_CURSOR);
        blockTexture = Resources.Load<Texture2D>(BLOCK_CURSOR);
        handTexture = Resources.Load<Texture2D>(HAND_CURSOR);

        TextureLoaded = true;
    }

    public static void SetCursor(EquipName _name)
    {
        if (!IsTexturesLoaded)
        {
            Debug.LogError("Texture not loaded");
            return;
        }

        switch (_name)
        {
            case EquipName.Fire_Axe:
                SetTexture(axeTexture);
                break;
            case EquipName.Sledgehammer:
                break;
            case EquipName.Cement_Saw:
                break;
            case EquipName.Fire_Extinguisher:
                break;
            case EquipName.Foam_Grenades:
                break;
            case EquipName.Water_Nozzle:
                break;
            case EquipName.Smoke_Fan:
                break;
            case EquipName.Medikit:
                break;
            case EquipName.Water_Shield:
                break;
            case EquipName.None:
                break;
            default:
                break;
        }
    }

    public static void SetCursor(BaseActionName _name)
    {
        if (!IsTexturesLoaded)
        {
            Debug.LogError("Texture not loaded");
            return;
        }

        switch (_name)
        {
            case BaseActionName.Pull:
                break;
            case BaseActionName.Push:
                break;
            case BaseActionName.Destroy:
                break;
            case BaseActionName.PickUp:
                break;
            case BaseActionName.PickupVictim:
                break;
            case BaseActionName.DropVictim:
                break;
            case BaseActionName.None:
                break;
            default:
                break;
        }
    }

    public static void SetCursor(CursorStyle _state)
    {
        switch (_state)
        {
            case CursorStyle.Default:
                SetTexture(defaultTexture);
                break;
            case CursorStyle.Block:
                SetTexture(blockTexture);
                break;
            case CursorStyle.Hand:
                SetTexture(handTexture);
                break;
        }
    }

    private static void SetTexture (Texture2D _Texture) => Cursor.SetCursor(_Texture, Vector2.zero, CursorMode.ForceSoftware);

    public static void SetCursorDefault() => SetTexture(defaultTexture);
 
}
