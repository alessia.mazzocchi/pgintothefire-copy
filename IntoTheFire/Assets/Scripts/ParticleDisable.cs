﻿using UnityEngine;

public class ParticleDisable : MonoBehaviour
{
    private float startTime;
    private float delay = 5;

    private void OnEnable()
    {
        startTime = Time.time;        
    }

    private void Update()
    {
        if (Time.time >= (startTime + delay))
            gameObject.SetActive(false);

    }
}
