﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class SND_FF : SNDListenEvent
{
    [Header("Equip")]
    public StudioEventEmitter UseFireAxe;
    public StudioEventEmitter UseSledgehammer;
    public StudioEventEmitter UseCircularSaw;
    public StudioEventEmitter UseExtinguisher;
    public StudioEventEmitter UseFoamGrenade;
    public StudioEventEmitter UseWaterNozzle;
    public StudioEventEmitter UseSmokeFan;
    public StudioEventEmitter UseFirstAid;
    public StudioEventEmitter UseWaterShield;
    
    [Header("Action")]
    public StudioEventEmitter Footstep;
    public StudioEventEmitter JetJump;
    public StudioEventEmitter TakeDamage;
    public StudioEventEmitter KnockedOut;
    public StudioEventEmitter PickUpCivilian;
    public StudioEventEmitter UsePunch;
    public StudioEventEmitter UsePush;
    public StudioEventEmitter GeneralPullOut;

    public override void PlayAudio(AudioData audioData)
    {
        if (audioData.SoundClass != SoundClasses.FF || audioData.FFSound == FFSounds.NONE)
            return;

        switch (audioData.FFSound)
        {
            case FFSounds.Footsteps:
                Footstep.Play();
                break;

            case FFSounds.TakeDamage:
                TakeDamage.Play();
                break;

            case FFSounds.KnockedOut:
                KnockedOut.Play();
                break;

            case FFSounds.GeneralEquip:
                GeneralPullOut.Play();
                break;

            case FFSounds.UseEquip:
                PlayEquip(audioData);
                break;

            case FFSounds.JetJump:
                JetJump.Play();
                break;

            default:
                break;
        }
    }

    private void PlayEquip(AudioData data)
    {
        switch (data.ActionType)
        {
            case BaseActionName.Push:
                UsePush.Play();
                break;

            case BaseActionName.Destroy:
                UsePunch.Play();
                break;

            case BaseActionName.PickUp:
                PickUpCivilian.Play();
                break;

            case BaseActionName.DropVictim:
                // MISSING
                break;

            case BaseActionName.None:

                switch (data.EquipType)
                {
                    case EquipName.Fire_Axe:
                        UseFireAxe.Play();
                        break;

                    case EquipName.Sledgehammer:
                        UseSledgehammer.Play();
                        break;

                    case EquipName.Cement_Saw:
                        UseCircularSaw.Play();
                        break;

                    case EquipName.Fire_Extinguisher:
                        UseExtinguisher.Play();
                        break;

                    case EquipName.Foam_Grenades:
                        UseFoamGrenade.Play();
                        break;

                    case EquipName.Water_Nozzle:
                        UseWaterNozzle.Play();
                        break;

                    case EquipName.Smoke_Fan:
                        UseSmokeFan.Play();
                        break;

                    case EquipName.Medikit:
                        UseFirstAid.Play();
                        break;

                    case EquipName.Water_Shield:
                        UseWaterShield.Play();
                        break;

                    default:
                        break;
                }

                break;

            default:
                break;
        }
    }
}
