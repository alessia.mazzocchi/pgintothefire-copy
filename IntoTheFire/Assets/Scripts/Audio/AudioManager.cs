﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;
using FMODUnity;

public enum SoundClasses { NOT_SET, FF, Env, UI, Music }

public enum UISounds
{
    NONE,
    ButtonSelect,
    ButtonConfirm,
    ShopEquip,
    PlayerSwitch,
    ChangeTurnEnv,
    ChangeTurnPlayer,
    RewardMedals,
    RewardStars,
    Victory,
    Defeat,
    SelectEquip,
    CellScrolling,
    LevelUnlock,
    ButtonBack,
    StartButton,
    BrFFSelect,
    BrEquipSelect,
    CivilianSaved
}

public enum MusicOSTs
{
    NONE,
    Hub, 
    LevelSelection, 
    InGame
}

public enum EnvSounds
{
    NONE,
    //FireEvolution,
    //FireExtinction,
    FireBurning,
    CivilianScream,
    CivilianDeath,
    CivilianFalling,
    //ObstacleBreakWood,
    //ObstacleBreakConcrete,
    //ObstacleBreakGlass,
    ObstacleBreak,
    ObstaclePush,
    DebrisFall,
    WaterSpill,
    WaterEvaporate
}

public enum FFSounds
{
    NONE,
    Footsteps,
    TakeDamage,
    KnockedOut,
    GeneralEquip,
    UseEquip,
    JetJump
}

public class AudioData
{
    public SoundClasses SoundClass = SoundClasses.NOT_SET;
    public UISounds UISound = UISounds.NONE;
    public FFSounds FFSound = FFSounds.NONE;
    public EnvSounds EnvSound = EnvSounds.NONE;
    public MusicOSTs MusicOST = MusicOSTs.NONE;

    public EquipName EquipType = EquipName.None;
    public BaseActionName ActionType = BaseActionName.None;
    public Materials MaterialType = Materials.NONE;

    public List<FMODParameterData> ParametersToChange;

    /// <summary>
    /// Set what kind of audio is going to play (generic)
    /// </summary>
    /// <param name="_SoundType"> Choose sound class</param>
    /// <param name="_UISound"> Choose specific UI sound (may require an equip or base action to set)</param>
    /// <param name="_FFSound"> Choose specific FF sound (may require an equip or base action to set)</param>
    /// <param name="_EnvSound"> Choose specific ENV sound</param>
    /// <param name="_OST"> Choose specific OST</param>
    /// <param name="_Equip"> Choose equip</param>
    /// <param name="_BaseAction"> Choose base action</param>
    public void SetAllAudioData(SoundClasses _SoundType = SoundClasses.NOT_SET, UISounds _UISound = UISounds.NONE, FFSounds _FFSound = FFSounds.NONE, 
        EnvSounds _EnvSound = EnvSounds.NONE, MusicOSTs _OST = MusicOSTs.NONE, EquipName _Equip = EquipName.None, BaseActionName _BaseAction = BaseActionName.None,
        Materials _Material = Materials.NONE)
    {
        SoundClass = _SoundType;
        UISound = _UISound;
        FFSound = _FFSound;
        EnvSound = _EnvSound;
        MusicOST = _OST;

        EquipType = _Equip;
        ActionType = _BaseAction;
        MaterialType = _Material;
    }

    public void SetUISound(UISounds _Sound, EquipName _Equip = EquipName.None, BaseActionName _BaseAction = BaseActionName.None)
    {
        SoundClass = SoundClasses.UI;
        UISound = _Sound;

        EquipType = _Equip;
        ActionType = _BaseAction;
    }

    public void SetFFSound(FFSounds _Sound, EquipName _Equip = EquipName.None, BaseActionName _BaseAction = BaseActionName.None)
    {
        SoundClass = SoundClasses.FF;
        FFSound = _Sound;

        EquipType = _Equip;
        ActionType = _BaseAction;
    }

    public void SetEnvSound(EnvSounds _Sound, Materials _Material = Materials.NONE)
    {
        SoundClass = SoundClasses.Env;
        EnvSound = _Sound;

        MaterialType = _Material;
    }

    public void SetOST(MusicOSTs _OST)
    {
        SoundClass = SoundClasses.Music;
        MusicOST = _OST;
    }

    public void AddParameterToChange(string parName, int parValue)
    {
        FMODParameterData newPar = new FMODParameterData();
        newPar.ParameterName = parName;
        newPar.ParameterValue = parValue;

        ParametersToChange.Add(newPar);
    }

    public void SetEquip(EquipName type)
    {
        EquipType = type;
    }

    public void SetAction(BaseActionName type)
    {
        ActionType = type;
    }

    public void SetMaterial(Materials type)
    {
        MaterialType = type;
    }
}

public class FMODParameterData
{
    public string ParameterName;
    public int ParameterValue;
}

public class AudioManager : Singleton<AudioManager>
{
    [Header("Buses")]
    public string MasterBusString = "Bus:/Master";
    public Bus MasterBus;
    public string MusicBusString = "Bus:/Master/Music";
    public Bus MusicBus;
    public string SFXBusString = "Bus:/Master/SFX";
    public Bus SFXBus;

    [Space]
    public StudioEventEmitter LowerSnapshot;

    protected override void Awake()
    {
        base.Awake();
        InitializeBuses();
    }

    // Retrieve buses from path
    private void InitializeBuses()
    {
        MasterBus = RuntimeManager.GetBus(MasterBusString);
        MusicBus = RuntimeManager.GetBus(MusicBusString);
        SFXBus = RuntimeManager.GetBus(SFXBusString);

        SetPlayerPrefs(1, 1, 1);
    }

    private void SetPlayerPrefs(float MasterVolume, float MusicVolume, float SFXVolume)
    {
        if (!PlayerPrefs.HasKey("MasterVolume"))
            PlayerPrefs.SetFloat("MasterVolume", MasterVolume);
        else
            SetBusVolume(MasterBus, PlayerPrefs.GetFloat("MasterVolume"));

        if (!PlayerPrefs.HasKey("MusicVolume"))
            PlayerPrefs.SetFloat("MusicVolume", MusicVolume);
        else
            SetBusVolume(MusicBus, PlayerPrefs.GetFloat("MusicVolume"));

        if (!PlayerPrefs.HasKey("SFXVolume"))
            PlayerPrefs.SetFloat("SFXVolume", SFXVolume);
        else
            SetBusVolume(SFXBus, PlayerPrefs.GetFloat("SFXVolume"));

        PlayerPrefs.Save();
    }

    // Get input bus volume
    public float GetBusVolume(Bus bus)
    {
        bus.getVolume(out float volume);
        return volume;
    }

    // Get input bus path volume
    public float GetBusVolume(string busPath)
    {
        Bus bus = RuntimeManager.GetBus(busPath);
        bus.getVolume(out float volume);
        return volume;
    }

    // Set input bus volume
    public void SetBusVolume(Bus bus, float value)
    {
        bus.setVolume(value);
    }

    // Set input bus path volume
    public void SetBusVolume(string busPath, float value)
    {
        Bus bus = RuntimeManager.GetBus(busPath);
        bus.setVolume(value);

        if (busPath == MasterBusString)
            PlayerPrefs.SetFloat("MasterVolume", value);
        else if (busPath == MusicBusString)
            PlayerPrefs.SetFloat("MusicVolume", value);
        else if (busPath == SFXBusString)
            PlayerPrefs.SetFloat("SFXVolume", value);
    }

    public void Snapshot(bool on)
    {
        if (on)
            LowerSnapshot?.Play();
        else
            LowerSnapshot?.Stop();
    }
}
