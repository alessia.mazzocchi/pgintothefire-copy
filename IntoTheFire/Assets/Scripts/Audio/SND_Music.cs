﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class SND_Music : SNDListenEvent
{
    public StudioEventEmitter Hub;
    public StudioEventEmitter LevelSelection;
    public StudioEventEmitter InGame;

    public override void PlayAudio(AudioData audioData)
    {
        if (audioData.SoundClass != SoundClasses.Music || audioData.MusicOST == MusicOSTs.NONE)
            return;

        switch (audioData.MusicOST)
        {
            case MusicOSTs.Hub:
                if (!Hub.IsPlaying())
                    Hub.Play();

                if (LevelSelection.IsPlaying())
                    LevelSelection.Stop();
                if (InGame.IsPlaying())
                    InGame.Stop();
                break;

            case MusicOSTs.LevelSelection:
                if (!LevelSelection.IsPlaying())
                    LevelSelection.Play();

                if (Hub.IsPlaying())
                    Hub.Stop();
                if (InGame.IsPlaying())
                    InGame.Stop();
                break;

            case MusicOSTs.InGame:
                if (!InGame.IsPlaying())
                    InGame.Play();

                if (LevelSelection.IsPlaying())
                    LevelSelection.Stop();
                if (Hub.IsPlaying())
                    Hub.Stop();
                break;

            default:
                break;
        }
    }
}
