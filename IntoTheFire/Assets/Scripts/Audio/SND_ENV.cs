﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class SND_ENV : SNDListenEvent
{
    public StudioEventEmitter CivilianFall;
    public StudioEventEmitter DebrisFall;
    public StudioEventEmitter FireBurning;

    public override void PlayAudio(AudioData audioData)
    {
        if (audioData.SoundClass != SoundClasses.Env || audioData.EnvSound == EnvSounds.NONE)
            return;

        switch (audioData.EnvSound)
        {
            case EnvSounds.FireBurning:
                FireBurning.Play();
                break;
            case EnvSounds.CivilianScream:
                break;
            case EnvSounds.CivilianDeath:
                break;

            case EnvSounds.CivilianFalling:
                CivilianFall.Play();
                break;

            case EnvSounds.DebrisFall:
                DebrisFall.Play();
                break;

            case EnvSounds.WaterSpill:
                break;
            case EnvSounds.WaterEvaporate:
                break;
            default:
                break;
        }
    }
}
