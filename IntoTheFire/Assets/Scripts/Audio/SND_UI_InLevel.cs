﻿using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

public class SND_UI_InLevel : SNDListenEvent
{
    [Header("General")]
    public StudioEventEmitter PlayerSwitch;
    public StudioEventEmitter ChangeTurnEnv;
    public StudioEventEmitter ChangeTurnPlayer;
    public StudioEventEmitter CellScrolling;

    [Header("Gameover")]
    public StudioEventEmitter RewardMedals;
    public StudioEventEmitter RewardStars;
    public StudioEventEmitter CivilianSaved;
    public StudioEventEmitter Victory;
    public StudioEventEmitter Defeat;

    [Header("Equip")]
    public StudioEventEmitter SelectFireAxe;
    public StudioEventEmitter SelectSledgehammer;
    public StudioEventEmitter SelectCircularSaw;
    public StudioEventEmitter SelectExtinguisher;
    public StudioEventEmitter SelectFoamGrenade;
    public StudioEventEmitter SelectWaterNozzle;
    public StudioEventEmitter SelectSmokeFan;
    public StudioEventEmitter SelectFirstAid;
    public StudioEventEmitter SelectWaterShield;
    public StudioEventEmitter SelectPunch;
    public StudioEventEmitter SelectPush;
    public StudioEventEmitter SelectPickup;

    //protected override void OnEnable()
    //{
    //    EventManager.StartListening<AudioData>(EventsID.PlayAudio, PlayAudio);
    //}
    //protected override void OnDisable()
    //{
    //    EventManager.StopListening<AudioData>(EventsID.PlayAudio, PlayAudio);
    //}

    public override void PlayAudio(AudioData audioData)
    {
        if (audioData.SoundClass != SoundClasses.UI || audioData.UISound == UISounds.NONE)
            return;

        switch (audioData.UISound)
        {
            case UISounds.PlayerSwitch:
                PlayerSwitch.Play();
                break;

            case UISounds.ChangeTurnEnv:
                ChangeTurnEnv.Play();
                break;

            case UISounds.ChangeTurnPlayer:
                ChangeTurnPlayer.Play();
                break;

            case UISounds.RewardMedals:
                RewardMedals.Play();
                break;

            case UISounds.RewardStars:
                RewardStars.Play();
                break;

            case UISounds.CivilianSaved:
                CivilianSaved.Play();
                break;

            case UISounds.Victory:
                Victory.Play();
                break;

            case UISounds.Defeat:
                Defeat.Play();
                break;

            case UISounds.SelectEquip:
                PlayEquip(audioData);
                break;

            case UISounds.CellScrolling:
                CellScrolling.Play();
                break;

            default:
                break;
        }
    }

    private void PlayEquip(AudioData data)
    {
        switch (data.ActionType)
        {
            case BaseActionName.Push:
                SelectPush.Play();
                break;

            case BaseActionName.Destroy:
                SelectPunch.Play();
                break;

            case BaseActionName.PickUp:
                SelectPickup.Play();
                break;

            //case BaseActionName.PickupVictim:
            //    break;

            case BaseActionName.DropVictim:
                SelectPickup.Play();
                break;

            case BaseActionName.None:

                switch (data.EquipType)
                {
                    case EquipName.Fire_Axe:
                        SelectFireAxe.Play();
                        break;

                    case EquipName.Sledgehammer:
                        SelectSledgehammer.Play();
                        break;

                    case EquipName.Cement_Saw:
                        SelectCircularSaw.Play();
                        break;

                    case EquipName.Fire_Extinguisher:
                        SelectExtinguisher.Play();
                        break;

                    case EquipName.Foam_Grenades:
                        SelectFoamGrenade.Play();
                        break;

                    case EquipName.Water_Nozzle:
                        SelectWaterNozzle.Play();
                        break;

                    case EquipName.Smoke_Fan:
                        SelectSmokeFan.Play();
                        break;

                    case EquipName.Medikit:
                        SelectFirstAid.Play();
                        break;

                    case EquipName.Water_Shield:
                        SelectWaterShield.Play();
                        break;

                    default:
                        break;
                }

                break;
        }
    }
}
