﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class SND_UI_Hub : SNDListenEvent
{
    public StudioEventEmitter ShopEquip;
    public StudioEventEmitter LevelUnlock;
    public StudioEventEmitter BriefingFFSelection;
    public StudioEventEmitter BriefingEquipSelection;

    public override void PlayAudio(AudioData audioData)
    {
        if (audioData.SoundClass != SoundClasses.UI || audioData.UISound == UISounds.NONE)
            return;

        switch (audioData.UISound)
        {
            case UISounds.ShopEquip:
                ShopEquip.Play();
                break;

            case UISounds.LevelUnlock:
                LevelUnlock.Play();
                break;

            case UISounds.BrEquipSelect:
                BriefingEquipSelection.Play();
                break;

            case UISounds.BrFFSelect:
                BriefingFFSelection.Play();
                break;

            default:
                break;
        }
    }
}
