﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class SND_Obstacle : SNDListenEvent
{
    public StudioEventEmitter ObjPushed;
    public StudioEventEmitter ObjDestroyedConcreteLight;
    public StudioEventEmitter ObjDestroyedConcreteHeavy;
    public StudioEventEmitter ObjDestroyedWoodLight;
    public StudioEventEmitter ObjDestroyedWoodHeavy;
    public StudioEventEmitter ObjDestroyedGlassLight;
    public StudioEventEmitter ObjDestroyedGlassHeavy;
    public StudioEventEmitter ObjDestroyedCeramicLight;
    public StudioEventEmitter ObjDestroyedCeramicHeavy;
    public StudioEventEmitter ObjDestroyedMetalLight;
    public StudioEventEmitter ObjDestroyedMetalHeavy;

    public override void PlayAudio(AudioData audioData)
    {
        if (audioData.SoundClass != SoundClasses.Env || audioData.EnvSound == EnvSounds.NONE)
            return;

        switch (audioData.EnvSound)
        {
            case EnvSounds.ObstacleBreak:
                PlayBreak(audioData);
                break;

            case EnvSounds.ObstaclePush:
                ObjPushed.Play();
                break;

            default:
                break;
        }
    }

    private void PlayBreak(AudioData data)
    {
        switch (data.MaterialType)
        {
            case Materials.ConcreteLight:
                ObjDestroyedConcreteLight.Play();
                break;
            case Materials.ConcreteHeavy:
                ObjDestroyedConcreteHeavy.Play();
                break;
            case Materials.GlassLight:
                ObjDestroyedGlassLight.Play();
                break;
            case Materials.GlassHeavy:
                ObjDestroyedGlassHeavy.Play();
                break;
            case Materials.WoodLight:
                ObjDestroyedWoodLight.Play();
                break;
            case Materials.WoodHeavy:
                ObjDestroyedWoodHeavy.Play();
                break;
            case Materials.CeramicLight:
                ObjDestroyedCeramicLight.Play();
                break;
            case Materials.CeramicHeavy:
                ObjDestroyedCeramicHeavy.Play();
                break;
            case Materials.MetalLight:
                ObjDestroyedMetalLight.Play();
                break;
            case Materials.MetalHeavy:
                ObjDestroyedMetalHeavy.Play();
                break;
            default:
                break;
        }
    }
}
