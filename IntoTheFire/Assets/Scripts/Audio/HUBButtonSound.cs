﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HUBButtonSound : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler
{
    public bool HasSelectSound = false;

    public UISounds OnClickSoundType;

    private Button m_Button;

    private void Awake()
    {
        m_Button = GetComponent<Button>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (m_Button == null)
            return;

        if (!m_Button.IsInteractable())
            return;

        AudioData audio = new AudioData();
        audio.SetUISound(OnClickSoundType);
        EventManager.TriggerEvent(EventsID.PlayAudio, audio);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!HasSelectSound)
            return;

        AudioData audio = new AudioData();
        audio.SetUISound(UISounds.ButtonSelect);
        EventManager.TriggerEvent(EventsID.PlayAudio, audio);
    }

    public void OnPointerExit(PointerEventData eventData)
    {

    }
}
