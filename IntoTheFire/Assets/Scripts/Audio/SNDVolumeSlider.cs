﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SNDVolumeSlider : MonoBehaviour
{
    public Slider Slider;
    public string BusPath = "";

    private float Volume;

    void Start()
    {
        Volume = AudioManager.Instance.GetBusVolume(BusPath);
        Slider.value = Volume;
    }

    private void OnEnable()
    {
        Volume = AudioManager.Instance.GetBusVolume(BusPath);
        Slider.value = Volume;
    }

    public void OnValueChanged()
    {
        Volume = Slider.value;
        AudioManager.Instance.SetBusVolume(BusPath, Volume);
    }
}
