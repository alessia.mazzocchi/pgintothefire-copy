﻿using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

public class SND_UI_Menu : SNDListenEvent
{
    public StudioEventEmitter ButtonHover;
    public StudioEventEmitter ButtonConfirm;
    public StudioEventEmitter ButtonBack;
    public StudioEventEmitter StartButton;

    public override void PlayAudio(AudioData audioData)
    {
        if (audioData.SoundClass != SoundClasses.UI || audioData.UISound == UISounds.NONE)
            return;

        switch (audioData.UISound)
        {
            case UISounds.ButtonSelect:
                ButtonHover.Play();
                break;

            case UISounds.ButtonConfirm:
                ButtonConfirm.Play();
                break;

            case UISounds.ButtonBack:
                ButtonBack.Play();
                break;

            case UISounds.StartButton:
                StartButton.Play();
                break;

            default:
                break;
        }
    }
}
