﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SNDListenEvent : MonoBehaviour
{
    protected virtual void OnEnable()
    {
        EventManager.StartListening<AudioData>(EventsID.PlayAudio, PlayAudio);
    }
    protected virtual void OnDisable()
    {
        EventManager.StopListening<AudioData>(EventsID.PlayAudio, PlayAudio);
    }

    public virtual void PlayAudio(AudioData audioData)
    {

    }
}
