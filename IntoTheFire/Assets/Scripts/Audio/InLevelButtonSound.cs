﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InLevelButtonSound : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler
{
    [Header("Check true if it can be interacted with only during player turn")]
    public bool AvailableOnlyOnPlayerTurn;

    [Header("Check true if it doesn't use the standard click confirm sound")]
    public bool HasSpecificClickSound;

    private Button m_Button;

    private void Awake()
    {
        m_Button = GetComponent<Button>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //if (HasSpecificClickSound)
        //    return;

        //if (AvailableOnlyOnPlayerTurn && LevelManager.instance.CurrentPhase != TurnPhase.Player)
        //    return;

        //AudioData audio = new AudioData();
        //audio.SetUISound(UISounds.ButtonConfirm);
        //EventManager.TriggerEvent(EventsID.PlayAudio, audio);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (m_Button == null)
            return;

        if (!m_Button.IsInteractable())
            return;

        if (HasSpecificClickSound)
            return;

        if (AvailableOnlyOnPlayerTurn && LevelManager.instance.CurrentPhase != TurnPhase.Player)
            return;

        AudioData audio = new AudioData();
        audio.SetUISound(UISounds.ButtonConfirm);
        EventManager.TriggerEvent(EventsID.PlayAudio, audio);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (AvailableOnlyOnPlayerTurn && LevelManager.instance.CurrentPhase != TurnPhase.Player)
            return;

        AudioData audio = new AudioData();
        audio.SetUISound(UISounds.ButtonSelect);
        EventManager.TriggerEvent(EventsID.PlayAudio, audio);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        
    }
}
