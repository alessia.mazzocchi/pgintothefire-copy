﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;

public class WallTransparency : MonoBehaviour
{
    public float FadeTime = 0.5f;
    [Range(0,1)]
    public float AlphaValue = 0.1f;
    private float currentFadeTime;

    private MeshRenderer m_Renderer;

    private MeshRenderer[] childRenderer;
    private Material currentMaterial;
    private float value;

    private bool fixTransparency;
    [HideInInspector]
    public bool isShowing;

    private bool clearChildren;

    void Awake()
    {
        m_Renderer = GetComponent<MeshRenderer>();
        childRenderer = GetComponentsInChildren<MeshRenderer>();

        currentMaterial = m_Renderer.material;
        /// The first component is the self-one, so ignore child fade if there are none
        clearChildren = (childRenderer.Length == 1) ? false : true;
    }


    private void FixedUpdate()
    {
        if (fixTransparency)
        {
            /// Fade In
            if (isShowing)
            {
                currentFadeTime += Time.fixedDeltaTime;

                float t = currentFadeTime / FadeTime;
                value = 0 + t;
              
                currentMaterial.SetFloat("_Alpha", value);

                if (clearChildren)
                {
                    foreach (MeshRenderer item in childRenderer)
                    {
                        item.material.SetFloat("_Alpha", value);
                    }
                }

                if (t >= 1)
                {
                    currentFadeTime = 0;
                    fixTransparency = false;
                }
            }

            /// Fade Out
            else
            {
                currentFadeTime += Time.fixedDeltaTime;

                float t = currentFadeTime / FadeTime;
                value = 1 - t;
                //value = Mathf.Clamp(value, AlphaValue, 1);
                currentMaterial.SetFloat("_Alpha", value);

                if (clearChildren)
                {
                    foreach (MeshRenderer item in childRenderer)
                    {
                        item.material.SetFloat("_Alpha", value);
                    }
                }

                if (t >= 1)
                {
                    currentFadeTime = 0;
                    fixTransparency = false;
                }
            }
        }
    }

    public void Hide()
    {
        if (fixTransparency == true && isShowing == false) return;

        if (fixTransparency && isShowing)
        {
            value = currentMaterial.GetFloat("_Alpha");
            currentFadeTime = FadeTime - currentFadeTime;
        }
        else
        {
            value = 1;
        }

        fixTransparency = true;

        isShowing = false;


    }

    public void Show()
    {
        if (fixTransparency == true && isShowing == true) return;

        if (fixTransparency && !isShowing)
        {
            value = currentMaterial.GetFloat("_Alpha");
            currentFadeTime = FadeTime - currentFadeTime;
        }
        else
        {
            value = 0;
        }

        fixTransparency = true;

        isShowing = true;


        
    }
}
