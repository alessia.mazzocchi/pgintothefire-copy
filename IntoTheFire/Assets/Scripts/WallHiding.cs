﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHiding : MonoBehaviour
{
    [Tooltip("The center of the sphere at the start of the capsule cast.")]
    public Transform StartPoint;
    [Tooltip("The center of the sphere at the end of the capsule cast.")]
    public Transform EndPoint;
    public float Radius;
    public LayerMask wallsToHideMask;

    private float currentWallCheckTime;

    void FixedUpdate()
    {
        currentWallCheckTime += Time.fixedDeltaTime;

        if (currentWallCheckTime >= 0.1f)
        {
            currentWallCheckTime = 0;
            HideWalls();
        }
    }

    private Collider[] collidersHit;

    private List<GameObject> previousWalls = new List<GameObject>();

    private List<GameObject> wallsToHide = new List<GameObject>();

    private void HideWalls()
    {
        previousWalls.Clear();
        previousWalls = new List<GameObject>(wallsToHide);
        wallsToHide.Clear();
        collidersHit = Physics.OverlapCapsule(StartPoint.position, EndPoint.position, Radius, wallsToHideMask);

        foreach (Collider wall in collidersHit)
        {
            if (wall != null)
            {
                wallsToHide.Add(wall.gameObject);
                if(!previousWalls.Contains(wall.gameObject))
                    wall.gameObject.GetComponent<WallTransparency>().Hide();
            }
        }

        foreach (GameObject wall in previousWalls)
        {
            if (!wallsToHide.Contains(wall))
                wall.GetComponent<WallTransparency>().Show();
        }
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawWireSphere(StartPoint.position, Radius);
    //    Gizmos.DrawWireSphere(EndPoint.position, Radius);
    //}
}
