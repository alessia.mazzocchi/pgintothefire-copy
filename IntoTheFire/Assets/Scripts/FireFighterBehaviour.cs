﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum FFHealhStatus
{
    Healthy,
    Tired,
    Exhausted
}

public enum FFState
{
    Stop,
    Moving
}

public struct FFStats
{
    public string Name;
    public float HealthMax;
    public int Movement;
    public FFHealhStatus HealthStatus;
}
[System.Serializable]
public struct FFMeshes
{
    public GameObject Mesh;

    public Transform HandPoint;
    public Transform VictimPoint;
}

public class BaseActions
{
    public EPull pullAcition;

    public EPush pushAction;

    public EDestroy destroyAction;

    //public EPickUp pickupAction;

    public BaseActions(FireFighterBehaviour _ff)
    {
        pullAcition = new GameObject(_ff.Name.ToString() + " - Pull").AddComponent<EPull>();
        pullAcition.SetOwner(_ff);
        pullAcition.SetEquipmentStat(GameManager.Instance.GetActionNewInstance(BaseActionName.Pull));


        pushAction = new GameObject(_ff.Name.ToString() + " - Push").AddComponent<EPush>();
        pushAction.SetOwner(_ff);
        pushAction.SetEquipmentStat(GameManager.Instance.GetActionNewInstance(BaseActionName.Push));


        destroyAction = new GameObject(_ff.Name.ToString() + " - Destroy").AddComponent<EDestroy>();
        destroyAction.SetOwner(_ff);
        destroyAction.SetEquipmentStat(GameManager.Instance.GetActionNewInstance(BaseActionName.Destroy));


        //pickupAction = new GameObject(_ff.Name.ToString() + " - PickUp").AddComponent<EPickUp>();
        //pickupAction.SetOwner(_ff);
        //pickupAction.SetEquipmentStat(GameManager.Instance.GetActionNewInstance(BaseActionName.PickUp));
    }
}

public class FireFighterBehaviour : Entity, IInteractable
{
    private FFData m_DataStat;
    public FFData DataStat
    {
        get { return m_DataStat; }

        set
        {
            m_DataStat = value;

            InitFFData();

            GridPos = GetGridPosition();
        }
    }

    private FFState m_FFState;
    public FFState GetFFState { get => m_FFState; }

    private bool m_Evacuated;
    public bool m_waterShield;

    public bool WasEvacuated() => m_Evacuated;

    public bool CanTakeFireDamage 
    { 
        get => (!m_waterShield) ? true : false;
    }

    public bool IsUsable()
    {
        if (m_Evacuated || HealthCurrent <= 0)
            return false;
        return true;
    }

    [HideInInspector]
    public int ID;
    public Transform HUDPosition;
    public float MoveLerpTime = 0.7f;
    public AnimationCurve MoveCurve;
    [Space]

    private Color inactiveColor;
    public Color activeColor;

    private List<Victim> victimsCarried = new List<Victim>();

    private List<SmokeBehaviour> currentlyNearbySmoke = new List<SmokeBehaviour>();
    private List<SmokeBehaviour> previouslyNearbySmoke = new List<SmokeBehaviour>();

    public bool HasVictim()
    {
        if ((Inventory[0] as EPickUp)?.pickedVictim != null) return true;
        if ((Inventory[1] as EPickUp)?.pickedVictim != null) return true;

        if(Quirk.QuirkName == PassiveSkill.Strong)
            if ( Inventory[2] != null && (Inventory[2]  as EPickUp)?.pickedVictim != null) return true;

        return false;
    }

    /// <summary>
    /// List of possible actions 
    /// </summary>
    public List<PossibleActions> Actions;

    public BaseActions baseActions;
    public HUDPlayerInfo UIHealthPrefab;
    private HUDPlayerInfo HUDInfo;

    #region FF Statistics
    [HideInInspector]
    public QuirkData Quirk;

    [HideInInspector]
    public FFName Name;

    [HideInInspector]
    public FFHealhStatus HealthStatus;

    [HideInInspector]
    public float HealthMax;
    [HideInInspector]
    public float HealthCurrent;

    [HideInInspector]
    public int baseMovement;
    [HideInInspector]
    public int currentMovement;
    public int GetCurrentMovement { get { if (HealthCurrent <= 0) return 0; else return currentMovement; } }

    [HideInInspector]
    public Equip[] Inventory;
    private int m_inventorSpace = 2;

    #endregion

    [Space]
    #region Meshes / Animations
    private Transform handPoint;
    private Transform m_VictimPosition;
    public Transform VictimPosition
    {
        get
        {
            if (m_VictimPosition == null)
                return transform;

            return m_VictimPosition;
        }
    }

    public FFMeshes ForgeMesh;
    public FFMeshes AtlasMesh;
    public FFMeshes JetMesh;
    public FFMeshes OracleMesh;
    public FFMeshes GraceMesh;
    public FFMeshes ShusuiMesh;

    public System.Action animationExecution;

    [Header("Equipments")]
    public GameObject Axe;
    public GameObject Extinguisher;
    public GameObject Sledgehammer;
    public GameObject Medikit;
    public GameObject Grenade;
    public GameObject Nozzle;
    public GameObject Fan;
    public GameObject Shield;
    public GameObject Saw;

    private FireFighterBehaviour carringFirefighter;
    #endregion

    [Header("Passive skills value")]
    public int medikitHealBoost = 0;
    public int medikitMovementBoost = 2;
    private Animator m_Animator;

    #region Pathfinding

    /// <summary>
    /// Count the number of steps in the fire to take damage
    /// </summary>
    private int stepInFire;

    private Pathfinding Pathinder = new Pathfinding();
    public List<Node> Path;

    private Vector3 pathStartPosition;
    private Vector3 currentDestination;
    private int pathIndex;
    private float currentMovementTimer;

    private LineRenderer line;

    #endregion

    private bool m_canAct;
    public bool CanAct
    {
        get { return m_canAct; }
        set { m_canAct = value; }
    }

    [Header("Sound")]
    public SND_FF snd_FF;

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.EndTurn, ResetTurnValues);
        // PROVVISORIO
        EventManager.StartListening<FireFighterBehaviour>(EventsID.FFChange, FFSelected);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.EndTurn, ResetTurnValues);
        EventManager.StopListening<FireFighterBehaviour>(EventsID.FFChange, FFSelected);
    }

    protected override void Awake()
    {
        base.Awake();

        //m_Renderer = GetComponentInChildren<SkinnedMeshRenderer>();

        //inactiveColor = m_Renderer.sharedMaterial.color;
    }

    private void Start()
    {
        if (Name == FFName.Atlas)
            m_Animator.SetBool("isAtlas", true);
    }

    public override void Load()
    {
        MyNode.OnEntityEnter(this);

        //m_Renderer.material.SetColor("_BaseColor", activeColor);
    }

    #region Setup

    public void SetDataStat(FFData _stat, FFEquipSet _set)
    {
        DataStat = _stat;

        LinkToCell();

        LoadPassiveSkillPreInventory();

        Inventory = new Equip[m_inventorSpace];

        SetEquip(_set);

        LoadPassiveSkillPostInventory();

        ResetTurnValues();

        #region Equip Animations

        TurnOffEquipMesh();
        Shield.gameObject.SetActive(false);
        Grenade.transform.SetParent(handPoint);
        Grenade.transform.localPosition = Vector3.zero;
        //Grenade.transform.localEulerAngles = Vector3.zero;
        #endregion

        m_FFState = FFState.Stop;

        HealthStatus = _stat.HealthStatus;

        HUDInfo = Instantiate(UIHealthPrefab, UIManager.Instance.Canvas.transform);
        HUDInfo.Load(this);
        HUDInfo.transform.SetSiblingIndex(1);

        gameObject.SetActive(true);
    }

    public void SetEquip(FFEquipSet _set)
    {
        baseActions = new BaseActions(this);
      
        // -----------------------------

        Equip slot1 = null;
        Equip slot2 = null;

        GameObject _eq1;
        GameObject _eq2;

        /// Equip slot One
        switch (_set.EquipOne)
        {
            case EquipName.Fire_Axe:
                _eq1 = new GameObject("Player " + ID + "FireAxe");
                slot1 = _eq1.AddComponent<EFireAxe>();
                break;

            case EquipName.Sledgehammer:
                _eq1 = new GameObject("Player " + ID + "Sledgehammer");
                slot1 = _eq1.AddComponent<ESledgehammer>();
                break;

            case EquipName.Cement_Saw:
                _eq1 = new GameObject("Player " + ID + "Cement_Saw");
                slot1 = _eq1.AddComponent<ECementSaw>();
                break;

            case EquipName.Fire_Extinguisher:
                _eq1 = new GameObject("Player " + ID + "Extinguisher");
                slot1 = _eq1.AddComponent<EFireExtinguisher>();
                break;

            case EquipName.Foam_Grenades:
                _eq1 = new GameObject("Player " + ID + "Foam_Grenades");
                slot1 = _eq1.AddComponent<EFoamGrenades>();
                break;

            case EquipName.Water_Nozzle:
                _eq1 = new GameObject("Player " + ID + "Water_Nozzle");
                slot1 = _eq1.AddComponent<EWaterNozzle>();
                break;

            case EquipName.Smoke_Fan:
                _eq1 = new GameObject("Player " + ID + "Smoke_Fan");
                slot1 = _eq1.AddComponent<ESmokeFan>();
                break;

            case EquipName.Medikit:
                _eq1 = new GameObject("Player " + ID + "Medikit");
                slot1 = _eq1.AddComponent<EMedikit>();

                break;
            case EquipName.Water_Shield:
                _eq1 = new GameObject("Player " + ID + "Water_Shield");
                slot1 = _eq1.AddComponent<EWaterShield>();
                break;

            case EquipName.None:
                _eq1 = new GameObject("Player " + ID + " - None_Pickup");
                slot1 = _eq1.AddComponent<EPickUp>();
                break;
            default:
                break;
        }

        slot1.SetOwner(this);
        slot1.SetEquipmentStat(GameManager.Instance.GetEquipNewInstance(_set.EquipOne));
        Inventory[0] = slot1;

        /// Equip slot two
        switch (_set.EquipTwo)
        {
            case EquipName.Fire_Axe:
                _eq2 = new GameObject("Player " + ID + "FireAx");
                slot2 = _eq2.AddComponent<EFireAxe>();
                break;

            case EquipName.Sledgehammer:
                _eq2 = new GameObject("Player " + ID + "Sledgehammer");
                slot2 = _eq2.AddComponent<ESledgehammer>();
                break;

            case EquipName.Cement_Saw:
                _eq2 = new GameObject("Player " + ID + "Player " + ID + "Cement_Saw");
                slot2 = _eq2.AddComponent<ECementSaw>();
                break;

            case EquipName.Fire_Extinguisher:
                _eq2 = new GameObject("Player " + ID + "Extinguisher");
                slot2 = _eq2.AddComponent<EFireExtinguisher>();
                break;

            case EquipName.Foam_Grenades:
                _eq2 = new GameObject("Player " + ID + "Foam_Grenades");
                slot2 = _eq2.AddComponent<EFoamGrenades>();
                break;

            case EquipName.Water_Nozzle:
                _eq2 = new GameObject("Player " + ID + "Water_Nozzle");
                slot2 = _eq2.AddComponent<EWaterNozzle>();
                break;

            case EquipName.Smoke_Fan:
                _eq2 = new GameObject("Player " + ID + "Smoke_Fan");
                slot2 = _eq2.AddComponent<ESmokeFan>();
                break;

            case EquipName.Medikit:
                _eq2 = new GameObject("Player " + ID + "Medikit");
                slot2 = _eq2.AddComponent<EMedikit>();
                break;

            case EquipName.Water_Shield:
                _eq2 = new GameObject("Player " + ID + "Water_Shield");
                slot2 = _eq2.AddComponent<EWaterShield>();
                break;

            case EquipName.None:
                _eq2 = new GameObject("Player " + ID + " - None_Pickup");
                slot2 = _eq2.AddComponent<EPickUp>();
                break;
            default:
                break;
        }

        slot2.SetOwner(this);
        slot2.SetEquipmentStat(GameManager.Instance.GetEquipNewInstance(_set.EquipTwo));

        Inventory[1] = slot2;

        if(Quirk.QuirkName == PassiveSkill.Strong)
        {
            GameObject _eq3 = new GameObject("Player " + ID + " - Pickup");
            Equip slot3 = _eq3.AddComponent<EPickUp>();

            slot3.SetOwner(this);
            slot3.SetEquipmentStat(GameManager.Instance.GetActionNewInstance(BaseActionName.PickUp));

            Inventory[2] = slot3;
        }
    }

    private void InitFFData()
    {
        Name = DataStat.FName;
        HealthStatus = DataStat.HealthStatus;

        HealthMax = DataStat.baseHealth;
        HealthCurrent = HealthMax;
        medikitMovementBoost = DataStat.medikitMovementBoost;

        baseMovement = DataStat.baseMovement;
        currentMovement = baseMovement;

        Quirk = DataStat.quirk;

        HealthStatus = FFHealhStatus.Healthy;

        Transform victimPoint = null;
        Transform handP = null;
        if(Name == FFName.Forge)
        {
            ForgeMesh.Mesh.SetActive(true);
            m_Animator = ForgeMesh.Mesh.GetComponentInChildren<Animator>();
            victimPoint = ForgeMesh.VictimPoint;
            handP = ForgeMesh.HandPoint;
            //HandPoint = ForgeMesh.Mesh.transform.Find("JNT_Hand_R");
        }
        else if(Name == FFName.Jet)
        {
            JetMesh.Mesh.SetActive(true);
            m_Animator = JetMesh.Mesh.GetComponentInChildren<Animator>();
            victimPoint = JetMesh.VictimPoint;
            handP = JetMesh.HandPoint;

            //HandPoint = JetMesh.transform.Find("JNT_Hand_R");

        }
        else if(Name == FFName.Atlas)
        {
            AtlasMesh.Mesh.SetActive(true);
            m_Animator = AtlasMesh.Mesh.GetComponentInChildren<Animator>();
            victimPoint = AtlasMesh.VictimPoint;
            handP = AtlasMesh.HandPoint;

            //HandPoint = AtlasMesh.Mesh.transform.Find("JNT_Hand_R");

        }
        else if (Name == FFName.Oracle)
        {
            OracleMesh.Mesh.SetActive(true);
            m_Animator = OracleMesh.Mesh.GetComponentInChildren<Animator>();
            victimPoint = OracleMesh.VictimPoint;
            handP = OracleMesh.HandPoint;

            //HandPoint = OracleMesh.Mesh.transform.Find("JNT_Hand_R");

        }
        else if (Name == FFName.Grace)
        {
            GraceMesh.Mesh.SetActive(true);
            m_Animator = GraceMesh.Mesh.GetComponentInChildren<Animator>();
            victimPoint = GraceMesh.VictimPoint;
            handP = GraceMesh.HandPoint;

            //HandPoint = GraceMesh.Mesh.transform.Find("JNT_Hand_R");

        }
        else if (Name == FFName.Shusui)
        {
            ShusuiMesh.Mesh.SetActive(true);
            m_Animator = ShusuiMesh.Mesh.GetComponentInChildren<Animator>();
            victimPoint = ShusuiMesh.VictimPoint;
            handP = ShusuiMesh.HandPoint;
            //HandPoint = ShusuiMesh.transform.Find("JNT_Hand_R");

        }
        else
        {
            ShusuiMesh.Mesh.SetActive(true);
            m_Animator = ShusuiMesh.Mesh.GetComponentInChildren<Animator>();
            victimPoint = ShusuiMesh.VictimPoint;
            handP = ShusuiMesh.HandPoint;

            //HandPoint = ShusuiMesh.transform.Find("JNT_Hand_R");
        }

        //while (victimPoint.childCount != 0)
        //{
        //    victimPoint = victimPoint.transform.GetChild(victimPoint.transform.childCount - 1);
        //}

        handPoint = handP;
        m_VictimPosition = victimPoint;

    }

    public void LoadPassiveSkillPreInventory()
    {
        switch (Quirk.QuirkName)
        {
            case PassiveSkill.Strong:
                m_inventorSpace += 1;
                break;
            case PassiveSkill.IgnoreObstacle:
                break;
            case PassiveSkill.NoFireDamage:
                break;
            case PassiveSkill.MoreHeal:
                break;
            case PassiveSkill.SeeThroughSmoke:
                break;
            case PassiveSkill.SuperExtinguish:
                break;
            default:
                break;
        }
    }

    public void LoadPassiveSkillPostInventory()
    {
        switch (Quirk.QuirkName)
        {
            case PassiveSkill.Strong:
                break;
            case PassiveSkill.IgnoreObstacle:
                break;
            case PassiveSkill.NoFireDamage:
                break;
            case PassiveSkill.MoreHeal:
                foreach (Equip item in Inventory)
                {
                    if(item is EMedikit)
                    {
                        item.Charges += Quirk.ChargeBoost;
                        item.CurrentChargets = item.Charges;

                        item.Value += Quirk.HelathBoost;
                    }
                }
                break;
            case PassiveSkill.SeeThroughSmoke:
                break;
            case PassiveSkill.SuperExtinguish:
                foreach (Equip item in Inventory)
                {
                    if (item is EEextinguisher)
                    {
                        item.Value += 1;
                    }
                }
                break;
            default:
                break;
        }

        foreach (SPEquipUpgrade item in DataStat.hiddenQuirk)
        {
            item.EnhanceEquip(Inventory);
            Debug.Log("Upgrade equip");
        }
    }

    public void FFSelected(FireFighterBehaviour _ff)
    {

        if(_ff.Name == Name)
        {
            //m_Renderer.material.SetColor("_BaseColor", activeColor);
            LevelManager.instance.GetNode(GridPos).EnlightPlayerNode(true);

        }
        else
        {
            //m_Renderer.material.SetColor("_BaseColor", inactiveColor);
            LevelManager.instance.GetNode(GridPos).EnlightPlayerNode(false);

        }
    }

    #endregion

    // ----------------------------------------------------------------
    private void FixedUpdate()
    {
        //if (Input.GetKeyDown(KeyCode.L) && Name == FFName.Forge)
        //{
        //    TakeDamage(4);

        //}

        //if (Input.GetKeyDown(KeyCode.I) && Name == FFName.Atlas)
        //{
        //    Heal(2);

        //}

        switch (m_FFState)
        {
            case FFState.Stop:
                break;

            case FFState.Moving:
                GridPos = GetGridPosition();

                currentMovementTimer += Time.fixedDeltaTime;
                float t = currentMovementTimer / MoveLerpTime;

                transform.position = Vector3.Lerp(pathStartPosition, currentDestination, MoveCurve.Evaluate(t));                

                if(t >= 1)
                {
                    Node n = LevelManager.instance.GetNode(GridPos.x, GridPos.y);

                    if (n.IsOnFire)
                        TakeFireDamage(n.Flame.currentStats.PassingDamageToFFs);


                    if (pathIndex < Path.Count - 1)
                    {
                        currentMovementTimer = 0;
                        pathIndex++;

                        pathStartPosition = transform.position;
                        currentDestination = Path[pathIndex].WorldPosition;

                        transform.DOLookAt(Path[pathIndex].WorldPosition, 0.5f);

                        UpdateMovementHUD();

                        if (Path[pathIndex].HasObstacle)
                        {
                            m_Animator.SetTrigger("Jump");
                        }
                    }
                    else
                    {
                        currentMovementTimer = 0;
                        m_FFState = FFState.Stop;
                        m_Animator.SetBool("Run", false);

                        n.OnEntityEnter(this);

                        LevelManager.instance.CheckIfOnExitNode(GridPos);

                        EventManager.TriggerEvent<bool>(EventsID.FFStoppedMoving, false);
                    }
                }

                break;

            default:
                break;
        }
    }

    public void StartMoving(Vector2Int _pos)
    {
        #region Test instruction

        if (line != null)
        {
            line.gameObject.SetActive(false);
            line = null;
        }
        #endregion

        Path = Pathinder.RetracePath(GridPos, _pos);
        //Path = Pathinder.FindPath(GridPos, _pos);

        pathIndex = 0;

        pathStartPosition = transform.position;
        currentDestination = Path[pathIndex].WorldPosition;
      
        transform.DOLookAt(Path[pathIndex].WorldPosition, 0.3f);
        
        ResetPreview();

        if (!GameManager.Instance.NoMovementCost)
            ChangeMovementValue(Path[Path.Count - 1].GetCost());

        //HUDInfo.UpdateHUDMovement(currentMovement);
        UpdateMovementHUD();

        LevelManager.instance.GetNode(GridPos).OnEntityExit(this);

        m_FFState = FFState.Moving;
        m_Animator.SetBool("Carry", HasVictim());

        if (Path[pathIndex].HasObstacle)
        {
            PlayJetJump();
            Debug.Log("Jump!");
            m_Animator.SetTrigger("Jump");
        }

        m_Animator.SetBool("Run", true);

        EventManager.TriggerEvent<bool>(EventsID.FFStartedMoving, true);
    }

    private void UpdateMovementHUD()
    {
        bool found = false;
        foreach (Vector2Int n in Get4DirectionNode(new Vector2Int(1, 1)))
        {
            if (Path[pathIndex].GridPosition == n)
            {
                HUDInfo.SubtractMovement(1);
                found = true;
                return;
            }
        }

        if(!found)
            HUDInfo.SubtractMovement(2);
    }


    public void PathPreview(List<Node> _path)
    {
        HUDInfo.PreviewHUDMovement(_path[_path.Count - 1].GetCost());

        int damage = 0;
        foreach (Node n in _path)
        {
            if (n.IsOnFire)
            {
                int tmp = (Quirk.QuirkName == PassiveSkill.NoFireDamage) ? n.GetFireDamage - 1 : n.GetFireDamage;
                tmp = (tmp < 0) ? 0 : tmp; 

                damage += tmp;
            }
        }

        if(damage != 0)
        {
            HUDInfo.PreviewHUDHealth(damage);
        }
    }

    public void ResetPreview()
    {
        HUDInfo.UpdateHUDMovement(currentMovement, false);

        HUDInfo.UpdateHUDHealth((int)HealthCurrent, false);
    }

    public void UndoMove(int _value)
    {
        currentMovement += _value;

        HUDInfo.UpdateHUDMovement(currentMovement, false);
    }

    public void ForceMovePosition(Vector2Int _pos)
    {
        LevelManager.instance.GetNode(GridPos).OnEntityExit(this);

        Node n = LevelManager.instance.GetNode(_pos);

        transform.position = n.WorldPosition;

        GridPos = _pos;

        LevelManager.instance.GetNode(GridPos).OnEntityEnter(this);

    }
    public void EventTry()
    {
        Debug.Log("Event success");
    }

    public void PlayFootstep()
    {
        AudioData audio = new AudioData();
        audio.SetFFSound(FFSounds.Footsteps);
        snd_FF.PlayAudio(audio);
    }

    public void PlayJetJump()
    {
        AudioData audio = new AudioData();
        audio.SetFFSound(FFSounds.JetJump);
        snd_FF.PlayAudio(audio);
    }

    public void TakeDamage(int _damage)
    {
        if (HealthCurrent > 0)
        {
            HealthCurrent -= _damage;
            HealthCurrent = Mathf.Clamp(HealthCurrent, 0, HealthMax);

            HUDInfo.UpdateHUDHealth((int)HealthCurrent);

            EventManager.TriggerEvent(EventsID.UpdateUIValues);
            if (HealthCurrent <= 0)
            {
                if(LevelManager.instance.GetSelectedFF == this)
                    LevelManager.instance.ShowFFRange(false);

                if (Name == FFName.Oracle)
                    CheckSmokeNearby();

                if(m_FFState == FFState.Moving)
                {
                    // Clear path and the FF will stop in this cell
                    Path.Clear();
                }
                m_Animator.SetBool("Death", true);
                m_Animator.SetTrigger("DeathTrigger");

                EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.StrongWilled);

                Debug.Log("Died");

                if (HasVictim())
                {
                    Node n = Pathinder.GetFirstAvailableNode_Civ(GridPos, 1);

                    RemoveVictimCarried(n.GridPosition, GetVictim(), true);
                }

                AudioData audio = new AudioData();
                audio.SetFFSound(FFSounds.KnockedOut, EquipName.None, BaseActionName.PickUp);
                snd_FF.PlayAudio(audio);

                LevelManager.instance.CheckIfGameOver();
            }
            else
            {
                if (_damage > 0)
                {
                    AudioData audio = new AudioData();
                    audio.SetFFSound(FFSounds.TakeDamage, EquipName.None, BaseActionName.PickUp);
                    snd_FF.PlayAudio(audio);
                }
            }
        }

        //UIManager.Instance.DamageFeedback(transform.position, _damage);
        //EventManager.TriggerEvent(EventsID.UpdateUIValues);
    }

    private void ResetTurnValues()
    {
        if (IsUsable())
        {
            currentMovement = baseMovement;
            HUDInfo?.UpdateHUDMovement(currentMovement, false);
        }

        m_waterShield = false;
        Shield.SetActive(false);
        CanAct = true;
        //Debug.Log("RESEEEET");
        //Debug.Log("Curr mov " + currentMovement);
    }

    public void ChangeMovementValue(int n)
    {
        currentMovement -= n;
    }

    /// <summary>
    /// Use the flame entity for exception
    /// </summary>
    /// <param name="_flame"></param>
    /// <param name="_damage"></param>
    public void TakeStandingDamage()
    {
        if (!IsUsable()) return;

        //Node n = LevelManager.instance.GetNode(GridPos);

        if (MyNode.IsOnFire)
        {
            int dmg = MyNode.Flame.currentStats.StandingDamageToFFs;
            TakeFireDamage(dmg);
        }
    }

    /// <summary>
    /// Take damage while running
    /// </summary>
    /// <param name="_damage"></param>
    public void TakeFireDamage(int _damage)
    {
        //    Debug.Log("Can take fire damage: " + CanTakeFireDamage);
        //Debug.Log(PassiveSkill);

        if (!m_waterShield)
        {
            if(Quirk.QuirkName == PassiveSkill.NoFireDamage)
            {
                _damage = _damage - 1;
                _damage = Mathf.Clamp(_damage, 0, 9999);
            }

            TakeDamage(_damage);

            LevelManager.instance.SetUndoCurrentFF(false);

            EventManager.TriggerEvent(EventsID.UpdateUIValues);

            if(Quirk.QuirkName != PassiveSkill.NoFireDamage)
                UIManager.Instance.DamageFeedback(transform.position, _damage.ToString(), DamageType.Fire);
            else
                UIManager.Instance.DamageFeedback(transform.position, _damage.ToString(), DamageType.IgnoreFlame);

        }
        else
        {
            UIManager.Instance.DamageFeedback(transform.position, "", DamageType.WShield);
        }
    }

    public void TakeDebriesDamage(int _damage)
    {
        TakeDamage(_damage);

        GetVictim()?.TakeDamange(_damage);

        EventManager.TriggerEvent(EventsID.UpdateUIValues);
        UIManager.Instance.DamageFeedback(transform.position, _damage.ToString(), DamageType.Hit);

    }

    public void CheckSmokeNearby()
    {
        LevelManager lm = LevelManager.instance;

        if (lm.GameHasStarted && lm.CurrentPhase == TurnPhase.Player)
        {
            // Move previous smokes to correct list and clear "current" list
            previouslyNearbySmoke.Clear();
            previouslyNearbySmoke = new List<SmokeBehaviour>(currentlyNearbySmoke);
            currentlyNearbySmoke.Clear();

            // If this node has smoke, add it to the current list
            if (lm.GetNode(GridPos).HasSmoke)
                currentlyNearbySmoke.Add(lm.GetNode(GridPos).Smoke);

            // If current ff is the Smoker, all nodes of path area must be taken in consideration
            if (Quirk.QuirkName == PassiveSkill.SeeThroughSmoke)
            {
                int temp_range = 0;

                if (HealthCurrent > 0)
                    temp_range = baseMovement;

                Pathfinding pathfinder = new Pathfinding();
                List<Node> areaPath = new List<Node>(pathfinder.GetSmokeFOV(GridPos, temp_range, this));

                foreach (Node n in areaPath)
                {
                    if (n.HasSmoke)
                        currentlyNearbySmoke.Add(n.Smoke);
                }
            }

            // Else, get only the four adiacent nodes
            else
            {
                List<Vector2Int> AdiacentNodes = GetDirectionNodes(new Vector2Int(1, 1), CheckRadius.FourDirection);

                foreach (Vector2Int node in AdiacentNodes)
                {
                    Node n = lm.GetNode(node);

                    if (n.HasSmoke)
                        currentlyNearbySmoke.Add(n.Smoke);
                }
            }

            // If smokes in current list were not present in the previous list, add ff to those smokes lists
            foreach (SmokeBehaviour smoke in currentlyNearbySmoke)
            {
                if (!previouslyNearbySmoke.Contains(smoke))
                    smoke.AddFFToList(this);
            }

            // IF smokes in previous list are not present in the current list, remove ff from those smokes lists
            foreach (SmokeBehaviour smoke in previouslyNearbySmoke)
            {
                if (!currentlyNearbySmoke.Contains(smoke))
                    smoke.RemoveFFFromList(this);
            }
        }
    }

    #region Interactions

    public void SetEquipAnimation(Equip _equip, Transform _target ,System.Action _action)
    {
        animationExecution = _action;
        transform.DOLookAt(_target.position, 0.25f, AxisConstraint.Y);
        AudioData audio = new AudioData();

        switch (_equip.EName)
        {
            case EquipName.Fire_Axe:
                audio.SetFFSound(FFSounds.UseEquip, EquipName.Fire_Axe);
                m_Animator.SetTrigger("Melee");
                Axe.SetActive(true);
                break;

            case EquipName.Sledgehammer:
                audio.SetFFSound(FFSounds.UseEquip, EquipName.Sledgehammer);
                m_Animator.SetTrigger("Melee");
                Sledgehammer.SetActive(true);
                break;

            case EquipName.Cement_Saw:
                audio.SetFFSound(FFSounds.UseEquip, EquipName.Cement_Saw);
                m_Animator.SetTrigger("Saw");
                Saw.SetActive(true);
                break;

            case EquipName.Fire_Extinguisher:
                audio.SetFFSound(FFSounds.UseEquip, EquipName.Fire_Extinguisher);
                m_Animator.SetTrigger("Extinguish");
                Extinguisher.SetActive(true);

                break;
            case EquipName.Foam_Grenades:
                audio.SetFFSound(FFSounds.UseEquip, EquipName.Foam_Grenades);
                m_Animator.SetTrigger("Grenade");
                Grenade.gameObject.SetActive(true);
                break;

            case EquipName.Water_Nozzle:
                audio.SetFFSound(FFSounds.UseEquip, EquipName.Water_Nozzle);
                m_Animator.SetTrigger("Nozzle");
                Nozzle.SetActive(true);

                break;
            case EquipName.Smoke_Fan:
                audio.SetFFSound(FFSounds.UseEquip, EquipName.Smoke_Fan);
                m_Animator.SetTrigger("Nozzle");
                Fan.SetActive(true);

                break;
            case EquipName.Medikit:
                audio.SetFFSound(FFSounds.UseEquip, EquipName.Medikit);
                m_Animator.SetTrigger("Medikit");
                Medikit.gameObject.SetActive(true);

                break;
            case EquipName.Water_Shield:
                audio.SetFFSound(FFSounds.UseEquip, EquipName.Water_Shield);
                m_Animator.SetTrigger("Medikit");

                break;
            case EquipName.None:
                // Find difference between anctions
                if(_equip is EDestroy)
                {
                    audio.SetFFSound(FFSounds.UseEquip, EquipName.None, BaseActionName.Destroy);
                    m_Animator.SetTrigger("Punch");
                }
                else if(_equip is EPickUp)
                {
                    audio.SetFFSound(FFSounds.UseEquip, EquipName.None, BaseActionName.PickUp);
                    m_Animator.SetTrigger("PickUp");
                }
                else
                {
                    audio.SetFFSound(FFSounds.UseEquip, EquipName.None, BaseActionName.Push);
                    m_Animator.SetTrigger("Push");
                }

                break;

            default:
                break;
        }

        snd_FF.PlayAudio(audio);
    }

    public void ExecuteEquipAnimation()
    {
        if (animationExecution != null)
            animationExecution.DynamicInvoke();

        LevelManager.instance.ShowFFRange(true);
        InputManager.Instance.selectedEquip = null;
    }

    public void TurnOffEquipMesh()
    {
        Axe.SetActive(false);
        Extinguisher.SetActive(false);
        Sledgehammer.SetActive(false);
        Medikit.SetActive(false);
        Grenade.SetActive(false);
        Nozzle.SetActive(false);
        Fan.SetActive(false);
        Saw.SetActive(false);
    }

    /// <summary>
    /// DEPRECATO
    /// </summary>
    /// <param name="inventory"></param>
    public void GetActiveActions(Equip[] inventory)
    {
        
    }

    public void GetActiveActions(Equip _equip)
    {
        if(_equip is EMedikit)
        {
            Heal((int)_equip.Value);
            if (_equip.Owner.Name == FFName.Grace)
            {
                currentMovement += _equip.Owner.medikitMovementBoost;
                HUDInfo.UpdateHUDMovement(currentMovement);
            }
        }
        else if(_equip is EWaterShield)
        {
            m_waterShield = true;
            Shield.SetActive(true);
        }
        else if(_equip is EPickUp)
        {
            PickUp();
        }
    }

    public void Open()
    {
        throw new System.NotImplementedException();
    }

    public void Break()
    {
        throw new System.NotImplementedException();
    }

    public void TakeDamange(int _damage)
    {
        throw new System.NotImplementedException();
    }

    public void Heal(int _value)
    {
        if (HealthCurrent == 0)
        {
            m_Animator.SetBool("Death", false);
        }

        HealthCurrent += _value;
        HealthCurrent = Mathf.Clamp(HealthCurrent, 0, HealthMax);

        CheckSmokeNearby();

        HUDInfo.AddHealth(_value);
        UIManager.Instance.DamageFeedback(transform.position, _value.ToString(), DamageType.Heal);

    }

    public void Pull(Vector2Int _PlayerPos, Vector2Int _MyNextPos)
    {
        throw new System.NotImplementedException();
    }

    public void Push(Vector2Int _PlayerPos, Vector2Int _MyNextPos)
    {
        throw new System.NotImplementedException();
    }

    public void Throw()
    {
        throw new System.NotImplementedException();
    }

    public void PickUp()
    {
        Debug.Log("FIRST PICKUP");
        LevelManager lm = LevelManager.instance;

        (InputManager.Instance.selectedEquip as EPickUp).pickedVictim = this;
        lm.GetNode(GridPos).OnEntityExit(this);
        lm.GetSelectedFF.AddFFCarried(this);

        HUDInfo.gameObject.SetActive(false);
        carringFirefighter = lm.GetSelectedFF;

        if (lm.GetSelectedFF.Name != FFName.Atlas)
        {
            m_Animator.SetInteger("CarryPosition", 0);
            m_Animator.SetBool("Carry", true);

        }
        else
        {
            m_Animator.SetInteger("CarryPosition", lm.GetSelectedFF.VictimCount());
            m_Animator.SetBool("Carry", true);

            //if (lm.GetSelectedFF.VictimCount() == 1)
            //{
            //    m_Animator.SetInteger("CarryPosition", 1);
            //}
            //else
            //{
            //    m_Animator.SetInteger("CarryPosition", 2);
            //}
        }
        Debug.Log("PickUp");

        //// TUTORIAL ONLY
        //if (m_WaitingForTutorialProgress && m_TutorialType == PromptTypes.PickupCivilian)
        //    CallForTutorialNextStep();

        EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);

        EventManager.TriggerEvent(EventsID.UpdateUIValues);

    }

    public void PickUp(FireFighterBehaviour _ff)
    {
        LevelManager.instance.GetSelectedFF.PassVictimToAnother();

        transform.SetParent(null);
        _ff.SetVictimToFreeSlot(this);
        _ff.AddFFCarried(this);
        carringFirefighter = _ff;

        // TOTEST
        if (_ff.Name != FFName.Atlas)
        {
            m_Animator.SetInteger("CarryPosition", 0);
        }
        else
        {
            if (_ff.VictimCount() == 1)
            {
                m_Animator.SetInteger("CarryPosition", 1);
                Debug.Log("pos 1");
            }
            else
            {
                m_Animator.SetInteger("CarryPosition", 2);
                Debug.Log("pos 2");

            }
        }

        EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);
    }


    public void Drop()
    {
        HUDInfo.gameObject.SetActive(true);
        m_Animator.SetBool("Carry", false);
        m_Animator.SetTrigger("PickUp");

        if (GetVictim() != null)
            GetVictim().UpdatePosition();

        if (GetFFVictim() != null)
            GetFFVictim().UpdatePosition();
    }

    public void UpdatePosition()
    {
        // TOTEST
        if (carringFirefighter.Name != FFName.Atlas)
        {
            m_Animator.SetInteger("CarryPosition", 0);
        }
        else
        {
            if (carringFirefighter.VictimCount() == 1)
            {
                m_Animator.SetInteger("CarryPosition", 1);

            }
            else
            {
                m_Animator.SetInteger("CarryPosition", 2);
            }
        }
        Debug.Log("UPDATE POSITION");
    }
    //public void Drop()
    //{
    //    throw new System.NotImplementedException();
    //}

    public bool HasCompatibility(Equip _equip)
    {
        for (int i = 0; i < Actions.Count; i++)
            if (Actions[i] == _equip.GetEquipAction()) return true;

        return false;
    }

    public void AddVictimCarried(Victim victim)
    {
        victim.transform.DOMove(transform.position, 0.25f).OnComplete(() => 
        { 
            victim.transform.SetParent(m_VictimPosition);
            victim.transform.forward = transform.forward;
        });

        if(Name == FFName.Atlas)
        {
            m_Animator.SetBool("Carry", true);
            m_Animator.SetInteger("CarryPosition", VictimCount());
        }
        else
        {
            m_Animator.SetBool("Carry", true);
        }
    }

    public void AddFFCarried(FireFighterBehaviour ff)
    {
        ff.transform.DOMove(transform.position, 0.25f).OnComplete(() => 
        {
            ff.transform.SetParent(m_VictimPosition);
            ff.transform.forward = transform.forward;
        });

        if (Name == FFName.Atlas)
        {
            m_Animator.SetBool("Carry", true);
            m_Animator.SetInteger("CarryPosition", VictimCount());
            Debug.Log("Count: " + VictimCount());
        }
        else
        {
            m_Animator.SetBool("Carry", true);
        }

    }

    public int VictimCount()
    {
        int count = 0;
        if ((Inventory[0] as EPickUp)?.pickedVictim != null) count++;
        if ((Inventory[1] as EPickUp)?.pickedVictim != null) count++;

        if (Quirk.QuirkName == PassiveSkill.Strong)
            if (Inventory[2] != null && (Inventory[2] as EPickUp)?.pickedVictim != null) count++;

        return count;
    }

    public void RemoveVictimCarried(Vector2Int pos, Entity _victim, bool forced = false)
    {
        if (!forced)
        {
            InputManager.Instance.SelectedPickup().pickedVictim = null;
        }
        else
        {
            if ((Inventory[0] as EPickUp)?.pickedVictim != null && (Inventory[0] as EPickUp)?.pickedVictim == _victim)
            {
                (Inventory[0] as EPickUp).pickedVictim = null;
            }
            else if ((Inventory[1] as EPickUp)?.pickedVictim != null && (Inventory[1] as EPickUp)?.pickedVictim == _victim)
            {
                (Inventory[1] as EPickUp).pickedVictim = null;
            }
            else
            {
                if (Quirk.QuirkName == PassiveSkill.Strong)
                    if (Inventory[2] != null && (Inventory[2] as EPickUp)?.pickedVictim != null && (Inventory[2] as EPickUp)?.pickedVictim == _victim)
                    {
                        (Inventory[2] as EPickUp).pickedVictim = null;
                    }
            }

            m_Animator.SetInteger("CarryPosition", VictimCount());
        }

        // Move victim position
        float rotY = transform.rotation.eulerAngles.y;
        _victim.transform.DOMove(LevelManager.instance.GetNode(pos).WorldPosition, 0.25f).OnComplete(() => _victim.transform.eulerAngles = new Vector3(0, rotY, 0));
        _victim.transform.parent = null;
        _victim.GridPos = pos;
        LevelManager.instance.SetUndoCurrentFF(false);

        if (_victim is Victim)
        {
            LevelManager.instance.GetNode(pos).ObstacleEnter(_victim as Victim);
            (_victim as Victim).Drop();
            (_victim as Victim).CheckForExit();
        }
        else if (_victim is FireFighterBehaviour)
        {
            LevelManager.instance.GetNode(pos).OnEntityEnter(_victim as FireFighterBehaviour);
            (_victim as FireFighterBehaviour).Drop();
            (_victim as FireFighterBehaviour).CheckForExit();
        }

        m_Animator.SetInteger("CarryPosition", VictimCount());

        if (!HasVictim())
        {
            m_Animator.SetBool("Carry", false);
        }
        else
        {    
            if (GetVictim() != null) GetVictim().UpdatePosition();
            if (GetFFVictim() != null) GetFFVictim().UpdatePosition();
        }

        EventManager.TriggerEvent(EventsID.UpdateUIValues);
        EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);
    }

    public void DropOnExitPoint()
    {
        // FFremove
        Drop();
        LevelManager.instance.GetSelectedFF.DropFFOnExitPoint(this);

        //LevelManager.instance.ChangeSavedCivilians(+1);
        gameObject.SetActive(false);

        EventManager.TriggerEvent(EventsID.UpdateUIValues);
        EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);
    }

    public void CheckForExit()
    {
        LevelManager lm = LevelManager.instance;
        if (lm.CheckIfOnExitNode(GridPos))
        {
            lm.GetNode(GridPos).ObstacleExit();
            // animations
            //lm.ChangeSavedCivilians(+1);
            m_Evacuated = true;
            gameObject.SetActive(false);
        }
        else
            Debug.Log("NOPE");

        Debug.Log("Grid pos " + GridPos);
    }

    public void DropVictimOnExitPoint(Victim _victim)
    {
        if ((Inventory[0] as EPickUp)?.pickedVictim != null && (Inventory[0] as EPickUp)?.pickedVictim == _victim)
        {
            (Inventory[0] as EPickUp).pickedVictim = null;

            _victim.transform.parent = null;
            LevelManager.instance.SetUndoCurrentFF(false);

            m_Animator.SetBool("Carry", false);
            m_Animator.SetInteger("CarryPosition", VictimCount());
            GetVictim()?.UpdatePosition();


        }
        else if ((Inventory[1] as EPickUp)?.pickedVictim != null && (Inventory[1] as EPickUp)?.pickedVictim == _victim)
        {
            (Inventory[1] as EPickUp).pickedVictim = null;

            _victim.transform.parent = null;
            LevelManager.instance.SetUndoCurrentFF(false);

            m_Animator.SetBool("Carry", false);
            m_Animator.SetInteger("CarryPosition", VictimCount());
            GetVictim()?.UpdatePosition();

        }
        else
        {
            if (Quirk.QuirkName == PassiveSkill.Strong)
                if (Inventory[2] != null && (Inventory[2] as EPickUp)?.pickedVictim != null && (Inventory[2] as EPickUp)?.pickedVictim == _victim)
                {
                    (Inventory[2] as EPickUp).pickedVictim = null;

                    _victim.transform.parent = null;
                    LevelManager.instance.SetUndoCurrentFF(false);

                    m_Animator.SetBool("Carry", false);
                    m_Animator.SetInteger("CarryPosition", VictimCount());
                    GetVictim()?.UpdatePosition();

                }
        }
    }

    public void DropFFOnExitPoint(FireFighterBehaviour _victim)
    {
        if ((Inventory[0] as EPickUp)?.pickedVictim != null && (Inventory[0] as EPickUp)?.pickedVictim == _victim)
        {
            (Inventory[0] as EPickUp).pickedVictim = null;

            _victim.transform.parent = null;
            LevelManager.instance.SetUndoCurrentFF(false);
            _victim.m_Evacuated = true;
            m_Animator.SetBool("Carry", false);
            m_Animator.SetInteger("CarryPosition", VictimCount());
            GetVictim().UpdatePosition();

        }
        else if ((Inventory[1] as EPickUp)?.pickedVictim != null && (Inventory[1] as EPickUp)?.pickedVictim == _victim)
        {
            (Inventory[1] as EPickUp).pickedVictim = null;

            _victim.transform.parent = null;
            LevelManager.instance.SetUndoCurrentFF(false);
            _victim.m_Evacuated = true;
            m_Animator.SetBool("Carry", false);
            m_Animator.SetInteger("CarryPosition", VictimCount());
            GetVictim().UpdatePosition();

        }
        else
        {
            if (Quirk.QuirkName == PassiveSkill.Strong)
                if (Inventory[2] != null && (Inventory[2] as EPickUp)?.pickedVictim != null && (Inventory[2] as EPickUp)?.pickedVictim == _victim)
                {
                    (Inventory[2] as EPickUp).pickedVictim = null;

                    _victim.transform.parent = null;
                    LevelManager.instance.SetUndoCurrentFF(false);
                    _victim.m_Evacuated = true;
                    m_Animator.SetBool("Carry", false);
                    m_Animator.SetInteger("CarryPosition", VictimCount());
                    GetVictim().UpdatePosition();

                }
        }
    }

    public void RemoveVictimsCarriedOnExitPoint(Vector2Int pos)
    {
        for (int i = 0; i < Inventory.Length; i++)
        {
            if ((Inventory[i] as EPickUp)?.pickedVictim != null)
            {
                Entity victim = (Inventory[i] as EPickUp)?.pickedVictim;
                (Inventory[i] as EPickUp).Execute(pos);

                m_Animator.SetInteger("CarryPosition", VictimCount());

                //victim.transform.parent = null;
                //victim.GridPos = pos;
                //LevelManager.instance.SetUndoCurrentFF(false);
                //LevelManager.instance.GetNode(pos).ObstacleEnter(victim as Victim);
                //(victim as Victim).Drop();
                //(victim as Victim).CheckForExit();
            }
        }
        m_Animator.SetBool("Carry", false);
        m_Animator.SetInteger("CarryPosition", VictimCount());
        //EventManager.TriggerEvent(EventsID.UpdateUIValues);
        //EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);
    }

    public Victim GetVictim()
    {
        Debug.Log("Get victim");

        if ((Inventory[0] as EPickUp)?.pickedVictim != null)
        {
            return (Inventory[0] as EPickUp)?.pickedVictim as Victim;
        }
        else if ((Inventory[1] as EPickUp)?.pickedVictim != null)
        {
            return (Inventory[1] as EPickUp)?.pickedVictim as Victim;
        }
        else if (Quirk.QuirkName == PassiveSkill.Strong && Inventory[2] != null && (Inventory[2] as EPickUp)?.pickedVictim != null)
        {
            return (Inventory[2] as EPickUp)?.pickedVictim as Victim;
        }
        else
            return null;
    }

    public FireFighterBehaviour GetFFVictim()
    {
        if ((Inventory[0] as EPickUp)?.pickedVictim != null)
        {
            return (Inventory[0] as EPickUp)?.pickedVictim as FireFighterBehaviour;
        }
        else if ((Inventory[1] as EPickUp)?.pickedVictim != null)
        {
            return (Inventory[1] as EPickUp)?.pickedVictim as FireFighterBehaviour;
        }
        else if (Quirk.QuirkName == PassiveSkill.Strong && Inventory[2] != null && (Inventory[2] as EPickUp)?.pickedVictim != null)
        {
            return (Inventory[2] as EPickUp)?.pickedVictim as FireFighterBehaviour;
        }
        else
            return null;
    }

    public void PassVictimToAnother()
    {
        LevelManager.instance.SetUndoCurrentFF(false);
        InputManager.Instance.SelectedPickup().pickedVictim = null;

        m_Animator.SetInteger("CarryPosition", VictimCount());
        m_Animator.SetBool("Carry", false);

        if(Name == FFName.Atlas && GetVictim() != null)
            GetVictim().UpdatePosition();

        if (Name == FFName.Atlas && GetFFVictim() != null)
            GetFFVictim().UpdatePosition();

    }
    #endregion

    #region Utility

    public bool CanIgnoreObstacle() { return (Quirk.QuirkName == PassiveSkill.IgnoreObstacle) ? true : false; }

    public bool CanIgnoreFireDamage() { return (Quirk.QuirkName == PassiveSkill.NoFireDamage) ? true : false; }

    public bool DealsMoreDamageToFlames() { return (Quirk.QuirkName == PassiveSkill.SuperExtinguish) ? true : false; }

    public bool CanPickupVictim()
    {
        //if (baseActions.pickupAction.pickedVictim == null) return true;

        if (Inventory[0] as EPickUp && (Inventory[0] as EPickUp).pickedVictim == null) return true;
        if (Inventory[1] as EPickUp && (Inventory[1] as EPickUp).pickedVictim == null) return true;

        if(Quirk.QuirkName == PassiveSkill.Strong)
            if (Inventory[2] != null && Inventory[2] as EPickUp && (Inventory[2] as EPickUp).pickedVictim == null) return true;

        return false;
    }

    public void SetVictimToFreeSlot(Victim _victim)
    {
        if (Inventory[0] as EPickUp && (Inventory[0] as EPickUp).pickedVictim == null)
        {
            (Inventory[0] as EPickUp).pickedVictim = _victim;
            Debug.Log("On inv 0");

            return;
        }

        if (Inventory[1] as EPickUp && (Inventory[1] as EPickUp).pickedVictim == null)
        {
            (Inventory[1] as EPickUp).pickedVictim = _victim;
            Debug.Log("On inv 1");

            return;
        }

        if (Inventory[2] != null && Inventory[2] as EPickUp && (Inventory[2] as EPickUp).pickedVictim == null) 
        {
            (Inventory[2] as EPickUp).pickedVictim = _victim;
            Debug.Log("On inv 2");

            return;
        }

        m_Animator.SetBool("Pass", true);
        m_Animator.SetInteger("CarryPosition", VictimCount());
    }

    public void SetVictimToFreeSlot(FireFighterBehaviour _victim)
    {
        if (Inventory[0] as EPickUp && (Inventory[0] as EPickUp).pickedVictim == null)
        {
            (Inventory[0] as EPickUp).pickedVictim = _victim;
            Debug.Log("On inv 0");

            return;
        }

        if (Inventory[1] as EPickUp && (Inventory[1] as EPickUp).pickedVictim == null)
        {
            (Inventory[1] as EPickUp).pickedVictim = _victim;
            Debug.Log("On inv 1");

            return;
        }

        if (Inventory[2] != null && Inventory[2] as EPickUp && (Inventory[2] as EPickUp).pickedVictim == null)
        {
            (Inventory[2] as EPickUp).pickedVictim = _victim;
            Debug.Log("On inv 2");

            return;
        }

        m_Animator.SetBool("Pass", true);
        m_Animator.SetInteger("CarryPosition", VictimCount());
    }

    public void UseAction()
    {
        HUDInfo.UseAction();
    }

    #endregion
}

