﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomHandler
{
    public List<Vector2Int> AreaNode;
    public int Size { get => AreaNode.Count; }
    private Vector3 nonPos = new Vector3(-1, -1, -1);

    private Vector3 m_WorldPosition;
    public Vector3 WorldPosition
    {
        get
        {
            if(m_WorldPosition == nonPos)
            {
                GetCenterOfRoom();
            }

            return m_WorldPosition;
        }
    }

    public bool hasAlert;
    public bool Contain(Vector2Int _cell)
    {
        return AreaNode.Contains(_cell);
    }

    public RoomHandler()
    {
        AreaNode = new List<Vector2Int>();
        m_WorldPosition = nonPos;
    }

    public bool IsThereCellToSetFire()
    {
        LevelManager lm = LevelManager.instance;
        foreach (Vector2Int item in AreaNode)
        {
            if (lm.GetNode(item).CanBeOnFire())
            {
                return true;
            }
        }

        return false;
    }

    public bool IsThereCellToSetSmoke()
    {
        foreach (Vector2Int item in AreaNode)
        {
            if (!LevelManager.instance.GetNode(item).HasSmoke || !LevelManager.instance.GetNode(item).IsOnFire)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsThereCellForObstalce()
    {
        foreach (Vector2Int item in AreaNode)
        {
            Node nd = LevelManager.instance.GetNode(item);

            if (!nd.HasObstacle)
            {
                return true;
            }
            else if(nd.HasObstacle && nd.HasVictim)
            {
                return true;
            }
        }

        return false;
    }

    public void GetCenterOfRoom()
    {
        int minX = 1000;
        int minY = 1000;

        int MaxX = 0;
        int MaxY = 0;

        for (int i = 0; i < AreaNode.Count; i++)
        {
            minX = Mathf.Min(minX, AreaNode[i].x);
            minY = Mathf.Min(minY, AreaNode[i].y);

            MaxX = Mathf.Max(MaxX, AreaNode[i].x);
            MaxY = Mathf.Max(MaxY, AreaNode[i].y);
        }

        float distX = Mathf.Abs(MaxX - minX);
        float distY = Mathf.Abs(MaxY - minY);

        distX = (distX == 0) ? 1 : distX;
        distY = (distY == 0) ? 1 : distY;

        int midX = MaxX - Mathf.FloorToInt(distX / 2f);
        int midY = MaxY - Mathf.FloorToInt(distY / 2f);


        m_WorldPosition = LevelManager.instance.GetNode(midX, midY).WorldPosition;

    }

    public void SetAlertOnRoom(bool _active)
    {
        hasAlert = _active;

        LevelManager lm = LevelManager.instance;

        for (int i = 0; i < AreaNode.Count; i++)
        {
            lm.GetNode(AreaNode[i]).Locator.ActiveAlert(_active);
        }
    }
}
