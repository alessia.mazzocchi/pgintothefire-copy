﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIUndoButton : MonoBehaviour/*, IPointerEnterHandler, IPointerExitHandler*/
{
    private Button m_Button;

    public string Description;

    private void Awake()
    {
        m_Button = GetComponent<Button>();
    }
    private void Start()
    {
        m_Button.onClick.AddListener(OnClick);
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.UpdateUIValues, SetActiveUndo);
        EventManager.StartListening<FireFighterBehaviour>(EventsID.FFChange, SetActiveUndo);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.UpdateUIValues, SetActiveUndo);
        EventManager.StopListening<FireFighterBehaviour>(EventsID.FFChange, SetActiveUndo);

    }

    private void OnClick()
    {
        if (LevelManager.instance.GameStatus == GameStatus.Pause) return;

        if (LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            LevelManager.instance.UndoMove();
        }
    }

    private void SetActiveUndo()
    {
        if (LevelManager.instance.CanUndo)
        {
            m_Button.interactable = true;
        }
        else
        {
            m_Button.interactable = false;
        }
    }

    private void SetActiveUndo(FireFighterBehaviour _ff)
    {
        if (LevelManager.instance.CanUndo)
        {
            m_Button.interactable = true;
        }
        else
        {
            m_Button.interactable = false;
        }
    }

    //public void OnPointerEnter(PointerEventData eventData)
    //{
    //    if (!LevelManager.instance.ActiveTooltip) return;

    //    UIManager.Instance.SetTooltip(true, Description);
    //}

    //public void OnPointerExit(PointerEventData eventData)
    //{
    //    if (!LevelManager.instance.ActiveTooltip) return;

    //    UIManager.Instance.SetTooltip(false, Description);
    //}
}
