﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIEventLogHandler : MonoBehaviour
{
    public UIEventLog LogPrefab;
    public RectTransform Content;
    public RectTransform Container;
    private CanvasGroup group;

    [Header("UI references")]
    public Image EventLabel;
    [Space]
    public GameObject CollapseButton;
    public Image WarningIcon;
    private Sprite baseSprite;
    public Sprite EnlightSprite;

    private List<UIEventLog> logs;

    public UIEventLog CurrentEventLog;
    private Sequence currentLogSequence;

    private bool thereIsLog;
    private bool IsLogOn;
    public float yPosEnd = 400;
    private float xPosStart;
    private float yPosStart;

    public void Load()
    {
        logs = new List<UIEventLog>();

        for (int i = 0; i < 3; i++)
        {
            UIEventLog tmp = Instantiate(LogPrefab, Content);
            logs.Add(tmp);

            tmp.Deactive();
        }

        IsLogOn = false;
        xPosStart = Container.anchoredPosition.x;
        yPosStart = Container.anchoredPosition.y;

        CurrentEventLog.gameObject.SetActive(false);
        Content.gameObject.SetActive(false);
        //ShowLog(false);
        baseSprite = WarningIcon.sprite;
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.ChangePhase, UpdateLogsOnChangePhase);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.ChangePhase, UpdateLogsOnChangePhase);
    }

    private void Awake()
    {
        group = Content.GetComponent<CanvasGroup>();
    }

    public void ClickOnEventIcon()
    {
        if (!thereIsLog) return;

        IsLogOn = !IsLogOn;

        ShowLog(IsLogOn);
        //if (IsLogOn)
        //{
        //    ShowLog(true);
        //}
        //else
        //{
        //    ShowLog(false);
        //}
    }

    public void ShowLog(bool _active)
    {
        IsLogOn = _active;

        if (_active)
        {
            Content.gameObject.SetActive(true);
            Container.DOAnchorPos(new Vector2(xPosStart, yPosEnd), 0.5f).SetEase(Ease.InSine);
            EventLabel.DOColor(Color.white, 0.5f);

            group.DOFade(1, 0.5f).SetEase(Ease.InSine);

        }
        else
        {
            Container.DOAnchorPos(new Vector2(xPosStart, yPosStart), 0.5f).SetEase(Ease.OutSine).OnComplete(() => Content.gameObject.SetActive(false));
            EventLabel.DOColor(Color.clear, 0.5f);

            group.DOFade(0, 0.5f).SetEase(Ease.OutSine);
        }
    }

    public void NewEvent(LEvent _event)
    {
        string desc = _event.EventDestription;

        //switch (_event.EventEType)
        //{
        //    case LEventType.Cell:
        //        desc += " on a cell";
        //        break;
        //    case LEventType.Room:
        //        if (_event.FullCover)
        //        {
        //            desc += " on the entire room";
        //        }
        //        else
        //        {
        //            desc += " on an entire cell of a room";
        //        }
        //        break;
        //    case LEventType.Level:
        //        if (_event.FullCover)
        //        {
        //            desc += " on the entire Level";
        //        }
        //        else
        //        {
        //            desc += " on an entire cell of the level";
        //        }
        //        break;
        //    default:
        //        break;
        //}

        UIEventLog log = GetEmptyLog();
        log.SetLog(_event.EventName, desc);
        log.transform.SetSiblingIndex(3);

        thereIsLog = true;
        WarningIcon.sprite = EnlightSprite;
    }

    public void ResetLog()
    {
        foreach (UIEventLog item in logs)
        {
            item.Deactive();
        }

        //Content.gameObject.SetActive(true);
        thereIsLog = false;
        WarningIcon.sprite = baseSprite;
    }

    private UIEventLog GetEmptyLog()
    {
        foreach (UIEventLog item in logs)
        {
            if (!item.IsActive)
                return item;
        }

        UIEventLog tmp = Instantiate(LogPrefab, Content);
        logs.Add(tmp);

        tmp.Deactive();

        return tmp;
    }

    public void UpdateLogsOnChangePhase()
    {
        if(LevelManager.instance.CurrentPhase == TurnPhase.AI)
        {
            ShowLog(false);
            ResetLog();
        }
        else if(LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            // Open event log if there is at least one
            foreach (UIEventLog item in logs)
            {
                if (item.IsActive)
                {
                    ShowLog(true);
                    return;
                }
            }
        }
    }

    public void ShowCurrentEventLog(LEvent _event)
    { 
        CurrentEventLog.TurnOnText();
        CurrentEventLog.Title.text = _event.EventName;

        CurrentEventLog.Description.text = _event.EventDestription;

        if(currentLogSequence == null)
        {
            currentLogSequence = DOTween.Sequence().SetAutoKill(false);
            currentLogSequence.Append((CurrentEventLog.transform as RectTransform).DOAnchorPos(new Vector2(-30, 0), 0.35f).SetEase(Ease.OutSine));
            currentLogSequence.Append((CurrentEventLog.transform as RectTransform).DOAnchorPos(new Vector2(10, 0), 1.5f));
            currentLogSequence.Append((CurrentEventLog.transform as RectTransform).DOAnchorPos(new Vector2(1600, 0), 0.25f).SetEase(Ease.InSine).OnComplete(CurrentEventLog.TurnOffText));
        }
        else
        {
            currentLogSequence.Restart();
            Debug.Log("REstart");
        }


    }

    public void TurnOffCurrentEventLog()
    {
        currentLogSequence.SmoothRewind();
    }
}
