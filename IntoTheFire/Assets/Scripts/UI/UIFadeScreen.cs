﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using UnityEditor;
using System.Diagnostics.Contracts;

public class UIFadeScreen : MonoBehaviour
{
    public float FadeTime;
    private float currentFadeTime;
    public float StartDelay = 0.25f;

    private bool canFade;
    private CanvasGroup background;
    public Image LoadindImage;

    private void OnEnable()
    {
        //EventManager.StartListening(EventsID.GameStart, FadeOut);
    }

    private void OnDisable()
    {
        //EventManager.StopListening(EventsID.GameStart, FadeOut);
    }

    private void Awake()
    {
        background = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        background.blocksRaycasts = true;

        background.DOFade(0, FadeTime).SetDelay(StartDelay).OnComplete(StartGame);
    }

    private void StartGame()
    {
        LevelManager.instance.TriggerGameStart();
        background.blocksRaycasts = false;
        LoadindImage.gameObject.SetActive(false);
    }

    public void ReturnToMenu()
    {
        Time.timeScale = 1;
        background.alpha = 0;
        background.blocksRaycasts = true;
        LoadindImage.gameObject.SetActive(true);

        background.DOFade(1, FadeTime).OnComplete(() => GameManager.Instance.ReturnToMenu());
    }
}
