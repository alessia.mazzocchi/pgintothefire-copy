﻿using DG.Tweening;
using FMOD;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameCanvas : MonoBehaviour
{
    private Canvas canvas;
    private CanvasGroup canvasGruop;
    public CanvasGroup CanvasGroup { get => canvasGruop; }
    public Toggle CameraPanToggle;

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.Victory, OnVictory);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.Victory, OnVictory);
    }

    public Canvas Canvas 
    {
        get 
        {
            if(canvas== null)
                canvas = GetComponent<Canvas>();

            return canvas;
        }
    }

    private void Awake()
    {
        canvas = GetComponent<Canvas>();
        canvasGruop = GetComponent<CanvasGroup>();
    }

    private void OnVictory()
    {
        CanvasGroup.DOFade(0.001f, 0.5f).SetEase(Ease.InSine);
    }

    public void ToggleCameraPan()
    {
        CameraController.Instance.MouseBorderPan = CameraPanToggle.isOn;
    }
}
