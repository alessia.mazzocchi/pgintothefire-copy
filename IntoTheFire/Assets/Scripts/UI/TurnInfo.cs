﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class TurnInfo : MonoBehaviour
{
    public Image BGImage;
    public TextMeshProUGUI m_text;
    public Color PlayerColor;
    public Color EnvColor;

    private RectTransform rect;
    private CanvasGroup group;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        group = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        group.alpha = 0;
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.ChangePhase, UpdateUI);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.ChangePhase, UpdateUI);
    }

    public void UpdateUI()
    {
        if(/*LevelManager.instance.CurrentPhase == TurnPhase.Player || */LevelManager.instance.CurrentPhase == TurnPhase.AI)
        {
            TurnOnText();
            UpdateText();
            //BGImage.color = (LevelManager.instance.CurrentPhase == TurnPhase.Player) ? PlayerColor : EnvColor;

            rect.DOAnchorPos(Vector2.zero, 0.5f).SetEase(Ease.OutSine);
            group.DOFade(1, 0.6f);

            group.DOFade(0, 0.5f).SetDelay(1.75f);
            rect.DOAnchorPos(new Vector2(1400, 0), 0.5f).SetEase(Ease.InSine).SetDelay(1.75f)
                .OnComplete(OnEndAnimation);
        }
    }

    private void UpdateText()
    {
        if (LevelManager.instance.CurrentPhase == TurnPhase.AI)
        {
            m_text.text = "Fire spreading";
        }
        else
        {
            m_text.text = "Enviroment turn";
        }
    }

    private void TurnOffText() => m_text.enabled = false;
    private void TurnOnText() => m_text.enabled = true;

    private void OnEndAnimation()
    {
        rect.anchoredPosition = new Vector2(-1400, 0);
        TurnOffText();
    }
}
