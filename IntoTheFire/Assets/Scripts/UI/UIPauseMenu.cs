﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPauseMenu : MonoBehaviour
{

    void Start()
    {
        UIManager.Instance.PauseMenu = this.gameObject;

        gameObject.SetActive(false);

    }

}
