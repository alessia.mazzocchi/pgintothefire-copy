﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempUIRecapTooltip : UIMouseOverTooltips
{
    public GameObject OverOver;

    protected override void Update()
    {

    }

    public void SetUIRecapTooltip(ScriptableObject data)
    {
        if(data is QuirkData)
        {
            CharDescription.text = (data as QuirkData).Description;
            CharSprite.sprite = (data as QuirkData).Sprite;

            ActiveContainer(TooltipTypes.UICharacter);
        }
        else if(data is EquipData)
        {
            EquipName.text = (data as EquipData).UIName;
            EquipDescription.text = (data as EquipData).Description;

            if ((data as EquipData).Charges > 0)
                EquipCharges.text = "Charges: " + (data as EquipData).Charges;
            else
                EquipCharges.text = "";

            ActiveContainer(TooltipTypes.UIEquip);
        }

        OverOver.SetActive(false);
    }

    public override void DeactiveAll()
    {
        base.DeactiveAll();

        OverOver.SetActive(true);
    }
}
