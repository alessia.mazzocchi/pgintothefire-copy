﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyUITest : MonoBehaviour
{
    private void OnEnable()
    {
        EventManager.StartListening<FireFighterBehaviour>(EventsID.FFChange, Test);
    }

    private void OnDisable()
    {
        EventManager.StopListening<FireFighterBehaviour>(EventsID.FFChange, Test);
    }

    public void Test(FireFighterBehaviour _ff)
    {
        Debug.Log("Test done");
    }
}
