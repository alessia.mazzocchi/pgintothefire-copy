﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class TempUIInfo : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ITooltip
{
    public ScriptableObject data;
    private bool setup = false;
    public Image Icon;
    
    public GameObject go => throw new System.NotImplementedException();

    public void OnPointerEnter(PointerEventData eventData)
    {
        ShowTip(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ShowTip(false);
    }

    private void Start()
    {
        if (setup) return;

        if (Icon == null) return;

        if (data is QuirkData)
        {
            Icon.sprite = (data as QuirkData).Sprite;
        }
        else if (data is EquipData)
        {
            Icon.sprite = (data as EquipData).Sprite;
        }
    }

    public void ShowTip(bool _active)
    {
        //if (data == null) return;
        //Debug.Log("OVer");
        UIManager.Instance.SetUIRecapTooltip(_active, data);
    }

    public void Setup(ScriptableObject _data)
    {
        data = _data;

        if (data is QuirkData)
        {
            Icon.sprite = (data as QuirkData).Sprite;
        }
        else if (data is EquipData)
        {
            Icon.sprite = (data as EquipData).Sprite;
        }

        setup = true;
    }
}
