﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TempScrollbar : MonoBehaviour
{
    public GameObject container;
    public Scrollbar scroll;
    public float speed = 1;
    public AnimationCurve curve;
    private bool move;

    List<UIPlayerButton> iconsList;
    private FFName selectedFF;

    private bool onScroll = false;
    public Image selectedImage;
    public Color see;
    public Color clear;

    private bool fadeSelected;
    private bool isIn;
    float currentLerpTime;
    private void Start()
    {
        scroll.value = 0;
        for (int i = 0; i < 3; i++)
        {
            //iconsList.Add(container.transform.GetChild(i).GetComponent<UIPlayerButton>());

            //iconsList[i].Index = i;

            //iconsList[i].FName = LevelManager.instance.FFInMission[i];
        }

        Roll(FFName.Jet);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            move = true;
        }

        if (move)
        {
            //float value = scroll.value + Time.deltaTime * speed;

            scroll.value += Time.deltaTime * speed; /*curve.Evaluate(value);*/


            if (scroll.value >= 1) 
            {
                scroll.value = 0;

                container.transform.GetChild(container.transform.childCount - 1).SetAsFirstSibling();

                UIPlayerButton btn = container.transform.GetChild(0).GetComponent<UIPlayerButton>();

                if (btn.FName != selectedFF)
                {
                    //Debug.Log(container.transform.GetChild(0).GetComponent<UIPlayerButton>().FName + " on " + selectedFF);
                    container.transform.GetChild(0).GetComponent<UIPlayerButton>().Fade(true);
                    move = true;
                }
                else
                {
                    move = false;
                    onScroll = false;
                    selectedImage.sprite = btn.IconImage.sprite;
                    FadeSelected(true);
                }
            }
        }

        if (fadeSelected)
        {
            currentLerpTime += Time.deltaTime;
            float t = currentLerpTime / 0.25f;

            if (isIn)
            {
                selectedImage.color = Color.Lerp(clear, see, t);
            }
            else
            {
                selectedImage.color = Color.Lerp(see, clear, t);
            }

            if(t >= 1)
            {
                currentLerpTime = 0;
                fadeSelected = false;

                selectedImage.color = (isIn) ? see : clear;
            }
        }

    }


    public void Roll(FFName _name)
    {
        if (onScroll) return;

        selectedFF = _name;

        move = true;
        onScroll = true;

        container.transform.GetChild(0).GetComponent<UIPlayerButton>().Fade(true);
        container.transform.GetChild(container.transform.childCount - 1).GetComponent<UIPlayerButton>().Fade(false);
        FadeSelected(false);

        Debug.Log(selectedFF);
    }

    private void FadeSelected(bool _isIn)
    {
        fadeSelected = true;

        isIn = _isIn;

        currentLerpTime = 0;
    }
}
