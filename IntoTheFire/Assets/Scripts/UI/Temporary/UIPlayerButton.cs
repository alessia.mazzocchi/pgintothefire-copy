﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UIPlayerButton : MonoBehaviour
{
    [HideInInspector]
    public int ID;

    [HideInInspector]
    public UIFFScrollHandler scroll;
    private Button button;

    public Image IconImage;
    public Image QuirkImage;
    public TextMeshProUGUI StepText;
    public TextMeshProUGUI HealthText;
    [Space]
    public Image ActionIcon;
    public Sprite CanActSprite;
    public Sprite CanNotActSprite;
    private Slider healthSlider;

    private bool fade;
    private bool isIn;
    private float currentLerpTime;
    [HideInInspector]
    public FFName FName;
    private Color seeColor = Color.white;
    private Color clearColor;
    public Color InactiveColor;

    private Tween ColorTween;
    private Tween ScaleTween;

    private void Awake()
    {
        button = GetComponent<Button>();
        healthSlider = GetComponentInChildren<Slider>();
        scroll = GetComponentInParent<UIFFScrollHandler>();
    }

    private void Start()
    {
        seeColor = IconImage.color;
        clearColor = new Color(seeColor.r, seeColor.g, seeColor.b, 0);
        ActionIcon.sprite = CanActSprite;
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.UpdateUIValues, UpdateOnChangeValues);
        EventManager.StartListening(EventsID.ChangePhase, UpdateOnEndTurn);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.UpdateUIValues, UpdateOnChangeValues);
        EventManager.StopListening(EventsID.ChangePhase, UpdateOnEndTurn);

    }

    void FixedUpdate()
    {
        if (!fade) return;

        currentLerpTime += Time.fixedDeltaTime;
        float t = currentLerpTime / 0.25f;

        if (isIn)
        {
            IconImage.color = Color.Lerp(clearColor, seeColor, t);
            QuirkImage.color = Color.Lerp(clearColor, seeColor, t);
        }
        else
        {
            IconImage.color = Color.Lerp(seeColor, clearColor, t);
            QuirkImage.color = Color.Lerp(seeColor, clearColor, t);

        }

        if (t >= 1)
        {
            currentLerpTime = 0;
            fade = false;

            IconImage.color = (isIn)? seeColor : clearColor;
        }
    }

    public void OnClick()
    {
        if (LevelManager.instance.CurrentPhase != TurnPhase.Player) return;

        AudioData audio = new AudioData();
        audio.SetUISound(UISounds.PlayerSwitch);
        EventManager.TriggerEvent(EventsID.PlayAudio, audio);

        LevelManager.instance.ChangeSelectedFF(ID);
    }

    public void Fade(bool _active)
    {
        fade = true;
        isIn = _active;

    }

    public void UpdateOnChangeValues()
    {
        FireFighterBehaviour ff = LevelManager.instance.GetFF(FName);
        if (healthSlider.value != ff.HealthCurrent * 100)
        {
            healthSlider.DOValue(ff.HealthCurrent * 100, 1f).SetEase(Ease.OutSine);
            //healthSlider.value = ff.HealthCurrent;
            //HealthText.transform.DOShakeRotation(1.0f, 50, 10, 90);
            if (ScaleTween == null)
                ScaleTween = HealthText.transform.DOShakeScale(0.5f, 1, 1, 180).SetAutoKill(false);
            else
                ScaleTween.Restart();

            if (ColorTween == null)
                ColorTween = HealthText.DOColor(Color.red, 0.3f).SetLoops(2, LoopType.Yoyo).SetAutoKill(false);
            else
                ColorTween.Restart();

            //HealthText.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f).SetLoops(2, LoopType.Yoyo).SetRelative();
            HealthText.text = ff.HealthCurrent.ToString();

        }
        
        StepText.text = "x" + ff.currentMovement.ToString();

        ActionIcon.sprite = (ff.CanAct) ? CanActSprite : CanNotActSprite;
    }

    public void UpdateOnEndTurn()
    {
        if (LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            IconImage.DOColor(seeColor, 0.75f);

        }
        else if (LevelManager.instance.CurrentPhase == TurnPhase.AI)
        {
            IconImage.DOColor(InactiveColor, 0.75f);
        }
    }

    public void SetFF(FFName _name, int _ID)
    {
        FName = _name;
        ID = _ID;

        FireFighterBehaviour ff = LevelManager.instance.GetFF(FName);
        IconImage.sprite = ff.DataStat.BaseSprite;

        healthSlider.maxValue = ff.HealthMax * 100;
        healthSlider.value = ff.HealthCurrent * 100;
        HealthText.text = ff.HealthCurrent.ToString();

        StepText.text = "x" + ff.currentMovement.ToString();

        QuirkImage.sprite = ff.DataStat.quirk.Sprite;
        //QuirkImage.Icon.sprite = ff.DataStat.quirk.Sprite;
    }
}
