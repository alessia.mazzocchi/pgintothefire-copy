﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDefeat : MonoBehaviour
{
    public GameObject DefeatUI;

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.Defeat, Defeat);
    }

    void Start()
    {
        DefeatUI.SetActive(false);
    }

    private void Defeat()
    {
        DefeatUI.SetActive(true);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.Defeat, Defeat);
    }
}
