﻿
using UnityEngine;

public class UIReturnMenuButton : MonoBehaviour
{
    public void OnClick()
    {
        GameManager.Instance.ReturnToMenu();
    }

}
