﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UITempCiviliansCounter : MonoBehaviour
{
    private TextMeshProUGUI m_text;

    private void Awake()
    {
        m_text = GetComponent<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.UpdateUIValues, UpdateUI);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.UpdateUIValues, UpdateUI);
    }

    public void UpdateUI()
    {
        if (LevelManager.instance.GetSavedCivilians() != LevelManager.instance.m_MinimumCiviliansToSave)
            m_text.text = LevelManager.instance.GetSavedCivilians() + " / " + LevelManager.instance.m_MinimumCiviliansToSave + " Civilians";
        else
            m_text.text = "Evacuate the firefighters";
    }
}
