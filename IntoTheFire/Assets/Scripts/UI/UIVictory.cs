﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class UIVictory : MonoBehaviour
{
    public GameObject VictoryUI;

    [Header("UI elements")]
    public TextMeshProUGUI LevelName;
    [Space]
    public GameObject CivIconPrefab;
    public Transform CivilianIconsContainer;
    [Space]
    public TextMeshProUGUI TurnsLeft;
    public UIFFStatusVariation FirefightersHurt;
    public GameObject FFStatusContainer;
    [Space]
    public TextMeshProUGUI MedalsTot;
    public TextMeshProUGUI MedalsMax;

    public Slider MedalsSlider;
    public TextMeshProUGUI StarsEarned;
    [Space]
    public GameObject SideObjectivesPrefab;
    public Transform SideObjectivesContainer;

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.Victory, Victory);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.Victory, Victory);
    }

    void Start()
    {
        VictoryUI.SetActive(false);
    }

    private void Update()
    {

    }

    private void Victory()
    {
        VictoryUI.SetActive(true);
        LoadResults();
    }

    private void LoadResults()
    {
        #region Calculation

        MissionManager.Instance.EndLevelCheck();

        MissionResults mr = new MissionResults();

        mr.GetFinalResults();
        #endregion

        #region SetUp
        LevelName.text = mr.Name;

        GameObject[] icons = new GameObject[mr.CiviliansSaved];
        for (int i = 0; i < mr.CiviliansSaved; i++)
        {
            icons[i] = Instantiate(CivIconPrefab, CivilianIconsContainer);
            icons[i].SetActive(false);
        }

        // Possible civilian sound
        //AudioData audio = new AudioData();
        //audio.SetUISound(UISounds.CivilianSaved);
        //EventManager.TriggerEvent(EventsID.PlayAudio, audio);

        int maxPossibleStars = 0;
        foreach (SideObjective so in MissionManager.Instance.SideObjectives)
        {
            maxPossibleStars += so.Reward.Amount;
        }



        for (int i = 0; i < mr.SideObjectives.Count; i++)
        {
            GameObject newSideObj = Instantiate(SideObjectivesPrefab, SideObjectivesContainer);
            newSideObj.GetComponent<UISideObjResult>().SetSideObj(mr.SideObjectives[i]);
        }

        for (int i = 0; i < LevelManager.instance.GetFFTeamList.Length; i++)
        {
            FireFighterBehaviour current = LevelManager.instance.GetFFTeamList[i];
            UIFFStatusVariation status = Instantiate(FirefightersHurt, FFStatusContainer.transform);
            status.Set(current.DataStat.BaseSprite, current.HealthStatus, GameManager.Instance.GetFFDataLoaded(current.Name).HealthStatus);
            status.gameObject.SetActive(true);
        }

        StarsEarned.text = mr.TotStars.ToString() + "/" + maxPossibleStars;

        #region Medals

        int medalsRecord = GameManager.Instance.GetLevelMedalsRecord();

        if (!LevelManager.instance.FirefightersLocked)
            MedalsSlider.maxValue = MissionManager.Instance.GetMaxPossibleMedals();
        else
            MedalsSlider.maxValue = MissionManager.Instance.GetMaxPossibleMedals(LevelManager.instance.FFInMission);

        MedalsMax.text = MedalsSlider.maxValue.ToString();
        MedalsSlider.value = 0;
        MedalsTot.text = "0";

        MedalsSlider.DOValue(mr.TotMedals, 0.75f).SetEase(Ease.InSine).SetDelay(1).OnComplete(MedalsBump);
        MedalsTot.DOText(mr.TotMedals.ToString(), 0.75f).SetDelay(1);

        #endregion
       
        StarsEarned.text = 0 + "/" + maxPossibleStars;

        TurnsLeft.text = mr.RoundsLeft.ToString();

        #endregion

        #region Animations

        float delay = 0.3f;
        for (int i = 0; i < icons.Length; i++)
        {
            icons[i].SetActive(true);
            // Last icon
            if(i == icons.Length - 1)
                icons[i].transform.DOScale(Vector3.zero, 0.3f).From().SetEase(Ease.OutBack).SetDelay(delay)/*.OnComplete(IconsComplete)*/;
            else
                icons[i].transform.DOScale(Vector3.zero, 0.3f).From().SetEase(Ease.OutBack).SetDelay(delay);

            delay += 0.3f;
        }


        void MedalsBump()
        {
            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.RewardMedals);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);

            if (mr.TotMedals == MedalsSlider.maxValue)
            {
                MedalsMax.DOScale(2f, 0.25f).SetEase(Ease.OutSine).SetLoops(2, LoopType.Yoyo);
                MedalsMax.DOColor(Color.yellow, 0.5f).SetEase(Ease.OutSine).SetLoops(2, LoopType.Yoyo);
            }
        }

        #endregion
    }

    public void BackToMenu()
    {
        GameManager.Instance.ReturnToMenu();
    }
}
