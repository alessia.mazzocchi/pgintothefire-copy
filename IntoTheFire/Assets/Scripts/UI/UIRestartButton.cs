﻿using UnityEngine;

public class UIRestartButton : MonoBehaviour
{
    public void OnClick()
    {
        GameManager.Instance.ReloadCurrentScene();
    }
}
