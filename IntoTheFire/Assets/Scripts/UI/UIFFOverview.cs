﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class UIFFOverview : MonoBehaviour
{
    public FFData data;
    [HideInInspector]
    public FFName FName;
    private GameObject m_Mesh;
    public GameObject Mesh
    {
        get
        {
            if (m_Mesh == null)
                m_Mesh = HubBriefingHandler.Instance.GetMesh(FName);

            return m_Mesh;
        }
    }

    public TextMeshProUGUI MedalsText;

    [Header("Head")]
    public TextMeshProUGUI NameText;
    public Image FFIcon;

    [Header("Stats")]
    public Image HealthStatusIcon;
    public TextMeshProUGUI StatusText;
    public TextMeshProUGUI HealthText;
    public TextMeshProUGUI MovementText;
    public string HealthStatusDescription;

    [Header("Perk")]
    public Image QuirkImage;
    public TextMeshProUGUI QuirkNameText;
    public TextMeshProUGUI QuirkDesctiptionText;

    [Header("Skills")]
    public List<UISkillButton> ProgBarOne;
    public List<UISkillButton> ProgBarTwo;
    public List<UISkillButton> UnlockMelee;
    public List<UISkillButton> UnlockExtinguish;
    public List<UISkillButton> UnlockSuppport;

    [Header("Equip Tooltip")]
    public CanvasGroup EquipTipContainer;
    public TextMeshProUGUI EquipName;
    public TextMeshProUGUI EquipCharges;
    public TextMeshProUGUI EquipDescription;
    public TextMeshProUGUI EquipDamage;
    public TextMeshProUGUI EquipRange;
    public TextMeshProUGUI EquipAoE;
    public Image DirectionSprite;
    public Sprite FourDirection ,EightDirection;

    [Header("Skills Tooltip")]
    public CanvasGroup SkillContainer;
    public TextMeshProUGUI SkillName;
    public TextMeshProUGUI SkillDescription;

    private CanvasGroup currentTip;

    [Header("Stats Tip")]
    public CanvasGroup StatsTipContainer;
    public TextMeshProUGUI StatTipText;
    
    private bool closeSkillTip;
    private float currentSkillCloseTime;
    private bool closeStatsTip;
    private float currentStatsCloseTime;
    private Tween FadeTween;

    public float CloseTime = 0.75f;

    [HideInInspector]
    public CanvasGroup group;
    private UIFFOverviewSelection selection;

    public void SetRecap(FFData _data)
    {
        data = _data;
        FName = data.FName;

        LoadValues();

        EquipTipContainer.gameObject.SetActive(false);
        EquipTipContainer.alpha = 0;

        StatsTipContainer.gameObject.SetActive(false);
        StatsTipContainer.alpha = 0;


        group = GetComponent<CanvasGroup>();
        selection = GetComponentInChildren<UIFFOverviewSelection>();

        selection.Init();
    }

    private void OnEnable()
    {
        MedalsText.text = GameManager.Instance.Medals.ToString();

        EquipTipContainer.gameObject.SetActive(false);
        SkillContainer.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (closeSkillTip)
        {
            currentSkillCloseTime += Time.deltaTime;

            if(currentSkillCloseTime >= CloseTime)
            {
                FadeTween = currentTip.DOFade(0, 0.35f).OnComplete(() => currentTip.gameObject.SetActive(false));
                
                closeSkillTip = false;
            }
        }

        if (closeStatsTip)
        {
            currentStatsCloseTime += Time.deltaTime;

            if (currentStatsCloseTime >= CloseTime)
            {
                FadeTween = StatsTipContainer.DOFade(0, 0.35f).OnComplete(() => StatsTipContainer.gameObject.SetActive(false));

                closeStatsTip = false;
            }
        }
    }

    public void LoadValues()
    {
        NameText.text = data.FName.ToString() + " - " + data.FFRole;
        FFIcon.sprite = data.BaseSprite;
        StatusText.text = data.HealthStatus.ToString();
        HealthStatusIcon.sprite = GameManager.Instance.GetHealthStatusSprite(data.HealthStatus);

        HealthText.text = data.baseHealth.ToString();
        MovementText.text = data.baseMovement.ToString();

        QuirkImage.sprite = data.quirk.Sprite;
        //QuirkNameText.text = data.quirk.QuirkName.ToString();
        QuirkDesctiptionText.text = data.quirk.Description;

        SkillTreeTemplate sk = GameManager.Instance.GetSkillTreeTemplate(FName);
        for (int i = 0; i < ProgBarOne.Count; i++)
        {
            ProgBarOne[i].SetSkillButton(i, sk.ProgressionOne[i], GetButtonState(data.SkillTree.ProgressiveOne, i));
            try
            {
                ProgBarOne[i].SetLinkToNext(ProgBarOne[i + 1]);
            }
            catch (System.ArgumentOutOfRangeException) { }


            ProgBarTwo[i].SetSkillButton(i, sk.ProgressionTwo[i], GetButtonState(data.SkillTree.ProgressiveTwo, i));
            try
            {
                ProgBarTwo[i].SetLinkToNext(ProgBarTwo[i + 1]);
            }
            catch (System.ArgumentOutOfRangeException) { }


            UnlockMelee[i].SetSkillButton(i, sk.Melee[i], GetButtonState(data.SkillTree.Melee, i));
            try
            {
                UnlockMelee[i].SetLinkToNext(UnlockMelee[i + 1]);
            }
            catch (System.ArgumentOutOfRangeException) { }


            UnlockExtinguish[i].SetSkillButton(i, sk.Extinguish[i], GetButtonState(data.SkillTree.Extinguish, i));
            try
            {
                UnlockExtinguish[i].SetLinkToNext(UnlockExtinguish[i + 1]);
            }
            catch (System.ArgumentOutOfRangeException) { }


            UnlockSuppport[i].SetSkillButton(i, sk.Utility[i], GetButtonState(data.SkillTree.Utility, i));
            try
            {
                UnlockSuppport[i].SetLinkToNext(UnlockSuppport[i + 1]);
            }
            catch (System.ArgumentOutOfRangeException) { }
        }
    }

    public SkillButtonState GetButtonState(int _progression, int _skillPosition)
    {
        if (_skillPosition <= _progression - 1) return SkillButtonState.Achieve;

        else if (_skillPosition == _progression) return SkillButtonState.Unlock;

        return SkillButtonState.Lock;
    }

    public void SkillBuy(UISkillButton _button, int _ID)
    {
        if (ProgBarOne.Contains(_button))
        {
            HubHandler.Instance.BuySkill(FName, SkillBranch.ProgressionOne, _ID);
        }
        else if (ProgBarTwo.Contains(_button))
        {
            HubHandler.Instance.BuySkill(FName, SkillBranch.ProgressionTwo, _ID);
        }
        else if (UnlockMelee.Contains(_button))
        {
            HubHandler.Instance.BuySkill(FName, SkillBranch.Melee, _ID);
        }
        else if (UnlockExtinguish.Contains(_button))
        {
            HubHandler.Instance.BuySkill(FName, SkillBranch.Extinguish, _ID);
        }
        else if (UnlockSuppport.Contains(_button))
        {
            HubHandler.Instance.BuySkill(FName, SkillBranch.Utility, _ID);
        }

        UpdateMedals();
        HubBriefingHandler.Instance.OnUnlockSkill(FName);
    }

    public void CloseRecap()
    {
        HubHandler.Instance.CloseRecap();
    }

    public void ShowSkillTip(SkillPoint _sk)
    {
        closeSkillTip = false;

        if (_sk is SPUnlockEquip)
        {
            if (currentTip != EquipTipContainer) 
            {
                SkillContainer.gameObject.SetActive(false);
                currentTip = EquipTipContainer;
            }

            EquipTipContainer.gameObject.SetActive(true);

            EquipData _equip = GameManager.Instance.GetEquipData((_sk as SPUnlockEquip).UnlockEquip());

            //EquipName.enabled = true;
            //EquipCharges.enabled = true;
            //EquipDamage.enabled = true;
            //EquipRange.enabled = true;
            //EquipAoE.enabled = true;
            //DirectionSprite.enabled = true;

            EquipName.text = _equip.UIName;
            EquipDescription.text = _equip.Description;

            DirectionSprite.sprite = (_equip.ActivateDirections == CheckRadius.FourDirection) ? FourDirection : EightDirection;
            EquipAoE.text = "";

            switch (_equip.EName)
            {
                case global::EquipName.Fire_Axe:
                    EquipDamage.text = GetDamage(_equip.Value);
                    EquipRange.text = GetRange(_equip.RangeMinMax);
                    EquipAoE.text = GetAoEInt(_equip.RangeMinMax.y);

                    break;
                case global::EquipName.Sledgehammer:
                    EquipDamage.text = GetDamage(_equip.Value);
                    EquipRange.text = GetRange(_equip.RangeMinMax);
                    EquipAoE.text = GetAoEInt(_equip.RangeMinMax.y);

                    break;
                case global::EquipName.Cement_Saw:
                    EquipDamage.text = GetDamage(_equip.Value);
                    EquipRange.text = GetRange(_equip.RangeMinMax);
                    EquipAoE.text = GetAoEInt(_equip.RangeMinMax.y);

                    break;
                case global::EquipName.Fire_Extinguisher:
                    EquipDamage.text = GetDamage(_equip.Value);
                    EquipRange.text = GetRange(_equip.RangeMinMax);
                    EquipAoE.text = GetAoE(_equip.EffectRange);

                    break;
                case global::EquipName.Foam_Grenades:
                    EquipDamage.text = GetDamage(_equip.Value);
                    EquipRange.text = GetRange(_equip.RangeMinMax);
                    EquipAoE.text = GetAoE(_equip.EffectRange);

                    break;
                case global::EquipName.Water_Nozzle:
                    EquipDamage.text = GetDamage(_equip.Value);
                    EquipRange.text = GetRange(_equip.RangeMinMax);
                    EquipAoE.text = GetAoE(_equip.EffectRange);

                    break;
                case global::EquipName.Smoke_Fan:
                    EquipDamage.text = "";
                    EquipRange.text = GetRange(_equip.RangeMinMax);
                    EquipAoE.text = GetAoE(_equip.EffectRange);

                    break;
                case global::EquipName.Medikit:
                    EquipDamage.text = GetHeal(_equip.Value);
                    EquipRange.text = GetRange(_equip.RangeMinMax);
                    EquipAoE.text = GetAoEInt(_equip.RangeMinMax.y);

                    break;
                case global::EquipName.Water_Shield:
                    EquipDamage.text = "";
                    EquipAoE.text = GetAoEInt(_equip.RangeMinMax.y);
                    EquipRange.text = "Range \n <#43b8f2> self";
                    break;
                case global::EquipName.None:
                    EquipDamage.text = (_equip.Value != 0) ? GetDamage(_equip.Value) : "";
                    EquipRange.text = GetRange(_equip.RangeMinMax);
                    GetAoEInt(_equip.RangeMinMax.y);
                    break;

                default:

                    Debug.Log("Default tooltip state");
                    break;
            }

            if (_equip.Charges > 0)
                EquipCharges.text = "Charges \n <color=green>" + _equip.Charges + " / " + _equip.Charges;
            else
            {
                EquipCharges.text = "Charges \n <color=green> \u221E ";
            }
        }

        else
        {
            if (currentTip != SkillContainer)
            {
                EquipTipContainer.gameObject.SetActive(false);
                currentTip = SkillContainer;
            }

            SkillContainer.gameObject.SetActive(true);

            SkillName.text = _sk.SkillPointName;
            SkillDescription.text = _sk.GetDesctiption();

            //EquipCharges.enabled = false;
            //EquipDamage.enabled = false;
            //EquipRange.enabled = false;
            //EquipAoE.enabled = false;
            //DirectionSprite.enabled = false;
        }

        if (FadeTween != null)
        {
            FadeTween.Rewind();
            FadeTween.Kill();
        }

        currentTip.DOFade(1, 0.35f);

        string GetDamage(float _dmg)
        {
            return $"<color=red> {(int)_dmg} </color><size=70%>        DMG";
        }

        string GetHeal(float _dmg)
        {
            return $"<color=green> {(int)_dmg} </color><size=70%>        HEAL";
        }

        string GetAoEInt(int _area)
        {
            return $"AoE \n <#43b8f2> {_area}";
        }

        string GetAoE(Vector2Int _area)
        {
            return $"AoE \n <#43b8f2> {_area.x} X {_area.y}";
        }

        string GetRange(Vector2Int _area)
        {
            return $"Range \n <#43b8f2> {_area.y}";
        }
    }

    public void CloseSkillTip()
    {
        closeSkillTip = true;
        currentSkillCloseTime = 0;
    }

    public void ShowStatsTip(string _text)
    {
        closeStatsTip = false;
        StatTipText.text = _text;
        StatsTipContainer.gameObject.SetActive(true);

        if (FadeTween != null)
        {
            FadeTween.Rewind();
            FadeTween.Kill();
        }

        StatsTipContainer.DOFade(1, 0.35f);
    }

    public void CloseStatsTip()
    {
        closeStatsTip = true;
        currentStatsCloseTime = 0;
    }

    public void UpdateMedals()
    {
        MedalsText.text = GameManager.Instance.Medals.ToString();
        MedalsText.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.15f).SetEase(Ease.OutSine).SetLoops(2, LoopType.Yoyo);
        MedalsText.DOColor(Color.red, 0.20f).SetEase(Ease.OutSine).SetLoops(2, LoopType.Yoyo);
    }

    public void SwitchRecap(FFName _name)
    {
        HubHandler.Instance.SwitchRecap(_name);
    }
}
