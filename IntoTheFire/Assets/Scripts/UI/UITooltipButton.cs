﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UITooltipButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Button m_button;
    private Image bgImage;
    public string description;

    private void Awake()
    {
        m_button = GetComponent<Button>();
        bgImage = GetComponent<Image>();
    }

    public void OnClick()
    {
        LevelManager.instance.ActiveTooltip = !LevelManager.instance.ActiveTooltip;

        if(LevelManager.instance.ActiveTooltip == false)
        {
            bgImage.color = Color.white;
            InputManager.Instance.ReleaseTooltip();
        }
        else
        {
            bgImage.color = Color.yellow;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!LevelManager.instance.ActiveTooltip) return;

        UIManager.Instance.SetTooltip(true, description);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //if (!LevelManager.instance.ActiveTooltip) return;

        UIManager.Instance.SetTooltip(false, description);
    }
}
