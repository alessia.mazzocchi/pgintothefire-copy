﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIPlayerIcon : MonoBehaviour
{
    public Image Icon;

    private void OnEnable()
    {
        EventManager.StartListening<FireFighterBehaviour>(EventsID.FFChange, SetImage);
        EventManager.StartListening(EventsID.ChangePhase, ActiveIcon);

    }

    private void OnDisable()
    {
        EventManager.StopListening<FireFighterBehaviour>(EventsID.FFChange, SetImage);
        EventManager.StopListening(EventsID.ChangePhase, ActiveIcon);

    }

    public void SetImage(FireFighterBehaviour _ff)
    {
        Icon.sprite = _ff.DataStat.BaseSprite;
    }

    public void ActiveIcon()
    {
        if(LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            //Icon.gameObject.SetActive(true);
        }
        else
        {
            //Icon.gameObject.SetActive(false);
        }
    }
}
