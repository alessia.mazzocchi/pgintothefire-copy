﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public struct DefaultColorData
{
    public Color baseColor;

    public Color clearColor;

    public DefaultColorData (Color _baseColor)
    {
        baseColor = _baseColor;

        clearColor = new Color(baseColor.r, baseColor.g, baseColor.b, 0);
    }
}

public class UIScrollSelectedFF : MonoBehaviour
{
    public Image IconImage;
    public Color InactiveColor;
    public UIPlayerQuirk Quirk;
    public TextMeshProUGUI QuirkNameText;
    public TextMeshProUGUI FFNameText;

    public TextMeshProUGUI StepText;
    public TextMeshProUGUI HealthText;
    [Space]
    public Image ActionIcon;
    public Sprite CanActSprite;
    public Sprite CanNotActSprite;
    private Slider healthSlider;

    [HideInInspector]
    public FFName FName;
    private bool fade;
    private bool isIn;
    private float currentLerpTime;
    private Color seeColor = Color.white;
    private Color clearColor;

    private Image[] childrenImage;
    private DefaultColorData[] childrenImageColor;
    public TextMeshProUGUI[] childrenTextes;
    public DefaultColorData[] childrenTextColor;

    private Tween ColorTween;
    private Tween ScaleTween;

    private void Awake()
    {
        healthSlider = GetComponentInChildren<Slider>();
        childrenImage = GetComponentsInChildren<Image>();
        childrenTextes = GetComponentsInChildren<TextMeshProUGUI>();

        childrenImageColor = new DefaultColorData[childrenImage.Length];

        for (int i = 0; i < childrenImage.Length; i++)
        {
            childrenImageColor[i] = new DefaultColorData (childrenImage[i].color);
        }

        childrenTextColor = new DefaultColorData[childrenTextes.Length];

        for (int i = 0; i < childrenTextes.Length; i++)
        {
            childrenTextColor[i] = new DefaultColorData (childrenTextes[i].color);
        }
    }

    void Start()
    {
        seeColor = IconImage.color;
        clearColor = new Color(seeColor.r, seeColor.g, seeColor.b, 0);
        ActionIcon.sprite = CanActSprite;
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.UpdateUIValues, UpdateOnChangeValues);
        EventManager.StartListening(EventsID.ChangePhase, UpdateOnEndTurn);

    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.UpdateUIValues, UpdateOnChangeValues);
        EventManager.StopListening(EventsID.ChangePhase, UpdateOnEndTurn);

    }

    private void FixedUpdate()
    {
        if (fade)
        {
            currentLerpTime += Time.deltaTime;
            float t = currentLerpTime / 0.25f;

            // Fade IN
            if (isIn)
            {
                // Fade image
                for (int i = 0; i < childrenImage.Length; i++)
                {
                    if(childrenImage[i] != null)
                        childrenImage[i].color = Color.Lerp(childrenImageColor[i].clearColor, childrenImageColor[i].baseColor, t);
                }
                // Fade text
                for (int i = 0; i < childrenTextes.Length; i++)
                {
                    if(childrenTextes[i] != null)
                        childrenTextes[i].color = Color.Lerp(childrenTextColor[i].clearColor, childrenTextColor[i].baseColor, t);
                }

            }
            // Fade OUT
            else
            {
                // Fade image
                for (int i = 0; i < childrenImage.Length; i++)
                {
                    if (childrenImage[i] != null)
                        childrenImage[i].color = Color.Lerp(childrenImageColor[i].baseColor, childrenImageColor[i].clearColor, t);
                }
                // Fade text
                for (int i = 0; i < childrenTextes.Length; i++)
                {
                    if (childrenTextes[i] != null)
                        childrenTextes[i].color = Color.Lerp(childrenTextColor[i].baseColor, childrenTextColor[i].clearColor, t);
                }
            }

            if (t >= 1)
            {
                currentLerpTime = 0;
                fade = false;

                for (int i = 0; i < childrenImage.Length; i++)
                {
                    if (childrenImage[i] != null)
                        childrenImage[i].color = (isIn) ? childrenImageColor[i].baseColor : childrenImageColor[i].clearColor;

                }

                // Fade text
                for (int i = 0; i < childrenTextes.Length; i++)
                {
                    if (childrenTextes[i] != null)
                        childrenTextes[i].color = (isIn) ? childrenTextColor[i].baseColor : childrenTextColor[i].clearColor;

                }

                if(!isIn)
                {
                    SetFF(FName);
                    fade = true;
                    isIn = true;
                }
            }
        }
    }

    public void UpdateOnChangeValues()
    {
        if (fade) return;

        FireFighterBehaviour ff = LevelManager.instance.GetFF(FName);

        if (healthSlider.value != ff.HealthCurrent * 100)
        {
            healthSlider.DOValue(ff.HealthCurrent * 100, 1f).SetEase(Ease.OutSine);
            //healthSlider.value = ff.HealthCurrent;
            //HealthText.transform.DOShakeRotation(1.0f, 50, 10, 90);
            if (ScaleTween == null)
                ScaleTween = HealthText.transform.DOShakeScale(0.5f, 1, 1, 180).SetAutoKill(false);
            else
                ScaleTween.Restart();

            if (ColorTween == null)
                ColorTween = HealthText.DOColor(Color.red, 0.3f).SetLoops(2, LoopType.Yoyo).SetAutoKill(false);
            else
                ColorTween.Restart();
            HealthText.text = ff.HealthCurrent.ToString();

        }
        
        StepText.text = "x" + ff.currentMovement.ToString();
        ActionIcon.sprite = (ff.CanAct) ? CanActSprite : CanNotActSprite;
    }


    public void Fade(FFName _name)
    {
        currentLerpTime = 0;
        fade = true;
        
        FName = _name;

        isIn = false;
    }

    public void SetFF(FFName _name)
    {
        FName = _name;

        FireFighterBehaviour ff = LevelManager.instance.GetFF(FName);
        FFNameText.text = ff.Name.ToString();
        QuirkNameText.text = ff.DataStat.quirk.GetUIName;
        Quirk.Setup(ff.DataStat);

        IconImage.sprite = ff.DataStat.BaseSprite;

        healthSlider.maxValue = ff.HealthMax * 100;
        healthSlider.value = ff.HealthCurrent * 100;
        HealthText.text = ff.HealthCurrent.ToString();

        StepText.text = "x" + ff.currentMovement.ToString();
        ActionIcon.sprite = (ff.CanAct) ? CanActSprite : CanNotActSprite;

    }

    public void UpdateOnEndTurn()
    {
        if (LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            IconImage.DOColor(seeColor, 0.75f);
        }
        else if (LevelManager.instance.CurrentPhase == TurnPhase.AI)
        {
            IconImage.DOColor(InactiveColor, 0.75f);
        }
    }
}
