﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UISideObjResult : MonoBehaviour
{
    public TextMeshProUGUI Text;

    public TextMeshProUGUI Check;

    public Image CompletitionMark;
    public Sprite V_Sprite;
    public Sprite X_Sprite;

    public Color GreenColor;
    public Color RedColor;
    private string ObjDescription = "";

    public void SetSideObj(SideObjective obj)
    {
        if (obj.IsCompleted())
        {
            CompletitionMark.sprite = V_Sprite;
            CompletitionMark.color = GreenColor;
            //Check.text = "<#15D400>V</color> ";
        }
        else
        {
            CompletitionMark.sprite = X_Sprite;
            CompletitionMark.color = RedColor;
            //Check.text = "<#FF5100>X</color> ";
        }

        ObjDescription = obj.GetDescription;

        if (obj.IsCompleted())
        {
            Text.text = "<b><s>" + ObjDescription+ "</s></b>";
        }
        else
        {
            Text.text = ObjDescription;
        }
    }
}
