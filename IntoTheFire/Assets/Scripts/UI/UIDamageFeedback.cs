﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum DamageType
{
    Fire,
    Water,
    Hit,
    Heal,
    WShield,
    IgnoreFlame
}
public class UIDamageFeedback : MonoBehaviour
{
    private TextMeshProUGUI m_Text;
    public GameObject child;
    private Image m_BG;

    [Header("Sprite BG")]
    public Sprite FireDamage;
    public Sprite WaterDamage;
    public Sprite HitDamage;
    public Sprite Heal;
    public Sprite WaterShield;
    public Sprite IgnoreFlame;

    [Space]
    public float FlyTime;
    private float currentFlyTime;

    public float speed = 1;
    public float YOffset = 50;

    private Vector3 worldPos;

    private void Awake()
    {
        m_Text = GetComponentInChildren<TextMeshProUGUI>();
        m_BG = GetComponentInChildren<Image>();
    }

    private void Update()
    {
        transform.position = Camera.main.WorldToScreenPoint(worldPos);
    }

    void FixedUpdate()
    {
        transform.position = Camera.main.WorldToScreenPoint(worldPos);

        currentFlyTime += Time.fixedDeltaTime;

        if(currentFlyTime <= FlyTime)
        {
            child.transform.Translate(Vector2.up * speed * Time.fixedDeltaTime, Space.Self);


            if(currentFlyTime >= FlyTime / 2)
            {
                float t = currentFlyTime / FlyTime;

                m_Text.color = Color.Lerp(Color.white, Color.clear, t);
                m_Text.outlineColor = Color.Lerp(Color.black, Color.clear, t);
            }
        }

        else
        {
            gameObject.SetActive(false);
        }
    }

    public void Set(Vector3 _pos , string _dmg, DamageType _type)
    {
        m_Text.text = _dmg.ToString();

        m_BG.sprite = GetSprite(_type);

        worldPos = _pos;

        transform.position = Camera.main.WorldToScreenPoint(_pos);

        child.transform.localPosition = new Vector2(0, YOffset);
    }

    private void OnDisable()
    {
        //child.transform.localPosition = Vector2.zero;
        currentFlyTime = 0;

        m_Text.color = Color.white;
        m_Text.outlineColor = Color.black;
    }

    private Sprite GetSprite(DamageType _type)
    {
        switch (_type)
        {
            case DamageType.Fire: return FireDamage;
            case DamageType.Water: return WaterDamage;
            case DamageType.Hit: return HitDamage;
            case DamageType.Heal: return Heal;
            case DamageType.IgnoreFlame: return IgnoreFlame;
            case DamageType.WShield: return WaterShield;

            default: return HitDamage;
        }
    }
}
