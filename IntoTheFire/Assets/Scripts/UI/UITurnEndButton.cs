﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UITurnEndButton : MonoBehaviour
{
    private TextMeshProUGUI m_Text;
    private Button m_Button;
    public GameObject EndTurnWarning;

    private Tween bounceTween;

    private void Awake()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
        m_Button = GetComponent<Button>();
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.UpdateUIValues, NoMoreActionsCheck);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.UpdateUIValues, NoMoreActionsCheck);
    }

    private void Start()
    {
        LevelManager.instance.endButton = this;

        m_Button.onClick.AddListener(OnClick);

        EndTurnWarning.SetActive(false);
    }

    public void OnClick()
    {
        LevelManager lm = LevelManager.instance;
        if(lm.GameStatus == GameStatus.Pause && !lm.wantToEndTurn) return;

        if(lm.CurrentPhase == TurnPhase.Player)
        {
            if (!lm.wantToEndTurn)
            {
                bounceTween.SmoothRewind();

                if (lm.NoMoreActions())
                {
                    lm.EndPlayerPhase();
                }
                else
                {
                    EndTurnWarning.SetActive(true);
                    lm.ChangeFlow(false);
                    lm.wantToEndTurn = true;
                }
            }
            else
            {
                ConfirmEndTurn();
            }
        }
    }

    public void ConfirmEndTurn()
    {
        EndTurnWarning.SetActive(false);
        bounceTween.SmoothRewind();

        LevelManager.instance.EndPlayerPhase();
        LevelManager.instance.ChangeFlow(true);

    }

    public void RefuseEndTurn()
    {
        EndTurnWarning.SetActive(false);
        bounceTween.SmoothRewind();
        LevelManager.instance.ChangeFlow(true);
    }


    public void NoMoreActionsCheck()
    {
        if (LevelManager.instance.NoMoreActions())
        {
            Debug.Log("Actions try");

            if (bounceTween == null)
                bounceTween = transform.DOScale(new Vector3(1.15f, 1.15f, 1.15f), 0.75f).SetLoops(-1, LoopType.Yoyo).SetAutoKill(false);
            else
                bounceTween.Restart();
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
