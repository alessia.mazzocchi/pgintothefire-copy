﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class UIFFInfo : MonoBehaviour
{
    public int index;

    public Image Quirk;
    public Image Eq1;
    public Image Eq2;
    public TextMeshProUGUI HP;

    private FireFighterBehaviour m_fireFighter;

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.UpdateUIValues, UpdateHPValue);
        UpdateHPValue();
    }
    private void OnDisable()
    {
        EventManager.StopListening(EventsID.UpdateUIValues, UpdateHPValue);
    }

    private void Start()
    {
        m_fireFighter = LevelManager.instance.FFInLevel[index];

        Quirk.sprite = m_fireFighter.Quirk.Sprite;

        Eq1.sprite = m_fireFighter.Inventory[0].Sprite;
        Eq2.sprite = m_fireFighter.Inventory[1].Sprite;

        UpdateHPValue();
    }

    private void UpdateHPValue()
    {
        if(m_fireFighter != null)
            HP.text = "HP: " + m_fireFighter.HealthCurrent + "/" + m_fireFighter.HealthMax;
    }
}
