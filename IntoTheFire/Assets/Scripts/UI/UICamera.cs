﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class UICamera : MonoBehaviour, IPointerClickHandler
{
    //public Canvas Canvas;
    //private RectTransform canvasRect;
    public Camera cam;
    public LayerMask mask;

    private RectTransform m_Rect;

    private void Awake()
    {
        m_Rect = GetComponent<RectTransform>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //Debug.Log("Click");
        Vector2 localPosition;
        //RectTransformUtility.ScreenPointToLocalPointInRectangle(m_Rect, eventData.pressPosition, cam, out localPosition);

        //Debug.Log(localPosition);

        RaycastHit hit;
        if (Physics.Raycast(cam.ScreenPointToRay(eventData.pressPosition), out hit,  Mathf.Infinity, mask))
        {
            //Debug.Log("Hit" + hit.collider.gameObject.name);
            hit.collider.gameObject.GetComponent<UIPlayer>().OnClick();

        }
        else
        {
            Debug.Log("not hit");
        }

    }

}
