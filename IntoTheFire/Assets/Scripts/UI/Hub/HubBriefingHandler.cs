﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public struct RecapSaveValues
{
    public FFName Firefighter;
    public FFEquipSet FFEquipSet;

    public RecapSaveValues(int _value)
    {
        Firefighter = FFName.None;
        FFEquipSet = new FFEquipSet();
        FFEquipSet.EquipOne = EquipName.Fire_Axe;
        FFEquipSet.EquipTwo= EquipName.None;
    }
}

public class HubBriefingHandler : MonoBehaviour
{
    public static HubBriefingHandler Instance;
    public GameObject BriefingContainer;

    public List<HUBFFSocket> TeamSockets;

    public HubFFInfobox[] FFInfoBoxes;
    private RecapSaveValues[] recapSaves = new RecapSaveValues[6];
    [Header("EquipGrids")]
    public HubEquipGrid EquipGridPrefab;
    public RectTransform EQuipGridsContainer;

    private Dictionary<FFName, HubEquipGrid> EquipGridDictionary;
    private HubEquipGrid currentGrid;

    public Transform[] ShowcasePosition;
    [Header("Meshes")]
    public GameObject AtlasMesh;
    public GameObject ForgeMesh;
    public GameObject JetMesh;
    public GameObject OracleMesh;
    public GameObject GraceMesh;
    public GameObject ShusuiMesh;
    private Dictionary<FFName, GameObject> FFMesh;

    private Dictionary<int, GameObject> positionLink = new Dictionary<int, GameObject>();
    [Space]

    #region Drag and Drop FF
    public float clickOffset = 5f;
    public const string DRAGGABLE_TAG = "UIDraggable";

    private bool dragging = false;
    private bool swap = false;
    private float swapOffset;
    private Vector3 dragOffset;

    private Vector2 originalPosition;
    private HUBFFSocket objectToDrag;
    private Image objectToDragImage;

    List<RaycastResult> hitObjects = new List<RaycastResult>();
    #endregion

    [Header("UI")]
    public CanvasGroup FadeScreen;

    public Image PopUpFeedback;
    public TextMeshProUGUI PopUpFeedbackText;
    [Space]
    public TextMeshProUGUI MultiplierText;
    private MultiplierDisplayer multiplierDisplayer;
    [Space]
    public HubPlayerTip PlayerTip;
    public HubEquipTip EquipTip;
    [Space]
    public TextMeshProUGUI LevelName;
    public TextMeshProUGUI LevelObjective;
    public TextMeshProUGUI LevelSideObjective;
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        multiplierDisplayer = new MultiplierDisplayer(MultiplierText);
        EquipGridDictionary = new Dictionary<FFName, HubEquipGrid>();

        var values = Enum.GetValues(typeof(FFName));
        foreach (FFName item in values)
        {
            if(item != FFName.None)
            {
                HubEquipGrid tmp = Instantiate(EquipGridPrefab, EQuipGridsContainer);
                tmp.Load(item);

                EquipGridDictionary.Add(item, tmp);

                tmp.gameObject.SetActive(false);
            }
        }


        swapOffset = 40;
        dragOffset = new Vector3();

        for (int i = 0; i < recapSaves.Length; i++)
        {
            recapSaves[i].Firefighter = (FFName)i;
            recapSaves[i].FFEquipSet.EquipOne = EquipName.None;
            recapSaves[i].FFEquipSet.EquipTwo= EquipName.None;
        }

        FFMesh = new Dictionary<FFName, GameObject>();

        FFMesh.Add(FFName.Atlas, AtlasMesh);
        FFMesh.Add(FFName.Forge, ForgeMesh);
        FFMesh.Add(FFName.Jet, JetMesh);
        FFMesh.Add(FFName.Oracle, OracleMesh);
        FFMesh.Add(FFName.Grace, GraceMesh);
        FFMesh.Add(FFName.Shusui, ShusuiMesh);

        for (int i = 0; i < ShowcasePosition.Length; i++)
        {
            positionLink.Add(i, null);
        }

        PopUpFeedback.gameObject.SetActive(false);

        //FadeScreen.color = Color.black;
        //FadeScreen.color = new Color(FadeScreen.color.r, FadeScreen.color.g, FadeScreen.color.b, 0);

        FadeScreen.alpha = 0;

        FadeScreen.gameObject.SetActive(false);

        foreach (HubFFInfobox box in FFInfoBoxes)
        {
            box.socket.Load();
        }
    }

    private void Update()
    {
        if (HubHandler.Instance.HubState != HubState.Briefing) return;

        if (Input.GetMouseButtonDown(0))
        {
            objectToDrag = GetDraggableTransformUnderMouse();

            if (objectToDrag != null)
            {
                // Deny drag on empty socket
                if (objectToDrag.selectedtFF != FFName.None || !objectToDrag.lockFF)
                {
                    dragging = true;
                    dragOffset = objectToDrag.transform.position - Input.mousePosition;
                    // Render in front of everyone
                    objectToDrag.transform.SetAsLastSibling();

                    originalPosition = objectToDrag.transform.position;
                    objectToDragImage = objectToDrag.IconImage;
                    objectToDragImage.raycastTarget = false;

                    if (IsInTeamList(objectToDrag))
                    {
                        swap = true;
                    }
                }
                else
                {
                    objectToDrag = null;
                }
            }
        }

        if (dragging)
        {
            objectToDrag.transform.position = Input.mousePosition + dragOffset;
        }

        // RETURN IF There is no object to drag
        //---------------------------------------
        if (objectToDrag == null) return;
        //---------------------------------------

        if (Input.GetMouseButtonUp(0))
        {
            ////Click
            //if (Vector2.Distance(objectToDrag.transform.position, originalPosition) <= clickOffset)
            //{
            //    HubHandler.Instance.OpenRecap(objectToDrag.selectedtFF);

            //    objectToDragImage.raycastTarget = true;
            //    objectToDrag = null;

            //    swap = false;
            //    dragging = false;

            //    return;
            //}

            //foreach (HUBFFSocket item in TeamSockets)
            //{
            //    item.TurnOffEquipSelection();
            //}
            // Drag
            if (!swap)
            {
                if (objectToDrag != null)
                {
                    HUBFFSocket objectUnderMouse = GetDraggableTransformUnderMouse();

                    if (objectUnderMouse != null)
                    {
                        if (objectUnderMouse.selectedtFF == FFName.None || IsInTeamList(objectUnderMouse))
                        {
                            if (!FFAlreadyPlaced(objectToDrag.selectedtFF))
                            {
                                objectUnderMouse.UpdateFFSelection(objectToDrag.selectedtFF);
                                //SaveFFPosition(objectUnderMouse.infobox);
                                objectUnderMouse.UpdateEquip();
                            }
                        }
                    }

                    objectToDrag.transform.position = originalPosition;

                    if (objectToDragImage != null)
                        objectToDragImage.raycastTarget = true;

                    objectToDrag = null;
                }

                dragging = false;
            }
            else
            {
                if (objectToDrag != null)
                {
                    var objectToReplace = GetDraggableTransformUnderMouse();

                    if (objectToReplace != null)
                    {
                        if (objectToReplace.selectedtFF == FFName.None || IsInTeamList(objectToReplace))
                        {
                            FFName tmpName = objectToDrag.selectedtFF;
                            objectToDrag.TurnOffEquipSelection();
                            objectToReplace.TurnOffEquipSelection();

                            objectToDrag.UpdateFFSelection(objectToReplace.selectedtFF);

                            objectToReplace.UpdateFFSelection(tmpName);

                            //SwapFFPosition(objectToReplace.infobox, objectToDrag.infobox);

                            objectToReplace.UpdateEquip();
                            objectToDrag.UpdateEquip();

                            objectToDrag.transform.position = originalPosition;
                        }
                        else
                        {
                            objectToDrag.transform.position = originalPosition;
                        }
                    }
                    else
                    {
                        if (Vector2.Distance(objectToDrag.transform.position, originalPosition) > swapOffset)
                        {
                            objectToDrag.transform.position = originalPosition;
                            objectToDrag.UpdateFFSelection(FFName.None);
                        }
                        else
                        {
                            objectToDrag.transform.position = originalPosition;
                            Debug.Log("Near");

                        }
                    }
                    objectToDragImage.raycastTarget = true;
                    objectToDrag = null;
                }

                swap = false;
                dragging = false;
            }
        }
    }

    public void OpenEquipGrid(HubEquipGrid _go, int _index)
    {
        if(currentGrid != null)
        {
            HubFFInfobox prevff = GetInfoBox(currentGrid);
            if(prevff != null)
                prevff.ForceEquipClose();
        }

        currentGrid = _go;
        HubFFInfobox ff = GetInfoBox(currentGrid);
        ff.OpenEquipGrid(_index);
        currentGrid.gameObject.SetActive(true);
    }

    public void CloseEquipGrid()
    {
        currentGrid?.gameObject.SetActive(false);
        currentGrid = null;
    }

    public void UpdateFFSelected(HubFFInfobox _info)
    {
        if(_info.GetSelectedFF != FFName.None)
        {
            _info.EquipGrid = EquipGridDictionary[_info.GetSelectedFF];
            EquipGridDictionary[_info.GetSelectedFF].LinkHubInfobox(_info);
        }

        _info.UpdateInfobox();
       
        ActiveMesh(_info.GetSelectedFF, GetInfoBoxIndex(_info));

        multiplierDisplayer.SetMissionMultiplier(FFInfoBoxes);

        // Turn off all mesh exept the one selected
        foreach (KeyValuePair<FFName, GameObject> item in FFMesh)
        {
            if (!FFAlreadyPlaced(item.Key))
                if(item.Value != null)
                    item.Value?.SetActive(false);
        }
    }

    public void UpdateLevelInfo()
    {
        if (AMLevelSelection.instance.GetLevelToLoad() == null) return;

        LevelName.text = AMLevelSelection.instance.GetLevelToLoad().LevelName;
        LevelObjective.text = AMLevelSelection.instance.GetLevelToLoad().GetMissionType().ToString() + " " + AMLevelSelection.instance.GetLevelToLoad().GetMissionCivilian().ToString() + " Civilians";
        LevelSideObjective.text = AMLevelSelection.instance.GetLevelToLoad().GetSideObjectiveExtesiveDescription();
    }

    public void ShowFFTip(bool _on, FFName _name = FFName.None, bool _delay = true)
    {
        PlayerTip.Show(_on, _name, _delay);
    }

    public void ShowEquipTip(bool _on, EquipName _name = EquipName.None, bool _delay = true)
    {
        EquipTip.Show(_on, _name, _delay);
    }

    public void ResetValues()
    {
        //TurnOffAllMeshes();
        for (int i = 0; i < FFInfoBoxes.Length; i++)
        {
            FFInfoBoxes[i].ResetSocket();
        }

        positionLink[0] = null;
        positionLink[1] = null;
        positionLink[2] = null;
        PopUpFeedback.gameObject.SetActive(false);

        LevelName.text = "";
        LevelObjective.text = "";
        LevelSideObjective.text = "";

        multiplierDisplayer.Reset();
    }

    #region Drag
    private GameObject GetObjectUnderMouse()
    {
        var pointer = new PointerEventData(EventSystem.current);

        pointer.position = Input.mousePosition;

        EventSystem.current.RaycastAll(pointer, hitObjects);

        if (hitObjects.Count <= 0) return null;

        return hitObjects[0].gameObject;
    }

    private HUBFFSocket GetDraggableTransformUnderMouse()
    {
        var clickedObject = GetObjectUnderMouse();

        // get top level object hit
        if (clickedObject != null && clickedObject.tag == DRAGGABLE_TAG)
        {
            return clickedObject.GetComponent<HUBFFSocket>();
        }

        return null;
    }

    #endregion

    public void SetLevelSetting()
    {
        if (AMLevelSelection.instance.GetLevelToLoad().IsFFTeamLocked())
        {
            FFName[] ffteamlock = AMLevelSelection.instance.GetLevelToLoad().GetFFLocked();
            FFEquipSet[] set = AMLevelSelection.instance.GetLevelToLoad().GetEquipLocked();

            for (int i = 0; i < ffteamlock.Length; i++)
            {
                if(i < set.Length)
                    FFInfoBoxes[i].LockSocket(ffteamlock[i], set[i]);
                else
                    FFInfoBoxes[i].LockSocket(ffteamlock[i], new FFEquipSet());
            }
        }

        UpdateLevelInfo();
    }

    #region Save

    //public void SwapFFPosition(HubFFInfobox _name1, HubFFInfobox _name2)
    //{
    //    int index_1 = GetInfoBoxIndex(_name1);
    //    int index_2 = GetInfoBoxIndex(_name2);

    //    //for (int i = 0; i < FFPosition.Length; i++)
    //    //{
    //    //    if (FFPosition[i] == _name1)
    //    //        index_1 = i;

    //    //    if (FFPosition[i] == _name2)
    //    //        index_2 = i;
    //    //}

    //    FFName temp = FFPosition[index_1];

    //    FFPosition[index_1] = FFPosition[index_2];

    //    FFPosition[index_2] = temp;
    //}

    public void SaveFFPosition(HubFFInfobox _info)
    {
        int index = GetInfoBoxIndex(_info);

        if(recapSaves[index].Firefighter == FFName.None)
        {
            recapSaves[index].FFEquipSet.EquipOne = EquipName.None;
            recapSaves[index].FFEquipSet.EquipTwo = EquipName.None;
        }


        recapSaves[index].Firefighter = _info.GetSelectedFF;
    }

    public void SelectEquip(EquipData _equip)
    {
        FFName name = GetInfoBox(currentGrid).GetSelectedFF;

        int equipIndex = GetInfoBox(currentGrid).GetCurrentEquipSlotIndex;

        for (int i = 0; i < recapSaves.Length; i++)
        {
            if (recapSaves[i].Firefighter == name)
            {
                if(equipIndex == 0)
                {
                    recapSaves[i].FFEquipSet.EquipOne = _equip.EName;
                }
                else
                {
                    recapSaves[i].FFEquipSet.EquipTwo = _equip.EName;
                }
            }
        }
    }


    public void OnUnlockSkill(FFName _name)
    {
        for (int i = 0; i < FFInfoBoxes.Length; i++)
        {
            if (FFInfoBoxes[i].GetSelectedFF == _name)
                FFInfoBoxes[i].UpdateSkillUnlockment();
        }
    }

    public void OnUnlockSkill()
    {
        for (int i = 0; i < FFInfoBoxes.Length; i++)
        {
            FFInfoBoxes[i].UpdateSkillUnlockment();
        }
    }

    public void ActiveMesh(FFName _name, int _index)
    {
        if (FFInfoBoxes[_index].GetSelectedFF == FFName.None)
        {
            if(positionLink.ContainsKey(_index) && positionLink[_index] != null)
                positionLink[_index].SetActive(false);
        }
        else
        {
            FFMesh[_name].SetActive(false);
            FFMesh[_name].transform.position = ShowcasePosition[_index].transform.position;
            FFMesh[_name].transform.rotation = ShowcasePosition[_index].transform.rotation;
            FFMesh[_name].SetActive(true);
            positionLink[_index] = FFMesh[_name];
        }
    }

    public void TurnOffMesh(FFName _name)
    {
        if(_name != FFName.None)
        {
            if (!FFAlreadyPlaced(_name))
            {
                FFMesh[_name].SetActive(false);
            }
        }

        Debug.Log(_name);
    }

    private void TurnOffAllMeshes()
    {
        foreach (KeyValuePair<FFName, GameObject> item in FFMesh)
        {
            item.Value.SetActive(false);
        }
    }
    #endregion

    #region Utility
    public bool IsInTeamList(HUBFFSocket _socket)
    {
        if (TeamSockets.Contains(_socket))
            return true;

        return false;
    }

    public bool FFAlreadyPlaced(FFName _name)
    {
        for (int i = 0; i < TeamSockets.Count; i++)
        {
            if (TeamSockets[i].selectedtFF == _name)
            {
                return true;
            }
        }


        return false;
    }

    public FFName[] GetFFTeam()
    {
        int cont = 0;
        for (int i = 0; i < FFInfoBoxes.Length; i++)
        {
           if(FFInfoBoxes[i].GetSelectedFF != FFName.None)
               cont++;
        }

        FFName[] tmp = new FFName[cont];
        int m_index = 0;
        for (int i = 0; i < FFInfoBoxes.Length; i++)
        {
            if(FFInfoBoxes[i].GetSelectedFF != FFName.None)
            {
                tmp[m_index] = FFInfoBoxes[i].GetSelectedFF;
                m_index++;
            }
        }

        return tmp;
    }

    public FFEquipSet[] GetTeamEquipSet()
    {
        FFName[] _name = GetFFTeam();
        FFEquipSet[] tmp = new FFEquipSet[3];

        for (int j = 0; j < _name.Length; j++)
        {
            for (int i = 0; i < recapSaves.Length; i++)
            {
                if(recapSaves[i].Firefighter == _name[j])
                    tmp[i] = recapSaves[i].FFEquipSet;
            }
        }
      
        return tmp;
    }

    public FFEquipSet GetFFEquipSet(FFName _name)
    {
        for (int i = 0; i < recapSaves.Length; i++)
        {
            if (recapSaves[i].Firefighter == _name)
                return recapSaves[i].FFEquipSet;
        }

        Debug.LogError("New EquipSet");
        FFEquipSet tmp = new FFEquipSet();
        tmp.EquipOne = EquipName.Fire_Axe;
        tmp.EquipTwo= EquipName.None;

        return tmp;
    }

    public void MoveToLevelCheck()
    {
        FFName[] team = GetFFTeam();

        if (AMLevelSelection.instance.GetLevelToLoad().IsFFTeamLocked())
        {
            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.StartButton);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);

            FadeScreen.gameObject.SetActive(true);
            FadeScreen.DOFade(1, 1f).OnComplete(() => GameManager.Instance.MoveFromHubToLevel());
        }
        else
        {
            for (int i = 0; i < recapSaves.Length; i++)
            {
                if (recapSaves[i].FFEquipSet.EquipTwo == EquipName.Cement_Saw)
                {
                    recapSaves[i].FFEquipSet.EquipOne = EquipName.Cement_Saw;
                    recapSaves[i].FFEquipSet.EquipTwo = EquipName.None;
                }
            }

            if (team.Length == 3)
            {
                for (int i = 0; i < team.Length; i++)
                {
                    if (!HasEquipSelected(team[i]))
                    {
                        SetPopUpFeedback("Choose at least one equip for every FF");
                        return;
                    }
                }

                AudioData audio = new AudioData();
                audio.SetUISound(UISounds.StartButton);
                EventManager.TriggerEvent(EventsID.PlayAudio, audio);

                FadeScreen.gameObject.SetActive(true);
                FadeScreen.DOFade(1, 1f).OnComplete(() => GameManager.Instance.MoveFromHubToLevel());
            }
            else if (team.Length > 0 && team.Length < 3)
            {
                for (int i = 0; i < team.Length; i++)
                {
                    if (!HasEquipSelected(team[i]))
                    {
                        SetPopUpFeedback("Choose at least one equip for every FF");
                        return;
                    }
                }

                AudioData audio = new AudioData();
                audio.SetUISound(UISounds.StartButton);
                EventManager.TriggerEvent(EventsID.PlayAudio, audio);

                FadeScreen.gameObject.SetActive(true);
                FadeScreen.DOFade(1, 1f).OnComplete(() => GameManager.Instance.MoveFromHubToLevel());
            }
            else
            {
                SetPopUpFeedback("Select at least one firefighter");
            }
        }
    }

    private bool HasEquipSelected(FFName _FFname)
    {
        for (int i = 0; i < recapSaves.Length; i++)
        {
            if(recapSaves[i].Firefighter == _FFname)
            {
                if (recapSaves[i].FFEquipSet.EquipOne != EquipName.None || recapSaves[i].FFEquipSet.EquipTwo != EquipName.None)
                    return true;
            }
        }

        return false;
    }

    public GameObject GetMesh(FFName _name)
    {
        return FFMesh[_name];
    }
    #endregion

    private HubFFInfobox GetInfoBox(HubEquipGrid _grid)
    {
        foreach (HubFFInfobox item in FFInfoBoxes)
        {
            if (item != null)
                if (item.EquipGrid == _grid)
                    return item;
        }

        return null;
    }

    private int GetInfoBoxIndex(HubFFInfobox _info)
    {
        for (int i = 0; i < FFInfoBoxes.Length; i++)
        {
            if (FFInfoBoxes[i] == _info) return i;
        }

        return 0;
    }

    public void SetPopUpFeedback(string _text)
    {
        PopUpFeedbackText.text = _text;
        PopUpFeedback.gameObject.SetActive(true);
        PopUpFeedback.gameObject.transform.DOScale(Vector2.zero, 0.5f).From();
    }

}
