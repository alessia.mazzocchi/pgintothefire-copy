﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubLevelSelectionCollider : MonoBehaviour , IHub
{
    private HubInteractable interact;

    private void Awake()
    {
        //interact = GetComponent<HubInteractable>();
    }
    public void OnClick()
    {
        HubHandler.Instance.MoveToLevelSeleciton();
        HubHandler.Instance.DeactiveSelector();
        //interact.ActiveOutline(false);
    }
}
