﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class HubEquipGrid : MonoBehaviour
{
    public HubEquipButton Empty;

    public List<HubEquipButton> Melee;
    public List<HubEquipButton> Extinguish;
    public List<HubEquipButton> Support;

    private HubFFInfobox infobox;

    public void Load(FFName _name)
    {
        SkillTreeTemplate tree = GameManager.Instance.GetSkillTreeTemplate(_name);

        Empty.SetEquip(EquipName.None);

        for (int i = 0; i < tree.Melee.Length; i++)
        {
            Melee[i].SetEquip(tree.Melee[i].SkillPoint.UnlockEquip());
        }

        for (int i = 0; i < tree.Extinguish.Length; i++)
        {
            Extinguish[i].SetEquip(tree.Extinguish[i].SkillPoint.UnlockEquip());
        }

        for (int i = 0; i < tree.Utility.Length; i++)
        {
            Support[i].SetEquip(tree.Utility[i].SkillPoint.UnlockEquip());
        }
    }

    public void LinkHubInfobox(HubFFInfobox _box)
    {
        infobox = _box;
    }

    public void CloseGrid()
    {
        infobox.ForceEquipClose();
    }

    public void CheckEquipUnlock(FFName _name)
    {
        SkillTreeData data = GameManager.Instance.GetFFDataLoaded(_name).SkillTree;

        foreach (HubEquipButton item in Melee)
        {
            if (data.IsEquipUnlock(item.Equip.EName))
                item.LockEquip(false);
            else
                item.LockEquip(true);
        }

        foreach (HubEquipButton item in Extinguish)
        {
            if (data.IsEquipUnlock(item.Equip.EName))
                item.LockEquip(false);
            else
                item.LockEquip(true);
        }

        foreach (HubEquipButton item in Support)
        {
            if (data.IsEquipUnlock(item.Equip.EName))
                item.LockEquip(false);
            else
                item.LockEquip(true);
        }
    }

    public void SelectEquip(EquipData _data)
    {
        infobox.SelectEquip(_data);
        gameObject.SetActive(false);
    }

    public void SaveEquip(EquipName _EquipName)
    {
        infobox.SaveEquip(_EquipName);
    }
}
