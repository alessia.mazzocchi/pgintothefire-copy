﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FFOverviewTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string Type; // Perk, Status
    public UIFFOverview Recap;
    private string m_text;

    private void Start()
    {
        if (Type == "Perk")
        {
            m_text = Recap.data.quirk.Description;
        }
        else if (Type == "Status")
        {
            m_text = Recap.HealthStatusDescription;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Recap.ShowStatsTip(m_text);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Recap.CloseStatsTip();
    }
}
