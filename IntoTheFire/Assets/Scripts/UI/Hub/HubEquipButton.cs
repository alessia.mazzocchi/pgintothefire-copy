﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HubEquipButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public EquipData m_equip;
    public EquipData Equip { get { return m_equip; } }
    [Space]
    public GameObject Lock;
    public Image IconImage;

    private Button button;
    private HubEquipGrid grid;

    private void Awake()
    {
        grid = GetComponentInParent<HubEquipGrid>();
        button = GetComponent<Button>();

        //Debug.Log("Awake button");
    }

    public void OnClick()
    {
        grid.SelectEquip(m_equip);

        if (HubHandler.Instance.HubState == HubState.Briefing)
        {
            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.BrEquipSelect);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);
        }

        HubBriefingHandler.Instance.ShowEquipTip(false, EquipName.None, false);
        HubBriefingHandler.Instance.ShowFFTip(false, FFName.None, false);
    }

    public void LockEquip(bool _lock)
    {
        button.interactable = !_lock;
        Lock.SetActive(_lock);
    }

    public void SetEquip(EquipName _equip)
    {
        if(_equip == EquipName.None)
        {
            m_equip = GameManager.Instance.GetActionNewInstance(BaseActionName.PickUp);
            Lock.SetActive(false);
        }
        else
        {
            m_equip = GameManager.Instance.GetEquipData(_equip);
        }

        if (m_equip == null) Debug.Log(_equip);

        IconImage.sprite = m_equip.Sprite;

        grid = GetComponentInParent<HubEquipGrid>();
        button = GetComponent<Button>();

        //grid.SaveEquip(Equip.EName);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        HubBriefingHandler.Instance.ShowEquipTip(true, m_equip.EName);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        HubBriefingHandler.Instance.ShowEquipTip(false, m_equip.EName);
    }
}
