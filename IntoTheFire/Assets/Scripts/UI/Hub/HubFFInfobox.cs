﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Diagnostics.Contracts;
using UnityEditor;

[System.Serializable]
public struct HubEquipSlot
{
    public Image Container;
    public Image Icon;
    public Button Button;
    [HideInInspector]
    public EquipName EName;
}

public class HubFFInfobox : MonoBehaviour
{
    public TextMeshProUGUI NameText;
    // Index 0
    public HubEquipSlot EquipOne;
    // Index 1
    public HubEquipSlot EquipTwo;

    private int equipIndex;
    public int GetCurrentEquipSlotIndex
    {
        get => equipIndex;
    }

    public HubEquipGrid EquipGrid;

    public Color EnlightButtonColor;
    public HUBFFSocket socket;

    public FFName GetSelectedFF
    {
        get
        {
            return socket.selectedtFF;
        }
    }

    private void Awake()
    {
        //socket = GetComponentInChildren<HUBFFSocket>();
        socket.infobox = this;
    }

    private void Start()
    {
        //EquipGrid.gameObject.SetActive(false);
        equipIndex = -1;
    }

    public void ChooseFF(FFName _name)
    {
        if (socket.selectedtFF == FFName.None)
        {
            NameText.text = "";
            ForceEquipClose();
            EquipOne.Container.gameObject.SetActive(false);
            EquipTwo.Container.gameObject.SetActive(false);
        }

        HubBriefingHandler.Instance.UpdateFFSelected(this);

    }

    // Equipment
    public void OpenEquipClick(int _index)
    {
        if (_index == equipIndex)
        {
            HubBriefingHandler.Instance.CloseEquipGrid();
            HubBriefingHandler.Instance.ShowFFTip(false, GetSelectedFF);

            ForceEquipClose();
        }
        else
        {
            //equipIndex = _index;
            //EnlightColor(equipIndex, true);
            HubBriefingHandler.Instance.OpenEquipGrid(EquipGrid, _index);
            HubBriefingHandler.Instance.ShowFFTip(true, GetSelectedFF);
        }
    }

    public void OpenEquipGrid(int _index)
    {
        equipIndex = _index;
        EnlightColor(equipIndex, true);
        EquipGrid?.gameObject.SetActive(true);
    }

    public void ForceEquipClose()
    {
        EquipOne.Container.color = Color.white;
        EquipTwo.Container.color = Color.white;
        HubBriefingHandler.Instance.ShowFFTip(false, FFName.None, false);

        EquipGrid?.gameObject.SetActive(false);
        equipIndex = -1;
    }

    public void EnlightColor(int _index, bool _Enlight)
    {
        if(_index == 0)
        {
            EquipOne.Container.color = (_Enlight)? EnlightButtonColor : Color.white;
            EquipTwo.Container.color = (!_Enlight) ? EnlightButtonColor : Color.white;
        }
        else if( _index == 1)
        {
            EquipOne.Container.color = (!_Enlight) ? EnlightButtonColor : Color.white;
            EquipTwo.Container.color = (_Enlight) ? EnlightButtonColor : Color.white;
        }
    }

    public void SelectEquip(EquipData _data)
    {
        if(equipIndex == 0)
        {
            if(EquipTwo.EName == _data.EName)
            {
                HubBriefingHandler.Instance.SetPopUpFeedback("Choose two different equipment");
            }
            else
            {
                if(_data.EName == EquipName.Cement_Saw)
                {
                    EquipTwo.Container.gameObject.SetActive(false);
                    EquipTwo.EName = EquipName.None;
                }
                else
                {
                    if(EquipOne.EName == EquipName.Cement_Saw)
                    {
                        EquipTwo.Container.gameObject.SetActive(true);
                        EquipTwo.Icon.sprite = GameManager.Instance.GetEquipData(EquipTwo.EName).Sprite;
                    }
                }

                EquipOne.Icon.sprite = _data.Sprite;
                EquipOne.EName = _data.EName;
                EquipOne.Container.color = Color.white;

                HubBriefingHandler.Instance.SelectEquip(_data);
            }
        }
        else
        {
            if (EquipOne.EName == _data.EName)
            {
                HubBriefingHandler.Instance.SetPopUpFeedback("Choose two different equipment");
            }
            else
            {
                if(_data.EName == EquipName.Cement_Saw)
                {
                    EquipOne.Container.gameObject.SetActive(false);
                    EquipOne.EName = EquipName.None;
                }
                else
                {
                    if(EquipTwo.EName == EquipName.Cement_Saw)
                    {
                        EquipOne.Container.gameObject.SetActive(true);
                        EquipOne.Icon.sprite = GameManager.Instance.GetEquipData(EquipOne.EName).Sprite;

                    }
                }

                EquipTwo.Icon.sprite = _data.Sprite;
                EquipTwo.EName = _data.EName;
                EquipTwo.Container.color = Color.white;

                HubBriefingHandler.Instance.SelectEquip(_data);
            }
        }

        //if (set.EquipOne == EquipName.Cement_Saw)
        //{
        //    EquipOne.EName = set.EquipOne;
        //    EquipOne.Icon.sprite = GameManager.Instance.GetEquipData(EquipOne.EName).Sprite;
        //    Debug.Log("Cement saw");
        //    EquipTwo.Container.gameObject.SetActive(false);
        //    EquipTwo.EName = EquipName.None;
        //}
        //else
        //{
        //    if (EquipOne.EName == EquipName.Cement_Saw && set.EquipOne != EquipName.Cement_Saw)
        //    {
        //        EquipTwo.Container.gameObject.SetActive(true);
        //        EquipTwo.Icon.sprite = GameManager.Instance.GetEquipData(EquipTwo.EName).Sprite;
        //    }

        //    EquipOne.EName = set.EquipOne;
        //    EquipOne.Icon.sprite = GameManager.Instance.GetEquipData(EquipOne.EName).Sprite;
        //}

        //if (set.EquipTwo == EquipName.Cement_Saw)
        //{
        //    EquipTwo.EName = set.EquipTwo;
        //    EquipTwo.Icon.sprite = GameManager.Instance.GetEquipData(EquipTwo.EName).Sprite;

        //    EquipOne.Container.gameObject.SetActive(false);
        //    EquipOne.EName = EquipName.None;
        //}
        //else
        //{
        //    if (EquipTwo.EName == EquipName.Cement_Saw && set.EquipTwo != EquipName.Cement_Saw)
        //    {
        //        EquipOne.Container.gameObject.SetActive(true);
        //        EquipOne.Icon.sprite = GameManager.Instance.GetEquipData(EquipOne.EName).Sprite;
        //    }

        //    EquipTwo.EName = set.EquipTwo;
        //    EquipTwo.Icon.sprite = GameManager.Instance.GetEquipData(EquipTwo.EName).Sprite;
        //}

        ForceEquipClose();
    }

    public void SaveEquip(EquipName _Ename)
    {
        //HubHandler.Instance.SaveFFEquip(socket.selectedtFF, _Ename, equipIndex);
    }

    public void SaveEquip(EquipName _Ename, int _index)
    {
        //HubHandler.Instance.SaveFFEquip(socket.selectedtFF, _Ename, _index);
    }

    public void SetEquip(FFEquipSet set)
    {
        EquipOne.EName = set.EquipOne;
        EquipOne.Icon.sprite = GameManager.Instance.GetEquipData(EquipOne.EName).Sprite;

        EquipTwo.EName = set.EquipTwo;
        EquipTwo.Icon.sprite = GameManager.Instance.GetEquipData(EquipTwo.EName).Sprite;
    }

    public void UpdateEquip()
    {
        FFEquipSet set = HubBriefingHandler.Instance.GetFFEquipSet(GetSelectedFF);
        SetEquip(set);
    }

    // Player Socket
    public void UpdateInfobox()
    {
        if (GetSelectedFF == FFName.None) return;

        NameText.text = GetSelectedFF.ToString();
        EquipOne.Container.gameObject.SetActive(true);
        EquipTwo.Container.gameObject.SetActive(true);

        EquipGrid.CheckEquipUnlock(GetSelectedFF);
    }

    public void ResetSocket()
    {
        socket.ResetSocket();

        EquipOne.Button.interactable = true;
        EquipTwo.Button.interactable = true;

        EquipOne.Container.gameObject.SetActive(false);
        EquipTwo.Container.gameObject.SetActive(false);
    }

    public void UpdateSkillUnlockment()
    {
        EquipGrid.CheckEquipUnlock(GetSelectedFF);
    }
    public void LockSocket(FFName _name, FFEquipSet _set)
    {
        socket.SetLockFF(_name);

        SetEquip(_set);

        EquipOne.Button.interactable = false;
        EquipTwo.Button.interactable = false;

        //EquipOne.Container.gameObject.SetActive(false);
        //EquipTwo.Container.gameObject.SetActive(false);
    }
}
