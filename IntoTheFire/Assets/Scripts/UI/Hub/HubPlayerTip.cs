﻿using DG.Tweening;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HubPlayerTip : MonoBehaviour
{
    public TextMeshProUGUI FFNameText;
    public TextMeshProUGUI QuirkDescription;
    public TextMeshProUGUI HealthText;
    public TextMeshProUGUI MovementText;
    public Image QuirkIcon;

    private bool isVisible;
    private bool active;

    private Tween SlideTween;
    private Tween FadeTween;
    private CanvasGroup group;
    private RectTransform rect;

    public float SlideTime = 0.25f;
    public float TimeToFade;
    private float currentTime;

    private void Awake()
    {
        group = GetComponent<CanvasGroup>();
        rect = transform as RectTransform;
    }

    private void Start()
    {
        isVisible = false;

        group.alpha = 0;
    }

    void Update()
    {
        if (isVisible && !active)
        {
            currentTime += Time.deltaTime;
            if(currentTime >= TimeToFade)
            {
                isVisible = false;

                Show(false);
                currentTime = 0;
            }
        }
    }

    public void Show(bool _on, FFName _name = FFName.None, bool _delay = true)
    {
        if (isVisible)
        {
            if (_on)
            {
                SetValues(_name);
                currentTime = 0;
            }
        }
        else
        {
            if (_on)
            {
                isVisible = true;
                if (SlideTween == null)
                    SlideTween = rect.DOAnchorPos(new Vector2(-400, 0), SlideTime).SetRelative().SetAutoKill(false);
                else
                    SlideTween.Restart();

                if (FadeTween == null)
                    FadeTween = group.DOFade(1, SlideTime).SetAutoKill(false);
                else
                    FadeTween.Restart();
                
                SetValues(_name);
            }
            else
            {
                if (SlideTween != null)
                    SlideTween.SmoothRewind();

                if (FadeTween != null)
                    FadeTween.SmoothRewind();
            }
        }

        if (!_delay) currentTime = TimeToFade;

        active = _on;
    }

    private void SetValues(FFName _name)
    {
        FFData data = GameManager.Instance.GetFFDataLoaded(_name);

        FFNameText.text = data.FName.ToString();
        HealthText.text = data.baseHealth.ToString();
        MovementText.text = data.baseMovement.ToString();

        QuirkIcon.sprite = data.quirk.Sprite;
        QuirkDescription.text = data.quirk.Description;
    }
}
