﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubBoard : MonoBehaviour, IHub
{
    //private HubInteractable interact;
    //private void Awake()
    //{
    //    interact = GetComponent<HubInteractable>();
    //}

    public void OnClick()
    {
        HubHandler.Instance.MoveToBoard();
    }
}
