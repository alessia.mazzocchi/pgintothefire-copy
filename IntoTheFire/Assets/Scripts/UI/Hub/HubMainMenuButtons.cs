﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HubMainMenuButtons : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private TextMeshProUGUI text;
    private Image image;

    private Tween colorTweenOn;
    private Tween scaleTweenOn;

    private Color startColor;
    private Vector3 startScale;

    public Color BaseColor;
    public Color EnlightColor;

    private void Awake()
    {
        text = GetComponentInChildren<TextMeshProUGUI>();
        image = GetComponent<Image>();

        startColor = text.color;
        startScale = transform.localScale;

        text.color = BaseColor;
    }

    private void OnEnable()
    {
        text.color = startColor;
        transform.localScale = startScale;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (colorTweenOn == null)
            colorTweenOn = text.DOBlendableColor(EnlightColor, 0.25f).SetAutoKill(false);
        else
            colorTweenOn.Restart();

        if (scaleTweenOn == null) 
            scaleTweenOn = transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.25f).SetAutoKill(false);
        else
            scaleTweenOn.Restart();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        colorTweenOn.SmoothRewind();
        scaleTweenOn.SmoothRewind();
    }

    public void Quit()
    {
        GameManager.Instance.Quit();
    }
}
