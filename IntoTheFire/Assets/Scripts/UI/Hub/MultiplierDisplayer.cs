﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MultiplierDisplayer
{
    TextMeshProUGUI text;

    public  MultiplierDisplayer(TextMeshProUGUI _text)
    {
        text = _text;
        text.text = "";
    }

    public void SetMissionMultiplier(HubFFInfobox[] box)
    {
        bool update = false;
        text.text = "";
        float tot = 1;
        int line = 3;

        for (int i = 0; i < box.Length; i++)
        {
            if (box[i].GetSelectedFF!= FFName.None)
            {
                update = true;
                line--;

                float tempValue = GetMuiltiplierValue(GameManager.Instance.GetFFDataLoaded(box[i].GetSelectedFF).HealthStatus);
                tot += tempValue;
                text.text += box[i].GetSelectedFF + "(" + GetHealthStatus(GameManager.Instance.GetFFDataLoaded(box[i].GetSelectedFF).HealthStatus) + ")    "  + " +" +  tempValue + "\n";
                //text.text += box[i].GetSelectedFF + " (" + GetHealthStatus(GameManager.Instance.GetFFDataLoaded(box[i].GetSelectedFF).HealthStatus) + ") " + "\n";
            }
        }

        // Do not write the cost unless there is at least one firefighter
        if (update)
        {
            //text.text += "<size=130%> = " + tot + "</size>" + "\n \n";

            //int value = (int)(AMLevelSelection.instance.GetLevelToLoad().GetBaseMedals() * tot);

            for (int i = 0; i < line ; i++)
                text.text += "\n";
            
            text.text += "\n <size=150%><color=yellow> Multiplier:     " + tot + "</color> </size>";
        }
    }

    private float GetMuiltiplierValue(FFHealhStatus _status)
    {
        switch (_status)
        {
            case FFHealhStatus.Healthy: return 0.5f;

            case FFHealhStatus.Tired: return 0.25f;

            case FFHealhStatus.Exhausted: return 0;
            
            default: return 0;
        }
    }

    private string GetHealthStatus(FFHealhStatus _status)
    {
        switch (_status)
        {
            case FFHealhStatus.Healthy: return "<color=green> Healthy </color>"; 

            case FFHealhStatus.Tired: return "<color=orange> Tired </color>";

            case FFHealhStatus.Exhausted: return "<color=red> Exhausted </color>";

            default: return "";
        }
    }

    public void Reset()
    {
        text.text = "";
    }
}
