﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class HubEquipTip : MonoBehaviour
{
    public TextMeshProUGUI EquipNameText;
    public TextMeshProUGUI EquipDescription;
    public Image EquipIcon;

    private bool isVisible;
    private bool active;

    private Tween SlideTween;
    private Tween FadeTween;
    private CanvasGroup group;
    private RectTransform rect;

    public float SlideTime = 0.25f;
    public float TimeToFade;
    private float currentTime;

    private void Awake()
    {
        group = GetComponent<CanvasGroup>();
        rect = transform as RectTransform;
    }

    private void Start()
    {
        isVisible = false;

        group.alpha = 0;
    }

    void Update()
    {
        if (isVisible && !active)
        {
            currentTime += Time.deltaTime;
            if (currentTime >= TimeToFade)
            {
                isVisible = false;

                Show(false);
                currentTime = 0;
            }
        }
    }

    public void Show(bool _on, EquipName _name = global::EquipName.None, bool delay = true)
    {
        if (isVisible)
        {
            if (_on)
            {
                SetValues(_name);
                currentTime = 0;
            }
        }
        else
        {
            if (_on)
            {
                isVisible = true;
                if (SlideTween == null)
                    SlideTween = rect.DOAnchorPos(new Vector2(400, 0), SlideTime).SetRelative().SetAutoKill(false);
                else
                    SlideTween.Restart();

                if (FadeTween == null)
                    FadeTween = group.DOFade(1, SlideTime).SetAutoKill(false);
                else
                    FadeTween.Restart();

                SetValues(_name);
            }
            else
            {
                if (SlideTween != null)
                    SlideTween.SmoothRewind();

                if (FadeTween != null)
                    FadeTween.SmoothRewind();
            }
        }

        if (!delay) currentTime = TimeToFade;

        active = _on;
    }

    private void SetValues(EquipName _name)
    {
        EquipData data = GameManager.Instance.GetEquipData(_name);

        EquipNameText.text = data.UIName.ToString();

        EquipIcon.sprite = data.Sprite;
        EquipDescription.text = data.Description;
    }
}
