﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HubFFPool : MonoBehaviour
{
    public Image IconImage;
    public FFName currentFF;
    private Vector2 startPosition;

    private void Start()
    {
        startPosition = transform.localPosition;
        IconImage.sprite = GameManager.Instance.GetFFInfo(currentFF).BaseSprite;
    }


}
