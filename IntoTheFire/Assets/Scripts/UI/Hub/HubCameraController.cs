﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class HubCameraController : MonoBehaviour
{
    private float velocity;
    public float Speed = 1;

    public Vector2 CameraLimit;

    public float moveTime = 0.35f;
    public AnimationCurve Curve;
    private bool fixPos;
    private float currentMoveToPositionTime;
    private Vector3 startPos;
    private Vector3 endPos;

    private HubHandler hub;

    private void Start()
    {
        hub = HubHandler.Instance;
    }

    void Update()
    {
        if (HubHandler.Instance.HubState != HubState.Hub || HubHandler.Instance.blockMouseOver) return;

        velocity = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate()
    {
        if (fixPos)
        {
            currentMoveToPositionTime += Time.fixedDeltaTime;

            float t = currentMoveToPositionTime / moveTime;

            transform.position = Vector3.Lerp(startPos, endPos, Curve.Evaluate(t));

            if (t >= 1)
            {
                fixPos = false;

                currentMoveToPositionTime = 0;

                transform.position = endPos;
            }
        }

        //if (hub.HubState != HubState.Hub || hub.inTutorial) return;

    }

    private void LateUpdate()
    {
        if (hub.HubState != HubState.Hub || hub.inTutorial) return;

        if (velocity != 0)
            transform.Translate(transform.right * velocity * Time.deltaTime * Speed);
        
        float xLimit = Mathf.Clamp(transform.position.x, CameraLimit.x, CameraLimit.y);
        Vector3 limit = new Vector3(xLimit, transform.position.y, transform.position.z);

        transform.position = limit;
    }

    public void MoveToPosition(Vector3 _pos)
    {
        fixPos = true;
        currentMoveToPositionTime = 0;

        startPos = transform.position;
        endPos = _pos;
    }
}
