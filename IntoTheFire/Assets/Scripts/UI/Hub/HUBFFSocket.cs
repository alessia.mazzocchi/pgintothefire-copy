﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HUBFFSocket : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public bool IsPoolSocket = false;
    [HideInInspector]
    public bool lockFF = false;

    [HideInInspector]
    public HubFFInfobox infobox;

    public Image IconImage;
    public Image LockImage;

    public FFName selectedtFF = FFName.None;

    private Vector2 SocketSpaceMin;
    private Vector2 SocketSpaceMax;

    private RectTransform rect;
    private Image socket;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        socket = GetComponent<Image>();

        if(LockImage != null)
            LockImage.gameObject.SetActive(false);
    }

    private void Start()
    {
        //UpdateFFSelection(selectedtFF); // DEV
        //SetInteractableSpace();

        if (selectedtFF != FFName.None)
        {
            Load();
        }

    }

    public void Load()
    {
        UpdateFFSelection(selectedtFF);
    }

    public void UpdateFFSelection(FFName _name)
    {
        if (lockFF) return;

        selectedtFF = _name;
        //HubBriefingHandler.Instance.TurnOffMesh(selectedtFF);

        if (selectedtFF == FFName.None)
            IconImage.color = Color.clear;
        else
        {
            IconImage.sprite = GameManager.Instance.GetFFInfo(selectedtFF).BaseSprite;
            IconImage.color = Color.white;

            //Debug.Log("DEV! SET FF");

            if (HubHandler.Instance.HubState == HubState.Briefing)
            {
                AudioData audio = new AudioData();
                audio.SetUISound(UISounds.BrFFSelect);
                EventManager.TriggerEvent(EventsID.PlayAudio, audio);
            }

            //DEV: NEEDS FIX

        }

        infobox?.ChooseFF(selectedtFF); // DEV
    }

    public void UpdateEquip()
    {
        infobox?.UpdateEquip();
    }

    public void ResetSocket()
    {
        lockFF = false;
        IconImage.raycastTarget = true;
        LockImage.gameObject.SetActive(false);

        UpdateFFSelection(FFName.None);
    }

    public void OnClick()
    {
        //HubHandler.Instance.OpenRecap(selectedtFF);
    }

    public void TurnOffEquipSelection()
    {
        infobox.ForceEquipClose();
    }

    public void SetLockFF(FFName _name)
    {
        UpdateFFSelection(_name);

        lockFF = true;
        IconImage.raycastTarget = false;

        if (LockImage != null)
            LockImage.gameObject.SetActive(true);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(IsPoolSocket)
            HubBriefingHandler.Instance.ShowFFTip(true, selectedtFF);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(IsPoolSocket)
            HubBriefingHandler.Instance.ShowFFTip(false, selectedtFF);

    }
}
