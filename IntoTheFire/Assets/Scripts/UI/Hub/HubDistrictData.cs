﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class HubDistrictData : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool IsLocked;
    public District District;
    private int IndexDistrict;

    public AMLevelObject[] Levels;

    public Sprite SelectedSprite;
    private Sprite baseSprite;
    private Tween PopTween;
    public TextMeshProUGUI NameText;
    public TextMeshProUGUI MedalsText;
    private Button button;

    private void Awake()
    {
        NameText =  GetComponentInChildren<TextMeshProUGUI>();
        button = GetComponent<Button>();
    }

    private void Start()
    {
        NameText.text = AMLevelSelection.instance.GetDistrictName(District);

        baseSprite = button.image.sprite;
        button.onClick.AddListener(OnClick);

        //if (IsLocked)
        //    button.interactable = false;
    }

    public void SetDistrictIndex(int _index)
    {
        IndexDistrict = _index;

        int maxM = 0;
        int currentM = 0;
        for (int i = 0; i < Levels.Length; i++)
        {
            Levels[i].SetLevelIndex(IndexDistrict, i);
            currentM += GameManager.Instance.GetLevelMedalsRecord(Levels[i].GetLevelData.LevelIndex);

            //if(_index != 0)
                maxM += Levels[i].GetLevelData.GetMaxMedals();
            //else
                //maxM += Levels[i].GetLevelData.GetBaseMedals();

        }

        Vector2Int limit = new Vector2Int(_index, Levels.Length - 1);
        GameManager.Instance.ProgressionLimit.Add(limit);
        MedalsText.text = $"{currentM}/{maxM}";
    }

    public bool HasLevel(LevelData _data, out Vector2Int LIndex)
    {
        for (int i = 0; i < Levels.Length; i++)
        {
            if (Levels[i].GetLevelData == _data)
            {
                LIndex = Levels[i].LevelIndex;
                return true;
            }
        }

        LIndex = new Vector2Int(-1, 0);
        return false;
    }

    public void OnClick()
    {
        AMLevelSelection.instance.SelectDistrict(District);

        button.image.sprite = baseSprite;
    }

    public void OpenDistrict()
    {
        GameManager gm = GameManager.Instance;
        Vector2Int animationIndex = gm.LevelToUnlock;

        foreach (AMLevelObject item in Levels)
        {
            item.ShowName(true);

            if(animationIndex != gm.NoLevelToUnlock)
            {
                if(item.GetLevelData.LevelIndex == animationIndex)
                {
                    item.UnlockAnimation();

                    animationIndex = gm.NoLevelToUnlock;
                    gm.ResetLevelToLoad();
                }
            }
        }
    }

    public void UpdateLevelUIPosition()
    {
        foreach (AMLevelObject item in Levels)
        {
            item.UpdatePosition();
        }
    }

    public void CloseDistrict()
    {
        foreach (AMLevelObject item in Levels)
        {
            item.ShowName(false);
        }
    }

    public void LockDistrict(bool _isLock)
    {
        button.interactable = !_isLock;
        IsLocked = _isLock;

        if (_isLock == true)
            LockLevels();
        else
            UnlockLevels();
    }

    public void LockLevels()
    {
        foreach (AMLevelObject item in Levels)
        {
            item.Lock(true);
        }
    }

    public void UnlockLevels(int _progression)
    {
        //Debug.Log(_progression);
        for (int i = 0; i < Levels.Length; i++)
        {
            if (i == 0 &&_progression == 0)
                Levels[i].Lock(false);
            else
            {
                Levels[i].Lock(i > _progression);
            }
        }
    }

    /// <summary>
    /// Unlock all levels
    /// </summary>
    public void UnlockLevels()
    {
        for (int i = 0; i < Levels.Length; i++)
        {
            Levels[i].Lock(false);
        }
    }

    public AMLevelObject GetLastLevel()
    {
        return Levels[Levels.Length - 1];
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (IsLocked) return;

        if (PopTween == null)
            PopTween = transform.DOScale(new Vector3(1.15f, 1.15f, 1), 0.25f).SetEase(Ease.OutSine).SetAutoKill(false);
        else
            PopTween.Restart();

        button.image.sprite = SelectedSprite;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (IsLocked) return;

        PopTween.SmoothRewind();

        button.image.sprite = baseSprite;

    }
}
