﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIWinGameRecap : MonoBehaviour
{
    [Header("Titles")]
    public CanvasGroup StarsTitle;
    public CanvasGroup MedalsTitle;
    public CanvasGroup CopmpletionTitle;
    public CanvasGroup ContinueButtonTitle;
    [Space]
    public TextMeshProUGUI StarsText;
    public TextMeshProUGUI CompletitionText;
    public TextMeshProUGUI MedalsMin;
    public TextMeshProUGUI MedalsMax;
    public Slider MedalSlider;
    [Space]
    public GameObject VictimPrefab;
    public RectTransform victimContainer;
    public float victimDelay;
    List<GameObject> victimRef;
    Sequence victimSequence;

    int currentStars = 0;
    int totStars = 0;

    int currentMedals = 0;
    int totMedals;
    int percentage;

    private void Awake()
    {
        victimRef = new List<GameObject>();
        for (int i = 0; i < 41; i++)
        {
            GameObject v = Instantiate(VictimPrefab, victimContainer);
            victimRef.Add(v);
            v.transform.localScale = Vector3.zero;
        }

        CopmpletionTitle.alpha = 0;
        StarsTitle.alpha = 0;
        MedalsTitle.alpha = 0;
        ContinueButtonTitle.alpha = 0;
    }

    public void SetStat()
    {
        foreach (KeyValuePair<Vector2Int, LevelSaveData> item in GameManager.Instance.LevelSaveData)
        {
            currentStars += item.Value.GetSideObjAchived;
            totStars += item.Value.SideObjectiveAchived.Length;

            currentMedals += item.Value.MedalsEarned;
        }

        for (int i = 0; i < AMLevelSelection.instance.DistrictRef.Length; i++)
        {
            for (int j = 0; j < AMLevelSelection.instance.DistrictRef[i].Levels.Length; j++)
            {
                totMedals += AMLevelSelection.instance.DistrictRef[i].Levels[j].GetLevelData.GetMaxMedals();
            }
        }

        int pctStars = (currentStars * 100) / totStars;
        int pctMedals = (currentMedals * 100) / totMedals;

        percentage = Mathf.CeilToInt((pctStars + pctMedals) / 2);
        MedalSlider.maxValue = totMedals;

        MedalsMin.text = currentMedals.ToString();
        MedalsMax.text = totMedals.ToString();

        MedalsMin.transform.localScale = Vector3.zero;
        StarsText.transform.localScale = Vector3.zero;
        CompletitionText.transform.localScale = Vector3.zero;

        StarsText.text = $"{currentStars} / {totStars}";

        CompletitionText.text = $"{percentage}%";
    }

    public void StartAnimation()
    {
        victimSequence = DOTween.Sequence();
        for (int i = 0; i < victimRef.Count; i++)
        {
            victimSequence.Append(victimRef[i].transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack));
            //victimSequence.SetDelay(victimDelay);
        }

        victimSequence.Append(MedalsTitle.DOFade(1, 0.25f));
        victimSequence.AppendInterval(0.5f);

        victimSequence.Append(MedalsMin.transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack));
        victimSequence.AppendInterval(0.5f);

        victimSequence.Append(MedalSlider.DOValue(currentMedals, 0.75f).SetEase(Ease.OutSine));
        victimSequence.AppendInterval(0.5f);

        victimSequence.Append(StarsTitle.DOFade(1, 0.25f));
        victimSequence.Append(StarsText.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack));
        victimSequence.AppendInterval(0.5f);

        victimSequence.Append(CopmpletionTitle.DOFade(1, 0.25f));
        victimSequence.Append(CompletitionText.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack));

        victimSequence.AppendInterval(0.5f);
        victimSequence.Append(ContinueButtonTitle.DOFade(1, 0.25f).OnComplete(() => ContinueButtonTitle.transform.DOScale(1.15f, 0.5f).SetLoops(-1, LoopType.Yoyo)));
    }
}
