﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFFOverviewSelection : MonoBehaviour
{
    [HideInInspector]
    public UIFFOverview recap;

    private void Awake()
    {
        recap = GetComponentInParent<UIFFOverview>();
    }


    public void Init()
    {
        List<FFData> team = GameManager.Instance.GetFFTeamData;

        for (int i = 0; i < transform.childCount; i++)
        {
            FFOverviewButton btn = transform.GetChild(i).GetComponent<FFOverviewButton>();
            btn.Init(this, team[i]);
        }
    }

    public void MoveTo(FFName _name)
    {
        recap.SwitchRecap(_name);
        Debug.Log("MoveTp" + _name);
    }
}
