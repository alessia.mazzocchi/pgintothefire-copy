﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HubMainMenu : MonoBehaviour
{
    public Transform CameraPos;
    [Header("UI")]
    public Image BackGround;
    public GameObject Title;
    public GameObject ButtonContainer;
    public GameObject CreditsContainer;


    private Tween titleTween;
    private Tween buttonTween;
    private Tween bgTween;

    private void Start()
    {
        CreditsContainer.gameObject.SetActive(false);
    }

    public void TurnOn(bool Instant = false)
    {
        //if (titleTween == null)
        //    titleTween = (Title.transform as RectTransform).DOAnchorPos(new Vector2(-529, 402), 0.5f).SetEase(Ease.OutBack).SetAutoKill(false);
        //else
        //    titleTween.Restart();

        //if (buttonTween == null)
        //    buttonTween = (ButtonContainer.transform as RectTransform).DOAnchorPos(new Vector2(250, -60.8f), 0.5f).SetEase(Ease.OutBack).SetAutoKill(false);
        //else
        //    buttonTween.Restart();

        Title.SetActive(true);
        ButtonContainer.SetActive(true);
        BackGround.gameObject.SetActive(false);
        bgTween.SmoothRewind();

    }

    public void TurnOff()
    {
        //titleTween.SmoothRewind();
        //buttonTween.SmoothRewind();

        Title.SetActive(false);
        ButtonContainer.SetActive(false);

        BackGround.gameObject.SetActive(true);

        if (bgTween == null)
            bgTween = BackGround.DOColor(Color.clear, 0.5f).SetAutoKill(false).OnComplete(() => BackGround.gameObject.SetActive(false));
        else
            bgTween.Restart();
    }

    public void ActiveCredit(bool _isActive)
    {
        CreditsContainer.SetActive(_isActive);

        Title.SetActive(!_isActive);
        ButtonContainer.SetActive(!_isActive);
    }
}
