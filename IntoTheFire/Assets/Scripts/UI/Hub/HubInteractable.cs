﻿using UnityEngine;

public class HubInteractable : MonoBehaviour, IHubMouseOver
{
    private LAOutlineScript outline;
    private IHub brain;

    private void Awake()
    {
        brain = GetComponent<IHub>();

        outline = GetComponentInChildren<LAOutlineScript>();
        
        if(outline == null)
        {
            outline = GetComponent<LAOutlineScript>();
        }
    }

    private void Start()
    {
        //outline.enabled = false;
    }

    public void ActiveOutline(bool _active) { outline.enabled = _active; }

    public GameObject ASGameObject() => this.gameObject;

    public void MouseEnter()
    {
        if (HubHandler.Instance.HubState != HubState.Hub) return;

        //HubHandler.Instance.ActiveSelector(transform.position);
        outline?.Show(true);
        CustomCursor.SetCursor(CursorStyle.Hand);

    }

    public void MouseExit()
    {
        if (HubHandler.Instance.HubState != HubState.Hub) return;

        //HubHandler.Instance.DeactiveSelector();

        outline?.Show(false);
        CustomCursor.SetCursorDefault();
    }

    public void MouseClick()
    {
        brain?.OnClick();
        CustomCursor.SetCursorDefault();

        AudioData audio = new AudioData();
        audio.SetUISound(UISounds.ButtonConfirm);
        EventManager.TriggerEvent(EventsID.PlayAudio, audio);
    }
}
