﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubFF : MonoBehaviour, IHub
{
    public FFName Name;
    private HubInteractable interact;

    private void Awake()
    {
        interact = GetComponent<HubInteractable>();    
    }

    public void OnClick()
    {
        HubHandler.Instance.OpenRecap(Name);
        HubHandler.Instance.DeactiveSelector();
        //interact.ActiveOutline(false);
    }

}
