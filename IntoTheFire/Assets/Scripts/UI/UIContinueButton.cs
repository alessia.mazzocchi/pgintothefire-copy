﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIContinueButton : MonoBehaviour
{
    public void OnClick()
    {
        LevelManager.instance.PauseMenu();
    }
}
