﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public enum TooltipTypes
{
    UICharacter,
    UIEquip,
    Entity,
    Info
}

public class UIMouseOverTooltips : MonoBehaviour
{
    public LayerMask MouseOverAvailableLayer;

    [Header("Character")]
    public CanvasGroup CharacterContainer;
    public TextMeshProUGUI CharDescription;
    public Image CharSprite;

    [Header("Equip")]
    public CanvasGroup EquipContainer;
    public TextMeshProUGUI EquipName;
    public TextMeshProUGUI EquipCharges;
    public TextMeshProUGUI EquipDescription;
    public TextMeshProUGUI EquipDamage;
    public TextMeshProUGUI EquipRange;
    public TextMeshProUGUI EquipAoE;
    public Image DirectionSprite;
    public Sprite FourDirection, EightDirection;

    [Header("Entity")]
    public GameObject EntityContainer;
    public TextMeshProUGUI EntityName;
    public Slider EntityHealthSlider;
    public TextMeshProUGUI EntityHealth;


    [Header("Info")]
    public GameObject InfoContainer;
    public TextMeshProUGUI InfoText;


    protected virtual void Update()
    {
        FollowMouse();
    }

    private void FollowMouse()
    {
        // Works for canvas render mode "screen space overlay"
        transform.position = Input.mousePosition;
    }

    public void SetTooltip(string _description)
    {
        InfoText.text = _description;

        ActiveContainer(TooltipTypes.Info);
    }

    public void SetTooltip(Obstacle _obs)
    {
        EntityName.text = _obs.name;
        EntityHealthSlider.maxValue = _obs.HealthMax;
        EntityHealthSlider.value = _obs.HealthCurrent;

        EntityHealth.text = _obs.HealthCurrent + " / " + _obs.HealthMax;


        ActiveContainer(TooltipTypes.Entity);
    }

    public void SetTooltip(LiquidBehaviour _obs)
    {
        if (_obs.Type == LiquidType.Fuel) return;

        EntityName.text = _obs.Type.ToString();
        EntityHealthSlider.maxValue = _obs.HealthMax;
        EntityHealthSlider.value = _obs.HealthCurrent;

        EntityHealth.text = _obs.HealthCurrent + " / " + _obs.HealthMax;

        ActiveContainer(TooltipTypes.Entity);
    }

    public void SetTooltip(FireFighterBehaviour _entity)
    {
        EntityName.text = _entity.Name.ToString();
        EntityHealthSlider.maxValue = _entity.HealthMax;
        EntityHealthSlider.value = _entity.HealthCurrent;

        EntityHealth.text = _entity.HealthCurrent + " / " + _entity.HealthMax;

        ActiveContainer(TooltipTypes.Entity);
    }

    public void SetTooltip(FlameBehaviour _entity)
    {
        ActiveContainer(TooltipTypes.Entity);
    }

    public void SetTooltip(FFData _ffData)
    {
        CharDescription.text = _ffData.quirk.Description;
        CharSprite.sprite = _ffData.quirk.Sprite;

        ActiveContainer(TooltipTypes.UICharacter);
    }

    public void SetTooltip(Equip _equip)
    {
        EquipName.text = _equip.UIName;
        EquipDescription.text = _equip.Description;

        DirectionSprite.sprite = (_equip.Directions == CheckRadius.FourDirection) ? FourDirection : EightDirection;
        EquipAoE.text = "";

        switch (_equip.EName)
        {
            case global::EquipName.Fire_Axe:
                EquipDamage.text = GetDamage(_equip.Value);
                EquipRange.text = GetRange(_equip.RangeMinMax);
                EquipAoE.text = GetAoEInt(_equip.RangeMinMax.y);
                    
                break;
            case global::EquipName.Sledgehammer:
                EquipDamage.text = GetDamage(_equip.Value);
                EquipRange.text = GetRange(_equip.RangeMinMax);
                EquipAoE.text = GetAoEInt(_equip.RangeMinMax.y);

                break;
            case global::EquipName.Cement_Saw:
                EquipDamage.text = GetDamage(_equip.Value);
                EquipRange.text = GetRange(_equip.RangeMinMax);
                EquipAoE.text = GetAoEInt(_equip.RangeMinMax.y);

                break;
            case global::EquipName.Fire_Extinguisher:
                EquipDamage.text = GetDamage(_equip.Value);
                EquipRange.text = GetRange(_equip.RangeMinMax);
                EquipAoE.text = GetAoE(_equip.EffectRange);

                break;
            case global::EquipName.Foam_Grenades:
                EquipDamage.text = GetDamage(_equip.Value);
                EquipRange.text = GetRange(_equip.RangeMinMax);
                EquipAoE.text = GetAoE(_equip.EffectRange);

                break;
            case global::EquipName.Water_Nozzle:
                EquipDamage.text = GetDamage(_equip.Value);
                EquipRange.text = GetRange(_equip.RangeMinMax);
                EquipAoE.text = GetAoE(_equip.EffectRange);

                break;
            case global::EquipName.Smoke_Fan:
                EquipDamage.text = "";
                EquipRange.text = GetRange(_equip.RangeMinMax);
                EquipAoE.text = GetAoE(_equip.EffectRange);

                break;
            case global::EquipName.Medikit:
                EquipDamage.text = GetHeal(_equip.Value);
                EquipRange.text = GetRange(_equip.RangeMinMax);
                EquipAoE.text = GetAoEInt(_equip.RangeMinMax.y);

                break;
            case global::EquipName.Water_Shield:
                EquipDamage.text = "";
                EquipAoE.text = GetAoEInt(_equip.RangeMinMax.y);
                EquipRange.text = "Range \n <#43b8f2> self";
                break;
            case global::EquipName.None:
                EquipDamage.text = (_equip.Value != 0) ? GetDamage(_equip.Value) : "";
                EquipRange.text = GetRange(_equip.RangeMinMax);
                GetAoEInt(_equip.RangeMinMax.y);
                break;

            default:

                Debug.Log("Default tooltip state");
                break;
        }

        if (_equip.Charges > 0)
            EquipCharges.text = "Charges \n <color=green>" + _equip.CurrentChargets + " / " + _equip.Charges;
        else
        {
            EquipCharges.text = "Charges \n <color=green> \u221E ";
        }

        ActiveContainer(TooltipTypes.UIEquip);
    
        string GetDamage(float _dmg)
        {
            return $"<color=red> {(int)_dmg} </color><size=70%>        DMG";
        }

        string GetHeal(float _dmg)
        {
            return $"<color=green> {(int)_dmg} </color><size=70%>        HEAL";
        }

        string GetAoEInt(int _area)
        {
            return $"AoE \n <#43b8f2> {_area}";
        }

        string GetAoE(Vector2Int _area)
        {
            return $"AoE \n <#43b8f2> {_area.x} X {_area.y}";
        }

        string GetRange(Vector2Int _area) 
        {
            return $"Range \n <#43b8f2> {_area.y}";
        }
    }

    protected void ActiveContainer(TooltipTypes _type)
    {
        if(_type == TooltipTypes.UIEquip)
        {
            EquipContainer.DOFade(1, 0.15f);
        }

        if (_type == TooltipTypes.UICharacter)
        {
            CharacterContainer.DOFade(1, 0.3f);
            (CharacterContainer.transform as RectTransform).DOAnchorPos(new Vector2(41.8f, -145.9f), 0.3f).SetEase(Ease.OutBack);
        }

        EntityContainer.SetActive(_type == TooltipTypes.Entity);

        InfoContainer.SetActive(_type == TooltipTypes.Info);
    }

    public virtual void DeactiveAll()
    {
        EquipContainer.DOFade(0, 0.15f);

        CharacterContainer.DOFade(0, 0.3f);
        (CharacterContainer.transform as RectTransform).DOAnchorPos(new Vector2(41.8f, -75.6f), 0.15f).SetEase(Ease.OutSine);

        EntityContainer.SetActive(false);

        InfoContainer.SetActive(false);
    }
}
