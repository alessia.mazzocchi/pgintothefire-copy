﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class UILevelObjective : MonoBehaviour
{
    public TextMeshProUGUI MainObjectiveText;

    public Transform SideObjectiveContainer;
    public TextMeshProUGUI SideObjectivePrefab;

    private List<TextMeshProUGUI> SOList = new List<TextMeshProUGUI>();
    
    public void Load()
    {
        UpdateMainObjective();

        LoadSideObjective();
    }

    public void UpdateMainObjective()
    {
        switch (MissionManager.Instance.MissionType)
        {
            case MissionTypes.Escape:
                MainObjectiveText.text = "Bring all firefighters to the exit points";
                break;
            case MissionTypes.Rescue:
                MainObjectiveText.text = LevelManager.instance.GetSavedCivilians().ToString() + " / " + LevelManager.instance.m_MinimumCiviliansToSave.ToString() + " Civilians";
                break;
            case MissionTypes.Extinguish:
                MainObjectiveText.text = "Extinguish all flames in the level";

                break;
            default:
                break;
        }
    }

    public void UpdateSideObjectives()
    {
        for (int i = 0; i < SOList.Count; i++)
        {
            if (MissionManager.Instance.SideObjectives[i].IsCompleted())
            {
                SOList[i].text = "<b>[<#15D400>V</color>] </b> <s>" + MissionManager.Instance.SideObjectives[i].GetDescription+ "</s>";
            }
        }
    }

    private void LoadSideObjective()
    {
        for (int i = 0; i < MissionManager.Instance.SideObjectives.Count; i++)
        {
            TextMeshProUGUI temp = Instantiate(SideObjectivePrefab, SideObjectiveContainer);

            temp.text = "<b>[ ] </b> " + MissionManager.Instance.SideObjectives[i].GetDescription;
            temp.gameObject.SetActive(true);

            SOList.Add(temp);
        }
    }
}
