﻿using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UILevelRecap : MonoBehaviour
{
    public TextMeshProUGUI LevelNameText;
    public TextMeshProUGUI ObjectiveText;
    public TextMeshProUGUI TurnLimitText;
    public TextMeshProUGUI SituationText;
    public TextMeshProUGUI SideObjText;
    [Space]
    public TextMeshProUGUI CompletitionText;
    [Header("Medals")]
    public TextMeshProUGUI MedalsText;
    public TextMeshProUGUI MinMedals;
    public TextMeshProUGUI MaxMedals;
    public Slider MedalSlider;
    public Color MedalColor;

    [Space]
    public TextMeshProUGUI StarsText;
    public RectTransform CircleOut, CircleIn;
    [Space]
    public Image CompleteImage;
    public Sprite CompleteSprite;
    public Sprite NotCompleteSprite;
    [Space]
    public UISideObjRecap SideObjPrefab;
    public RectTransform SideObjContaienr;
    private List<UISideObjRecap> UIRecapList;

    private void Awake()
    {

    }

    public void Set(LevelData _data)
    {
        if(UIRecapList == null)
        {
            UIRecapList = new List<UISideObjRecap>();
            for (int i = 0; i < 3; i++)
            {
                UISideObjRecap tmp = Instantiate(SideObjPrefab, SideObjContaienr);

                UIRecapList.Add(tmp);

                tmp.Disactive();
            }
        }

        ClearUIList();
        MaxMedals.color = Color.white;

        LevelNameText.text = _data.LevelName;
        TurnLimitText.text = _data.GetTurns().ToString() + " turns";
        ObjectiveText.text = _data.GetMissionType().ToString() + " " + _data.GetMissionCivilian().ToString() + " Civilians";
        //SituationText.text = _data.GetMissionDescription();
        SituationText.text = "";
        SituationText.DOText(_data.GetMissionDescription(), 2f).SetDelay(0.75f);

        List<SideObjective> obj = _data.GetSideObjective();

        for (int i = 0; i < obj.Count; i++)
        {
            PoolUISideObj().Set(obj[i], GameManager.Instance.GetLevelSideObjectiveCompleted(_data.LevelIndex, i));
        }
        

        //SideObjText.text = _data.GetSideObjectiveExtesiveDescription();

        if (GameManager.Instance.LevelCompleted(_data.LevelIndex))
        {
            CompletitionText.text = "Completed";
            CompleteImage.sprite = CompleteSprite;
        }
        else
        {
            CompletitionText.text = "Not completed";
            CompleteImage.sprite = NotCompleteSprite;
        }
        int current, max;

        current = GameManager.Instance.GetLevelMedalsRecord(_data.LevelIndex);
        max = _data.GetMaxMedals();
        MedalsText.text = current.ToString();
        MinMedals.text = "0";
        MaxMedals.text = max.ToString();

        MedalSlider.maxValue = max;
        MedalSlider.value = current;

        if (current == max)
        {
            MedalsText.text = "";
            MaxMedals.color = MedalColor;
        }

        //if(_data.LevelIndex.x != 0)
        //    MedalsText.text = $"{GameManager.Instance.GetLevelMedalsRecord(_data.LevelIndex)} / <color=red> {_data.GetBaseMedals()} </color> - <color=green>{ _data.GetMaxMedals()} </color>";
        //else
        //    MedalsText.text = $"{GameManager.Instance.GetLevelMedalsRecord(_data.LevelIndex)} / {_data.GetBaseMedals()} - { _data.GetMaxMedals()}";

        StarsText.text = $"{GameManager.Instance.GetLevelStarsRecord(_data.LevelIndex)} / {_data.GetSiseObjectiveSize()}";
    }

    private District GetDistrict(LevelData _data)
    {
        int x = _data.LevelIndex.x;

        switch (x)
        {
            case 0: return District.D1;
            case 1: return District.D2;
            case 2: return District.D3;
            case 3: return District.D4;
            case 4: return District.D5;

            default: return District.None;
        }
    }

    //public void RotateAnimation()
    //{
    //    if(RotateTweenIn == null)
    //    {
    //        RotateTweenIn = CircleIn.DORotate(new Vector3(0, 0, 360), 1f).SetLoops(-1, LoopType.Incremental).SetAutoKill(false);
    //    }
    //    else
    //    {
    //        RotateTweenIn.Restart();
    //    }

    //    if (RotateTweenOut == null)
    //    {
    //        RotateTweenOut = CircleOut.DORotate(new Vector3(0, 0, 360), 1f).SetLoops(-1, LoopType.Incremental).SetAutoKill(false);
    //    }
    //    else
    //    {
    //        RotateTweenOut.Restart();
    //    }
    //}

    private UISideObjRecap PoolUISideObj()
    {
        foreach (UISideObjRecap item in UIRecapList)
        {
            if (!item.isActive)
            {
                item.gameObject.SetActive(true);
                return item;
            }
        }

        return null;
    }

    private void ClearUIList()
    {
        foreach (UISideObjRecap item in UIRecapList)
        {
            item.Disactive();
        }
    }

    public void Back()
    {
        AMLevelSelection.instance.BackButton();

        ClearUIList();
    }

    public void Continue()
    {
        HubHandler.Instance.MoveToBriefing();

        ClearUIList();
    }


}
