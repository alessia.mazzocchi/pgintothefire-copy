﻿using DG.Tweening;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UILevelHud : MonoBehaviour
{
    public TextMeshProUGUI LevelText;
    public TextMeshProUGUI MedalsText;
    public TextMeshProUGUI StarsText;

    public Image Outline;
    public Sprite CompleteSprite;
    public Sprite NotCompleteSprite;
    [Space]
    public Image LockFade;
    public Image LockIcon;
    public Image StarImage;
    private Tween PopTween;
    [Space]
    public Color BaseColor;
    public Color HighlightColor;

    public void SetHud(LevelData _data)
    {
        Debug.Log(_data.LevelName);
        LockFade.gameObject.SetActive(false);

        LevelText.text = _data.LevelName;

        int currentM = (GameManager.Instance.LevelSaveData.ContainsKey(_data.LevelIndex)) ? GameManager.Instance.LevelSaveData[_data.LevelIndex].MedalsEarned : 0;

        MedalsText.text = currentM +  " / " + /*((_data.LevelIndex.x == 0)? _data.GetBaseMedals() : _data.GetMaxMedals())*/_data.GetMaxMedals();

        if(_data.GetSiseObjectiveSize() != 0)
        {
            int currentS = 0;
            if (GameManager.Instance.LevelSaveData.ContainsKey(_data.LevelIndex))
            {
                for (int i = 0; i < GameManager.Instance.LevelSaveData[_data.LevelIndex].SideObjectiveAchived.Length; i++)
                {
                    if (GameManager.Instance.LevelSaveData[_data.LevelIndex].SideObjectiveAchived[i])
                        currentS++;
                }
            }

            StarsText.text = currentS + " / " + _data.GetSiseObjectiveSize();
        }
        else
        {
            //StarsText.text = "";
            StarImage.gameObject.SetActive(false);
        }

        Outline.sprite = (GameManager.Instance.LevelCompleted(_data.LevelIndex)) ? CompleteSprite : NotCompleteSprite;
    }


    public void Lock(bool isLock)
    {
        LockFade.gameObject.SetActive(isLock);
    }

    public void Popup(bool _Active)
    {
        if (_Active)
        {
            if (PopTween == null)
                PopTween = transform.DOScale(new Vector3(1.15f, 1.15f, 1), 0.25f).SetEase(Ease.OutSine).SetAutoKill(false);
            else
                PopTween.Restart();

            LevelText.color = HighlightColor;
        }
        else
        {
            LevelText.color = BaseColor;
            PopTween.SmoothRewind();
        }
    }

    public void ResetPopup()
    {
        LevelText.color = BaseColor;
        PopTween.Rewind();
    }

    public void UnlockAnimation()
    {
        LockFade.gameObject.SetActive(true);
        LockIcon.transform.DOShakePosition(0.35f, new Vector3(15, 0, 0)).SetDelay(1).SetRelative();
        LockIcon.transform.DOMoveY(40, 1.5f ).SetDelay(1).SetEase(Ease.OutExpo).SetRelative();
        //LockIcon.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.75f).SetDelay(0.75f).SetRelative();

        StartCoroutine(UnlockSound());
        
        LockIcon.DOFade(0, 0.6f).SetDelay(1.25f);
        LockFade.DOFade(0, 0.6f).SetDelay(1.25f).OnComplete(() => LockFade.gameObject.SetActive(false));
    }

    IEnumerator UnlockSound()
    {
        yield return new WaitForSeconds(1.0f);

        AudioData audio = new AudioData();
        audio.SetUISound(UISounds.LevelUnlock);
        EventManager.TriggerEvent(EventsID.PlayAudio, audio);

        StopCoroutine(UnlockSound());
    }

    private void OnDestroy()
    {
        PopTween.Kill();
    }
}
