﻿using UnityEngine;
using TMPro;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;

public enum District { None, D1, D2, D3, D4, D5 }
public enum LevelSelectionState { District, Levels, Recap}

public class AMLevelSelection : MonoBehaviour
{
    private LevelSelectionState LastLSState;
    private LevelSelectionState m_LevelSelectionState;
    public LevelSelectionState LevelSelectionState
    {
        get => m_LevelSelectionState;

        private set => ChangeState(value);
    }

    private bool canMove = true;
    private Animator animator;
    public static AMLevelSelection instance;

    #region District

    [HideInInspector]
    public District CurrentDistrict;

    public string GetDistrictName(District _district)
    {
        switch (_district)
        {
            case District.None:
                return "None";
            case District.D1:
                return "Tutorial";
            case District.D2:
                return "District 1";
            case District.D3:
                return "District 2";
            case District.D4:
                return "District 3";
            case District.D5:
                return "District 4";
        }

        return "";
    }

    public HubDistrictData[] DistrictRef;
    private Dictionary<District, HubDistrictData> DistrictDictionary;

    #endregion

    public ASMouseOverHandler<ILevelSelectioneOver> levelOver;

    [Header("Containers")]
    public GameObject MainContainer;
    public GameObject UIRecapContainer;
    public GameObject UIDisctricContainer;
    public GameObject UILevelsContainer;

    public GameObject AMCharacterContainer;

    public GameObject MapContainer;
    [Header("Camera")]
    public Camera MapCamera;
    public CinemachineStateDrivenCamera Cinemachine;
    private Tween lookTween;
    private Quaternion lastRotation;

    [Header("UI")]
    public CanvasGroup CanvasGroup;
    public UILevelRecap LevelRecap;

    public UILevelHud LevelHudPrefab;
    private LevelData currentLevel;
    //private UILevelButton selectedButton;

    private void Awake()
    {
        instance = this;
        levelOver = new ASMouseOverHandler<ILevelSelectioneOver>(MapCamera);

        DistrictDictionary = new Dictionary<District, HubDistrictData>();
        foreach (HubDistrictData item in DistrictRef)
        {
            DistrictDictionary.Add(item.District, item);
        }

        int index = 0;
        foreach (KeyValuePair<District, HubDistrictData> item in DistrictDictionary)
        {
            item.Value.SetDistrictIndex(index);
            index++;
        }

        UIRecapContainer.SetActive(false);

        animator = GetComponent<Animator>();
        animator.SetInteger("State", 0);
        CurrentDistrict = District.None;
    }


    private void Update()
    {
        if (HubHandler.Instance.HubState != HubState.LevelSelection) return;

        if (LevelSelectionState == LevelSelectionState.Levels)
        {
            levelOver.OnUpdateOver();

            if (Input.GetMouseButtonDown(0))
                levelOver.OnClick();
        }

        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            foreach (KeyValuePair<District, HubDistrictData> item in DistrictDictionary)
            {
                item.Value.LockDistrict(false);
                item.Value.UnlockLevels();
            }
        }
    }

    public void SetupProgression(Vector2Int _prog)
    {
        foreach (KeyValuePair<District, HubDistrictData> item in DistrictDictionary)
            item.Value.LockDistrict(true);
        
        foreach (KeyValuePair<District, HubDistrictData> item in DistrictDictionary)
        {
            if(DistrictIndex(item.Key) < _prog.x)
            {
                item.Value.LockDistrict(false);
            }
            else if(DistrictIndex(item.Key) == _prog.x)
            {
                item.Value.LockDistrict(false);
                item.Value.UnlockLevels(_prog.y);
            }
            else
                item.Value.LockDistrict(true);
        }

        int DistrictIndex(District _d)
        {
            switch (_d)
            {
                case District.D1: return 0;
                case District.D2: return 1;
                case District.D3: return 2;
                case District.D4: return 3;
                case District.D5: return 4;
                default: return 0;
            }
        }
    }

    public void SelectDistrict(District _district)
    {
        CurrentDistrict = _district;
        animator.SetInteger("State", (int)CurrentDistrict);
        canMove = false;

        LevelSelectionState = LevelSelectionState.Levels;
    }

    public void SetLevel(LevelData _Data, Transform _obj)
    {
        currentLevel = _Data;
        UpdateLevelInfo(_Data);
        LevelSelectionState = LevelSelectionState.Recap;
        canMove = false;

        CameraLookAt(_obj);
    }

    private void UpdateLevelInfo(LevelData levelData)
    {
        LevelRecap.Set(levelData);
        //LevelRecap.LevelNameText.text = levelData.LevelName;
        //LevelRecap.TurnLimitText.text = "Turns: " + levelData.GetTurns().ToString();
        //LevelRecap.ObjectiveText.text = levelData.GetMissionType().ToString() + " " + levelData.GetMissionCivilian().ToString() + " Civilians";
        //LevelRecap.SituationText.text = levelData.GetMissionDescription();

        //LevelRecap.SideObjText.text = levelData.GetSideObjectiveExtesiveDescription();
        UIRecapContainer.SetActive(true);
    }

    public UILevelHud SetLevelHud(AMLevelObject _data)
    {
        UILevelHud tmp = Instantiate(LevelHudPrefab, UILevelsContainer.transform);

        tmp.SetHud(_data.GetLevelData);

        tmp.transform.position = MapCamera.WorldToScreenPoint(_data.gameObject.transform.position);

        return tmp;
    }

    private void DisableLevelHud()
    {
        int tot = UILevelsContainer.transform.childCount;

        for (int i = 0; i < tot; i++)
        {
            Destroy(UILevelsContainer.transform.GetChild(i).gameObject);
        }
    }

    public string GetLevelToLoadName()
    {
        return currentLevel.LevelScene;
    }

    public LevelData GetLevelToLoad()
    {
        return currentLevel;
    }

    public Vector2Int GetLevelIndex (LevelData _level)
    {
        Vector2Int index = new Vector2Int(-1, 0);

        foreach (KeyValuePair<District,HubDistrictData> item in DistrictDictionary)
        {
            if(item.Value.HasLevel(_level, out index))
            {
                return index;
            }
        }

        return index;

    }

    public Vector2Int GetLastLevelIndex()
    {
        return DistrictRef[DistrictRef.Length - 1].GetLastLevel().LevelIndex;
    }

    public void ActiveLevelSelection(bool _isActive)
    {
        LevelSelectionState = LevelSelectionState.District;
        MapContainer.SetActive(_isActive);
        MainContainer.SetActive(_isActive);
        canMove = true;
    }

    private void ChangeState(LevelSelectionState _state)
    {
        LastLSState = LevelSelectionState;
        /// Exit old state
        switch (m_LevelSelectionState)
        {
            case LevelSelectionState.District:
                UIDisctricContainer.gameObject.SetActive(false);
                break;

            case LevelSelectionState.Levels:
                UILevelsContainer.gameObject.SetActive(false);
                levelOver.ClearLastHit();

                if (CurrentDistrict != District.None)
                    DistrictDictionary[CurrentDistrict].CloseDistrict();

                DisableLevelHud();
                break;

            case LevelSelectionState.Recap:
                UIRecapContainer.gameObject.SetActive(false);
                CanvasGroup.alpha = 0;
                MapCamera.transform.DORotateQuaternion(lastRotation, 0.5f).SetEase(Ease.OutSine).OnComplete(() => 
                { 
                    TurnOnCinemachine();
                    canMove = true;
                });
                break;
        }

        /// -------- Assign new state ------------
        m_LevelSelectionState = _state;
        /// --------------------------------------

        /// Enter new state
        switch (m_LevelSelectionState)
        {
            case LevelSelectionState.District:
                UIDisctricContainer.gameObject.SetActive(true);

                if(CurrentDistrict != District.None)
                    DistrictDictionary[CurrentDistrict].CloseDistrict();

                animator.SetInteger("State", 0);

                break;

            case LevelSelectionState.Levels:
                //Debug.Log(CurrentDistrict);
                DistrictDictionary[CurrentDistrict].OpenDistrict();
                UILevelsContainer.gameObject.SetActive(true);
                break;

            case LevelSelectionState.Recap:
                UIRecapContainer.gameObject.SetActive(true);
                break;
        }
    }

    public void BackButton()
    {
        if (!canMove) return;
        switch (LevelSelectionState)
        {
            case LevelSelectionState.District:
                CurrentDistrict = District.None;

                HubHandler.Instance.SlideFade(() => HubHandler.Instance.MoveToHub());
                canMove = false;

                break;

            case LevelSelectionState.Levels:
                LevelSelectionState = LevelSelectionState.District;
                canMove = false;

                break;

            case LevelSelectionState.Recap:
                LevelSelectionState = LevelSelectionState.Levels;
                canMove = false;

                break;
        }
    }

    public void ResetLevelSelection()
    {

    }
    
    /// <summary>
    /// Used by cinemachinme camera
    /// </summary>
    public void FadeUI()
    {
        if (LastLSState != LevelSelectionState.Recap && CurrentDistrict != District.None)
        {
            CanvasGroup.alpha = 0;
            CanvasGroup.DOFade(1, 0).SetDelay(0.5f).OnComplete(() => 
            { 
                DistrictDictionary[CurrentDistrict].UpdateLevelUIPosition(); 
                canMove = true; 
            });
        }
    }

    public void CameraLookAt(Transform _pos)
    {
        lastRotation = MapCamera.transform.rotation;
        Cinemachine.gameObject.SetActive(false);
        CanvasGroup.alpha = 0;

        Quaternion rot = Quaternion.FromToRotation(MapCamera.transform.position, _pos.position);
        MapCamera.transform.DOLookAt(_pos.position, 0.5f).SetEase(Ease.OutSine).OnComplete(() => 
        {
            CanvasGroup.alpha = 1;
            canMove = true;
        });
    }

    public void TurnOnCinemachine()
    {
        Cinemachine.gameObject.SetActive(true);

        DistrictDictionary[CurrentDistrict].UpdateLevelUIPosition();
        CanvasGroup.alpha = 1;
    }
}
