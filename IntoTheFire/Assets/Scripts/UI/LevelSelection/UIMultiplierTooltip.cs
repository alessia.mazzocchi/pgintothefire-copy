﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIMultiplierTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public CanvasGroup Tooltip;
    public Tween animationTween;
    public Tween fadeTween;

    private void Start()
    {
        Tooltip.alpha = 0;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ShowTip(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ShowTip(false);
    }

    public void ShowTip(bool _active)
    {
        if (_active)
        {
            if (fadeTween == null)
                fadeTween = Tooltip.DOFade(1, 0.3f).SetEase(Ease.OutSine).SetAutoKill(false);
            else
                fadeTween.Restart();


            if (animationTween == null)
                animationTween = Tooltip.transform.DOLocalMoveX(-250, 0.3f).SetEase(Ease.OutSine).SetRelative().SetAutoKill(false);
            else
                animationTween.Restart();
        }
        else
        {
            fadeTween.SmoothRewind();
            animationTween.SmoothRewind();
        }
    }

    private void OnDestroy()
    {
        animationTween.Kill();
        fadeTween.Kill();
    }
}
