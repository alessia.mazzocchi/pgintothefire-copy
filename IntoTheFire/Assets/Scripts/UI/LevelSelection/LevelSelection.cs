﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using Malee;

[System.Serializable]
public class LevelsList : ReorderableArray<LevelData> { }


public class LevelSelection : MonoBehaviour
{
    public static LevelSelection instance;
    [Reorderable]
    public LevelsList LevelsToLoad;

    private LevelData[] m_levelArray;
    [Header("Containers")]
    public GameObject LevelSelectionContainer;
    public GameObject BriefingContainer;

    [Header("UI")]
    public RectTransform ButtonContainer;
    public UILevelButton buttonPrefab;
    public Button StartGameButton;
    [Space]
    public TextMeshProUGUI LevelNameText;
    public TextMeshProUGUI TurnsText;
    public TextMeshProUGUI ObjectivesText;
    public TextMeshProUGUI SitutationText;
    public TextMeshProUGUI SideObjText;
    public TextMeshProUGUI MissionMultiplierText;

    private UILevelButton selectedButton;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        m_levelArray = new LevelData[LevelsToLoad.Length];

        for (int i = 0; i < LevelsToLoad.Length; i++)
        {
            m_levelArray[i] = Instantiate(LevelsToLoad[i]);

            UILevelButton tmp = Instantiate(buttonPrefab, ButtonContainer);

            tmp.LData = m_levelArray[i];
            tmp.ID = i;
        }

        BriefingContainer.SetActive(false);
        StartGameButton.interactable = false;

        //Infobox.text = "Select a level";
        //LevelName.text = "";
        //TurnsText.text = "";
        //DifficultysText.text = "";
        //ObjectivesText.text = "";
    }

    public void LevelSelected(UILevelButton _active)
    {
        selectedButton?.Deselected();

        selectedButton = _active;

        StartGameButton.interactable = true;

        UpdateLevelInfo();
    }


    private void UpdateLevelInfo()
    {
        //Infobox.text = "";

        LevelNameText.text = selectedButton.LData.LevelName;
        TurnsText.text = "Turns: " + selectedButton.LData.GetTurns().ToString();
        ObjectivesText.text = selectedButton.LData.GetMissionType().ToString() + " " +  selectedButton.LData.GetMissionCivilian().ToString() + " Civilians";
        SitutationText.text = selectedButton.LData.GetMissionDescription();

        SideObjText.text = "---";
        MissionMultiplierText.text = "---";
        //DifficultysText.text = "Difficulty : " + selectedButton.LData.GetDifficultyType().ToString();
    }


    public void LoadScene()
    {
        SceneManager.LoadScene(selectedButton.LData.LevelScene, LoadSceneMode.Single);

        Time.timeScale = 1;
    }

    public string GetLevelToLoadName()
    {
        return selectedButton.LData.LevelScene;
    }

    public LevelData GetLevelToLoad()
    {
        return selectedButton.LData;
    }

    public void MoveToBriefing()
    {
        LevelSelectionContainer.SetActive(false);

        BriefingContainer.SetActive(true);

        Camera.main.targetDisplay = 2;
    }

    public void MoveToLevelSeleciton()
    {
        LevelSelectionContainer.SetActive(true);

        BriefingContainer.SetActive(false);

        Camera.main.targetDisplay = 1;

    }
}
