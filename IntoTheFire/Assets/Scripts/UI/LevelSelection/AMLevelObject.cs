﻿using DG.Tweening;
using UnityEngine;

public class AMLevelObject : MonoBehaviour, ILevelSelectioneOver
{
    public LevelData LData;
    public GameObject PareticleContainer;
    
    private UILevelHud m_Hud;
    private bool locked;

    private LevelData m_LevelData;
    public LevelData GetLevelData { get => m_LevelData; }
    [HideInInspector]
    public Vector2Int LevelIndex;
    
    private bool enable = false;

    private new BoxCollider collider;
    private MeshRenderer render;
    private Color baseColor;
    private Tween colorTween;

    private void Awake()
    {
        render = GetComponentInChildren<MeshRenderer>();
    }

    public void SetLevelIndex(int _DistrictIndex, int _LevelIndex)
    {
        LevelIndex = new Vector2Int(_DistrictIndex, _LevelIndex);

        m_LevelData = Instantiate(LData);
        m_LevelData.LevelIndex = LevelIndex;

        collider = GetComponent<BoxCollider>();
        collider.enabled = false;

        PareticleContainer.SetActive(!GameManager.Instance.LevelCompleted(LevelIndex));
    }

    public void Lock(bool _islock)
    {
        locked = _islock;
        //m_Hud.Lock(_islock);
    }

    private void Click()
    {
        if (!locked)
        {
            AMLevelSelection.instance.SetLevel(m_LevelData, transform);
            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.ButtonConfirm);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);
        }
    }

    public void ShowName(bool _isOn) 
    {
        enable = _isOn;
        collider.enabled = _isOn;

        if (_isOn)
        {
            m_Hud = AMLevelSelection.instance.SetLevelHud(this);
            m_Hud.Lock(locked);
        }
    }

    public void UpdatePosition()
    {
        if(m_Hud != null)
            m_Hud.transform.position = AMLevelSelection.instance.MapCamera.WorldToScreenPoint(transform.position);
    }

    public void MouseEnter()
    {
        if(m_Hud != null && !locked)
        {
            m_Hud.Popup(true);

            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.ButtonSelect);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);
        }
    }

    public void MouseExit()
    {
        if (m_Hud != null && !locked)
        {
            m_Hud.Popup(false);
        }
    }

    public void MouseClick()
    {
        if (enable && !locked)
        {
            m_Hud.ResetPopup();
            Click();
        }
    }

    public void UnlockAnimation()
    {
        m_Hud.UnlockAnimation();
    }

    public GameObject ASGameObject()
    {
        return this.gameObject;
    }
}
