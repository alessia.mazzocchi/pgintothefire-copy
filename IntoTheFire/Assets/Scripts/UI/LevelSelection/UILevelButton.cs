﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UILevelButton : MonoBehaviour
{
    [HideInInspector]
    public LevelData LData;
    public int ID;

    public Color SelectedColor;
    public Color DeselectedColor;

    private Image m_image;
    private Button m_button;
    private TextMeshProUGUI m_text;

    private void Awake()
    {
        m_image = GetComponent<Image>();
        m_button = GetComponent<Button>();
        m_text = GetComponentInChildren<TextMeshProUGUI>();
    }

    void Start()
    {
        m_text.text = LData.LevelName;

        m_image.color = DeselectedColor;
    }

    public void OnClick()
    {
        Selected();
    }

    private void Selected()
    {
        LevelSelection.instance.LevelSelected(this);

        m_image.color = SelectedColor;
    }

    public void Deselected()
    {
        m_image.color = DeselectedColor;
    }


}
