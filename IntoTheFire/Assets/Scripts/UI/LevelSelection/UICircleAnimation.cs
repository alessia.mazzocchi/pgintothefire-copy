﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICircleAnimation : MonoBehaviour
{
    private RectTransform rect;

    public float speed = 1;
    public bool Direction;
    private float currentSpeed;
    private Vector3 baseRot = new Vector3(0, 0, 1);
    private void Awake()
    {
        rect = GetComponent<RectTransform>();    
    }

    private void OnEnable()
    {
        rect.eulerAngles = Vector3.zero;
        currentSpeed = (!Direction)? (speed * -1) : speed;
    }

    private void FixedUpdate()
    {
        Vector3 rot = baseRot * currentSpeed * Time.fixedDeltaTime;

        transform.eulerAngles += rot;
    }
}
