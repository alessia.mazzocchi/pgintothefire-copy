﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIOptionsButton : MonoBehaviour
{
    public GameObject AudioSettings;

    public void OnClick()
    {
        AudioSettings?.SetActive(true);
    }
}
