﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.Security.Cryptography;

public class UIEventLog : MonoBehaviour
{
    [HideInInspector]
    public bool m_isActive;
    public bool IsActive
    {
        get
        {
            return m_isActive;
        }
        set
        {
            m_isActive = value;

            Title.gameObject.SetActive(value);
            Description.gameObject.SetActive(value);

            background.color = (value)? Color.white : DeactiveColor;
        }
    }
    private Image background;    
    public TextMeshProUGUI Title;
    public TextMeshProUGUI Description;
    public Color DeactiveColor = Color.grey;
    private void Awake()
    {
        background = GetComponent<Image>();
    }

    //private void Start()
    //{
    //    background.color = Color.clear;
    //    Title.color = Color.clear;
    //    Description.color = Color.clear;
    //}

    public void SetLog(string _Title, string _descriprion)
    {
        Title.text = _Title;

        //Description.enableAutoSizing = true;
        Description.text = _descriprion;
        //float size = Description.size;
        //Description.enableAutoSizing = false;
        //Description.fontSize = size;

        IsActive = true;
        //gameObject.SetActive(true);
    }

    public void Deactive()
    {
        IsActive = false;
        //gameObject.SetActive(false);
    }

    public void Fade(bool _IsVisible)
    {
        if (_IsVisible)
        {
            background.DOColor(Color.white, 0.5f).SetEase(Ease.InSine);
            Title.DOColor(Color.white, 0.5f).SetEase(Ease.InSine);
            Description.DOColor(Color.white, 0.5f).SetEase(Ease.InSine);
        }
        else
        {
            background.DOColor(Color.clear, 0.5f).SetEase(Ease.OutSine);
            Title.DOColor(Color.clear, 0.5f).SetEase(Ease.OutSine);
            Description.DOColor(Color.clear, 0.5f).SetEase(Ease.OutSine);
        }
    }

    private void ShowText(bool _active)
    {
        Title.enabled = _active;

        Description.enabled = _active;
    }

    public void TurnOffText()
    {
        ShowText(false);
        gameObject.SetActive(false);
    }

    public void TurnOnText()
    {
        gameObject.SetActive(true);
        ShowText(true);
    }

}
