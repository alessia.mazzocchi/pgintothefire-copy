﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class UIPressingButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public ColorBlock Colors;

    protected Image TargetImage;

    private void Awake()
    {
        TargetImage = GetComponent<Image>();    
    }

    void Update()
    {
        if (ispressed)
            OnClickPressing();
            return;
      
    }

    bool ispressed = false;
    public void OnPointerDown(PointerEventData eventData)
    {
        ispressed = true;
        OnClickStart();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        ispressed = false;
        OnClickEnd();
    }

    public abstract void OnClickPressing();
    public abstract void OnClickStart();
    public abstract void OnClickEnd();
}
