﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIRotateCameraButton : UIPressingButton, IPointerDownHandler, IPointerUpHandler
{
    public bool RotateRight;

    public override void OnClickPressing()
    {
        CameraController.Instance.RotateCamera(RotateRight);
        Debug.Log("Rotate");
    }

    public override void OnClickStart()
    {
        TargetImage.DOColor(Colors.selectedColor, 0.15f);
    }

    public override void OnClickEnd()
    {
        TargetImage.DOColor(Colors.normalColor, 0.15f);
    }
}
