﻿using UnityEngine;
using UnityEngine.UI;

public class FFOverviewButton : MonoBehaviour
{
    private FFData data;
    private UIFFOverviewSelection selection;
    private Button button;

    public void Init(UIFFOverviewSelection _selection, FFData _data)
    {
        data = _data;
        selection = _selection;

        button = GetComponent<Button>();
        button.image.sprite = data.BaseSprite;

        button.onClick.AddListener(MoveTo);

        if (selection.recap.FName == data.FName)
            button.interactable = false;
    }


    private void MoveTo()
    {
        selection.MoveTo(data.FName);
    }
}
