﻿using UnityEngine;
using UnityEngine.UI;

public class UIToggleTooltipButton : MonoBehaviour
{
    private Button button;
    private Image image;

    public Color SelectedButton;
    private void Awake()
    {
        button = GetComponent<Button>();
        image = GetComponent<Image>();
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.OnSceneLoad, UpdateButtonColor);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.OnSceneLoad, UpdateButtonColor);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            UpdateButtonColor();
        }
    }

    public void OnClick()
    {
        LevelManager.instance.ToggleToolTip();
        UpdateButtonColor();
    }

    private void UpdateButtonColor()
    {
        image.color = LevelManager.instance.ActiveTooltip ? SelectedButton : Color.white;
    }
}
