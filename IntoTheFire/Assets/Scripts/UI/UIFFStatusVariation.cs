﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIFFStatusVariation : MonoBehaviour
{
    public Image icon;

    public TextMeshProUGUI Prev;
    public TextMeshProUGUI Next;

    private FFHealhStatus statusPrev;
    private FFHealhStatus statusNext;

    public void Set(Sprite _icon , FFHealhStatus _prev, FFHealhStatus _next)
    {
        icon.sprite = _icon;

        statusPrev = _prev;
        statusNext = _next;

        Prev.text = $"({GetHealthStatus(_prev)})";
        Next.text = $"({GetHealthStatus(_next)})";
    }


    private string GetHealthStatus(FFHealhStatus _status)
    {
        switch (_status)
        {
            case FFHealhStatus.Healthy: return "<color=green> Healthy </color>";

            case FFHealhStatus.Tired: return "<color=orange> Tired </color>";

            case FFHealhStatus.Exhausted: return "<color=red> Exhausted </color>";

            default: return "";
        }
    }
}
