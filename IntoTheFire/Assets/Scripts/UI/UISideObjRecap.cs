﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UISideObjRecap : MonoBehaviour
{
    [HideInInspector]
    public bool isActive;

    public Image MarkIcon;

    public Sprite Complete;
    public Color CompleteColor;
    public Sprite NotCompleted;
    public Color NotCompleteColor;


    public TextMeshProUGUI Descrription;
    public TextMeshProUGUI MedalsText;


    public void Set(SideObjective _obj, bool _completed)
    {
        Descrription.text = _obj.GetDescription;

        MarkIcon.sprite = (_completed) ? Complete : NotCompleted;
        MarkIcon.color = (_completed) ? CompleteColor : NotCompleteColor;

        //MedalsText.text = $"+ {_obj.Reward.Amount}";

        isActive = true;
    }

    public void Disactive()
    {
        gameObject.SetActive(false);
        isActive = false;
    }
}
