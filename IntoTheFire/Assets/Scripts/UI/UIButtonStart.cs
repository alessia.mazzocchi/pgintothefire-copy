﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonStart : MonoBehaviour
{
    public void TriggerGameStart()
    {
        if(LevelManager.instance != null)
        {
            Debug.Log("LV found");
            LevelManager.instance.TriggerGameStart();
        }
        else
        {
            HubBriefingHandler.Instance.MoveToLevelCheck();
        }
    }
}
