﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class UITurnCounter : MonoBehaviour/*, IPointerEnterHandler, IPointerExitHandler*/
{
    TooltipTypes TooltipType = TooltipTypes.Info;

    public string Description;

    public Image Background;
    private TextMeshProUGUI m_text;

    private Sequence turnSequence;
    //private Tween turnTween;

    private void Awake()
    {
        m_text = GetComponentInChildren<TextMeshProUGUI>();
        Background = GetComponent<Image>();
    }

    private void Start()
    {
        Background.color = Color.clear;
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.ChangePhase, UpdateUI);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.ChangePhase, UpdateUI);
    }

    public void UpdateUI()
    {
        if(LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            FlameMovement();
            //if(turnSequence == null)
            //{
            //    //turnSequence = DOTween.Sequence().SetAutoKill(false);

            //    //turnSequence.AppendCallback(FlameMovement)
            //    // //.Append((transform as RectTransform).DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.75f))
            //    // //.AppendCallback(TurnTextTween)
            //    // .AppendInterval(0.5f)
            //    // .AppendCallback(ReverseFlameMovement);
            //    //(transform as RectTransform).DOScale(new Vector3(1, 1, 1), 1).SetLoops(2, LoopType.Yoyo).OnComplete(() => m_text.text = LevelManager.instance.GetCurrentTurns().ToString());
            //    //.Append((transform as RectTransform).DOScale(new Vector3(0.8f, 0.8f, 0.8f), 0.5f).SetDelay(0.75f))
            //    //.Append((transform as RectTransform).DOAnchorPos(new Vector2(0, 15), 0.5f).SetRelative());

            //    //turnSequence.Restart();
            //}
            //else
            //{
            //    turnSequence.Restart();
            //}
          
        }

    }

    private void ReverseFlameMovement()
    {
        (transform as RectTransform).DOScale(new Vector3(1f, 1f, 1f), 0.5f).SetDelay(0.25f);
        (transform as RectTransform).DOAnchorPos(new Vector2(0, 15), 0.5f).SetDelay(0.25f).SetRelative();
    }

    private void FlameMovement()
    {
        if(Background.color != Color.white)
            Background.DOColor(Color.white, 0.5f).SetEase(Ease.OutSine);

        (transform as RectTransform).DOAnchorPos(new Vector2(0, -15), 0.75f).SetRelative();
        (transform as RectTransform).DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.75f).OnComplete(TurnTextTween);
    }

    private void TurnTextTween()
    {
        m_text.text = LevelManager.instance.GetCurrentTurns().ToString();
        ReverseFlameMovement();
        //m_text.transform.DOShakePosition(0.5f, 10).OnComplete(ReverseFlameMovement);
    }

    //public void OnPointerEnter(PointerEventData eventData)
    //{
    //    if (!LevelManager.instance.ActiveTooltip) return;

    //    UIManager.Instance.SetTooltip(true, Description);
    //}

    //public void OnPointerExit(PointerEventData eventData)
    //{
    //    if (!LevelManager.instance.ActiveTooltip) return;

    //    UIManager.Instance.SetTooltip(false, Description);
    //}
}
