﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIPlayerElement : MonoBehaviour
{
    protected virtual void OnEnable()
    {
        EventManager.StartListening<FireFighterBehaviour>(EventsID.FFChange, UpdateOnChangeFF);
        EventManager.StartListening(EventsID.UpdateUIValues, UpdateOnChangeValues);
        EventManager.StartListening(EventsID.ChangePhase, UpdateOnEndTurn);

    }

    protected virtual void OnDisable()
    {
        EventManager.StopListening<FireFighterBehaviour>(EventsID.FFChange, UpdateOnChangeFF);
        EventManager.StopListening(EventsID.UpdateUIValues, UpdateOnChangeValues);
        EventManager.StopListening(EventsID.ChangePhase, UpdateOnEndTurn);
    }

    public virtual void UpdateOnChangeFF(FireFighterBehaviour _ff) { }

    public virtual void UpdateOnChangeValues() { }

    public virtual void UpdateOnEndTurn() { }
}
