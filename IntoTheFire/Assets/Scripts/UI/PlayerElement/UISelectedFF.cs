﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISelectedFF : MonoBehaviour
{
    private Animator m_anim;

    private void Awake()
    {
        m_anim = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.ChangePhase, OnPhaseSlide);
        EventManager.StartListening(EventsID.GameStart, TriggerSlideOut);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.ChangePhase, OnPhaseSlide);
        EventManager.StopListening(EventsID.GameStart, TriggerSlideOut);
    }

    private void TriggerSlideIn()
    {
        m_anim.SetBool("Hide", false);
    }

    private void TriggerSlideOut()
    {
        m_anim.SetBool("Hide", true);
    }

    private void OnPhaseSlide()
    {
        if (LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            TriggerSlideIn();
        }
        else if (LevelManager.instance.CurrentPhase == TurnPhase.AI)
            TriggerSlideOut();
    }
}
