﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SliderContainer
{
    public GameObject Container;

    public Transform SocketHolder;
    public Image[] Sockets;

    public Transform BarHolder;
    public Image[] Bars;

    public SliderContainer(int _health)
    {
        Sockets = new Image[_health];
        Bars = new Image[_health];
    }

    public void Active(bool _Active)
    {
        SocketHolder.gameObject.SetActive(_Active);
        BarHolder.gameObject.SetActive(_Active);
    }

    public void SetCurrentValue(int _currentValue)
    {
        //int limit = Bars.Length - _currentH;
        for (int i = 0; i < Bars.Length; i++)
        {
            if (i > _currentValue - 1)
            {
                Bars[i].color = Color.clear;
            }
            else
            {
                Bars[i].color = Color.white;
            }
        }
    }
}


public class UIPlayerMovement : UIPlayerElement
{
    public GameObject HolderPrafab;

    public GameObject SocketPrefab;
    public GameObject BarPrefab;

    private Dictionary<FFName, SliderContainer> FFMovementBar;
    private FFName currentFF;

    //protected override void OnEnable()
    //{
    //    base.OnEnable();
    //    EventManager.StartListening(EventsID.UpdateUIValues, TempUpdateUI);
    //}

    //protected override void OnDisable()
    //{
    //    base.OnDisable();
    //    EventManager.StopListening(EventsID.UpdateUIValues, TempUpdateUI);
    //}

    public void Load()
    {
        Debug.Log("Health load");
        FFMovementBar = new Dictionary<FFName, SliderContainer>();
        LevelManager lm = LevelManager.instance;

        for (int i = 0; i < lm.FFInLevel.Count; i++)
        {
            FFName cName = lm.FFInLevel[i].Name;
            int max = (int)lm.FFInLevel[i].baseMovement;

            SliderContainer htemp = new SliderContainer(max);

            FFMovementBar.Add(cName, htemp);

            //htemp.Container = new GameObject(cName + " Container");
            //htemp.Container.transform.SetParent(transform);

            htemp.SocketHolder = Instantiate(HolderPrafab, transform).transform;
            htemp.SocketHolder.name = (cName + " SocketHolder");

            htemp.BarHolder = Instantiate(HolderPrafab, transform).transform;
            htemp.BarHolder.name = (cName + " BarHolder");

            for (int j = 0; j < max; j++)
            {
                Image s_img = Instantiate(SocketPrefab, htemp.SocketHolder).GetComponent<Image>();
                htemp.Sockets[j] = s_img;

                Image b_img = Instantiate(BarPrefab, htemp.BarHolder).GetComponent<Image>();
                htemp.Bars[j] = b_img;
            }



            //GameObject hold = Instantiate(HolderPrafab, transform);
            //hold.name = lm.FFInLevel[i].Name + " - equip holder";

            //FFHealthBar.Add(lm.FFInLevel[i].Name, hold);
        }
    }

    public override void UpdateOnChangeFF(FireFighterBehaviour _ff)
    {
        //m_slider.maxValue = _ff.HealthMax;
        //m_slider.value = _ff.HealthCurrent;

        foreach (KeyValuePair<FFName, SliderContainer> item in FFMovementBar)
        {
            item.Value.Active(item.Key == _ff.Name);
        }

        currentFF = _ff.Name;

        FFMovementBar[currentFF].SetCurrentValue((int)_ff.currentMovement);

    }

    public override void UpdateOnChangeValues()
    {
        FFMovementBar[currentFF].SetCurrentValue((int)LevelManager.instance.GetSelectedFF.currentMovement);
    }

}
