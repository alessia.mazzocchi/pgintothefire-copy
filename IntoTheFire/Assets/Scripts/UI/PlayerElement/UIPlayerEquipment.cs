﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class UIPlayerEquipment : UIPlayerElement
{
    public GameObject EquipHolder;

    public GameObject EquipButtonPrefab;
    public GameObject PushButtonPrefab;
    public GameObject DestroyButtonPrefab;
    public GameObject PickupButtonPrefab;


    private Dictionary<FFName, GameObject> FFEquipBar;


    private RectTransform rect;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
    }

    public void LoadEquip()
    {
        FFEquipBar = new Dictionary<FFName, GameObject>();
        LevelManager lm = LevelManager.instance;

        for (int i = 0; i < lm.FFInLevel.Count; i++)
        {
            // Create equipment holder object
            GameObject hold = Instantiate(EquipHolder, transform);
            hold.name = lm.FFInLevel[i].Name + " - equip holder";

            FFEquipBar.Add(lm.FFInLevel[i].Name, hold);
            int KeyBindingIndex = 1;

            int limit = lm.FFInLevel[i].Inventory.Length;

            for (int j = 0; j < limit; j++)
            {
                // Remove an equip with the CementSaw
                if(j == 0)
                    if(lm.FFInLevel[i].Inventory[0].EName == EquipName.Cement_Saw || lm.FFInLevel[i].Inventory[1].EName == EquipName.Cement_Saw)
                        limit--;

                if (lm.FFInLevel[i].Inventory[j] != null)
                {
                    GameObject go = Instantiate(EquipButtonPrefab, hold.transform);
                    go.GetComponent<UIEquipButton>().SetEquip(lm.FFInLevel[i].Inventory[j], KeyBindingIndex);
                    KeyBindingIndex++;
                }
            }

            if (lm.FFInLevel[i].baseActions.destroyAction != null)
            {
                GameObject go = Instantiate(DestroyButtonPrefab, hold.transform);
                go.GetComponent<UIPlayerDestroy>().SetEquip(lm.FFInLevel[i].baseActions.destroyAction, KeyBindingIndex);
                KeyBindingIndex++;
            }

            if (lm.FFInLevel[i].baseActions.pushAction != null)
            {
                GameObject go = Instantiate(PushButtonPrefab, hold.transform);
                go.GetComponent<UIPlayerPush>().SetEquip(lm.FFInLevel[i].baseActions.pushAction, KeyBindingIndex);
                KeyBindingIndex++;
            }

            hold.SetActive(false);
        }
    }

    public override void UpdateOnChangeFF(FireFighterBehaviour _ff)
    {
        foreach (KeyValuePair<FFName, GameObject> item in FFEquipBar)
        {
            item.Value.SetActive(item.Key == _ff.Name);
        }
    }

    public override void UpdateOnEndTurn()
    {
        if (LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            rect.DOAnchorPos(new Vector2(0, -450), 0.5f).SetEase(Ease.OutBack);
        }
        else if (LevelManager.instance.CurrentPhase == TurnPhase.AI)
        {
            rect.DOAnchorPos(new Vector2(0, -616), 0.5f).SetEase(Ease.InBack);
        }
    }

    public void ActiveFFEquipAtKeycode(int _index)
    {
        if(FFEquipBar[LevelManager.instance.GetSelectedFF.Name].transform.childCount >= _index)
            FFEquipBar[LevelManager.instance.GetSelectedFF.Name].transform.GetChild(_index - 1)?.GetComponent<UIEquipButton>().OnClick();
    }
}
