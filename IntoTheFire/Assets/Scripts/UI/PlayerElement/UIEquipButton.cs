﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIEquipButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    TooltipTypes TooltipType = TooltipTypes.UIEquip;

    protected Button m_button;
    protected Image m_BGImage;

    public Image BGImage
    {
        get
        {
            if (m_BGImage == null)
            {
                m_BGImage = GetComponent<Image>();
            }

            return m_BGImage;
        }
    }

    public Image IconImage;
    public TextMeshProUGUI KeyBindText;

    protected Equip m_equip;
    public Equip ButtonEquip
    {
        get
        {
            return m_equip;
        }
    }

    public GameObject UseContaner;
    private TextMeshProUGUI m_useText;
    public TextMeshProUGUI UseText
    {
        get 
        {
           if(m_useText == null)
                m_useText = UseContaner?.GetComponentInChildren<TextMeshProUGUI>();

            return m_useText;
        }

    }

    protected virtual void Awake()
    {
        m_button = GetComponent<Button>();
        //m_useText = UseContaner.GetComponentInChildren<TextMeshProUGUI>();
    }

    protected virtual void Start()
    {
        //UIManager.Instance.SetUIEquipReference(this);

       
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.UpdateVictimActionsUI, UpdateBackground);
        EventManager.StartListening<FireFighterBehaviour>(EventsID.FFChange, SetInteractive);
        EventManager.StartListening(EventsID.UpdateUIValues, SetInteractive);

        //UpdateBackground();

        SetInteractive();
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.UpdateVictimActionsUI, UpdateBackground);
        EventManager.StopListening<FireFighterBehaviour>(EventsID.FFChange, SetInteractive);
        EventManager.StopListening(EventsID.UpdateUIValues, SetInteractive);

    }

    public virtual void OnClick()
    {
        if (LevelManager.instance.FfIsMoving || LevelManager.instance.GameStatus == GameStatus.Pause) return;

        if (LevelManager.instance.GetSelectedFF.IsUsable())
        {
            if (LevelManager.instance.GetSelectedFF.CanAct)
            {
                /// SameEquip, disequip
                if(InputManager.Instance.selectedEquip == m_equip)
                {
                    InputManager.Instance.EquipSelected(m_equip);
                    ActiveButton(true);
                    UIManager.Instance.EquipSelected(this);
                }
                // Equip
                else
                {
                    m_equip.OnClick();
                    UIManager.Instance.EquipSelected(this);

                    ActiveButton(false);
                }
            }
            else
            {
                if((m_equip as EPickUp) != null && (m_equip as EPickUp).pickedVictim != null)
                {
                    if (InputManager.Instance.selectedEquip == m_equip)
                    {
                        InputManager.Instance.EquipSelected(m_equip);
                        ActiveButton(true);
                        UIManager.Instance.EquipSelected(this);
                        
                    }
                    else
                    {
                        m_equip.OnClick();
                        UIManager.Instance.EquipSelected(this);

                        ActiveButton(false);
                    }  
                }
            }
        }
        else
        {
            Debug.Log("FF " + LevelManager.instance.GetSelectedFF.Name + " has no more actions left");
        }
    }

    public virtual void SetEquip(Equip equip, int _KB)
    {
        m_equip = equip;
        KeyBindText.text = _KB.ToString();
        //m_text.text = equip.name;
        IconImage.sprite = equip.Sprite;

        if(m_equip.EName == EquipName.Cement_Saw)
        {
            (BGImage.transform as RectTransform).sizeDelta = new Vector2((BGImage.transform as RectTransform).sizeDelta.x * 2.4f, (BGImage.transform as RectTransform).sizeDelta.y);
        }

        if (UseContaner == null) return;

        if (m_equip.Charges == -1)
        {
            UseContaner.SetActive(false);
        }
        else
        {
            UseText.text = m_equip.CurrentChargets.ToString();
        }
    }

    public virtual void ActiveButton(bool _isActive)
    {
        if (!_isActive)
        {
            BGImage.color = Color.yellow;
        }
        else
        {
            BGImage.color = Color.white;
        }

        //m_button.interactable = _isActive;
    }

    public void UpdateBackground()
    {
        if(m_equip as EPickUp)
        {
            if ((m_equip as EPickUp).pickedVictim == null)
            {
                IconImage.sprite = m_equip.Sprite;
            }
            else
            {
                IconImage.sprite = m_equip.AlternativeSprite;
            }
        }
    }

    public void SetInteractive()
    {
        if (m_equip.Owner.CanAct)
        {
            if ((m_equip as EEextinguisher) && !(m_equip as EEextinguisher).CanExecute())
            {
                m_button.interactable = false;
            }
            else
            {
                m_button.interactable = true;
            }

            if(m_equip is EWaterShield)
            {
                if (!m_equip.Owner.m_waterShield)
                    m_button.interactable = true;
                else
                    m_button.interactable = false;
            }

            if (LevelManager.instance.GetSelectedFF.HasVictim())
                m_button.interactable = false;
            //else
            //    m_button.interactable = true;


            if ((m_equip as EPickUp) != null && (m_equip as EPickUp).pickedVictim != null)
            {
                m_button.interactable = true;
            }
            else if ((m_equip as EPickUp) != null)
            {
                m_button.interactable = true;
            }
        }
        else
        {
            if(m_equip.Owner.HealthCurrent > 0)
            {
                if ((m_equip as EPickUp) != null && (m_equip as EPickUp).pickedVictim != null)
                {
                    m_button.interactable = true;
                }
                else
                {
                    m_button.interactable = false;
                }
            }
            else
            {
                m_button.interactable = false;
            }
        }

        UpdateBackground();
        UpdateEquipUse();
    }

    public void SetInteractive(FireFighterBehaviour _ff)
    {
        FFName name = m_equip.Owner.Name;

        UpdateBackground();

        if (_ff.Name == name)
            SetInteractive();

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //if (!LevelManager.instance.ActiveTooltip) return;

        UIManager.Instance.SetTooltip(true, m_equip);

        if (!m_button.interactable)
            CustomCursor.SetCursor(CursorStyle.Block);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //if (!LevelManager.instance.ActiveTooltip) return;

        UIManager.Instance.SetTooltip(false, m_equip);

        if (!m_button.interactable)
            CustomCursor.SetCursorDefault();
    }

    public void UpdateEquipUse()
    {
        if (UseContaner == null) return;

        UseText.text = m_equip.CurrentChargets.ToString();
    }
}
