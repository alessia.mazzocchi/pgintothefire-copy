﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFFScrollHandler : MonoBehaviour
{
    public GameObject Container;
    public Scrollbar scroll;

    public float scrollSpeed = 1;
    private bool onScroll;

    private FFName selectedFF;
   public UIScrollSelectedFF focusFF;

    private Color seeColor = Color.white;
    public Color InactiveColor;
    private Color clearColor;
    private RectTransform rect;

    private void Awake()
    {
        scroll = GetComponentInChildren<Scrollbar>();
        rect = GetComponent<RectTransform>();
    }

    //private void OnEnable()
    //{
    //    EventManager.StartListening(EventsID.ChangePhase, UpdateOnEndTurn);

    //}

    //private void OnDisable()
    //{
    //    EventManager.StopListening(EventsID.ChangePhase, UpdateOnEndTurn);

    //}

    public void Load()
    {
        seeColor = Color.white;
        clearColor = new Color(seeColor.r, seeColor.g, seeColor.b, 0);

        scroll.value = 0;

        if(LevelManager.instance.GetFFTeamList.Length == 1)
        {
            for (int i = 0; i < 3; i++)
            {
                Destroy(Container.transform.GetChild(i).gameObject);
            }

            focusFF.SetFF(LevelManager.instance.GetFFNameByIndex(0));
        }
        else
        {
            for (int i = 0; i < 3; i++)
            {
                if (i < LevelManager.instance.GetFFTeamList.Length)
                    Container.transform.GetChild(i).GetComponent<UIPlayerButton>().SetFF(LevelManager.instance.GetFFTeamList[i].Name, i);
                else
                {
                    Destroy(Container.transform.GetChild(i).gameObject);
                    Debug.Log("destroy");
                }
            }

            focusFF.SetFF(LevelManager.instance.GetFFNameByIndex(0));

        }

        switch (LevelManager.instance.GetFFTeamList.Length)
        {
            case 1:
                //(transform as RectTransform).rect.Set((transform as RectTransform).anchoredPosition.x, (transform as RectTransform).anchoredPosition.y, (transform as RectTransform).rect.width, 200);
                break;

            case 2:
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, 200);
                break;

            default:
                break;
        }
    }


    void FixedUpdate()
    {
        //if (Container.transform.childCount == 0) return;
        if (onScroll)
        {
            scroll.value += Time.fixedDeltaTime * scrollSpeed;

            if (scroll.value >= 1)
            {
                scroll.value = 0;

                Container.transform.GetChild(Container.transform.childCount - 1).SetAsFirstSibling();

                UIPlayerButton btn = Container.transform.GetChild(0).GetComponent<UIPlayerButton>();

                if (btn.FName != selectedFF)
                {
                    Container.transform.GetChild(0).GetComponent<UIPlayerButton>().Fade(true);
                }
                else
                {
                    onScroll = false;
                    //focusFF.SetFF(btn.FName);
                    //focusFF.Fade(true);

                }
            }
        }
    }


    public void Roll(FFName _name)
    {
        if (/*onScroll ||*/ _name == selectedFF) return;

        selectedFF = _name;

        if(Container.transform.childCount != 0)
        {
            onScroll = true;

            Container.transform.GetChild(0).GetComponent<UIPlayerButton>().Fade(true);
            Container.transform.GetChild(Container.transform.childCount - 1).GetComponent<UIPlayerButton>().Fade(false);
        }
      
        focusFF.Fade(selectedFF);
    }

    private void UpdateOnEndTurn()
    {
        if (LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
           
        }
        else if (LevelManager.instance.CurrentPhase == TurnPhase.AI)
        {
            
        }
    }

}
