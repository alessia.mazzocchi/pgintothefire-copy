﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIInfoEquip : MonoBehaviour, ITooltip, IPointerEnterHandler, IPointerExitHandler
{
    public int FFindex;
    public int EquipIndex;

    private Equip m_equip;

    public GameObject go => gameObject;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!LevelManager.instance.ActiveTooltip) return;

        ShowTip(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!LevelManager.instance.ActiveTooltip) return;

        ShowTip(false);
    }

    public void ShowTip(bool _active)
    {
        UIManager.Instance.SetTooltip(_active, m_equip);
    }

    void Start()
    {
        m_equip = LevelManager.instance.FFInLevel[FFindex].Inventory[EquipIndex];
    }


}
