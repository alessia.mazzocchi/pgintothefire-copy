﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerQuirk : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private FFData m_data;

    private Image m_Image;
    public Image Image
    {
        get
        {
            if(m_Image == null)
            {
                m_Image = GetComponent<Image>();
            }

            return m_Image;
        }
    }

    private void Awake()
    {
        if(m_Image == null)
            m_Image = GetComponent<Image>(); 
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!LevelManager.instance.ActiveTooltip) return;

        UIManager.Instance.SetTooltip(true, m_data);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!LevelManager.instance.ActiveTooltip) return;

        UIManager.Instance.SetTooltip(false, m_data);
    }

    public void Setup(FFData _data)
    {
        m_data = _data;

        Image.sprite = _data.quirk.Sprite;
    }
}
