﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class SegmentSlider
{
    public Transform SocketHolder;
    public Image[] Sockets;

    public Transform BarHolder;
    public Image[] Bars;

    public Color PreviewColor;
    public Color BaseColor;
    public int LastValue;

    public SegmentSlider(int _health)
    {
        Sockets = new Image[_health];
        Bars = new Image[_health];
    }

    public void Active(bool _Active)
    {
        SocketHolder.gameObject.SetActive(_Active);
        BarHolder.gameObject.SetActive(_Active);
    }

    public void SubtractValue(int _value)
    {
        LastValue -= _value;
        LastValue = Mathf.Clamp(LastValue, 0, Sockets.Length);

        float delay = 0f;
        for (int i = Bars.Length - 1; i >= 0; i--)
        {
            if (i < LastValue)
            {
                Bars[i].color = BaseColor;
            }
            else
            {
                if (Bars[i].color != Color.clear)
                {
                    Bars[i].DOColor(Color.clear, 0.35f).SetDelay(delay);

                    delay += 0.15f;
                }
            }
        }
    }

    // ToTest
    public void AddValue(int _value)
    {
        LastValue += _value;
        LastValue = Mathf.Clamp(LastValue, 0, Sockets.Length);
        Debug.Log("Add value");
        float delay = 0f;

        for (int i = 0; i < Bars.Length; i++)
        {
            if (i > LastValue - 1)
            {
                Bars[i].color = Color.clear;
            }
            else
            {
                if(Bars[i].color != BaseColor)
                {
                    Bars[i].DOColor(BaseColor, 0.35f).SetDelay(delay);
                    delay += 0.25f;
                }
            }
        }
    }

    public void SetCurrentValue(int _currentV)
    {
        LastValue = _currentV;

        for (int i = Bars.Length - 1; i >= 0; i--)
        {
            if (i < _currentV)
            {
                Bars[i].color = BaseColor;
            }
            else
            {
                Bars[i].color = Color.clear;
            }
        }
    }

    public void PreviewValue(int _value)
    {
        int pValue = LastValue - _value;

        for (int i = Bars.Length - 1; i >= 0; i--)
        {
            if (i < pValue)
            {
                Bars[i].color = BaseColor;
            }
            else if (i >= LastValue)
            {
                Bars[i].color = Color.clear;
            }
            else
            {
                //Color c = new Color(Bars[i].color.r, Bars[i].color.g, Bars[i].color.b, Bars[i].color.a / _SaturationValue);
                Bars[i].color = PreviewColor;
            }
        }
    }

}

public class HUDPlayerInfo : MonoBehaviour
{
    public Image QuirkImage;
    public GameObject HUDSegmentPrefab;
    public GameObject HUDSegmentInvertedPrefab;
    [Header("Action")]
    public Image ActionImage;

    [Header("Health")]
    public Color FillHealthColor;
    public Color HealthPreviewColor;
    public GameObject HealthBarContiner;
    public GameObject HealthSocketContiner;
    
    [Header("Movement")]
    public Color FillMovementColor;
    public Color MovementhPreviewColor;
    public GameObject MovementBarContiner;
    public GameObject MovementSocketContiner;
    [Space]
    public Color EmptyColor;
    
    private FireFighterBehaviour owner;
    private SegmentSlider HealthInfo;
    private SegmentSlider MovementInfo;

    private Camera cam;
    private CanvasGroup group;
    private Tween groupFadeTween;
    private void Awake()
    {
        group = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        cam = Camera.main;
        group.alpha = 0f;
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.ChangePhase, UpdateOnEndTurn);
        EventManager.StartListening<FireFighterBehaviour>(EventsID.FFChange, OnFFChange);

    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.ChangePhase, UpdateOnEndTurn);
        EventManager.StopListening<FireFighterBehaviour>(EventsID.FFChange, OnFFChange);

    }

    private void Update()
    {
        if(owner != null)
        {
            transform.position = cam.WorldToScreenPoint(owner.HUDPosition.position);
        }
    }

    private void LateUpdate()
    {
        if (owner != null)
        {
            transform.position = cam.WorldToScreenPoint(owner.HUDPosition.position);
        }
    }


    public void Load(FireFighterBehaviour _ff)
    {
        owner = _ff;
        group.alpha = 0;
        QuirkImage.sprite = _ff.DataStat.quirk.Sprite;

        int health = (int)_ff.HealthMax;
        int steps = _ff.baseMovement;

        #region Health

        HealthInfo = new SegmentSlider(health);
        HealthInfo.LastValue = health;
        if (_ff.HealthMax > 5)
        {
            RectTransform BarRt = (HealthBarContiner.transform as RectTransform);
            RectTransform SocketRt = (HealthSocketContiner.transform as RectTransform);

            BarRt.sizeDelta = new Vector2(150, BarRt.sizeDelta.y);
            BarRt.anchoredPosition = new Vector2(BarRt.anchoredPosition.x + 25, BarRt.anchoredPosition.y);

            SocketRt.sizeDelta = new Vector2(150, SocketRt.sizeDelta.y);
            SocketRt.anchoredPosition = new Vector2(SocketRt.anchoredPosition.x + 25, SocketRt.anchoredPosition.y);

        }

        HealthInfo.PreviewColor = HealthPreviewColor;

        HealthInfo.SocketHolder = HealthSocketContiner.transform;
        HealthInfo.BarHolder = HealthBarContiner.transform;

        for (int i = 0; i < health; i++)
        {
            Image s_img = Instantiate(HUDSegmentPrefab, HealthInfo.SocketHolder).GetComponent<Image>();
            s_img.color = EmptyColor;
            HealthInfo.Sockets[i] = s_img;

            Image b_img = Instantiate(HUDSegmentPrefab, HealthInfo.BarHolder).GetComponent<Image>();
            b_img.color = FillHealthColor;

            HealthInfo.Bars[i] = b_img;
        }

        HealthInfo.BaseColor = HealthInfo.Bars[0].color;
        #endregion

        #region Movement

        MovementInfo = new SegmentSlider(steps);
        MovementInfo.LastValue = steps;
        MovementInfo.PreviewColor = MovementhPreviewColor;

        MovementInfo.SocketHolder = MovementSocketContiner.transform;
        MovementInfo.BarHolder = MovementBarContiner.transform;

        for (int i = 0; i < steps; i++)
        {
            Image s_img = Instantiate(HUDSegmentInvertedPrefab, MovementInfo.SocketHolder).GetComponent<Image>();
            s_img.color = EmptyColor;

            MovementInfo.Sockets[i] = s_img;

            Image b_img = Instantiate(HUDSegmentInvertedPrefab, MovementInfo.BarHolder).GetComponent<Image>();
            b_img.color = FillMovementColor;

            MovementInfo.Bars[i] = b_img;
        }

        MovementInfo.BaseColor = MovementInfo.Bars[0].color;

        #endregion
    }

    #region Health

    public void UpdateHUDHealth(int _currentH, bool _useDelay = true)
    {
        if (_useDelay)
        {
            int newValue = HealthInfo.LastValue - _currentH;

            if (HealthInfo.LastValue < _currentH)
                HealthInfo.AddValue(Mathf.Abs(newValue));

            else
                HealthInfo.SubtractValue(Mathf.Abs(newValue));

        }
        else
            HealthInfo.SetCurrentValue(_currentH);
    }

    public void PreviewHUDHealth(int _value)
    {
        HealthInfo.PreviewValue(_value);
    }

    /// ----------------------------------------- ///
    /// Usefull to sync with Scene interactions
    /// ----------------------------------------- ///

    /// <summary>
    /// Subtract a specific value
    /// </summary>
    /// <param name="_value"></param>
    public void SubtractHealth(int _value)
    {
        HealthInfo.SubtractValue(_value);
    }

    /// <summary>
    /// Add a specific value
    /// </summary>
    /// <param name="_value"></param>
    public void AddHealth(int _value)
    {
        HealthInfo.AddValue(_value);
    }
    #endregion

    #region Movement

    /// <summary>
    /// Update the to current current value 
    /// </summary>
    /// <param name="_currentM"></param>
    /// <param name="_useDelay"> True for delay, False for instant </param>
    public void UpdateHUDMovement(int _currentM, bool _useDelay = true)
    {
        if (_useDelay)
        {
            int newValue = MovementInfo.LastValue - _currentM;

            if (MovementInfo.LastValue < _currentM)
                MovementInfo.AddValue(Mathf.Abs(newValue));
            
            else
                MovementInfo.SubtractValue(Mathf.Abs(newValue));
            
        }
        else
            MovementInfo.SetCurrentValue(_currentM);
        
    }

    public void PreviewHUDMovement(int value)
    {
        MovementInfo.PreviewValue(value);
    }

    /// ----------------------------------------- ///
    /// Usefull to sync with Scene interactions
    /// ----------------------------------------- ///
    
    /// <summary>
    /// Subtract a specific value
    /// </summary>
    /// <param name="_value"></param>
    public void SubtractMovement(int _value)
    {
        MovementInfo.SubtractValue(_value);
    }

    /// <summary>
    /// Add a specific value
    /// </summary>
    /// <param name="_value"></param>
    public void AddMovement(int _value)
    {
        MovementInfo.AddValue(_value);
    }
    #endregion

    public int GetHealthLastValue => HealthInfo.LastValue;
    public int GetMovementLastValue => MovementInfo.LastValue;

    public void UpdateOnEndTurn()
    {
        if (LevelManager.instance.CurrentPhase == TurnPhase.Player)
        {
            if(LevelManager.instance.GetSelectedFF == owner)
                group.DOFade(1f, 0.25f);
            else
                group.DOFade(0.5f, 0.25f);

            //if (owner.CanAct)
                //ActionImage.color = Color.blue;
        }
        else
            group.DOFade(0, 0.25f);
    }

    private void OnFFChange(FireFighterBehaviour _ff)
    {
        if(_ff == owner)
        {
            if (groupFadeTween == null)
                groupFadeTween = group.DOFade(1, 0.25f);
            else
            {
                groupFadeTween.Rewind();
                groupFadeTween = group.DOFade(1, 0.25f);
            }
        }
        else
        {
            if (groupFadeTween == null)
                groupFadeTween = group.DOFade(0.5f, 0.25f);
            else
            {
                groupFadeTween.Rewind();
                groupFadeTween = group.DOFade(0.5f, 0.25f);
            }
        }
    }

    public void UseAction()
    {
        //ActionImage.DOColor(Color.clear, 0.5f);
    }
}
