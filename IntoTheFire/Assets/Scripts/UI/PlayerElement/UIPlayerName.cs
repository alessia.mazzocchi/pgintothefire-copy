﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;

public class UIPlayerName : UIPlayerElement
{
    private TextMeshProUGUI m_text;

    private void Awake()
    {
        m_text = GetComponent<TextMeshProUGUI>();
    }

    public override void UpdateOnChangeFF(FireFighterBehaviour _ff)
    {
        m_text.text = _ff.Name.ToString(); 
    }
}
