﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIPlayer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    TooltipTypes TooltipType = TooltipTypes.UICharacter;

    public int index;

    [Space]
    public Image Background;
    public Image Icon;
    private Button m_button;

    private void Awake()
    {
        m_button = GetComponentInChildren<Button>();    
    }

    private void OnEnable()
    {
        EventManager.StartListening<FireFighterBehaviour>(EventsID.FFChange, UpdateIcons);
        EventManager.StartListening(EventsID.ChangePhase, ClearIcon);

    }

    private void OnDisable()
    {
        EventManager.StopListening<FireFighterBehaviour>(EventsID.FFChange, UpdateIcons);
        EventManager.StartListening(EventsID.ChangePhase, ClearIcon);
    }

    public void OnClick()
    {
        if (LevelManager.instance.GameStatus == GameStatus.Pause) return;

        LevelManager.instance.ChangeSelectedFF(index);
    }

    private void UpdateIcons(FireFighterBehaviour _ff)
    {
        if(_ff.ID == index)
        {
            Icon.color = m_button.colors.normalColor;
            Background.color = m_button.colors.normalColor;
        }
        else
        {
            Icon.color = m_button.colors.disabledColor;
            Background.color = m_button.colors.disabledColor;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //FireFighterBehaviour ff = LevelManager.instance.FFInLevel[index];
        //UIManager.Instance.SetTooltip(true, ff.DataStat);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //FireFighterBehaviour ff = LevelManager.instance.FFInLevel[index];
        //UIManager.Instance.SetTooltip(false, ff.DataStat);
    }

    private void ClearIcon()
    {
        if(LevelManager.instance.CurrentPhase == TurnPhase.AI)
        {
            Icon.color = m_button.colors.normalColor;
            Background.color = m_button.colors.normalColor;
        }
    }
}
