﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IFTutorialCanvas : MonoBehaviour
{
    [HideInInspector]
    public IFTutorialManager IFTutorial;
    public Image TutorialContainer;

    public TextMeshProUGUI ControlText;

    public Image TutorialBackground;
    public TextMeshProUGUI TutorialText;

    public void NextTutorialMessage()
    {
        IFTutorial.NextTutorialStep();
    }
}
