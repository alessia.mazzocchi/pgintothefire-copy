﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using DG.Tweening;

public enum SkillButtonState
{
    Achieve,
    Lock,
    Unlock
}

[System.Serializable]
public struct SkillButtonSetting
{
    public Color BackgroundColor;
    public Sprite BackgroundSprite;

}

public class UISkillButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private int ID;
    public SkillButtonState buttonState;
    private SkillLeaf SPoint;
    private UISkillButton nextSkill;
    private UIFFOverview recap;
    private Button m_button;
    public Button Button
    {
        get
        {
            if(m_button == null)
            {
                m_button = GetComponent<Button>();
            }

            return m_button;
        }  
    }

    [Header("References")]
    public Image BGEffect;
    public Image Background;
    public Image BackgroundFill;
    public Image Icon;
    public Image Lock;
    [Space]
    public TextMeshProUGUI PrizeText;
    public GameObject Tooltip;
    private TextMeshProUGUI TipText;
    public TextMeshProUGUI ValueText;
    [Space]
    public SkillButtonSetting AchieveState;
    public SkillButtonSetting UnlockkState;
    public SkillButtonSetting LockkState;
    public Color AchiveColor;
    public Color UnlockColor;
    public Color LockColor;

    private Tween NoBuyShake;
    private Tween NoBuyFeedback;

    public void OnClick()
    {
        if (CanBuy())
        {
            ChangeState(SkillButtonState.Achieve);
            nextSkill?.Unlock();
            recap.SkillBuy(this, ID);
            //BGEffect.gameObject.transform.DOPunchRotation(new Vector3(0,0,10),1).SetEase(Ease.OutSine);
            BGEffect.gameObject.transform.DORotate(new Vector3(0, 0, 180), 0.75f).SetEase(Ease.OutSine);
            BGEffect.gameObject.transform.DOScale(Vector3.zero, 0.5f).From().SetEase(Ease.OutBack);

            AudioData audio = new AudioData();
            audio.SetUISound(UISounds.ShopEquip);
            EventManager.TriggerEvent(EventsID.PlayAudio, audio);
        }
        else
        {
            if (NoBuyShake == null)
                NoBuyShake = transform.DOShakePosition(0.75f, new Vector2(10, 0)).SetAutoKill(false);
            else
                NoBuyShake.Restart();

            if (NoBuyFeedback == null)
                NoBuyFeedback = BackgroundFill.DOColor(Color.red, 0.25f).OnComplete(() => BackgroundFill.DOColor(Color.white, 0.25f)).SetAutoKill(false);
            else
                NoBuyFeedback.Restart();
        }
    }

    private void Awake()
    {
        recap = GetComponentInParent<UIFFOverview>();
        TipText = Tooltip.GetComponentInChildren<TextMeshProUGUI>();
        if(m_button == null)
            m_button = GetComponent<Button>();
    }

    private void Start()
    {
        Tooltip.SetActive(false);

        Background.color = Color.clear;
        BGEffect.gameObject.SetActive(buttonState == SkillButtonState.Achieve);
    }

    private void ChangeState(SkillButtonState _state)
    {
        buttonState = _state;

        switch (buttonState)
        {
            case SkillButtonState.Achieve:
                Button.interactable = false;
                PrizeText.transform.parent.gameObject.SetActive(false);
                BGEffect.gameObject.SetActive(true);
                BackgroundFill.color = AchiveColor;

                break;

            case SkillButtonState.Lock:
                Button.interactable = false;
                PrizeText.transform.parent.gameObject.SetActive(false);
                BackgroundFill.color = LockColor;

                break;

            case SkillButtonState.Unlock:
                ValueText.gameObject.transform.parent.gameObject.SetActive(SPoint.SkillPoint is SPModifyStat);

                PrizeText.transform.parent.gameObject.SetActive(true);
                PrizeText.text = SPoint.Prize.ToString();
                Button.interactable = true;
                BackgroundFill.color = UnlockColor;

                break;
        }
    }


    public void SetSkillButton(int _id, SkillLeaf _sp, SkillButtonState _state)
    {
        ID = _id;
        SPoint =  _sp;
        if (SPoint.SkillPoint.Sprite != null)
            Icon.sprite = SPoint.SkillPoint.Sprite;
        else
            Debug.LogWarning($"No sprite in { SPoint.SkillPoint.name}");

        //TipText.text = SPoint.SkillPoint.Description;

        if (SPoint.SkillPoint is SPModifyStat)
        {
            ValueText.text = "+" + (SPoint.SkillPoint as SPModifyStat).Value;
        }
        else
        {
            ValueText.gameObject.transform.parent.gameObject.SetActive(false);
        }


        ChangeState(_state);
    }

    public void SetLinkToNext(UISkillButton _skills)
    {
        nextSkill = _skills;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //if(buttonState == SkillButtonState.Achieve || buttonState == SkillButtonState.Lock)
        //Tooltip.SetActive(true);
        recap.ShowSkillTip(SPoint.SkillPoint);

        Background.color = Color.clear;
        switch (buttonState)
        {
            case SkillButtonState.Achieve:
                Background.DOColor(AchieveState.BackgroundColor, 0.25f);
                break;

            case SkillButtonState.Lock:
                Background.DOColor(LockkState.BackgroundColor, 0.25f);
                break;

            case SkillButtonState.Unlock:
                Background.DOColor(UnlockkState.BackgroundColor, 0.25f);
                break;

        }

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //if(buttonState == SkillButtonState.Achieve || buttonState == SkillButtonState.Lock)
        //Tooltip.SetActive(false);
        recap.CloseSkillTip();
        Background.DOColor(Color.clear, 0.25f);

    }

    public void Unlock()
    {
        ChangeState(SkillButtonState.Unlock);
    }

    private bool CanBuy()
    {
        if(buttonState == SkillButtonState.Unlock)
            if(GameManager.Instance.Medals >= SPoint.Prize)
            {
                GameManager.Instance.AddReward(RewardTypes.Medals, - SPoint.Prize);
                return true;
            }

        return false;
    }

    private void UpdateBackground(SkillButtonState _State)
    {
        switch (_State)
        {
            case SkillButtonState.Achieve:
                Background.sprite = AchieveState.BackgroundSprite;
                Background.color = AchieveState.BackgroundColor;
                break;
            case SkillButtonState.Lock:
                Background.sprite = LockkState.BackgroundSprite;
                Background.color = LockkState.BackgroundColor;
                break;
            case SkillButtonState.Unlock:
                Background.sprite = UnlockkState.BackgroundSprite;
                Background.color = UnlockkState.BackgroundColor;
                break;
            default:
                break;
        }
    }
}
