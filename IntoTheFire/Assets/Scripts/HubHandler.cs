﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum HubState { Hub, LevelSelection, Briefing, Overview, MainMenu, Board }
public class HubHandler : Singleton<HubHandler>
{
    public delegate void StateDelegate();
    public StateDelegate onChangeState;

    private HubState m_HubState;
    public HubState HubState { get => m_HubState; }
    [HideInInspector]
    public bool inTutorial;

    private ASMouseOverHandler<IHubMouseOver> mouseOver;
    [HideInInspector]
    public bool blockMouseOver = false;

    [Header("Camera Position")]
    public Transform BasePosition;
    public Transform ShowcasePosition;

    public HubCameraController MainCamera;
    public GameObject ShowcaseCamera;
    public Image SelectorImage;
    [Space]
    public GameObject StartScreen;
    [Space]
    public Image FadeScreen;

    private Tween selectorAnim;
    private Vector3 realPos;

    private LevelSelection m_LevelSelection;
    public LevelSelection LeveSelection
    {
        get
        {
            if (m_LevelSelection == null)
                m_LevelSelection = FindObjectOfType<LevelSelection>();

            return m_LevelSelection;
        }
    }

    private AMLevelSelection AMm_LevelSelection;
    public AMLevelSelection AMLevelSelection
    {
        get
        {
            if (AMm_LevelSelection == null)
                AMm_LevelSelection = FindObjectOfType<AMLevelSelection>();

            return AMm_LevelSelection;
        }
    }

    private HubBriefingHandler m_BriefingHandler;
    public HubBriefingHandler BriefingHandler
    {
        get
        {
            if (m_BriefingHandler == null)
                m_BriefingHandler = FindObjectOfType<HubBriefingHandler>();

            return m_BriefingHandler;
        }
    }

    public HubMainMenu MainMenu;

    [Header("FF Overview")]
    public UIFFOverview FFOverviewPrefab;
    public Transform CameraPoint;
    public Transform MeshPoint;
    [Space]
    public Image TutorialBoard;
    private Tween BoardTween;
    [Header("WinGame Section")]
    public CanvasGroup WinGameBackground;
    public CanvasGroup[] WinGameContainers;
    public UIWinGameRecap WinRecap;
    public float fadeTime = 1f;
    public float waitTime = 2f;

    private Canvas m_MainCanvas;
    public Canvas MainCanvas { get => (m_MainCanvas == null) ? FindObjectOfType<Canvas>() : m_MainCanvas; }
    private Dictionary<FFName, UIFFOverview> Recaps;
    private FFName currentOverview;
    private bool onSwitch;

    private void Start()
    {
        InitializeFFRecaps();
        Time.timeScale = 1;
        Cursor.visible = true;

        AMLevelSelection.SetupProgression(GameManager.Instance.Progression);
        mouseOver = new ASMouseOverHandler<IHubMouseOver>();
        MainCamera.gameObject.SetActive(false);
        ShowcaseCamera.SetActive(false);

        // Win
        WinGameBackground.gameObject.SetActive(false);
        WinGameBackground.alpha = 0;
        //WinRecap.gameObject.SetActive(false);

        for (int i = 0; i < WinGameContainers.Length; i++)
            WinGameContainers[i].gameObject.SetActive(false);
        


        AMLevelSelection.ActiveLevelSelection(false);
        BriefingHandler.BriefingContainer.SetActive(false);

        TutorialBoard.transform.localScale = Vector3.zero;

        if (GameManager.Instance.ShowTutorial)
        {
            IFHubTutorial.instance.ActiveTutorial(true);
        }

        //Debug.Log(AMLevelSelection.instance.GetLastLevelIndex());
        if (GameManager.Instance.NotFirstLoad)
        {
            ChangeState(HubState.Hub);
            MainMenu.TurnOff();
            MainCamera.gameObject.SetActive(true);
            
            if(GameManager.Instance.LevelLoaded.LevelIndex == AMLevelSelection.instance.GetLastLevelIndex())
            {
                WinGameAnimation();
            }
        }
        else
        {
            ChangeState(HubState.MainMenu);
        }

        AudioData audio = new AudioData();
        audio.SetOST(MusicOSTs.Hub);
        EventManager.TriggerEvent(EventsID.PlayAudio, audio);
    }

    void Update()
    {
        if (inTutorial) return;

        if(HubState == HubState.Hub)
        {
            if (SelectorImage.gameObject.activeInHierarchy)
            {
                SelectorImage.transform.position = Camera.main.WorldToScreenPoint(realPos);
            }
        }
      

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackButton();
        }


        if(HubState == HubState.Hub && !blockMouseOver)
        {
            mouseOver.OnUpdateOver();

            if (Input.GetMouseButtonDown(0))
            {
                mouseOver.OnClick();
            }
        }  

        if(Input.GetKeyDown(KeyCode.B))
            WinGameAnimation();
    }


    private void InitializeFFRecaps()
    {
        Recaps = new Dictionary<FFName, UIFFOverview>();

        List<FFData> ff = GameManager.Instance.GetFFTeamData;
        for (int i = 0; i < ff.Count; i++)
        {
            UIFFOverview tmp = Instantiate(FFOverviewPrefab, MainCanvas.transform);

            Recaps.Add(ff[i].FName, tmp);

            tmp.SetRecap(ff[i]);

            tmp.gameObject.SetActive(false);
        }
    }

    public void BuySkill(FFName _name, SkillBranch _branch, int _ID)
    {
        SkillTreeTemplate tree = GameManager.Instance.GetSkillTreeTemplate(_name);
        FFData data = GameManager.Instance.GetFFDataLoaded(_name);
                       
        switch (_branch)
        {
            case SkillBranch.ProgressionOne:
                data.SkillTree.ProgressiveOne = _ID + 1;
                ExecuteSkill(tree.ProgressionOne[_ID].SkillPoint, data);
                //if (tree.ProgressionOne[_ID].SkillPoint is SPModifyStat)
                //    (tree.ProgressionOne[_ID].SkillPoint as SPModifyStat).Execute(data);
                //else if(tree.ProgressionOne[_ID].SkillPoint is SPEquipUpgrade)
                //    (tree.ProgressionOne[_ID].SkillPoint as SPEquipUpgrade).Execute(data);

                break;

            case SkillBranch.ProgressionTwo:
                data.SkillTree.ProgressiveTwo = _ID + 1;
                //(tree.ProgressionTwo[_ID].SkillPoint as SPModifyStat).Execute(data);
                ExecuteSkill(tree.ProgressionTwo[_ID].SkillPoint, data);

                break;

            case SkillBranch.Melee:
                data.SkillTree.Melee = _ID + 1;
                //(tree.Melee[_ID].SkillPoint as SPUnlockEquip).Execute(data);
                ExecuteSkill(tree.Melee[_ID].SkillPoint, data);

                break;

            case SkillBranch.Extinguish:
                data.SkillTree.Extinguish = _ID + 1;
                //(tree.Extinguish[_ID].SkillPoint as SPUnlockEquip).Execute(data);
                ExecuteSkill(tree.Extinguish[_ID].SkillPoint, data);

                break;

            case SkillBranch.Utility:
                data.SkillTree.Utility = _ID + 1;
                //(tree.Utility[_ID].SkillPoint as SPUnlockEquip).Execute(data);
                ExecuteSkill(tree.Utility[_ID].SkillPoint, data);

                break;
        }
    

        Debug.Log(_name + " increase " + _branch + " skill to " + (_ID + 1).ToString());

        SaveSystem.Instance.SaveGame();

        void ExecuteSkill(SkillPoint _sp, FFData _Data)
        {
            if (_sp is SPModifyStat)
                (_sp as SPModifyStat).Execute(_Data);

            else if (_sp is SPEquipUpgrade)
                (_sp as SPEquipUpgrade).Execute(_Data);

            else if (_sp is SPUnlockEquip)
                (_sp as SPUnlockEquip).Execute(_Data);
        }
    }

    //public void OpenRecap(FFName _name)
    //{
    //    if (_name == FFName.None) return;

    //    foreach (KeyValuePair<FFName, UIFFOverview> item in Recaps)
    //    {
    //        item.Value.gameObject.SetActive(item.Key == _name);
    //    }

    //    ChangeState(HubState.Overview);
    //}

    public void OpenRecap(FFName _name)
    {
        if (_name == FFName.None) return;

        foreach (KeyValuePair<FFName, UIFFOverview> item in Recaps)
        {
            item.Value.gameObject.SetActive(item.Key == _name);
        }

        currentOverview = _name;
        Recaps[currentOverview].Mesh.SetActive(true);
        Recaps[currentOverview].Mesh.transform.position = MeshPoint.position;
        Recaps[currentOverview].Mesh.transform.rotation = MeshPoint.rotation;
        ChangeState(HubState.Overview);
        // Lerp camera
        //MainCamera.MoveToPosition(_pos.position);
        MainCamera.MoveToPosition(CameraPoint.position);

    }

    public void SwitchRecap(FFName _name)
    {
        if (onSwitch) return;

        onSwitch = true;
        //Recaps[currentOverview].group.DOFade(0, 0.15f).OnComplete(ChangeRecap);

        //void ChangeRecap()
        //{
        //    Recaps[currentOverview].gameObject.SetActive(false);
        //    currentOverview = _name;
        //    //Change Mesh
        //    Recaps[currentOverview].group.alpha = 0;
        //    Recaps[currentOverview].gameObject.SetActive(true);
        //    Recaps[currentOverview].group.DOFade(1, 0.15f).OnComplete(() => onSwitch = false);
        //}
        Recaps[currentOverview].Mesh.SetActive(false);
        Recaps[currentOverview].gameObject.SetActive(false);
        currentOverview = _name;
        Recaps[currentOverview].gameObject.SetActive(true);
        Recaps[currentOverview].Mesh.transform.position = MeshPoint.position;
        Recaps[currentOverview].Mesh.transform.rotation = MeshPoint.rotation;
        Recaps[currentOverview].Mesh.SetActive(true);

        onSwitch = false;

    }

    public void CloseRecap()
    {
        Recaps[currentOverview].Mesh.SetActive(false);
        ChangeState(HubState.Hub);
    }

    public void BackButton()
    {
        switch (HubState)
        {
            case HubState.Hub:
                ChangeState(HubState.MainMenu);
                break;
            case HubState.LevelSelection:
                AMLevelSelection.instance.BackButton();
               
                break;
            case HubState.Briefing:
                ChangeState(HubState.LevelSelection);
                break;
            case HubState.Overview:
                foreach (KeyValuePair<FFName, UIFFOverview> item in Recaps)
                {
                    item.Value.gameObject.SetActive(false);
                }

                CloseRecap();
                break;
            case HubState.MainMenu:
                break;

            case HubState.Board:
                ChangeState(HubState.Hub);
                break;
            default:
                break;
        }
    }

    public void MoveToBriefing()
    {
        ChangeState(HubState.Briefing);
    }

    public void MoveToLevelSeleciton()
    {
        SlideFade(() => ChangeState(HubState.LevelSelection));   
    }

    public void MoveToHub()
    {
         ChangeState(HubState.Hub);
    }

    public void MoveToBoard()
    {
        ChangeState(HubState.Board);
    }

    private void ChangeState(HubState _state)
    {
        if (HubState == _state) return;

        HubState prevState = m_HubState;

        // Turn off old state
        switch (m_HubState)
        {
            case HubState.MainMenu:
                MainMenu.TurnOff();
                break;

            case HubState.Hub:

                mouseOver.ClearLastHit();

                SelectorImage.gameObject.SetActive(false);

                //MainCamera.gameObject.SetActive(false);
                break;

            case HubState.LevelSelection:

                AMLevelSelection.ActiveLevelSelection(false);

                break;

            case HubState.Briefing:
                AMLevelSelection.AMCharacterContainer.SetActive(false);
                
                BriefingHandler.BriefingContainer.SetActive(false);
                ShowcaseCamera.SetActive(false);
                BriefingHandler.ResetValues();
                break;

            case HubState.Overview:
                MainCamera.MoveToPosition(BasePosition.position); 
                break;

            case HubState.Board:
                BoardTween.SmoothRewind();
                break;

            default:
                break;
        }

        m_HubState = _state;

        AudioData audio = new AudioData();

        // Turn on new state
        switch (m_HubState)
        {
            case HubState.MainMenu:
                MainCamera.gameObject.SetActive(true);
                MainMenu.gameObject.SetActive(true);
                if (!GameManager.Instance.NotFirstLoad)
                {
                    GameManager.Instance.NotFirstLoad = true;
                    MainMenu.TurnOn(true);
                    MainCamera.transform.position = MainMenu.CameraPos.transform.position;
                    MainCamera.transform.rotation = MainMenu.CameraPos.transform.rotation;
                }
                else
                {
                    MainMenu.TurnOn();
                    MainCamera.transform.position = MainMenu.CameraPos.transform.position;
                    MainCamera.transform.rotation = MainMenu.CameraPos.transform.rotation;
                }
                break;

            case HubState.Hub:
                MainCamera.gameObject.SetActive(true);

                if (prevState != HubState.Board)
                {
                    MainCamera.transform.position = BasePosition.position;
                    MainCamera.transform.rotation = BasePosition.rotation;
                }

                audio.SetOST(MusicOSTs.Hub);
                EventManager.TriggerEvent(EventsID.PlayAudio, audio);
                break;

            case HubState.LevelSelection:
                MainCamera.gameObject.SetActive(false);
                AMLevelSelection.ActiveLevelSelection(true);

                audio.SetOST(MusicOSTs.LevelSelection);
                EventManager.TriggerEvent(EventsID.PlayAudio, audio);
                break;

            case HubState.Briefing:
                AMLevelSelection.AMCharacterContainer.SetActive(true);

                BriefingHandler.SetLevelSetting();
                ShowcaseCamera.SetActive(true);
                break;

            case HubState.Overview:
                MainCamera.gameObject.SetActive(true);
                break;

            case HubState.Board:
                if (BoardTween == null)
                    BoardTween = TutorialBoard.transform.DOScale(1, 0.5f).SetEase(Ease.OutBack).SetAutoKill(false);
                else
                    BoardTween.Restart();
                break;
            default:
                break;
        }

        if(onChangeState != null && onChangeState.GetInvocationList().GetLength(0) > 0)
        {
            onChangeState.Invoke();
        }
    }

    public void ActiveSelector(Vector3 pos)
    {
        DeactiveSelector();

        realPos = new Vector3(pos.x, pos.y + 2, pos.z);

        SelectorImage.transform.position = Camera.main.WorldToScreenPoint(realPos);

        SelectorImage.gameObject.SetActive(true);

    }

    public void DeactiveSelector()
    {
        SelectorImage.gameObject.SetActive(false);
    }

    public void SlideFade(System.Action _action, bool instant = false)
    {
        if(instant)
            _action.Invoke();

        FadeScreen.gameObject.SetActive(true);

        FadeScreen.DOFade(1, 0.25f);
        FadeScreen.transform.DOLocalMoveX(40, 0.65f).SetEase(Ease.OutExpo).OnComplete(() =>
        {
            if(!instant)
                _action.Invoke();

            FadeScreen.DOFade(0, 0.5f).SetDelay(0.4f);

            FadeScreen.transform.DOLocalMoveX(1925, 0.65f).SetEase(Ease.InExpo).OnComplete(() =>
            {
                FadeScreen.transform.localPosition = new Vector2(-1925, 0);
                FadeScreen.gameObject.SetActive(false);
            });
        });
    }

    public void WinGameAnimation()
    {
        WinRecap.SetStat();

        WinGameBackground.gameObject.SetActive(true);
        WinGameBackground.DOFade(1, fadeTime);
        blockMouseOver = true;
        mouseOver.ClearLastHit();

        WinGameContainers[0].gameObject.SetActive(true);
        WinGameContainers[0].alpha = 0;
        WinGameContainers[0].DOFade(1, fadeTime);
        WinGameContainers[0].DOFade(0, fadeTime).SetDelay(fadeTime + waitTime).OnComplete(() => 
        {
            WinGameContainers[0].gameObject.SetActive(false);
            WinGameContainers[1].gameObject.SetActive(true);
            WinGameContainers[1].alpha = 0;
            WinGameContainers[1].DOFade(1, fadeTime);
            WinGameContainers[1].DOFade(0, fadeTime).SetDelay(fadeTime + waitTime).OnComplete(() =>
            {
                WinGameContainers[1].gameObject.SetActive(false);
                WinGameContainers[2].gameObject.SetActive(true);
                WinGameContainers[2].alpha = 0;
                WinGameContainers[2].DOFade(1, fadeTime).OnComplete(() => WinRecap.StartAnimation());
            });
        });
    }

    public void ContinueAnimation()
    {
        WinGameContainers[2].DOFade(0, fadeTime).OnComplete(() =>
        {
            WinGameContainers[2].gameObject.SetActive(false);
            WinGameContainers[3].gameObject.SetActive(true);
            WinGameContainers[3].alpha = 0;
            WinGameContainers[3].DOFade(1, fadeTime);
        });
    }

    public void CloseWinGame()
    {
        WinGameBackground.DOFade(0, fadeTime);

        WinGameContainers[WinGameContainers.Length - 1].DOFade(0, fadeTime).OnComplete(() => 
        {
            WinGameContainers[WinGameContainers.Length - 1].gameObject.SetActive(false); 
            blockMouseOver = false;
        });
    }

    private void OnDestroy()
    {
        if (BoardTween != null)
            BoardTween.Kill();
    }
}
