﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum PossibleActions
{
    Open,
    Heal,
    Throw,
    Break,
    TakeDamage,
    Pull,
    Push,
    PickUp
}

public enum Materials
{
    NONE,
    ConcreteLight,
    ConcreteHeavy,
    GlassLight,
    GlassHeavy,
    WoodLight,
    WoodHeavy,
    CeramicLight,
    CeramicHeavy,
    MetalLight,
    MetalHeavy
}

public class Obstacle : Entity, IInteractable, ITooltip
{
    protected bool m_WaitingForTutorialProgress;
    protected PromptTypes m_TutorialType;

    public bool CantJumpOver = false;

    [Header("Sound")]
    public SND_Obstacle snd_Obstacle;
    public Materials MaterialType;

    [HideInInspector]
    public bool m_CanInteract;
    /// <summary>
    /// Intarface property, indicate if the obstacle can be clicked 
    /// </summary>
    public bool CanAct
    {
        get
        {
            return m_CanInteract;
        }
    }

    /// <summary>
    /// Define an obstacle still in game (like doors) but not acting as an obstacle ( open doors or destructible walls)
    /// </summary>
    [HideInInspector]
    public bool isActive = true;

    /// <summary>
    /// List of possible actions 
    /// </summary>
    public List<PossibleActions> Actions;
    [Space]
    public float HealthMax;
    protected float m_healthCurrent;
    public float HealthCurrent
    {
        get
        {
            return m_healthCurrent;
        }
        set
        {
            m_healthCurrent = value;

            m_healthCurrent = Mathf.Clamp(m_healthCurrent, 0, HealthMax);

            //Debug.Log("current health: " + m_healthCurrent);
        }
    }

    public int FallDamage = 1;

    #region Movement

    private bool isMoving;
    private Vector3 m_nextPos;
    public float MovementLerpTime;
    private float currentTime;
    [Space]
    public float FallTime = 0.4f;
    protected float currentFallTime;
    public AnimationCurve FallAnimCurve;
    #endregion

    #region Components
    public GameObject Model;

    /// <summary>
    /// Set anchor points if the model occupied more then one cell
    /// </summary>
    public Transform[] AnchorPoints;

    [Space]
    public bool IsDebris;

    private Vector2Int[] anchorGridPosition;

    private MeshRenderer[] meshRenderers;
    #endregion

    protected override void Awake()
    {
        base.Awake();

        // DEV: First child did not have MeshRenderer component. "transform.childCount == 1" did not work because the mesh was child of the main child
        //if (transform.childCount == 1)
        //    Mesh = transform.GetChild(0).GetComponent<MeshRenderer>();
        //Mesh = GetComponentInChildren<MeshRenderer>();
    }


    protected void Update()
    {
        //if (isMoving)
        //{
        //    currentTime += Time.deltaTime;
        //    float t = currentTime / MovementLerpTime;
        //    transform.position = Vector3.Slerp(transform.position, m_nextPos, t);

        //    if (transform.position == m_nextPos)
        //    {
        //        isMoving = false;
        //        //GridPos = GetGridPosition();
        //        //LevelManager.instance.GetNode(GridPos).Obstacle = this;
        //    }
        //}
    }

    public void StartFall(Vector3 _end)
    {
        transform.position = new Vector3(_end.x, _end.y + 4, _end.z);
        gameObject.SetActive(true);

        transform.DOMoveY(0, 0.75f).SetEase(Ease.InExpo).OnComplete(LevelManager.instance.GetNode(GridPos).FallAnimationDamages);

        //StartCoroutine(Move(transform.position, _end, _action));
    }

    protected IEnumerator Move(Vector3 start, Vector3 end, System.Action action = null)
    {
        AudioData audio = new AudioData();
        audio.SetEnvSound(EnvSounds.ObstaclePush);
        snd_Obstacle?.PlayAudio(audio);

        float t = 0;
        while (t <= 1)
        {
            currentFallTime += Time.fixedDeltaTime;

            t = currentFallTime / FallTime;

            transform.position = Vector3.Lerp(start, end, FallAnimCurve.Evaluate(t));
            yield return new WaitForEndOfFrame();
        }

        transform.position = end;

        currentFallTime = 0;
        if (action != null) action.Invoke();
    }


    public override void LinkToCell()
    {
        LevelManager.instance.SetSceneReferences(this as Obstacle);
    }

    public override void Load()
    {
        base.Load();
        FallDamage = 1;
        gameObject.name = "Obstacle";

        if (AnchorPoints != null && AnchorPoints.Length > 0)
        {
            //Debug.Log("create new anchor" + GridPos);
            anchorGridPosition = new Vector2Int[AnchorPoints.Length + 1];

            for (int i = 0; i < AnchorPoints.Length; i++)
            {
                if (AnchorPoints[i] == null) { Debug.LogError("Obstacle in " + GridPos + ": Anchor point null;"); return; }
                anchorGridPosition[i] = GetGridPosition(AnchorPoints[i].position);

                LevelManager.instance.SetSceneReferences(anchorGridPosition[i], this);
            }

            // Add my own position;
            anchorGridPosition[AnchorPoints.Length] = GridPos;
        }

        meshRenderers = GetComponentsInChildren<MeshRenderer>();

        HealthCurrent = HealthMax;

        m_CanInteract = true;
        isMoving = false;
    }

    #region Actions methods

    public virtual void Open() { Debug.LogAssertion(" Open action empty"); }

    public virtual void Break()
    {
        if(Model != null)
            Model.SetActive(false);

        if (anchorGridPosition != null)
        {
            for (int i = 0; i < anchorGridPosition.Length; i++)
            {
                LevelManager.instance.GetNode(anchorGridPosition[i]).RoomsType = RoomsType.Floor;
            }
        }
        else
        {
            MyNode.RoomsType = RoomsType.Floor;
        }

        isActive = false;

        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.CleaningUp);

        AudioData audio = new AudioData();
        audio.SetEnvSound(EnvSounds.ObstacleBreak, MaterialType);
        snd_Obstacle?.PlayAudio(audio);

        GameObject go = PoolManager.instance.GetItem(PoolType.BreakParticle);
        go.transform.position = transform.position;
        go.transform.position += new Vector3(0, 0.5f, 0);
        go.SetActive(true);

        // TUTORIAL ONLY
        //if (m_WaitingForTutorialProgress && m_TutorialType == PromptTypes.Damage)
        //    CallForTutorialNextStep();
    }

    public virtual void TakeDamange(int _damage) 
    {
        HealthCurrent -= _damage;
        ApplyDamageToMaterial(HealthCurrent);

        UIManager.Instance.DamageFeedback(transform.position, _damage.ToString(), DamageType.Hit);

        DeathCheck();
        transform.DOShakePosition(0.5f, new Vector3(0.13f,0, 0.13f));

        //Debug.LogAssertion(" Takedamage action empty"); 
    }

    public virtual void Heal(int _value) { Debug.LogAssertion("Heal action empty"); }

    public virtual void Pull(Vector2Int _PlayerNextPos, Vector2Int _MyNextPos)
    {
        m_nextPos = LevelManager.instance.GetNode(_MyNextPos).WorldPosition;

        LevelManager.instance.GetNode(GridPos).Obstacle = null;
        LevelManager.instance.GetNode(GridPos).ObstacleExit(this);
        //LevelManager.instance.GetNode(GridPos).RoomsType = RoomsType.Floor;

        //isMoving = true;

        // TODO MAy break the game;
        LevelManager.instance.GetSelectedFF.StartMoving(_PlayerNextPos);
    }

    public virtual void Push(Vector2Int _PlayerPos, Vector2Int _MyNextPos)
    {
        m_nextPos = LevelManager.instance.GetNode(_MyNextPos).WorldPosition;

        LevelManager.instance.GetNode(GridPos).ObstacleExit();

        LevelManager.instance.GetNode(_MyNextPos).ObstacleEnter(this);
        GridPos = _MyNextPos;

        transform.DOMove(m_nextPos, 0.2f).SetEase(Ease.InSine).OnComplete(() => 
        {
            GridPos = GetGridPosition();
            LevelManager.instance.GetNode(GridPos).Obstacle = this;
        });

        AudioData audio = new AudioData();
        audio.SetEnvSound(EnvSounds.ObstaclePush);
        snd_Obstacle?.PlayAudio(audio);

        // TUTORIAL ONLY
        if (m_WaitingForTutorialProgress && m_TutorialType == PromptTypes.Push)
            CallForTutorialNextStep();
    }

    public virtual void Throw() { Debug.LogAssertion("Throw action empty"); }

    public virtual void PickUp() { Debug.LogAssertion("Pickup action empty "); }


    #endregion

    #region IInteractable interface

    public virtual void GetActiveActions(Equip _Equip)
    {
        for (int i = 0; i < Actions.Count; i++)
        {
            if(Actions[i] == _Equip.GetEquipAction())
            {
                switch (Actions[i])
                {
                    case PossibleActions.Open:
                        OpenActionCheck(_Equip);
                        break;
                    case PossibleActions.Heal:
                        HealActionCheck(_Equip);
                        break;
                    case PossibleActions.Throw:
                        ThrowActionCheck(_Equip);
                        break;
                    case PossibleActions.Break:
                        BreakActionCheck(_Equip);
                        break;
                    case PossibleActions.TakeDamage:
                        TakeDamageActionCheck(_Equip);
                        break;
                    case PossibleActions.Pull:
                        PullActionCheck(_Equip);
                        break;
                    case PossibleActions.Push:
                        PushActionCheck(_Equip);
                        break;
                    case PossibleActions.PickUp:
                        PickupActionCheck(_Equip);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    #region Actions check
    protected string GetDirectionFromEntity(Vector2Int _pos)
    {
        if (_pos.x > GridPos.x) return "Left";
        if (_pos.x < GridPos.x) return "Right";

        if (_pos.y > GridPos.y) return "Down";
        if (_pos.y < GridPos.y) return "Up";

        return "";
    }

    private bool HasCompatibility(Equip _equip)
    {
        for (int i = 0; i < Actions.Count; i++)
        {
            if (Actions[i] == _equip.GetEquipAction())
            {
                return true;
            }
        }

        return false;
    }

    public virtual bool LocatorEnlightCondition(Equip _equip)
    {
        if(_equip as EPush)
        {
            if (HasCompatibility(_equip) && EnoughSpaceForPush(_equip))
                return true;
            else
                return false;
        }

        return HasCompatibility(_equip);
    } 

    protected virtual void OpenActionCheck(Equip _i) { }
    protected virtual void OpenActionCheck(Equip[] _i) { }

    protected virtual void HealActionCheck(Equip _i) { }
    protected virtual void HealActionCheck(Equip[] _i) { }

    protected virtual void ThrowActionCheck(Equip _i) { }
    protected virtual void ThrowActionCheck(Equip[] _i) { }

    protected virtual void BreakActionCheck(Equip _i)
    {
        if(anchorGridPosition != null)
        {
            if (_i.CanExecute(anchorGridPosition))
                TakeDamange((int)m_healthCurrent);
        }
        else
        {
            if (_i.CanExecute(GridPos))
                TakeDamange((int)m_healthCurrent);
        }
      
    }
    protected virtual void BreakActionCheck(Equip[] _i) { }

    protected virtual void TakeDamageActionCheck(Equip _i)
    {
        if(anchorGridPosition != null)
        {
            //Debug.Log(anchorGridPosition.Length);
            if (_i.CanExecute(anchorGridPosition))
            {
                TakeDamange((int)_i.Value);
            }
            else
            {
                Debug.Log("Can't take damage in grid list");
            }
        }
        else
        {
            if (_i.CanExecute(GridPos))
            {
                TakeDamange((int)_i.Value);
            }
            else
            {
                Debug.Log("Can't take damage in grid position");
            }
        }
    }

    protected virtual void TakeDamageActionCheck(Equip[] _i) { }

    protected virtual void PullActionCheck(Equip _player) 
    {
        Debug.Log("Try pull");

        if (!_player.CanExecute(GridPos)) 
        {
            Debug.Log("Not in range");
            return;
        }

        Vector2Int tmp_Pos;

        // Move left   
        if (GridPos.x > _player.GridPos.x)
        {
            tmp_Pos = new Vector2Int(_player.GridPos.x - 1, _player.GridPos.y);
        }
        // Move right    
        else if (GridPos.x < _player.GridPos.x)
        {
            tmp_Pos = new Vector2Int(_player.GridPos.x + 1, _player.GridPos.y);
        }
        // Move down
        else if (GridPos.y > _player.GridPos.y)
        {
            tmp_Pos = new Vector2Int(_player.GridPos.x, _player.GridPos.y - 1);
        }
        // Move Up
        else
        {
            tmp_Pos = new Vector2Int(_player.GridPos.x, _player.GridPos.y + 1);
        }

        if (LevelManager.instance.IsInsideOfBounds(tmp_Pos.x, tmp_Pos.y) && LevelManager.instance.GetNode(tmp_Pos).IsWalkable())
        {
            Pull(tmp_Pos, _player.GridPos);
            Debug.Log("Pull");

        }
        else
        {
            Debug.LogWarning("Not enough space");
        }

        //Pull(_i.GridPos);
    }


    protected virtual void PushActionCheck(Equip _player) 
    {
        Debug.Log("Try push");

        if (!_player.CanExecute(GridPos))
        {
            Debug.Log("Not in range");
            return;
        }

        Vector2Int tmp_Pos;

        // Move Right   
        if (GridPos.x > _player.GridPos.x)
        {
            tmp_Pos = new Vector2Int(GridPos.x + 1, GridPos.y);
        }
        // Move Left    
        else if (GridPos.x < _player.GridPos.x)
        {
            tmp_Pos = new Vector2Int(GridPos.x - 1, GridPos.y);
        }
        // Move Up
        else if (GridPos.y > _player.GridPos.y)
        {
            tmp_Pos = new Vector2Int(GridPos.x, GridPos.y + 1);
        }
        // Move Down
        else
        {
            tmp_Pos = new Vector2Int(GridPos.x, GridPos.y - 1);
        }

        if (LevelManager.instance.IsInsideOfBounds(tmp_Pos.x, tmp_Pos.y) && LevelManager.instance.GetNode(tmp_Pos).IsWalkable())
        {
            Push(GridPos, tmp_Pos);
            Debug.Log("Push");
            EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.MasterOfIKEA);
        }
        else
        {
            Debug.LogWarning("Not enough space");
        }
    }

    public virtual bool EnoughSpaceForPush(Equip _player)
    {
        if (!_player.CanExecute(GridPos))
        {
            return false;
        }

        Vector2Int tmp_Pos;

        // Move Right   
        if (GridPos.x > _player.GridPos.x)
        {
            tmp_Pos = new Vector2Int(GridPos.x + 1, GridPos.y);
        }
        // Move Left    
        else if (GridPos.x < _player.GridPos.x)
        {
            tmp_Pos = new Vector2Int(GridPos.x - 1, GridPos.y);
        }
        // Move Up
        else if (GridPos.y > _player.GridPos.y)
        {
            tmp_Pos = new Vector2Int(GridPos.x, GridPos.y + 1);
        }
        // Move Down
        else
        {
            tmp_Pos = new Vector2Int(GridPos.x, GridPos.y - 1);
        }

        if (LevelManager.instance.IsInsideOfBounds(tmp_Pos.x, tmp_Pos.y) && LevelManager.instance.GetNode(tmp_Pos).IsWalkable())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected virtual void PushActionCheck(Equip[] _i) { }

    protected virtual void PickupActionCheck(Equip[] _i) { }

    protected virtual void PickupActionCheck(Equip _i) 
    {
        Debug.Log("Pickup check");
        if (!_i.CanExecute(GridPos))
        {
            Debug.Log("Not in range");
            return;
        }
        else
        {
            PickUp();
        }
    }

    #endregion

    #endregion

    public virtual void DeathCheck()
    {
        if (m_healthCurrent <= 0)
        {
            Break();
            // DEV: temp
            GetComponent<BoxCollider>().enabled = false;

            //MyNode.RoomsType = RoomsType.Floor;
            //isActive = false;

            LevelManager.instance.ShowFFRange(true);
        }
    }

    public void ShowInterface()
    {
        throw new System.NotImplementedException();
    }

    public void ShowTip(bool _active)
    {   
        if(!LevelManager.instance.GetNode(GridPos).HasSmoke || LevelManager.instance.GetNode(GridPos).HasSmoke && LevelManager.instance.GetNode(GridPos).Smoke.SeeThrough)
            UIManager.Instance.SetTooltip(_active, this);
    }

    public void ApplyDamageToMaterial(float _value)
    {
        float val = (HealthMax -_value) * 2.5f / HealthMax;
        Debug.Log(val);
        for (int i = 0; i < meshRenderers.Length; i++)
        {
            meshRenderers[i].material.SetFloat("_Cracks", val);
        }
    }

    public GameObject go { get => this.gameObject; }

    public void TutorialPromptReady(PromptTypes type = PromptTypes.Explanation)
    {
        m_TutorialType = type;
        m_WaitingForTutorialProgress = true;
    }

    protected void CallForTutorialNextStep()
    {
        EventManager.TriggerEvent(EventsID.NextTutorialPrompt);
    }

    public void TutorialPromptEnded()
    {
        m_WaitingForTutorialProgress = false;
    }
}
