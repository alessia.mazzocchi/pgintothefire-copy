﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowObs : Obstacle, IInteractable
{
    private bool WindowIsBroken;

    public Vector2Int ClearRange;

    private List<Vector2Int> m_NodesToClear;

    public List<Vector2Int> NodesToClear
    {
        get
        {
            if (m_NodesToClear == null)
            {
                m_NodesToClear = new List<Vector2Int>();
                SaveDirection();
            }
            return m_NodesToClear;
        }
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.OnSceneLoad, Load);

        EventManager.StartListening(EventsID.GameStart, SetNodesToClear);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.OnSceneLoad, Load);

        EventManager.StopListening(EventsID.GameStart, SetNodesToClear);
    }

    public override void Load()
    {
        base.Load();

        LinkToCell();
        WindowIsBroken = false;
    }

    public override void Break()
    {
        LevelManager.instance.GetNode(GridPos).RoomsType = RoomsType.Wall;
        Model.SetActive(false);
        WindowIsBroken = true;
        ClearSmoke();

        AudioData audio = new AudioData();
        audio.SetEnvSound(EnvSounds.ObstacleBreak, MaterialType);
        snd_Obstacle?.PlayAudio(audio);

        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.FreshAir);
    }

    private void ClearSmoke()
    {
        foreach (Vector2Int node in m_NodesToClear)
        {
            LevelManager.instance.GetNode(node).SignalBrokenWindow();
        }
    }

    private void SetNodesToClear()
    {
        LevelManager.instance.GetNode(GridPos).Locator.SetEnlightForWindows(NodesToClear);
    }

    private void SaveDirection()
    {
        // Up
        if (CheckIfIsFloor(new Vector2Int(GridPos.x, GridPos.y + 1)))
        {
            FindNeighboursLinear("Up", ClearRange);
        }
        // Down
        else if (CheckIfIsFloor(new Vector2Int(GridPos.x, GridPos.y - 1)))
        {
            FindNeighboursLinear("Down", ClearRange);
        }
        // Right
        else if (CheckIfIsFloor(new Vector2Int(GridPos.x + 1, GridPos.y)))
        {
            FindNeighboursLinear("Right", ClearRange);
        }
        // Left
        else if (CheckIfIsFloor(new Vector2Int(GridPos.x - 1, GridPos.y)))
        {
            FindNeighboursLinear("Left", ClearRange);
        }
    }

    private bool CheckIfIsFloor(Vector2Int pos)
    {
        if (LevelManager.instance.IsInsideOfBounds(pos) && LevelManager.instance.GetNode(pos).RoomsType == RoomsType.Floor) return true;

        return false;
    }

    private void FindNeighboursLinear(string _dir, Vector2 _range)
    {
        int X_start = 0;
        int X_end = 0;

        int Y_start = 0;
        int Y_end = 0;

        switch (_dir)
        {
            case "Up":
                X_start = GridPos.x - Mathf.FloorToInt(_range.x / 2);
                X_end = GridPos.x + Mathf.FloorToInt(_range.x / 2);

                Y_start = GridPos.y + 1;
                Y_end = GridPos.y + /*Mathf.FloorToInt(_range.y / 2)*/ Mathf.RoundToInt(_range.y) + 1;

                //Debug.Log("X Area: " + X_start + "-" + X_end);
                //Debug.Log("Y Area: " + Y_start + "-" + Y_end);

                for (int i = X_start; i <= X_end; i++)
                {
                    for (int j = Y_start; j < Y_end; j++)
                    {
                        m_NodesToClear.Add(new Vector2Int(i, j));
                        // CLEAR SMOKE HERE
                        //NodeExtistingCheck(i, j);
                        //ExtinguishNeighboursList.Add(new Vector2Int(i, j));
                    }
                }

                //Debug.Log("Size Up : " + ExtinguishNeighboursList.Count);
                break;

            case "Down":
                X_start = GridPos.x - Mathf.FloorToInt(_range.x / 2);
                X_end = GridPos.x + Mathf.FloorToInt(_range.x / 2);

                Y_start = GridPos.y - 1;
                Y_end = GridPos.y -/* Mathf.FloorToInt(_range.y / 2)*/ Mathf.RoundToInt(_range.y) - 1;

                //Debug.Log("X Area: " + X_start + "- " + X_end);
                //Debug.Log("Y Area: " + Y_start + "- " + Y_end);

                for (int i = X_start; i <= X_end; i++)
                {
                    for (int j = Y_start; j > Y_end; j--)
                    {
                        m_NodesToClear.Add(new Vector2Int(i, j));

                        //ExtinguishNeighboursList.Add(new Vector2Int(i, j));
                    }
                }
                break;

            case "Left":
                X_start = GridPos.y + Mathf.FloorToInt(_range.x / 2);
                X_end = GridPos.y - Mathf.FloorToInt(_range.x / 2);

                Y_start = GridPos.x - 1;
                Y_end = GridPos.x - Mathf.RoundToInt(_range.y) - 1;

                //Debug.Log("X Area: " + X_start + "-" + X_end + " = " + Mathf.Abs(X_start - X_end));
                //Debug.Log("Y Area: " + Y_start + "-" + Y_end + " = " + Mathf.Abs(Y_start - Y_end));

                for (int i = Y_start; i > Y_end; i--)
                {
                    for (int j = X_start; j >= X_end; j--)
                    {
                        m_NodesToClear.Add(new Vector2Int(i, j));

                        //ExtinguishNeighboursList.Add(new Vector2Int(i, j));
                    }
                }

                break;

            case "Right":
                X_start = GridPos.y + Mathf.FloorToInt(_range.x / 2);
                X_end = GridPos.y - Mathf.FloorToInt(_range.x / 2);

                Y_start = GridPos.x + 1;
                Y_end = GridPos.x + Mathf.RoundToInt(_range.y) + 1;

                //Debug.Log("X Area: " + X_start + "-" + X_end + " = " + Mathf.Abs(X_start - X_end));
                //Debug.Log("Y Area: " + Y_start + "-" + Y_end + " = " + Mathf.Abs(Y_start - Y_end));

                for (int i = Y_start; i < Y_end; i++)
                {
                    for (int j = X_start; j >= X_end; j--)
                    {
                        m_NodesToClear.Add(new Vector2Int(i, j));

                        //ExtinguishNeighboursList.Add(new Vector2Int(i, j));
                    }
                }
                break;
        }
    }

}
