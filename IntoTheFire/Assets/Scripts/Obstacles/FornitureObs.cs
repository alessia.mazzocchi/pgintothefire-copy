﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FornitureObs : Obstacle, IInteractable
{
    [HideInInspector]
    public bool IsOnFire;

    #region Actions

    public override void Heal(int _value)
    {
        base.Heal(_value);
    }

    public override void Break()
    {
        base.Break();
        //TODO implement
    }

    #endregion

    public void CatchFire()
    {
        // TODO add this method to the eventmanger

        return;

        if (m_healthCurrent <= 0)
            Break();
    }
}
