﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeddyBear : Obstacle
{
    public GameObject[] Meshes;
    private void Start()
    {
        int rnd = Random.Range(0, Meshes.Length);

        Model = Meshes[rnd];
        Model.SetActive(true);
    }

    public override void PickUp()
    {

        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.TeddyBear);

        LevelManager.instance.GetNode(GridPos).ObstacleExit(this);

        gameObject.SetActive(false);
    }
}
