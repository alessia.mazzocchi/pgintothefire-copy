﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallObs : Obstacle, IInteractable
{
    public bool Destructible;
    //public GameObject WallModel;

    //public override void GetActiveActions(Equip[] inventory)
    //{
    //    if (Destructible)
    //        base.GetActiveActions(inventory);
    //}

    //public override void GetActiveActions(Equip _Equip)
    //{
    //    base.GetActiveActions(_Equip);
    //}

    public override void Load()
    {
        base.Load();

        LinkToCell();

        gameObject.name = "Destructible wall";

        if (Destructible)
            LevelManager.instance.GetNode(GridPos).RoomsType = RoomsType.Destructible_Wall;
    }

    //public override void TakeDamange(int _damage)
    //{
    //    if (Destructible)
    //    {
    //        base.TakeDamange(_damage);
    //    }
    //    else
    //    {
    //        // Feedback
    //    }
    //}


    public override void Break()
    {
        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.OpenSpace);

        base.Break();
        //Model.SetActive(false);

        //MyNode.RoomsType = RoomsType.Floor;
        //isActive = false;
    }
}
