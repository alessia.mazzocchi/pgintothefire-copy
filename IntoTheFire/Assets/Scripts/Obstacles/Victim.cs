﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class Victim : Obstacle, IInteractable
{
    private Animator m_Animator;
    private string[] namePool = { "Robert", "Bob", "Jeff", "Lucas", "Andrea", "Henry", "Sebastian", "Daniel", "Bobbert", "Laurence", "George",
        "Timothy", "John", "Francis", "Earl", "Jamal", "Biggie", "Jason", "Donald", "Mario", "Elijah", "Jacob", "Elise", "David", "Mick", "Nick", "Salvo", "Gallo", "Banana",
        "Thomas", "Edison", "Sophie", "Emma", "Benny", "Dero", "Vios", "Chloe", "Emily", "Zoe", "Charlotte", "Hunter"};
    private FireFighterBehaviour firefighter;
    public GameObject[] Meshes;
    private GameObject currentMesh;
    //[Header("Victim values")]
    //public float FallTime = 0.4f;
    //private float currentFallTime;
    //public AnimationCurve FallAnimCurve;

    protected override void Awake()
    {
        base.Awake();

        int rnd = Random.Range(0, Meshes.Length);

        currentMesh = Meshes[rnd];
        currentMesh.SetActive(true);

        m_Animator = currentMesh.GetComponent<Animator>();
    }

    public override void Load()
    {
        base.Load();
        //Debug.Log("Load");
        gameObject.name = namePool[(int)Random.Range(0f, namePool.Length)];

    }

    public override void LinkToCell()
    {
        LevelManager.instance.SetSceneReferences(this);
    }

    public void RandomizeRotation()
    {
        //transform.rotation = new Quaternion(transform.rotation.x, Random.rotation.y, transform.rotation.z, transform.rotation.w);
    }

    public bool IsUnconscious()
    {
        if (m_healthCurrent > 0) return false;
        return true;
    }

    public override void TakeDamange(int _damage)
    {
        if (!IsUnconscious())
        {
            base.TakeDamange(_damage);
        }
        else
        {
            // Feedback
        }
    }

    public override void PickUp()
    {
        LevelManager lm = LevelManager.instance;

        // Use equipment in inputmanager
        (InputManager.Instance.selectedEquip as EPickUp).pickedVictim = this;
        lm.GetNode(GridPos).ObstacleExit(this);
        lm.GetSelectedFF.AddVictimCarried(this);

        firefighter = lm.GetSelectedFF;

        if(lm.GetSelectedFF.Name != FFName.Atlas)
        {
            m_Animator.SetInteger("CarryPosition", 0);
        }
        else
        {
            if(lm.GetSelectedFF.VictimCount() == 1)
            {
                m_Animator.SetInteger("CarryPosition", 1);
            }
            else
            {
                m_Animator.SetInteger("CarryPosition", 2);
            }
        }

        m_Animator.SetBool("Carry", true);

        // TUTORIAL ONLY
        if (m_WaitingForTutorialProgress && m_TutorialType == PromptTypes.PickupCivilian)
            CallForTutorialNextStep();

        EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);

        EventManager.TriggerEvent(EventsID.UpdateUIValues);

    }


    public void PickUp(FireFighterBehaviour _ff)
    {
        LevelManager.instance.GetSelectedFF.PassVictimToAnother();

        transform.SetParent(null);
        _ff.SetVictimToFreeSlot(this);
        _ff.AddVictimCarried(this);
        firefighter = _ff;

        // TOTEST
        if (_ff.Name != FFName.Atlas)
        {
            m_Animator.SetInteger("CarryPosition", 0);
        }
        else
        {
            if (_ff.VictimCount() == 1)
            {
                m_Animator.SetInteger("CarryPosition", 1);
                Debug.Log("pos 1");
            }
            else
            {
                m_Animator.SetInteger("CarryPosition", 2);
                Debug.Log("pos 2");

            }
        }

        EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);

        //if (lm.GetSelectedFF.CanPickupVictim())
        //{
        //    lm.GetNode(GridPos).ObstacleExit();
        //    lm.GetSelectedFF.AddVictimCarried(this);
        //}
    }

    public void UpdatePosition()
    {
        // TOTEST
        if (firefighter.Name != FFName.Atlas)
        {
            m_Animator.SetInteger("CarryPosition", 0);
        }
        else
        {
            if (firefighter.VictimCount() == 1)
            {
                m_Animator.SetInteger("CarryPosition", 1);

            }
            else
            {
                m_Animator.SetInteger("CarryPosition", 2);

            }
        }
        Debug.Log("UPDATE POSITION");
    }

    public void PickUpFallVictim(FireFighterBehaviour _ff)
    {
        _ff.SetVictimToFreeSlot(this);

        //lm.GetNode(GridPos).ObstacleExit();
        _ff.AddVictimCarried(this);

        firefighter = _ff;

        if (_ff.Name != FFName.Atlas)
        {
            m_Animator.SetInteger("CarryPosition", 0);
        }
        else
        {
            if (_ff.VictimCount() == 1)
            {
                m_Animator.SetInteger("CarryPosition", 1);

            }
            else
            {
                m_Animator.SetInteger("CarryPosition", 2);

            }
        }

        m_Animator.SetBool("Carry", true);

        Fall(firefighter.VictimPosition.position);

        EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);
    }

    public void Fall(Vector3 _endpos)
    {
        transform.position = new Vector3(_endpos.x, _endpos.y + 4, _endpos.z);
        gameObject.SetActive(true);

        transform.DOMoveY(0, 1f).SetEase(Ease.OutBounce);
        //StartCoroutine(Move(transform.position, _endpos));
    }

    public void Drop()
    {
        m_Animator.SetBool("Carry", false);
    }

    private void EndFall()
    {
       
    }

    public void DropOnExitPoint()
    {
        // FFremove
        Drop();
        firefighter.DropVictimOnExitPoint(this);

        LevelManager.instance.ChangeSavedCivilians(+1);
        go.SetActive(false);

        EventManager.TriggerEvent(EventsID.UpdateUIValues);
        EventManager.TriggerEvent(EventsID.UpdateVictimActionsUI);
    }

    public void CheckForExit()
    {
        LevelManager lm = LevelManager.instance;
        if (lm.CheckIfOnExitNode(GridPos))
        {
            lm.GetNode(GridPos).ObstacleExit();
            // animations
            lm.ChangeSavedCivilians(+1);
            Destroy(go);
        }
        else
            Debug.Log("NOPE");

    }
}
