﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorObs : Obstacle, IInteractable
{
    public bool Blocked = false;

    public override void Load()
    {
        base.Load();

        LinkToCell();

        gameObject.name = "Door";
    }

    /// <summary>
    /// DEPRECATO
    /// </summary>
    public override void Open()
    {
        MyNode.RoomsType = RoomsType.Floor;
        m_CanInteract = false;
        isActive = false;

        // Animations
        Model.transform.RotateAround(Vector3.up, 90);
    }

    //public override void Break()
    //{
    //    MyNode.RoomsType = RoomsType.Floor;
    //    isActive = false;
        
    //    Mesh.enabled = false;
    //}

    //public override void DeathCheck()
    //{
    //    if(HealthCurrent <= 0)
    //    {
    //        Break();

    //        //LevelManager.instance.ShowFFRange(true);
    //    }
    //}

    public void BlockDoor()
    {
        Blocked = true;
    }
    //private void OnMouseEnter()
    //{
    //    UIManager.Instance.SetTooltip(true, this);
    //}

    //private void OnMouseExit()
    //{
    //    UIManager.Instance.SetTooltip(false, this);
    //}

    public override void Break()
    {
        base.Break();

        LevelManager.instance.UpdateOracleFOV();

        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.OpenUp);
        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.DoNotKnock);
    }
}
