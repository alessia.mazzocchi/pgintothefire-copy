﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FlameLevel
{
    Pilot,
    Flame,
    Blaze
}

[System.Serializable]
public struct FlameStats
{
    public CheckRadius Directions;
    public int TurnsToEvolve;
    public int SpreadsPerTurn;
    public int SpreadCountDiminisher;
    public int StandingDamageToFFs;
    public int PassingDamageToFFs;
    public bool GeneratesSmoke;
}


public class FlameBehaviour : Entity
{
    public GameObject FlameLvl1_Object;
    public GameObject FlameLvl2_Object;
    public GameObject FlameLvl3_Object;
    [Header("Dirt")]
    public Vector2 DirtSpawnTimeMinMax = new Vector2(7, 12);

    public SpriteRenderer DirtSpriteLV1;
    public SpriteRenderer DirtSpriteLV2;
    public SpriteRenderer DirtSpriteLV3;

    private float RandomDirtSpawnTime
    {
        get
        {
            return Random.Range(DirtSpawnTimeMinMax.x, DirtSpawnTimeMinMax.y);
        }
    }

    [HideInInspector]
    public FlameLevel Level;

    [HideInInspector]
    public FlameStats currentStats;

    private void OnEnable()
    {
        ResetStats();
        EventManager.StartListening<FlameLevel>(EventsID.FireExpansion, Spread);
        EventManager.StartListening(EventsID.FireEvolution, Evolve);
        EventManager.StartListening(EventsID.SmokeCreation, CreateSmoke);
    }

    private void OnDisable()
    {
        EventManager.StopListening<FlameLevel>(EventsID.FireExpansion, Spread);
        EventManager.StopListening(EventsID.FireEvolution, Evolve);
        EventManager.StopListening(EventsID.SmokeCreation, CreateSmoke);
    }

    private void Start()
    {
        Load();
    }

    public override void Load()
    {
        base.Load();

        ChangeFlameVisual();
    }

    private void Spread(FlameLevel level)
    {
        if (!LevelManager.instance.ThereIsMinOneFlame)
            LevelManager.instance.SetFirePresence(true);

        if (level == Level && !IsReadyToEvolve())
        {
            switch (Level)
            {
                case FlameLevel.Pilot:
                    PilotSpread();
                    break;

                case FlameLevel.Flame:
                    FlameSpread();
                    break;

                case FlameLevel.Blaze:
                    SpreadAround();
                    break;

                default:
                    break;
            }
        }
    }

    private void PilotSpread()
    {
        ReduceTurnsToEvolve(1);
    }

    private void FlameSpread()
    {
        ReduceTurnsToEvolve(1);
        SpreadAround();
    }

    private void SpreadAround()
    {
        List<Vector2Int> AdiacentNodes = GetDirectionNodes(new Vector2Int(1,1), currentStats.Directions);

        List<Node> NodesWithFuel = new List<Node>();

        int spreads = currentStats.SpreadsPerTurn;

        List<Vector2Int> temp = new List<Vector2Int>(AdiacentNodes);

        foreach (Vector2Int node in temp)
        {
            if (spreads > 0)
            {
                Node n = LevelManager.instance.GetNode(node.x, node.y);
                if (n.HasFuel)
                {
                    NodesWithFuel.Add(n);
                    AdiacentNodes.Remove(node);
                }
            }
        }

        if (NodesWithFuel.Count > 0 && spreads > NodesWithFuel.Count)
        {
            int max = NodesWithFuel.Count;

            for (int i = 0; i < max; i++)
            {
                Node n = NodesWithFuel[Random.Range(0, NodesWithFuel.Count - 1)];
                NodesWithFuel.Remove(n);
                n.UseFuel();
                n.SetOnFire(false, 1);
                spreads--;
            }
        }
        else if (NodesWithFuel.Count > 0 && spreads <= NodesWithFuel.Count)
        {
            for (int i = 0; i < spreads; i++)
            {
                Node n = NodesWithFuel[Random.Range(0, NodesWithFuel.Count - 1)];
                NodesWithFuel.Remove(n);
                n.UseFuel();
                n.SetOnFire(false, 1);
                spreads--;
            }
        }

        for (int i = 0; i < spreads; i++)
        {
            Vector2Int node = AdiacentNodes[Random.Range(0, AdiacentNodes.Count - 1)];
            Node n = LevelManager.instance.GetNode(node.x, node.y);
            AdiacentNodes.Remove(node);
            n.TryToSetOnFire(this);
        }
    }

    public void Evolve()
    {
        if (IsReadyToEvolve() && Level != FlameLevel.Blaze)
        {
            if (Level == FlameLevel.Pilot)
                Level = FlameLevel.Flame;
            else if (Level == FlameLevel.Flame)
                Level = FlameLevel.Blaze;

            ResetStats();
            EvolveFlameVisual();
        }
    }

    private bool IsReadyToEvolve()
    {
        if (currentStats.TurnsToEvolve <= -1)
            return true;
        else
            return false;
    }

    public void ReduceTurnsToEvolve(int _amount)
    {
        if (Level != FlameLevel.Blaze)
            currentStats.TurnsToEvolve -= _amount;
    }

    public void ResetStats()
    {
        switch (Level)
        {
            case FlameLevel.Pilot:
                currentStats = GameManager.Instance.PilotStat;
                break;
            case FlameLevel.Flame:
                currentStats = GameManager.Instance.FlameStat;
                break;
            case FlameLevel.Blaze:
                currentStats = GameManager.Instance.BlazeStat;
                break;
            default:
                break;
        }

        DirtSpriteLV1.color = Color.clear;
        DirtSpriteLV2.color = Color.clear;
        DirtSpriteLV3.color = Color.clear;
    }

    public bool TryToExtinguish(int damage)
    {
        int grade;

        switch (Level)
        {
            case FlameLevel.Pilot:
                grade = 1;
                break;
            case FlameLevel.Flame:
                grade = 2;
                break;
            case FlameLevel.Blaze:
                grade = 3;
                break;
            default:
                grade = 1;
                break;
        }

        grade -= damage;

        UIManager.Instance.DamageFeedback(transform.position, damage.ToString(), DamageType.Water);

        GameObject particle = PoolManager.instance.GetItem(PoolType.Foam);

        particle.transform.position = transform.position;

        particle.transform.forward = Vector3.up;

        particle.SetActive(true);

        //Debug.Log("grade " + grade + " / damage " + damage);

        if (grade <= 0)
            return true;
        else
        {
            switch (grade)
            {
                case 1:
                    Level = FlameLevel.Pilot;
                    break;
                case 2:
                    Level = FlameLevel.Flame;
                    break;
                case 3:
                    Level = FlameLevel.Blaze;
                    break;
                default:
                    break;
            }

            ResetStats();
            ChangeFlameVisual();

            return false;
        }
    }

    public void Extinguish()
    {
        // Animations
        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.FireFree);
        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.FireWhat);

        switch (Level)
        {
            case FlameLevel.Pilot:
                EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.PilotOff);
                break;
            case FlameLevel.Flame:
                EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.FlameOff);
                break;
            case FlameLevel.Blaze:
                EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.BlazeOff);
                break;
            default:
                break;
        }
        
        gameObject.SetActive(false);
    }

    private void CreateSmoke()
    {
        if (!GameManager.Instance.NoSmokeOnFlame)
        {
            if (currentStats.GeneratesSmoke)
            {
                Pathfinding pathfinder = new Pathfinding();

                Node n = pathfinder.GetFirstAvailableNode_Smoke(GridPos, 1);

                // If returned node corresponds to this node, it means there was no node available in the area. No smoke will be created.
                if (n == LevelManager.instance.GetNode(GridPos))
                    return;

                n.SetSmoke();
            }
        }
    }

    private void EvolveFlameVisual()
    {
        if (FlameLvl1_Object.activeSelf)
            FlameLvl1_Object.transform.DOScale(new Vector3(0, 0, 0), 2).SetEase(Ease.OutSine);
        if (FlameLvl2_Object.activeSelf)
            FlameLvl2_Object.transform.DOScale(new Vector3(0, 0, 0), 2).SetEase(Ease.OutSine);
        //if (FlameLvl3_Object.activeSelf)
        //    FlameLvl3_Object.transform.DOScale(new Vector3(0, 0, 0), 2).SetEase(Ease.OutSine);

        switch (Level)
        {
            case FlameLevel.Pilot:
                FlameLvl1_Object.SetActive(true);
                DirtSpriteLV1.DOColor(Color.white, RandomDirtSpawnTime);
                break;
            case FlameLevel.Flame:
                FlameLvl2_Object.SetActive(true);
                DirtSpriteLV1.color = Color.white;
                DirtSpriteLV2.DOColor(Color.white, RandomDirtSpawnTime);

                break;
            case FlameLevel.Blaze:
                FlameLvl3_Object.SetActive(true);
                DirtSpriteLV1.color = Color.white;
                DirtSpriteLV2.color = Color.white;
                DirtSpriteLV3.DOColor(Color.white, RandomDirtSpawnTime);
                break;
            default:
                break;
        }
    }

    private void ChangeFlameVisual()
    {
        if (FlameLvl1_Object.activeSelf)
            FlameLvl1_Object.SetActive(false);
        if (FlameLvl2_Object.activeSelf)
            FlameLvl2_Object.SetActive(false);
        if (FlameLvl3_Object.activeSelf)
            FlameLvl3_Object.SetActive(false);

        switch (Level)
        {
            case FlameLevel.Pilot:
                FlameLvl1_Object.SetActive(true);
                DirtSpriteLV1.DOColor(Color.white, RandomDirtSpawnTime);
                break;
            case FlameLevel.Flame:
                FlameLvl2_Object.SetActive(true);
                DirtSpriteLV1.color = Color.white;
                DirtSpriteLV2.DOColor(Color.white, RandomDirtSpawnTime);

                break;
            case FlameLevel.Blaze:
                FlameLvl3_Object.SetActive(true);
                DirtSpriteLV1.color = Color.white;
                DirtSpriteLV2.color = Color.white;
                DirtSpriteLV3.DOColor(Color.white, RandomDirtSpawnTime);
                break;
            default:
                break;
        }
    }
}
