﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisObs : Obstacle, IInteractable
{
    private bool onDestroing;
    [SerializeField]
    private float LerpTime = 0.7f;
    private float currentTime;

    public override void Load()
    {
        base.Load();

        onDestroing = false;
    }

    //private void Update()
    //{
    //    if (onDestroing)
    //    {
    //        currentTime += Time.deltaTime;

    //        float t = currentTime / LerpTime; 
    //        Model.material.color = Color.Lerp(Color.white, Color.clear, t);

    //        if(currentTime >= LerpTime)
    //        {
    //            LevelManager.instance.GetNode(GridPos.x, GridPos.y).Obstacle = null;
    //            gameObject.SetActive(false);
    //        }
    //    }
    //}

    public override void Break()
    {
        MyNode.RoomsType = RoomsType.Floor;
        m_CanInteract = false;
        isActive = false;

        onDestroing = true;

        EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.CleaningUp);
        // Animations
    }


}
