﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeBehaviour : Entity
{
    public GameObject SmokeParticle;

    private ParticleSystem particle;
    private Renderer renderer;

    public Color semitransparent;
    private Color baseColor;
    private Color currentColor;

    public float FadeTime = 0.3f;
    private float currentFadeTime;
    private bool fixTransparency;
    [HideInInspector]
    public bool isShowing;

    private bool m_SeeThrough;

    public bool SeeThrough { get { return m_SeeThrough; } }

    private List<FireFighterBehaviour> nearbyFFs = new List<FireFighterBehaviour>();


    private void OnEnable()
    {
        EventManager.StartListening(EventsID.GameStart, QuickFFCheck);

        if (LevelManager.instance.GameHasStarted)
            QuickFFCheck();
    }
    private void OnDisable()
    {
        EventManager.StopListening(EventsID.GameStart, QuickFFCheck);
    }

    protected override void Awake()
    {
        base.Awake();

        if(SmokeParticle != null)
        {
            particle = SmokeParticle.GetComponent<ParticleSystem>();
            renderer = particle.GetComponent<Renderer>();
        }

        baseColor = renderer.material.GetColor("_BaseColor");
    }

    private void FixedUpdate()
    {
        if (fixTransparency)
        {
            /// Fade out
            if (isShowing)
            {
                currentFadeTime += Time.fixedDeltaTime;

                float t = currentFadeTime / FadeTime;

                renderer.material.SetColor("_BaseColor", Color.Lerp(currentColor, baseColor, t));
                //currentMaterial.SetColor("_BaseColor", Color.Lerp(transparentColor, Color.white, t));

                if (t >= 1)
                {
                    currentFadeTime = 0;
                    fixTransparency = false;
                }
            }
            /// Fade In
            else
            {
                currentFadeTime += Time.fixedDeltaTime;

                float t = currentFadeTime / FadeTime;

                renderer.material.SetColor("_BaseColor", Color.Lerp(currentColor, semitransparent, t));
                //currentMaterial.SetColor("_BaseColor", Color.Lerp(Color.white, transparentColor, t));

                if (t >= 1)
                {
                    currentFadeTime = 0;
                    fixTransparency = false;
                }
            }
        }
    }

    public void CheckVisibility()
    {
        IsSeeThrough(m_SeeThrough);

        //if (nearbyFFs.Count > 0)
        //    IsSeeThrough(true);
        //else
        //    IsSeeThrough(false);
    }

    private void QuickFFCheck()
    {
        IsSeeThrough(HasFFAround());
    }

    public void IsSeeThrough(bool seeThrough)
    {
        if (seeThrough)
        {
            currentColor = renderer.material.GetColor("_BaseColor");

            fixTransparency = true;

            isShowing = false;

            currentFadeTime = 0;

            //particle.startColor = Color.clear;
            //renderer.material.SetColor("_BaseColor", Color.clear);
            //Debug.Log("Visible");
        }
        else
        {
            currentColor = renderer.material.GetColor("_BaseColor");

            fixTransparency = true;

            isShowing = true;

            currentFadeTime = 0;

            //particle.startColor = Color.white;
            //renderer.material.SetColor("_BaseColor", baseColor);
            //Debug.Log("Not visible");
        }
    }

    private bool HasFFAround()
    {
        LevelManager lm = LevelManager.instance;

        if (lm.GetNode(GridPos).HasFF)
        {
            m_SeeThrough = true;
            AddFFToList(lm.GetNode(GridPos).FireFighter);
            return true;
        }

        List<Vector2Int> AdiacentNodes = GetDirectionNodes(new Vector2Int(1, 1), CheckRadius.FourDirection);

        foreach (Vector2Int node in AdiacentNodes)
        {
            Node n = lm.GetNode(node.x, node.y);

            if (n.HasFF)
            {
                m_SeeThrough = true;
                AddFFToList(n.FireFighter);
                return true;
            }
        }
        m_SeeThrough = false;
        return false;
    }

    public bool OracleIsAround()
    {
        foreach (FireFighterBehaviour ff in nearbyFFs)
        {
            if (ff.Name == FFName.Oracle)
                return true;
        }

        return false;
    }

    public void Erase(bool ffAction = false)
    {
        if (ffAction)
            EventManager.TriggerEvent(EventsID.SideObjectiveCheck, SideObjectiveTypes.CleanView);

        gameObject.SetActive(false);
    }

    public void AddFFToList(FireFighterBehaviour ff)
    {
        if (!nearbyFFs.Contains(ff))
            nearbyFFs.Add(ff);

        m_SeeThrough = true;

        CheckVisibility();
    }

    public void RemoveFFFromList(FireFighterBehaviour ff)
    {
         nearbyFFs.Remove(ff);

        if (nearbyFFs.Count <= 0)
            m_SeeThrough = false;

        CheckVisibility();
    }
}
