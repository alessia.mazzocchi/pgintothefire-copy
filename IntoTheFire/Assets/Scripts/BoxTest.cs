﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxTest : MonoBehaviour
{
    public Renderer Renderer;
    private Material myMat;

    public Material BurndMat;

    private float currentT;

    void Start()
    {
        myMat = Renderer.material;
    }


    void Update()
    {
        float lerp = Mathf.PingPong(Time.time, 7) / 7;
        Renderer.material.Lerp(myMat, BurndMat, lerp);

        Debug.Log("Lerp");

        //Renderer.material = BurndMat;
    }
}
