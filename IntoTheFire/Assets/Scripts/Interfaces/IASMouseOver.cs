﻿using UnityEngine;

public interface IASMouseOver 
{
    void MouseEnter();

    void MouseExit();

    void MouseClick();

    GameObject ASGameObject();
}


public interface IHubMouseOver : IASMouseOver { }

public interface ILevelSelectioneOver : IASMouseOver { }