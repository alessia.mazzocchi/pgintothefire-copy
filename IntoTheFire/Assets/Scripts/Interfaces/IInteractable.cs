﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    bool CanAct { get; }

    void GetActiveActions(Equip _equip);

    //void Interact();

    void Open();

    void Break();

    void TakeDamange(int _damage);

    void Heal(int _value);

    void Pull(Vector2Int _PlayerPos, Vector2Int _MyNextPos);

    void Push(Vector2Int _PlayerPos, Vector2Int _MyNextPos);

    void Throw();

    void PickUp();

}
