﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHub
{
    void OnClick();
}
