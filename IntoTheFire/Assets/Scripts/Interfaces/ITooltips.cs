﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITooltip
{
    GameObject go { get; }

    void ShowTip(bool _active);
}
