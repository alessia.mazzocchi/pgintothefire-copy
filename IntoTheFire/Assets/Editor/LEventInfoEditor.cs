﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Malee.Editor;

[CustomEditor(typeof(LEventInfo))]
public class LEventInfoEditor : Editor
{
    SerializedObject LInfo;

    ReorderableList list;

    private void OnEnable()
    {
        LInfo = new SerializedObject (target);
    }


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        ReorderableList list = new ReorderableList(LInfo.FindProperty("list"));
    }
}

