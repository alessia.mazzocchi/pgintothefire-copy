﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[CustomEditor(typeof(MissionManager))]
public class ObjectivesEditor : Editor
{
    MissionManager mm;

    //SerializedObject so;

    //Vector2 ScrollPos;

    public SerializedProperty[] Elements;

    public SerializedProperty SideObjectives_Prop;

    public SerializedProperty MainName_Prop;
    public SerializedProperty Description_Prop;
    public SerializedProperty Difficulty_Prop;
    public SerializedProperty MissionType_Prop;
    public SerializedProperty MinCiviliansToSave_Prop;
    public SerializedProperty MaxRounds_Prop;
    public SerializedProperty MainReward_Prop;
    public SerializedProperty Icon_Prop;


    void OnEnable()
    {
        mm = (MissionManager)target;

        //so = new SerializedObject(target);

        SideObjectives_Prop = serializedObject.FindProperty("SideObjectives");
        MainName_Prop = serializedObject.FindProperty("Name");
        Description_Prop = serializedObject.FindProperty("Description");
        Difficulty_Prop = serializedObject.FindProperty("Difficulty");
        MissionType_Prop = serializedObject.FindProperty("MissionType");
        MinCiviliansToSave_Prop = serializedObject.FindProperty("MinCiviliansToSave");
        MaxRounds_Prop = serializedObject.FindProperty("MaxRounds");
        MainReward_Prop = serializedObject.FindProperty("MainReward");
        Icon_Prop = serializedObject.FindProperty("Icon");

        Elements = new SerializedProperty[SideObjectives_Prop.arraySize];
    }

    public override void OnInspectorGUI()
    {
        GUILayout.Space(5);
        EditorGUILayout.LabelField("MAIN MISSION", EditorStyles.boldLabel);
        GUILayout.Space(5);

        EditorGUILayout.PropertyField(MainName_Prop);
        GUILayout.Space(5);
        EditorGUILayout.PropertyField(Description_Prop);
        GUILayout.Space(5);
        EditorGUILayout.PropertyField(Difficulty_Prop);
        GUILayout.Space(5);
        EditorGUILayout.PropertyField(MissionType_Prop);
        GUILayout.Space(5);
        if ((MissionTypes)(MissionType_Prop.enumValueIndex) == MissionTypes.Rescue)
            EditorGUILayout.PropertyField(MinCiviliansToSave_Prop);
        GUILayout.Space(5);
        EditorGUILayout.PropertyField(MaxRounds_Prop);
        GUILayout.Space(5);
        EditorGUILayout.PropertyField(MainReward_Prop, true);
        GUILayout.Space(10);
        EditorGUILayout.PropertyField(Icon_Prop);

        

        GUILayout.Space(15);
        EditorGUILayout.LabelField("SECONDARY OBJECTIVES", EditorStyles.boldLabel);
        GUILayout.Space(5);
        DrawProperties(SideObjectives_Prop, true);

        serializedObject.ApplyModifiedProperties();
    }

    private void DrawProperties(SerializedProperty sp, bool value)
    {
        for (int i = 0; i < SideObjectives_Prop.arraySize; i++)
        {
            Elements[i] = SideObjectives_Prop.GetArrayElementAtIndex(i);

            SerializedProperty ObjectiveType_Prop = Elements[i].FindPropertyRelative("ObjectiveType");
            SerializedProperty value_Prop = Elements[i].FindPropertyRelative("Value");
            SerializedProperty ffName_Prop = Elements[i].FindPropertyRelative("FirefighterName");
            SerializedProperty reward_Prop = Elements[i].FindPropertyRelative("Reward");

            bool isExpanded = Elements[i].isExpanded;

            GUI.backgroundColor = Color.cyan;
            if (GUILayout.Button(((SideObjectiveTypes)(ObjectiveType_Prop.enumValueIndex)).ToString()))
            {
                isExpanded = !isExpanded;
                Elements[i].isExpanded = isExpanded;
            }

            if (Elements[i].isExpanded)
            {
                GUI.backgroundColor = Color.white;

                EditorGUILayout.PropertyField(ObjectiveType_Prop, true);

                switch ((SideObjectiveTypes)ObjectiveType_Prop.enumValueIndex)
                {
                    case SideObjectiveTypes.PerfectTiming:
                    case SideObjectiveTypes.FireFree:
                    case SideObjectiveTypes.OpenUp:
                    case SideObjectiveTypes.FreshAir:
                    case SideObjectiveTypes.OpenSpace:
                    case SideObjectiveTypes.CleaningUp:
                    case SideObjectiveTypes.CleanView:
                    case SideObjectiveTypes.BlazeOff:
                    case SideObjectiveTypes.FlameOff:
                    case SideObjectiveTypes.PilotOff:
                        EditorGUILayout.PropertyField(value_Prop);
                        break;
                    case SideObjectiveTypes.YoureUp:
                        EditorGUILayout.PropertyField(ffName_Prop);
                        break;
                    default:
                        break;
                }
                GUILayout.Space(5);

                EditorGUILayout.PropertyField(reward_Prop, true);
            }

            EditorGUILayout.BeginHorizontal();

            GUI.backgroundColor = Color.yellow;
            if (GUILayout.Button("Move up"))
            {
                SideObjectives_Prop.MoveArrayElement(i, i - 1);
            }
            GUI.backgroundColor = Color.yellow;
            if (GUILayout.Button("Move down"))
            {
                SideObjectives_Prop.MoveArrayElement(i, i + 1);
            }
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(15);
        }

        EditorGUILayout.BeginHorizontal();

        Add();

        RemoveLast();

        EditorGUILayout.EndHorizontal();
    }

    private void Add()
    {
        GUI.backgroundColor = Color.green;
        if (GUILayout.Button("Add"))
        {
            if (SideObjectives_Prop == null)
            {
                mm.InitializeSideObjectives();
            }
            else
            {
                SideObjectives_Prop.arraySize++;
                Elements = new SerializedProperty[SideObjectives_Prop.arraySize];
            }
        }
    }

    private void RemoveLast()
    {
        GUI.backgroundColor = Color.red;
        if (GUILayout.Button("Remove Last"))
        {
            if (SideObjectives_Prop.arraySize <= 0)
            {
                Debug.LogWarning("The size of the array is 0");
                return;
            }

            if (SideObjectives_Prop.arraySize > 0)
            {
                SideObjectives_Prop.DeleteArrayElementAtIndex(SideObjectives_Prop.arraySize - 1);
                Elements = new SerializedProperty[SideObjectives_Prop.arraySize];
            }
        }
    }
}
