﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelGenerator))]
public class LevelGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Space(5);
        LevelGenerator LG = (LevelGenerator)(target);

        // Grid Values
        if(LG.GridRef != null)
        {
            LG.GridRef.cellSize = new Vector3(LG.GridWorldXSize, LG.GridWorldYSize, 1);
            LG.GridRef.cellGap = new Vector3(LG.GridCellsGap, LG.GridCellsGap, 0);
        }
        
        LG.LoadLevelPrefab();

        // Buttons
        if (GUILayout.Button("Generate new Level"))
        {
            LG.GenerateLevel();
        }
        GUILayout.Space(5);

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Load Models"))
        {
            LG.GenerateModels();
        }

        if (GUILayout.Button("Delete Models"))
        {
            LG.DeleteModels();
        }

        GUILayout.EndHorizontal();

        if (GUILayout.Button("New FF List"))
        {
            LG.CreateNewFFList();
        }

        //GUILayout.Space(10);

        //if (GUILayout.Button("Test LM"))
        //{
        //    LG.LMMemoryTest();
        //}

    }
}
