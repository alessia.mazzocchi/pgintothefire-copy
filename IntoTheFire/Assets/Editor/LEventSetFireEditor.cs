﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LEventSetFire))]
public class LEventSetFireEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        LEventSetFire tg = (LEventSetFire)(target);

        GUILayout.Space(15);
        if (GUILayout.Button("Update Name"))
        {
            tg.Rename();
        }
    }
}

[CustomEditor(typeof(LEventFall))]
public class LEventFallEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        LEventFall tg = (LEventFall)(target);

        GUILayout.Space(15);
        if (GUILayout.Button("Update Name"))
        {
            tg.Rename();
        }
    }
}

[CustomEditor(typeof(LEventExtinguish))]
public class LEventExtinguishEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        LEventExtinguish tg = (LEventExtinguish)(target);

        GUILayout.Space(15);
        if (GUILayout.Button("Update Name"))
        {
            tg.Rename();
        }
    }
}

[CustomEditor(typeof(LEventSetSmoke))]
public class LEventSetSmokeshEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        LEventSetSmoke tg = (LEventSetSmoke)(target);

        GUILayout.Space(15);
        if (GUILayout.Button("Update Name"))
        {
            tg.Rename();
        }
    }
}

[CustomEditor(typeof(LEventClearSmoke))]
public class LEventClearSmokeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        LEventClearSmoke tg = (LEventClearSmoke)(target);

        GUILayout.Space(15);
        if (GUILayout.Button("Update Name"))
        {
            tg.Rename();
        }
    }
}

[CustomEditor(typeof(LeventGenerateLiquid))]
public class LeventGenerateLiquidEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        LeventGenerateLiquid tg = (LeventGenerateLiquid)(target);

        GUILayout.Space(15);
        if (GUILayout.Button("Update Name"))
        {
            tg.Rename();
        }
    }
}